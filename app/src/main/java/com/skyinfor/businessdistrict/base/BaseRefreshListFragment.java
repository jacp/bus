package com.skyinfor.businessdistrict.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;


import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import javax.inject.Inject;

/**
 * Created by min on 2017/3/20.
 */

public abstract class BaseRefreshListFragment<T extends IPresenter> extends BaseFragment<T> implements MModel {
    @Inject
    protected T mPresenter;
   /* @BindView(R.id.rv_recyclerview)
    protected GiveParentTouchRecyclerView recyclerView;*/
   /* @BindView(R.id.fragment_rotate_header_with_view_group_frame)
    protected PtrClassicFrameLayout ptrFrame ;*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // initRefresh();
    }
   /* //初始化刷新
    public void initRefresh() {
        ptrFrame.setPtrHandler(new PtrHandler2() {
            @Override
            public boolean checkCanDoLoadMore(PtrFrameLayout frame, View content, View footer) {
                return PtrDefaultHandler2.checkContentCanBePulledUp(frame, recyclerView, footer);
            }

            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                loadMoreData();
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                updateData();
            }
        });
        ptrFrame.setResistance(2.3f);
        ptrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        ptrFrame.setDurationToClose(150);
        // default is false
        ptrFrame.setPullToRefresh(false);
        // default is true
        ptrFrame.setKeepHeaderWhenRefresh(true);

    }
    *//**
     * 刷新调用
     *//*
    protected  void updateData(){}

    *//**
     * 加载更多调用
     *//*
    protected  void loadMoreData(){}*/
}
