package com.skyinfor.businessdistrict.di.component;

import android.app.Activity;

import com.skyinfor.businessdistrict.di.PerFragment;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.fragment.main.contact.ContactFragment;
import com.skyinfor.businessdistrict.fragment.main.home.HomeFragment;
import com.skyinfor.businessdistrict.fragment.main.home.plan.plan_list.PlanListFragment;
import com.skyinfor.businessdistrict.fragment.main.msg.MsgFragment;
import com.skyinfor.businessdistrict.fragment.main.stu.StatusFragment;
import com.skyinfor.businessdistrict.fragment.main.stu.stu_item.StatusSonFragment;
import com.skyinfor.businessdistrict.fragment.main.stu.stu_sec.StatusSecFragment;
import com.skyinfor.businessdistrict.fragment.main.task.TaskFragment;
import com.skyinfor.businessdistrict.fragment.main.task.task_item.TaskSonFragment;
import com.skyinfor.businessdistrict.fragment.main.task.task_sec.TaskSecFragment;

import dagger.Component;

/**
 * Created by min on 2017/4/12.
 * Fragment 注入
 */
@PerFragment
@Component(dependencies = AppComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {
    Activity getActivity();

    //通讯录
    void inject(ContactFragment contactFragment);

    //主页
    void inject(HomeFragment homeFragment);

    //消息
    void inject(MsgFragment msgFragment);

    //情况
    void inject(StatusFragment statusFragment);

    //任务
    void inject(TaskFragment taskFragment);

    //stu具体信息
    void inject(StatusSonFragment statusSonFragment);

    //stu tab 容器
    void inject(StatusSecFragment statusSecFragment);

    //task tab 容器
    void inject(TaskSecFragment taskSecFragment);

    //task 具体信息
    void inject(TaskSonFragment taskSonFragment);

    //预案列表
    void inject(PlanListFragment planListFragment);

}
