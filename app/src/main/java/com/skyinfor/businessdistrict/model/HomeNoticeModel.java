package com.skyinfor.businessdistrict.model;

import java.util.List;

public class HomeNoticeModel {


    /**
     * total : 5
     * last_page : 1
     */

    private int total;
    private int last_page;
    private List<DataBean> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getLast_page() {
        return last_page;
    }

    public void setLast_page(int last_page) {
        this.last_page = last_page;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 17
         * create_time : 2018-11-13 14:43:21
         * update_time : 2018-11-15 15:25:31
         * title : 关于元旦放假的通知
         * content :
         各位同仁：</span></p>

         2018年1月1日——元旦为国家法定假日，放假一天。</span></p>

         为便于各部门及早合理地安排节假日生产等有关工作，现将元旦放假调休日期具体安排通知如下：</span></p>

         20018年12月30日—20018年1月1日放假，共3天。其中，1月1日(星期二)为法定节假日，12月30日(星期日)为公休日，12月29日(星期六)公休日调至12月31日(星期一)，12月29日(星期六)上班。</span></p>

         * picture : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bea6e6e51952.jpg
         * user_id : 26
         * nickname : 刘丹丹
         * group_id : 2
         * set_top : yes
         */

        private int id;
        private String create_time;
        private String update_time;
        private String title;
        private String content;
        private String picture;
        private int user_id;
        private String nickname;
        private int group_id;
        private String set_top;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public int getGroup_id() {
            return group_id;
        }

        public void setGroup_id(int group_id) {
            this.group_id = group_id;
        }

        public String getSet_top() {
            return set_top;
        }

        public void setSet_top(String set_top) {
            this.set_top = set_top;
        }
    }
}
