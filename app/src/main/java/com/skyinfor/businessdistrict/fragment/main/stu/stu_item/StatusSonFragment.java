package com.skyinfor.businessdistrict.fragment.main.stu.stu_item;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusSonFragment extends BaseFragment<StatusSonPresent> implements StatusSonContract.Model {

    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    private int page=1;
    private int setCurrentType = 1;
    private String mSelectType;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .fragmentModule(fragmentModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    public static StatusSonFragment getInstance() {
        return new StatusSonFragment();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_status_son;
    }

    @Override
    protected void initData() {
        super.initData();
        mSelectType =getmFragmentTitle();
        initRefresh(ptrStu, rvList);
        mPresenter.dialog.show();
        mPresenter.initAdapter(rvList);
        page = 1;
        initNetData();
    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page = 1;
        initNetData();
    }

    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page++;
        initNetData();
    }


    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }

    private void initNetData() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", Constants.USER_ID + "");
        params.put("page", page + "");
        params.put("unit", "10");
        if (Integer.valueOf(mSelectType) > 0) {
            params.put("status_code", mSelectType);
        }
        params.put("limit","10");

        setCurrentType = Constants.STATUS_TYPE;
        mPresenter.getList(params, page, setCurrentType);

    }

    public void setNotify(int selectType) {
        if (mPresenter.adapter != null) {
            mPresenter.dialog.show();
        }
        mSelectType= String.valueOf(selectType);
        page=1;
        initNetData();
    }

}
