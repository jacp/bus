package com.skyinfor.businessdistrict.listener;

import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * RItemRemoveListener 监听
 * Created by Min on 2017/8/24.
 */

public interface RItemRemoveListener {
    void onRItemRemove(RecyclerView.Adapter adapter, View view, int position);
}
