package com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info;

import android.app.Activity;

import com.skyinfor.businessdistrict.presenter.BasePresenter;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class CameraInfoPresent extends BasePresenter<CameraInfoContract.Model>implements
        CameraInfoContract.Presenter {

    @Inject
    public CameraInfoPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }



}
