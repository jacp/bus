package com.skyinfor.businessdistrict.liteorm;

import com.google.gson.Gson;
import com.litesuits.orm.db.annotation.Column;
import com.litesuits.orm.db.annotation.NotNull;
import com.litesuits.orm.db.annotation.PrimaryKey;
import com.litesuits.orm.db.annotation.Table;
import com.litesuits.orm.db.enums.AssignType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by min on 2017/6/26.
 * 全部联系人表
 */

@Table("contact_model")
public class ConTactAll {
    // 设置为主键,自增
    @PrimaryKey(AssignType.AUTO_INCREMENT)
    public int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // 非空字段
    @NotNull
    public String json;

    @NotNull
    @Column("saveTime")
    public long saveTime;

    public int type;

//    public ConTactAll(List<Map<String, List<ContectModels>>> list) {
//        this(new Gson().toJson(list));
//    }
//
//    public ConTactAll(String json) {
//        this.json = json;
//        this.saveTime = new Date().getTime();
//        type = 1;
//    }
}
