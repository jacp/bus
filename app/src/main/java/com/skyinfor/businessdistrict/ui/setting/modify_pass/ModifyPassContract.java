package com.skyinfor.businessdistrict.ui.setting.modify_pass;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

import okhttp3.MultipartBody;

public class ModifyPassContract {

    interface Model extends MModel {
        void setResultSuc();

    }

    interface Presenter extends IPresenter<Model> {
        void updateInfo(List<MultipartBody.Part> parts, final String passWord);

    }

}
