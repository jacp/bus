package com.skyinfor.businessdistrict.adapter;

import android.content.Context;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.model.RecordListModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class RecordListAdapter extends BaseMyRecyclerVIewAdapter<PlanListModel> {

    public RecordListAdapter(Context context, List<PlanListModel> list) {
        super(context, list, R.layout.item_record_list);
    }

    @Override
    protected void covert(PlanListModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        holder.setText(R.id.text_record_name,mode.getName());
        holder.setText(R.id.text_record_position,mode.getAddress());
        holder.setText(R.id.text_record_date,mode.getCreate_time());

    }
}
