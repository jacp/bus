package com.skyinfor.businessdistrict.ui.map.map_view.search;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface MapCheckSearchApi {

    @FormUrlEncoded
    @POST(ApiUrl.deviceUserSearch)
    Observable<Response<List<RelatedDeviceModel>>> deviceUserSearch(@Field("name") String name);

    @FormUrlEncoded
    @POST(ApiUrl.deviceSearch)
    Observable<Response<List<RelatedDeviceModel>>> deviceSearch(@Field("name") String name);

}
