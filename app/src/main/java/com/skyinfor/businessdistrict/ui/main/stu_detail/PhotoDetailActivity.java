package com.skyinfor.businessdistrict.ui.main.stu_detail;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.bm.library.Info;
import com.bm.library.PhotoView;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.util.GlideUtils;

import butterknife.BindView;

/**
 * Name: PhotoDetailActivity
 * Author: FrameJack
 * Email: framejackname@gmail.com
 * Date: 2018-01-09 10:57
 * Desc:
 * Comment: //TODO
 */
public class PhotoDetailActivity extends BaseActivity {
    @BindView(R.id.bg)
    ImageView bg;
    @BindView(R.id.img)
    PhotoView img;
    @BindView(R.id.parent)
    FrameLayout parent;
    public static final String INFO = "info";
    public static final String URL = "name";
    public static final String LOCAL_URL="local_url";
    AlphaAnimation in = new AlphaAnimation(0, 1);
    AlphaAnimation out = new AlphaAnimation(1, 0);
    private Info info;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_photo_detail;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        String name = getIntent().getStringExtra(URL);
        info = getIntent().getParcelableExtra(INFO);
        int draLayoutId=getIntent().getIntExtra(LOCAL_URL,-1);

        in.setDuration(300);
        img.startAnimation(in);
        out.setDuration(300);
        if (!TextUtils.isEmpty(name)) {
            GlideUtils.loadImg(name, img, R.mipmap.icon_create_task_img_default);
        } else {
            GlideUtils.loadCacheLocalUri(draLayoutId, img);
        }
        img.enable();

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bg.startAnimation(out);
                img.animaTo(info, new Runnable() {
                    @Override
                    public void run() {
                        back();
                        overridePendingTransition(0, 0);
                    }
                });

            }
        });
    }

    /**
     * 取消Activity关闭动画
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            bg.startAnimation(out);
            img.animaTo(info, new Runnable() {
                @Override
                public void run() {
                    back();
                    overridePendingTransition(0, 0);
                }
            });

            return true;
        }
        return false;
    }
}
