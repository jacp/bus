package com.skyinfor.businessdistrict.ui.chat.sound_chat;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

import io.agora.rtc.RtcEngine;

public class SoundChatContract {

    interface Model extends MModel {
        void setRtcEngine(RtcEngine mRtcEngine);

    }

    interface Presenter extends IPresenter<Model> {
        void initSingleConfig();

        void onSingleDestroy();

        void initGroupConfig(int cRole);

        void initAdapter(RecyclerView recMySelf,RecyclerView other);

        void setNotifyList(List<GroupMemberModel.UserListBean> mList);
    }


}
