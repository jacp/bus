package com.skyinfor.businessdistrict.ui.main.search_task_or_stu;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.StatusAdapter;
import com.skyinfor.businessdistrict.adapter.TaskAdapter;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.stu_detail.StatusDetailActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class SearchTaskAndStuPresent extends BasePresenter<SearchTaskAndStuContract.Model>
        implements SearchTaskAndStuContract.Presenter {

    private SearchTaskAndStuApi api;
    public HttpDialog dialog;
    private List<StatusSonModel> stuList = new ArrayList<>();
    private List<TaskSonModel> taskList = new ArrayList<>();
    private TaskAdapter taskAdapter;
    private StatusAdapter stuAdapter;


    @Inject
    public SearchTaskAndStuPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(SearchTaskAndStuApi.class);
    }


    @Override
    public void initTaskAdapter(RecyclerView recyclerView) {
        taskAdapter = new TaskAdapter(mActivity, taskList);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        SlideInBottomAnimatorAdapter slideInBottomAnimatorAdapter = new SlideInBottomAnimatorAdapter(adapter, recyclerView);
        recyclerView.setAdapter(taskAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity), Color.parseColor("#efeff4")));
        taskAdapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle data = new Bundle();
                data.putInt("type", 0);
                data.putSerializable("rele_id", taskList.get(position).getId());
                UIHelper.startActivity(mActivity, StatusDetailActivity.class, data);

            }
        });
    }

    @Override
    public void initStatusAdapter(RecyclerView recyclerView) {
        stuAdapter = new StatusAdapter(mActivity, stuList);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        SlideInBottomAnimatorAdapter slideInBottomAnimatorAdapter = new SlideInBottomAnimatorAdapter(adapter, recyclerView);
        recyclerView.setAdapter(stuAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity), Color.parseColor("#efeff4")));
        stuAdapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle data = new Bundle();
                data.putInt("type", 1);
                data.putSerializable("rele_id", stuList.get(position).getId());
                UIHelper.startActivity(mActivity, StatusDetailActivity.class, data);
            }
        });
    }

    @Override
    public void getTaskList(Map<String, String> parts, final int pages, int type) {

        Observable<Response<List<TaskSonModel>>> observable = null;

        switch (type) {
            case 0:
                observable = api.taskSendList(parts);

                break;
            case 1:
                observable = api.taskAcceptList(parts);

                break;
            case 2:
                observable = api.taskAll(parts);
                break;
        }

        addSubscription(observable.compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<TaskSonModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mModel.compelete();
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<TaskSonModel> bean) {
                        dialog.dismiss();

                        if (pages == 1) {
                            taskList.clear();
                        }
                        taskList.addAll(bean);

                        if (bean.size() < 10) {
                            mModel.close();
                            mModel.noLoadMode();
                            if (pages > 1) {
                                ToashUtils.show(mActivity, "已经是最后一页数据", 2000, Gravity.CENTER);
                            }
                        } else {
                            mModel.defalutMode();
                        }
                        taskAdapter.notifyDataSetChanged();
                    }
                });

    }

    @Override
    public void getStatusList(Map<String, String> parts, final int pages, int type) {
        dialog.show();

        Observable<Response<List<StatusSonModel>>> observable = null;

        switch (type) {
            case 0:
                observable = api.acceptList(parts);

                break;
            case 1:
                observable = api.sendList(parts);

                break;
            case 2:
                observable = api.conditionAll(parts);

                break;
        }

        addSubscription(observable.compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<StatusSonModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mModel.compelete();
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(List<StatusSonModel> bean) {
                        dialog.dismiss();

                        if (pages == 1) {
                            stuList.clear();
                        }
                        stuList.addAll(bean);

                        if (bean.size() < 10) {
                            mModel.close();
                            mModel.noLoadMode();
                            if (pages > 1) {
                                ToashUtils.show(mActivity, "已经是最后一页数据", 2000, Gravity.CENTER);
                            }
                        } else {
                            mModel.defalutMode();
                        }
                        stuAdapter.notifyDataSetChanged();
                    }
                });

    }

    @Override
    public void setTaskNotify() {
        taskList.clear();
        taskAdapter.notifyDataSetChanged();
    }

    @Override
    public void setStatusNotify() {
        stuList.clear();
        stuAdapter.notifyDataSetChanged();
    }


}
