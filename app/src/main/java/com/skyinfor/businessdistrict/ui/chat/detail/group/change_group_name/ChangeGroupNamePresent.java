package com.skyinfor.businessdistrict.ui.chat.detail.group.change_group_name;

import android.app.Activity;

import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class ChangeGroupNamePresent extends BasePresenter<ChangeGroupNameContract.Model>
        implements ChangeGroupNameContract.Presenter {

    private HttpDialog dialog;
    private ChangeGroupNameApi api;

    @Inject
    public ChangeGroupNamePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(ChangeGroupNameApi.class);

    }

    @Override
    public void ChangeNameAction(String group_uuid, final String group_name) {
        dialog.show();

        addSubscription(api.groupNameModify(group_uuid, group_name)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        mModel.setResult(group_name);
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();
                    }
                });

    }
}
