package com.skyinfor.businessdistrict.ui.main.video_rec;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class VideoRecContract {

    interface Model extends MModel {
    }

    interface Presenter extends IPresenter<Model> {

    }

}
