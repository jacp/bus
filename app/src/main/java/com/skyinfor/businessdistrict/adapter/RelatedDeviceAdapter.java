package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.ui.chat.detail.person.PersonInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info.CameraInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug.FirePlugInfoActivity;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.io.Serializable;
import java.util.List;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class RelatedDeviceAdapter extends BaseMyRecyclerVIewAdapter<RelatedDeviceModel> {
    private SonListener listener;

    public interface SonListener {
        void setSonListener(RelatedDeviceModel.ListBean bean, int position);
    }

    public RelatedDeviceAdapter(Context context, List<RelatedDeviceModel> list) {
        super(context, list, R.layout.item_related_device);
    }

    @Override
    protected void covert(final RelatedDeviceModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        final RecyclerView recyclerView = holder.getView(R.id.rec_relate_device_item);
        holder.setText(R.id.text_relate_device, mode.getName());
        View view = holder.getView(R.id.rel_relate_device);
        final ImageView img = holder.getView(R.id.img_relate_device);
        recyclerView.setVisibility(mode.isExpand() == false ? View.GONE : View.VISIBLE);
        img.setImageResource(mode.isExpand() == false ? R.mipmap.icon_task_create_equipment_icon_dow : R.mipmap.icon_task_create_equipment_icon_up);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mode.setExpand(!mode.isExpand());
                img.setImageResource(mode.isExpand() == false ? R.mipmap.icon_task_create_equipment_icon_dow : R.mipmap.icon_task_create_equipment_icon_up);

                recyclerView.setVisibility(mode.isExpand() == false ? View.GONE : View.VISIBLE);
            }
        });

        recyclerView.setFocusableInTouchMode(false);

        RelatedDeviceSonAdapter adapter = new RelatedDeviceSonAdapter(mContext, mode.getName(), mode.getList());

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String name = mode.getList().get(position).getTypeName();

                if (TextUtils.isEmpty(name)) {
                    if (listener != null) {
                        boolean check = !mode.getList().get(position).isCheck();
                        mode.getList().get(position).setCheck(check);
                        adapter.setNotify(mode.getList());
                        listener.setSonListener(mode.getList().get(position), position);
                    }
                } else {
                    Bundle data = new Bundle();
                    data.putSerializable("model", (Serializable) mode.getList().get(position));
                    if (name.contains("摄像头")) {
                        UIHelper.startActivity(mContext, CameraInfoActivity.class, data);

                    } else if (name.contains("消防栓")) {
                        UIHelper.startActivity(mContext, FirePlugInfoActivity.class, data);
                    } else if (name.equals("人员")) {
                        data.putString("user_id", mode.getList().get(position).getId() + "");
                        data.putString("user_name", mode.getList().get(position).getNickname());
                        UIHelper.startActivity(mContext, PersonInfoActivity.class, data);
                    }

                }

            }
        });

    }

    public SonListener getListener() {
        return listener;
    }

    public void setListener(SonListener listener) {
        this.listener = listener;
    }
}
