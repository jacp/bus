package com.skyinfor.businessdistrict.ui.startpager;

import android.app.Activity;


import com.skyinfor.businessdistrict.presenter.BasePresenter;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Name: StartPagePresent
 * Author: FrameJack
 * Email: framejackname@gmail.com
 * Date: 2018-02-06 16:08
 * Desc:
 * Comment: //TODO
 */
public class StartPagePresent extends BasePresenter<StartPageContract.Model> {

    @Inject
    public StartPagePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }


}
