package com.skyinfor.businessdistrict.model;

import java.util.List;

public class ContactAllModel {

    private int id;
    private String name;
    private int pid;
    private int level;
    private List<PersonListBean> person_list;
    private List<ContactAllModel> children;
    private int setCurrentLevel;

    public int getSetCurrentLevel() {
        return setCurrentLevel;
    }

    public void setSetCurrentLevel(int setCurrentLevel) {
        this.setCurrentLevel = setCurrentLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<PersonListBean> getPerson_list() {
        return person_list;
    }

    public void setPerson_list(List<PersonListBean> person_list) {
        this.person_list = person_list;
    }

    public List<ContactAllModel> getChildren() {
        return children;
    }

    public void setChildren(List<ContactAllModel> children) {
        this.children = children;
    }
}
