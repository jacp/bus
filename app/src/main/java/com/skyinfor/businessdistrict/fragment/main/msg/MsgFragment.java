package com.skyinfor.businessdistrict.fragment.main.msg;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.ui.chat.sound_chat.SoundChatActivity;
import com.skyinfor.businessdistrict.ui.chat.video_chat.VideoChatActivity;
import com.skyinfor.businessdistrict.ui.contact.MsgSearchActivity;
import com.skyinfor.businessdistrict.ui.main.MainActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select.TaskPersonSelectActivity;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class MsgFragment extends BaseFragment<MsgPresent> implements MsgContract.Model {

    @BindView(R.id.img_stu_search)
    ImageView imgStuSearch;
    @BindView(R.id.rec_msg)
    RecyclerView recMsg;
    @BindView(R.id.text_task_title)
    TextView textTaskTitle;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    @BindView(R.id.img_stu_add)
    ImageView imgStuAdd;

    private List<PersonListBean> personListBeanList = new ArrayList<>();
    private String TAG = MsgFragment.class.getSimpleName();
    private HttpDialog dialog;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .appComponent(appComponent)
                .fragmentModule(fragmentModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_msg;
    }

    @Override
    protected void initData() {
        super.initData();
        initRefresh(ptrStu, recMsg);
        textTaskTitle.setVisibility(View.VISIBLE);
        textTaskTitle.setText("消息");
        imgStuSearch.setVisibility(View.GONE);
        mPresenter.dialog.show();
        mPresenter.initAdapter(recMsg);
        mPresenter.getConversationList(String.valueOf(Constants.USER_ID), 1);
        imgStuAdd.setVisibility(Constants.PRIVILEGE_GROUP == true ? View.VISIBLE : View.GONE);

    }


    @OnClick(R.id.img_stu_add)
    public void onViewClicked() {
        personListBeanList.clear();
        PersonListBean bean = new PersonListBean();
        bean.setId(Constants.USER_ID);
        personListBeanList.add(bean);

        Bundle perData = new Bundle();
        perData.putInt("type", 1);

        UIHelper.startActivity(mContext, TaskPersonSelectActivity.class, perData);

    }

//    @Subscribe
//    public void getEvent(ChatNameEvent event) {
//        mPresenter.getConversationList(String.valueOf(Constants.USER_ID), 1);
//
//    }
//
//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void getEvent(ChatNotifyEvent event) {
//        mPresenter.getConversationList(String.valueOf(Constants.USER_ID), event.getType());
//
//    }

    public void setNameChange(){
        mPresenter.getConversationList(String.valueOf(Constants.USER_ID), 1);
    }

    public void setEvent(ChatNotifyEvent event){
        mPresenter.getConversationList(String.valueOf(Constants.USER_ID), event.getType());
    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        mPresenter.getConversationList(String.valueOf(Constants.USER_ID), 1);

    }

    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }


    @OnClick(R.id.edit_contact)
    public void onSearchClicked() {
        UIHelper.startActivity(mContext, MsgSearchActivity.class);

    }

}
