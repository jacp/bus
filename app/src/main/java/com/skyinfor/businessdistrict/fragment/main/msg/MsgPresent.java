package com.skyinfor.businessdistrict.fragment.main.msg;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.cjt2325.cameralibrary.util.LogUtil;
import com.skyinfor.businessdistrict.adapter.ConversationAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.eventbus.MsgNewCountEvent;
import com.skyinfor.businessdistrict.fragment.main.contact.ContactPresent;
import com.skyinfor.businessdistrict.model.ConversationModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.ui.chat.system_notice.SystemNoticeActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.agora.AgoraAPI;
import io.agora.AgoraAPIOnlySignal;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class MsgPresent extends BasePresenter<MsgContract.Model> implements MsgContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private String TAG = MsgPresent.class.getSimpleName();
    private MsgApi api;
    public HttpDialog dialog;
    private List<ConversationModel> list = new ArrayList<>();
    private ConversationAdapter adapter;
    private AgoraAPIOnlySignal agoraAPI;
    private int sum;


    @Inject
    public MsgPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(MsgApi.class);
    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new ConversationAdapter(mActivity, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

    }

    @Override
    public void getConversationList(String userId, final int type) {
        final int pages = 1;

        addSubscription(api.talkUserList(userId)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<ConversationModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        mModel.compelete();

                    }

                    @Override
                    public void onNext(List<ConversationModel> conversationModels) {

                        switch (type) {
                            case 3:

                                break;
                            case 4:

                                break;
                        }
                        if (pages == 1) {
                            list.clear();
                        }

                        dialog.dismiss();
                        list.clear();
                        list.addAll(conversationModels);
                        sum = 0;

                        for (ConversationModel mode : list) {
                            sum += mode.getNew_msg_num();
                            sum += mode.getUnread_num();
                        }
                        EventBus.getDefault().post(new MsgNewCountEvent(sum));

                        if (conversationModels.size() < 1000000000) {
                            mModel.close();
                            mModel.noLoadMode();
                            if (pages > 1) {
                                ToashUtils.show(mActivity, "已经是最后一页数据",
                                        2000, Gravity.CENTER);
                            }
                        } else {
                            mModel.defalutMode();
                        }
                        if (adapter != null) {
                            adapter.notifyItemRangeChanged(0,list.size());
                        }
                    }
                });

    }

    @Override
    public void onItemClick(View view, int position) {
        if (list.get(position).getList_type().equals("system_notice")) {
            UIHelper.startActivity(mActivity, SystemNoticeActivity.class);
            int num = list.get(position).getUnread_num();
            sum -= num;
            list.get(position).setUnread_num(0);
            adapter.notifyItemRangeChanged(0,list.size());
            EventBus.getDefault().post(new MsgNewCountEvent(sum));

        } else {
            Bundle data = new Bundle();
            data.putSerializable("model", list.get(position));
            data.putString("avatar", list.get(position).getAvatar());
            UIHelper.startActivity(mActivity, ChatRoomActivity.class, data);

        }
    }

}
