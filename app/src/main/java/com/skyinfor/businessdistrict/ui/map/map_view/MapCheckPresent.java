package com.skyinfor.businessdistrict.ui.map.map_view;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;

import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.MapCheckModel;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.detail.person.PersonInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info.CameraInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug.FirePlugInfoActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class MapCheckPresent extends BasePresenter<MapCheckContract.Model>
        implements MapCheckContract.Presenter {

    public HttpDialog dialog;
    private MapCheckApi api;
    private ArrayList<MarkerOptions> optionsArrayList = new ArrayList<>();
    private List<MapCheckModel> mMapCheckModels = new ArrayList<>();

    @Inject
    public MapCheckPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(MapCheckApi.class);

    }

    @Override
    public void deviceList() {
        dialog.show();
        addSubscription(api.deviceList()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<MapCheckModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(List<MapCheckModel> mapCheckModels) {
                        mMapCheckModels = mapCheckModels;

                        for (int i = 0; i < mapCheckModels.size(); i++) {
                            List<RelatedDeviceModel.ListBean> beanList = mapCheckModels.get(i).getList();

                            for (int j = 0; j < beanList.size(); j++) {
                                String position = beanList.get(j).getPosition();
                                if (!TextUtils.isEmpty(position)) {
                                    String str[] = position.split(",");
                                    position = String.format("%s,%s", str[1], str[0]);
                                    beanList.get(j).setPosition(position);
                                } else {
                                    beanList.remove(beanList.get(j));
                                    j--;
                                }
                            }
                        }
                        setMarker(mapCheckModels);
                    }
                });

    }

    @Override
    public void setMapClick(int id) {
        for (int i = 0; i < mMapCheckModels.size(); i++) {
            List<RelatedDeviceModel.ListBean> beanList = mMapCheckModels.get(i).getList();
            for (int j = 0; j < beanList.size(); j++) {
                int beanId = beanList.get(j).getId();
                if (beanId == id) {
                    Bundle data = new Bundle();
                    RelatedDeviceModel.ListBean model = beanList.get(j);
                    data.putSerializable("model", model);
                    if (mMapCheckModels.get(i).getName().contains("摄像头")) {
                        UIHelper.startActivity(mActivity, CameraInfoActivity.class, data);
                    } else if (mMapCheckModels.get(i).getName().contains("消防栓")) {
                        UIHelper.startActivity(mActivity, FirePlugInfoActivity.class, data);
                    }else if (mMapCheckModels.get(i).getName().contains("人员")){
                        data.putString("user_id", beanList.get(j).getId() + "");
                        data.putString("user_name",  beanList.get(j).getNickname());
                        UIHelper.startActivity(mActivity, PersonInfoActivity.class, data);
                    }
                }
            }
        }

    }

    /**
     * 处理返回的位置和名称
     *
     * @param mapCheckModels
     */
    private void setMarker(List<MapCheckModel> mapCheckModels) {
        for (int i = 0; i < mapCheckModels.size(); i++) {
            List<RelatedDeviceModel.ListBean> beanList = mapCheckModels.get(i).getList();
            String name = mapCheckModels.get(i).getName();

            for (int j = 0; j < beanList.size(); j++) {
                String position = beanList.get(j).getPosition();
                String nickName = beanList.get(j).getId() + "";
                String str[] = position.split(",");
                setBaseInfo(name, nickName, str[0], str[1]);
            }

        }
        mModel.setResult(optionsArrayList);
    }

    private void setBaseInfo(String name, String nickName, String one, String two) {
        MarkerOptions markerOption = new MarkerOptions();
        LatLng x = new LatLng(Double.valueOf(one), Double.valueOf(two));
        markerOption.position(x);
        markerOption.title(name);
        markerOption.snippet(nickName);
        markerOption.perspective(false);
        markerOption.draggable(true);
        int layId;
        switch (name) {
            case "摄像头":
                layId = R.mipmap.icon_map_position_icon_camera;
                break;
            case "消防栓":
                layId = R.mipmap.icon_map_position_xiao_fang;
                break;
            case "人员":
                layId = R.mipmap.icon_map_position_icon_people;
                break;
            default:
                layId = R.mipmap.icon_map_position_icon_people;
                break;
        }
        markerOption.icon(BitmapDescriptorFactory.fromResource(layId));//设置图标
        optionsArrayList.add(markerOption);
    }
}
