package com.skyinfor.businessdistrict.ui.map.map_view.search;

import android.support.v7.widget.RecyclerView;

import com.amap.api.maps.model.MarkerOptions;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.ArrayList;

public class MapCheckSearchContract {

    interface Model extends MModel {

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void taskDevice(String name,int type);

    }

}
