package com.skyinfor.businessdistrict.fragment.main.stu;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.flyco.tablayout.SegmentTabLayout;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(FragmentManager fm,ViewPager viewPager,SegmentTabLayout apBarStl);
        void setNotify(int position);
    }

}
