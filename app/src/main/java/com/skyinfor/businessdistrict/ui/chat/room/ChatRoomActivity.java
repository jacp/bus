package com.skyinfor.businessdistrict.ui.chat.room;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.ChatActionEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.model.ConversationModel;
import com.skyinfor.businessdistrict.model.GroupChatSoundModel;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.SoundChatModel;
import com.skyinfor.businessdistrict.ui.chat.detail.group.ChatDetailActivity;
import com.skyinfor.businessdistrict.ui.chat.detail.person.PersonInfoActivity;
import com.skyinfor.businessdistrict.ui.chat.select_person.SelectPersonActivity;
import com.skyinfor.businessdistrict.ui.chat.sound_chat.SoundChatActivity;
import com.skyinfor.businessdistrict.ui.chat.video_chat.VideoChatActivity;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.ImagePickerUtil;
import com.skyinfor.businessdistrict.util.LuBanCompress;
import com.skyinfor.businessdistrict.util.PopupWindowUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.http.HttpParams;
import com.tencent.android.tpush.XGLocalMessage;
import com.tencent.android.tpush.XGPushManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;
import io.agora.AgoraAPIOnlySignal;

public class ChatRoomActivity extends BaseActivity<ChatRoomPresent> implements ChatRoomContract.Model, View.OnClickListener, TextWatcher {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_common)
    ImageView ivCommon;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrFrame;
    @BindView(R.id.rel_chat)
    RelativeLayout relChat;
    @BindView(R.id.text_chat_send)
    TextView textChatSend;
    @BindView(R.id.edit_chat_boom_content)
    EditText editChatBoomContent;
    @BindView(R.id.img_chat_boom_add)
    ImageView imgChatBoomAdd;
    private PopupWindow popup;
    private ConversationModel model;
    private String nickName;
    private String userId;
    private int groupId;
    private String groupUuid;
    private int page = 1;
    private AgoraAPIOnlySignal agoraAPI;
    private int type = 1;
    private int maxImgCount = 1;
    public static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_CAMER = 200;
    public static final int REQUEST_CODE_GET_PERSON = 101;
    private ArrayList<ImageItem> selImageList = new ArrayList<>(); //当前选择的所有图片
    private ArrayList<ImageItem> images;
    private File file;
    private String avatar;
    private String imgPath;
    private List<GroupMemberModel.UserListBean> userListBeanList = new ArrayList<>();
    private String uuidChannel;
    private List<GroupMemberModel.UserListBean> groupList;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat_room;

    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        uuidChannel = UUID.randomUUID().toString();

        initRefresh(ptrFrame, rvList);
        EventBus.getDefault().register(this);
        ImagePickerUtil.initImageMorePicker();

        ivCommon.setVisibility(View.VISIBLE);
        ivCommon.setImageResource(R.mipmap.icon_news_chat_top_icon_more);

        model = (ConversationModel) getIntent().getSerializableExtra("model");

        if (model != null) {
            tvTitle.setText(model.getGroup_id() == 0 ? model.getNickname() : model.getGroup_name());
            groupId = model.getGroup_id();
            nickName = model.getNickname();
            groupUuid = model.getGroup_uuid();
            userId = String.valueOf(model.getUser_id());
            avatar = model.getAvatar();

        } else {
            groupId = getIntent().getIntExtra("group_id", 0);
            groupUuid = getIntent().getStringExtra("group_uuid");
            nickName = getIntent().getStringExtra("name");
            userId = getIntent().getStringExtra("id");
            avatar = getIntent().getStringExtra("avatar");

            tvTitle.setText(nickName);
        }

        mPresenter.talkRecordStatus(Constants.USER_ID + "", TextUtils.isEmpty(groupUuid)
                ? userId : groupUuid, nickName);

        Constants.CUR_ID=TextUtils.isEmpty(groupUuid)?userId:groupUuid;

        mPresenter.initAdapter(rvList);
        initNetData();
        agoraAPI = MyApplication.getInstance().getmAgoraAPI();
        editChatBoomContent.addTextChangedListener(this);

        if (!TextUtils.isEmpty(groupUuid)) {
            agoraAPI.channelJoin(groupUuid);
            mPresenter.getGroupInfo(groupUuid);
        }

    }

    @OnClick(R.id.img_chat_boom_add)
    public void onViewClicked() {
        hintKb();
        View popView = getLayoutInflater().inflate(R.layout.view_chat_boom_pop, null);
        popup = PopupWindowUtils.getInstance(mContext).showPopup(true, popView, relChat, Gravity.BOTTOM, 2);
        TextView img = popView.findViewById(R.id.text_chat_boom_send_img);
        TextView record = popView.findViewById(R.id.text_chat_boom_record);
        TextView sound = popView.findViewById(R.id.text_chat_boom_sound);
        TextView video = popView.findViewById(R.id.text_chat_boom_video);
        TextView cancel = popView.findViewById(R.id.text_chat_boom_cancel);
        img.setOnClickListener(this);
        record.setOnClickListener(this);
        sound.setOnClickListener(this);
        video.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_chat_boom_send_img:
                selectImg();
                popup.dismiss();

                break;
            case R.id.text_chat_boom_record:
                popup.dismiss();
                useCamera();

                break;
            case R.id.text_chat_boom_sound:
                type = 3;
                selectChat(type);
                break;
            case R.id.text_chat_boom_video:
                type = 4;
                selectChat(type);

                break;
            case R.id.text_chat_boom_cancel:
                popup.dismiss();

                break;
        }

    }


    @OnClick(R.id.iv_common)
    public void onMoreClicked() {
        if (!TextUtils.isEmpty(groupUuid)) {
            Bundle data = new Bundle();
            data.putString("group_uuid", groupUuid);
            UIHelper.startActivity(mContext, ChatDetailActivity.class, data);
        } else {
            Bundle data = new Bundle();
            data.putString("user_id", userId + "");
            data.putString("user_name", nickName);
            UIHelper.startActivity(mContext, PersonInfoActivity.class, data);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (!TextUtils.isEmpty(groupUuid)) {
            agoraAPI.channelLeave(groupUuid);
        }
    }

    @Subscribe
    public void getEvent(ChatNameEvent event) {
        if (!TextUtils.isEmpty(event.getName())) {
            tvTitle.setText(event.getName());
        }
    }

    @Subscribe
    public void getChatevent(ChatActionEvent event) {
        this.type = event.getType();
        selectChat(type);
    }

    private void selectChat(int type) {
        if (TextUtils.isEmpty(groupUuid)) {
            if (type == 3) {
                soundChatAction();
            } else {
                videoChatAction();
            }
        } else {
            Bundle data = new Bundle();
            data.putString("uuid", groupUuid);
            UIHelper.startActivityForResult(mContext, SelectPersonActivity.class, REQUEST_CODE_GET_PERSON, data);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getEvent(ChatNotifyEvent event) {
        page = 1;
        initNetData();
        //更新读取状态值
        mPresenter.talkRecordStatus(Constants.USER_ID + "", TextUtils.isEmpty(groupUuid)
                ? userId : groupUuid, nickName);
    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page++;
        initNetData();
    }

    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page = 1;
        initNetData();
    }

    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }

    @Override
    public void setSucImg(String path) {
        imgPath = path;
        type = 2;
        sendMsgAction(path);
        selImageList.clear();
    }

    @Override
    public void setGroupList(List<GroupMemberModel.UserListBean> groupList) {
        this.groupList = groupList;

    }


    private void initNetData() {
        HttpParams params = new HttpParams();

        if (TextUtils.isEmpty(groupUuid)) {
            params.put("id1", Constants.USER_ID + "");
            params.put("id2", userId + "");
        } else {
            params.put("group_uuid", groupUuid);
        }
        params.put("page", page + "");
        mPresenter.getConversationList(params.setType(), TextUtils.isEmpty(groupUuid) ? 0 : 1, page, nickName, avatar);

    }

    @OnClick(R.id.text_chat_send)
    public void onSendClicked() {
        String str = editChatBoomContent.getText().toString().trim();
        type = 1;
        sendMsgAction(str);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String str = editChatBoomContent.getText().toString().trim();

        imgChatBoomAdd.setVisibility(str.length() > 0 ? View.GONE : View.VISIBLE);
        textChatSend.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


//            mRtcEngine.muteLocalAudioStream(iv.isSelected());
//        mRtcEngine.setEnableSpeakerphone(view.isSelected());

    /**
     * 发送消息操作
     *
     * @param str 消息
     */
    private void sendMsgAction(String str) {

        if (type == 3) {
            str = "【语音聊天】";
        } else if (type == 4) {
            str = "【视频聊天】";
        }

        ChatContentModel model = new ChatContentModel(str, type, Constants.USER_ID,
                groupUuid, Constants.ICON, Constants.NICK_NAME);
        mPresenter.setNotifySend(model);

        if (TextUtils.isEmpty(groupUuid)) {
            agoraAPI.messageInstantSend(userId, 0, setSingleMsgInfo(str), "");
        } else {
            String strName;
            if (type == 3 || type == 4) {
                strName = setGroupMsgInfo();

                for (GroupMemberModel.UserListBean userListBean :
                        userListBeanList) {
                    if (userListBean.getId() != Constants.USER_ID) {
                        agoraAPI.messageInstantSend(String.valueOf(userListBean.getId()), 0, setGroupMsgInfo(), "");
                    }
                }

            } else {
                strName = setSingleMsgInfo(str);
            }

            if (groupList != null) {
                for (GroupMemberModel.UserListBean userListBean : groupList) {
                    agoraAPI.messageInstantSend(userListBean.getId() + "", 0, strName, "");
                }
            } else {
                agoraAPI.messageChannelSend(groupUuid, strName, "");
            }

        }
        editChatBoomContent.setText("");
        HttpParams params = new HttpParams();
        params.put("type", type + "");
        params.put("send_id", Constants.USER_ID + "");
        if (TextUtils.isEmpty(groupUuid)) {
            params.put("receive_id", userId + "");
        } else {
            params.put("group_uuid", groupUuid + "");
        }

        params.put("content", str);

        mPresenter.talkRecordSave(params.setType(), TextUtils.isEmpty(groupUuid) ? 0 : 1, nickName);

    }


    private String setSingleMsgInfo(String str) {
        String groupName=tvTitle.getText().toString().trim();
        Gson gson = new Gson();
        SoundChatModel model = new SoundChatModel(type + "", Constants.USER_ID + "",
                userId, uuidChannel, "", Constants.NICK_NAME, Constants.ICON, str, groupUuid,groupName);
        return gson.toJson(model);
    }

    private String setGroupMsgInfo() {
        String groupName=tvTitle.getText().toString().trim();

        Gson gson = new Gson();
        GroupChatSoundModel model = new GroupChatSoundModel(userListBeanList, groupUuid, type == 3 ? 33 : 44,groupName);
        return gson.toJson(model);
    }

    private void selectImg() {
        ImagePickerUtil.initImageMorePicker();
        //打开选择,本次允许选择的数量
        ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
        Intent intent1 = new Intent(ChatRoomActivity.this, ImageGridActivity.class);
        /* 如果需要进入选择的时候显示已经选中的图片，
         * 详情请查看ImagePickerActivity
         * */
//                                intent1.putExtra(ImageGridActivity.EXTRAS_IMAGES,images);
        startActivityForResult(intent1, REQUEST_CODE_SELECT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == REQUEST_CODE_SELECT) {
            images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                selImageList.clear();
                selImageList.addAll(images);
            }
            HttpParams params = new HttpParams();
            params.put("file", LuBanCompress.compressWithLs(new File(images.get(0).path), mContext));
            mPresenter.upFile(params.setMoreImgType(), 0);
        } else if (requestCode == REQUEST_CODE_CAMER && resultCode == RESULT_OK) {
            file.getAbsolutePath();
            HttpParams params = new HttpParams();
            params.put("file", LuBanCompress.compressWithLs(file, mContext));

            mPresenter.upFile(params.setMoreImgType(), 0);
        } else if (data != null && requestCode == REQUEST_CODE_GET_PERSON) {
            userListBeanList = (List<GroupMemberModel.UserListBean>) data.getSerializableExtra("data");
            if (type == 3) {
                soundChatAction();
            } else {
                videoChatAction();
            }

        }
    }

    /**
     * 使用相机
     */
    private void useCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/bus/" + System.currentTimeMillis() + ".jpg");
        file.getParentFile().mkdirs();

        //改变Uri  com.xykj.customview.fileprovider注意和xml中的一致
        Uri uri = FileProvider.getUriForFile(this, "com.skyinfor.businessdistrict.fileprovider", file);
        //添加权限
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CODE_CAMER);
    }

    private void soundChatAction() {
        type = 3;

        Gson gson = new Gson();

        //自己更新
        SoundChatModel chatModel = new SoundChatModel(nickName, avatar);
        String selfStr = gson.toJson(chatModel);
        Bundle bundle = new Bundle();
        bundle.putInt("type", 0);
        bundle.putString("str", selfStr);
        bundle.putString("user_id", userId);
        bundle.putInt("channelType", type);
        bundle.putSerializable("model", (Serializable) userListBeanList);

        bundle.putString("channel_id", TextUtils.isEmpty(groupUuid) ? uuidChannel : groupUuid);
        UIHelper.startActivity(mContext, SoundChatActivity.class, bundle);

        //别人发送
        sendMsgAction("【语音聊天】");
    }

    private void videoChatAction() {
        type = 4;
        Gson gson = new Gson();

        //自己更新
        SoundChatModel chatModel = new SoundChatModel(nickName, avatar);
        String selfStr = gson.toJson(chatModel);
        Bundle bundle = new Bundle();
        bundle.putInt("type", 0);
        bundle.putString("str", selfStr);
        bundle.putString("user_id", userId);
        bundle.putInt("channelType", type);
        bundle.putSerializable("model", (Serializable) userListBeanList);

        bundle.putString("channel_id", TextUtils.isEmpty(groupUuid) ? uuidChannel : groupUuid);
        UIHelper.startActivity(mContext, VideoChatActivity.class, bundle);

        //别人发送
        sendMsgAction("【视频聊天】");

    }

}
