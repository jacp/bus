package com.skyinfor.businessdistrict.ui.main.create_stu.related_device;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.RelatedDeviceAdapter;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class RelatedDevicePresent extends BasePresenter<RelatedDeviceContract.Model> implements
        RelatedDeviceContract.Presenter {

    private HttpDialog dialog;
    private RelatedDeviceApi api;
    private List<RelatedDeviceModel> list = new ArrayList<>();
    private RelatedDeviceAdapter adapter;
    private ArrayList<RelatedDeviceModel.ListBean> deviceList = new ArrayList<>();
    private int type;


    @Inject
    public RelatedDevicePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(RelatedDeviceApi.class);

    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new RelatedDeviceAdapter(mActivity, list);
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);

        adapter.setListener(new RelatedDeviceAdapter.SonListener() {
            @Override
            public void setSonListener(RelatedDeviceModel.ListBean bean, int position) {
                if (bean.isCheck()) {
                    deviceList.add(bean);
                } else {
                    deviceList.remove(bean);
                }
                mModel.setResultList(deviceList);
            }
        });

    }

    @Override
    public void taskDevice(final List<String> deviceIdList, final int type) {
        this.type=type;
        dialog.show();

        addSubscription(api.taskDevice()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<RelatedDeviceModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<RelatedDeviceModel> relatedDeviceModels) {
                        list.addAll(relatedDeviceModels);

                        if (type != 3) {
                            for (int i = 0; i < deviceIdList.size(); i++) {
                                String tmp = deviceIdList.get(i);
                                for (int j = 0; j < list.size(); j++) {
                                    List<RelatedDeviceModel.ListBean> bean = list.get(j).getList();
                                    for (int k = 0; k < bean.size(); k++) {
                                        if (tmp.equals(bean.get(k).getTheone())) {
                                            bean.get(k).setCheck(true);
                                            deviceList.add(bean.get(k));
                                        }
                                    }
                                }
                            }
                        } else {
                            for (RelatedDeviceModel mode :
                                    list) {
                                mode.setExpand(true);
                                List<RelatedDeviceModel.ListBean> data = mode.getList();
                                for (RelatedDeviceModel.ListBean bean :
                                        data) {
                                    bean.setShow(true);
                                    bean.setTypeName(mode.getName());
                                }
                            }
                        }

                        adapter.notifyDataSetChanged();
                        dialog.dismiss();

                    }
                });

    }

    @Override
    public void setNotify(List<RelatedDeviceModel> newList) {
        list.addAll(newList);
        adapter.notifyDataSetChanged();
    }
}
