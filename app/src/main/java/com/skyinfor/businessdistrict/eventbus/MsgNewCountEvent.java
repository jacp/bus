package com.skyinfor.businessdistrict.eventbus;

public class MsgNewCountEvent {
    private int count;

    public MsgNewCountEvent(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
