package com.skyinfor.businessdistrict.ui.main.stu_detail;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public interface StatusDetailApi {

    @FormUrlEncoded
    @POST(ApiUrl.taskStatusChange)
    Observable<Response<Object>> taskStatusChange(@Field("id") String id,
                                                  @Field("status_code") String status_code);

    @FormUrlEncoded
    @POST(ApiUrl.conditionStatusChange)
    Observable<Response<Object>> conditionStatusChange(@Field("id") String id,
                                                       @Field("status_code") String status_code);

    @FormUrlEncoded
    @POST(ApiUrl.taskDelete)
    Observable<Response<Object>> taskDelete(@Field("id") String id);

    @FormUrlEncoded
    @POST(ApiUrl.noticeDetail)
    Observable<Response<Object>> noticeDetail(@Field("rele_id") String rele_id,
                                              @Field("notice_type") String notice_type);

}
