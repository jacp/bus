package com.skyinfor.businessdistrict.fragment.main.task;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.skyinfor.businessdistrict.adapter.BaseFragmentAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.fragment.main.stu.stu_sec.StatusSecFragment;
import com.skyinfor.businessdistrict.fragment.main.task.task_sec.TaskSecFragment;
import com.skyinfor.businessdistrict.presenter.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class TaskPresent extends BasePresenter<TaskContract.Model> implements TaskContract.Presenter {

    private List<String> stringList = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();
    private ViewPager viewPager;

    @Inject
    public TaskPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }

    @Override
    public void initAdapter(FragmentManager fm, final ViewPager viewPager,
                            final SegmentTabLayout apBarStl) {
        stringList.add("上报我的");
        stringList.add("我上报的");

        int size = Constants.PRIVILEGE_TASK == true ? 2 : 1;

        if (Constants.PRIVILEGE_TASK == false) {
            Constants.TASK_TYPE = 1;
        } else {
            Constants.TASK_TYPE = 0;
        }

        for (int i = 0; i < size; i++) {
            TaskSecFragment fragment = new TaskSecFragment();
            fragment.setmFragmentTitle(i + "");
            fragments.add(fragment);
        }

        BaseFragmentAdapter adapter = new BaseFragmentAdapter(fm, stringList, fragments);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

        apBarStl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                Constants.TASK_TYPE = position;
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        this.viewPager = viewPager;
        this.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                apBarStl.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void setNotify(int position) {
        if (position == 1) {
            Constants.TASK_TYPE = 2;
        } else {
            Constants.TASK_TYPE = viewPager.getCurrentItem();
        }

        TaskSecFragment taskSecFragment = (TaskSecFragment) fragments.get(viewPager.getCurrentItem());
        taskSecFragment.setNotify();
    }

}
