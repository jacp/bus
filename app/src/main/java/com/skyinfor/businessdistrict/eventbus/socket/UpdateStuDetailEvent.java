package com.skyinfor.businessdistrict.eventbus.socket;

public class UpdateStuDetailEvent {

    private String type;
    private int status;

    public UpdateStuDetailEvent(String type, int status) {
        this.type = type;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

}
