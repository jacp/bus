package com.skyinfor.businessdistrict.ui.setting.modify_pass;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.util.MD5Utils;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.http.HttpParams;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ModifyPassActivity extends BaseActivity<ModifyPassPresent> implements ModifyPassContract.Model {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_pass_one)
    EditText textPassOne;
    @BindView(R.id.text_pass_two)
    EditText textPassTwo;
    @BindView(R.id.text_pass_three)
    EditText textPassThree;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    private UserInfoModel model;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_modify_pass;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        model = (UserInfoModel) getIntent().getSerializableExtra("model");
        tvTitle.setText("修改密码");
        textRightEditText.setText("完成");
        textRightEditText.setVisibility(View.VISIBLE);
    }

    /**
     *
     */
    private void initNetData() {
        String one = textPassOne.getText().toString().trim();
        String two = textPassTwo.getText().toString().trim();
        String pass = textPassThree.getText().toString().trim();

        if (TextUtils.isEmpty(one)) {
            ToashUtils.show(mContext, "原密码不能为空");
        } else if (TextUtils.isEmpty(two)) {
            ToashUtils.show(mContext, "密码不能为空");

        } else if (TextUtils.isEmpty(pass)) {
            ToashUtils.show(mContext, "密码不能为空");

        } else if (!MD5Utils.getMd5(one).equals(Constants.LOGIN_PASS_WORD)) {
            ToashUtils.show(mContext, "原密码不正确");

        } else if (!two.equals(pass)) {
            ToashUtils.show(mContext, "两次密码输入不正确");
        } else {
            HttpParams params = new HttpParams();
            params.put("id", model.getId() + "");
            params.put("nickname", model.getNickname() + "");
            params.put("password", MD5Utils.getMd5(pass) + "");
            params.put("email", model.getEmail() + "");
            params.put("qq", model.getQq() + "");
            params.put("wechat", model.getWechat() + "");
            params.put("address", model.getAddress());
            params.put("avatar", model.getAvatar());
            mPresenter.updateInfo(params.setType(), MD5Utils.getMd5(pass));
        }
    }

    @Override
    public void setResultSuc() {
        ToashUtils.show(mContext,"修改成功");
        appManager.finishActivity();
    }

    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        initNetData();
    }

}
