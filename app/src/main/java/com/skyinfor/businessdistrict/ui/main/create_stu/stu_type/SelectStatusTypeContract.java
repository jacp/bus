package com.skyinfor.businessdistrict.ui.main.create_stu.stu_type;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.StatusTypeModel;
import com.skyinfor.businessdistrict.model.TaskModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;
import com.skyinfor.businessdistrict.ui.main.create_stu.CreateStuOrTaskContract;

import java.util.List;

public class SelectStatusTypeContract {

    interface Model extends MModel {
        void setSelectResult(int id);
        void setGetList(List<StatusTypeModel> list);
        void setGetTaskList(List<TaskModel> list);
    }

    interface Presenter extends IPresenter<Model> {
        void getStatusList(int position);
        void getTaskTypeList(int position);
        void initAdapter(RecyclerView recyclerView,int type);
    }
}
