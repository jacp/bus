package com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select;

import android.app.Activity;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ContactEvent;
import com.skyinfor.businessdistrict.model.CreateGroupModel;
import com.skyinfor.businessdistrict.model.GroupChatSoundModel;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.http.HttpParams;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import io.agora.AgoraAPIOnlySignal;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

public class TaskPersonSelectPresent extends BasePresenter<TaskPersonSelectContract.Model>
        implements TaskPersonSelectContract.Presenter {

    private HttpDialog dialog;
    private TaskPersonSelectApi api;
    private AgoraAPIOnlySignal agoraAPI;

    @Inject
    public TaskPersonSelectPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(TaskPersonSelectApi.class);

    }


    @Override
    public void createGroup(final String group_name, String user_id, String group_owner_id) {
        dialog.show();

        addSubscription(api.talkGroupCreate(group_name, user_id, group_owner_id)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<CreateGroupModel>() {
                    @Override
                    public void onCompleted() {
                        mModel.setResult();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(CreateGroupModel o) {
                        EventBus.getDefault().post(new ContactEvent(0));
                        dialog.dismiss();
                        String[] idArray = user_id.split(",");

                        for (String data :
                                idArray) {
                            int id = Integer.valueOf(data);

                            if (id != Constants.USER_ID) {
                                agoraAPI = MyApplication.getInstance().getmAgoraAPI();
                                agoraAPI.messageInstantSend(String.valueOf(id), 0, setGroupMsgInfo(o.getGroup_uuid()), "");
                            }

                        }

                        HttpParams params = new HttpParams();
                        params.put("type", 1 + "");
                        params.put("send_id", Constants.USER_ID + "");
//                                params.put("receive_id", id + "");
                        params.put("group_uuid", o.getGroup_uuid() + "");

                        params.put("content", "我们开始聊天吧！");

                        talkRecordSave(params.setType(), 1);

                    }
                });

    }

    private String setGroupMsgInfo(String groupUuid) {
        Gson gson = new Gson();
        GroupChatSoundModel model = new GroupChatSoundModel(groupUuid, 1);
        return gson.toJson(model);
    }

    public void talkRecordSave(List<MultipartBody.Part> part, int type) {
        Observable<Response<Object>> observable;
        observable = api.talkGroupRecordSave(part);

        addSubscription(observable
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        EventBus.getDefault().post(new ChatNameEvent(""));
                    }
                });

    }
}
