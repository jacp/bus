package com.skyinfor.businessdistrict.ui.main.contact;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.ContactPersonAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class ContactSearchPresent extends BasePresenter<ContactSearchContract.Model>
        implements ContactSearchContract.Presenter {

    private List<PersonListBean> list = new ArrayList<>();
    private HttpDialog dialog;
    private ContactSearchApi api;
    private ContactPersonAdapter adapter;

    @Inject
    public ContactSearchPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(ContactSearchApi.class);

    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new ContactPersonAdapter(mActivity, list, false);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (Constants.USER_ID==list.get(position).getId()){
                    ToashUtils.show(mActivity,"自己和自己不能聊天");
                }else {
                    String userName = list.get(position).getNickname();
                    String userId = String.valueOf(list.get(position).getId());
                    String avatar = list.get(position).getAvatar();

                    Bundle data = new Bundle();
                    data.putString("name", userName);
                    data.putString("id", userId);
                    data.putString("avatar", avatar);
                    UIHelper.startActivity(mActivity, ChatRoomActivity.class, data);
                }
            }
        });

    }

    @Override
    public void searchUser(String name) {
        addSubscription(api.searchUser(name)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<PersonListBean>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<PersonListBean> personListBean) {
                        list.clear();
                        list.addAll(personListBean);
                        adapter.notifyDataSetChanged();
                    }
                });

    }

    @Override
    public void setNotify() {
        list.clear();
        adapter.notifyDataSetChanged();
    }
}
