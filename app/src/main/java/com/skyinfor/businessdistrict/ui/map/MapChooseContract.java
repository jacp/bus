package com.skyinfor.businessdistrict.ui.map;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class MapChooseContract {

    interface Model extends MModel {
    }

    interface Presenter extends IPresenter<Model> {

    }

}
