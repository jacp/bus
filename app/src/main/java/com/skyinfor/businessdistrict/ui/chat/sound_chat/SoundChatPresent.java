package com.skyinfor.businessdistrict.ui.chat.sound_chat;

import android.app.Activity;
import android.media.AudioManager;
import android.os.Environment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.SoundChatMyselfAdapter;
import com.skyinfor.businessdistrict.adapter.SoundChatOtherAdapter;
import com.skyinfor.businessdistrict.eventbus.MeaasgeEvent;
import com.skyinfor.businessdistrict.eventbus.SoundStatusEvent;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class SoundChatPresent extends BasePresenter<SoundChatContract.Model>
        implements SoundChatContract.Presenter {

    private RtcEngine mRtcEngine;
    private List<GroupMemberModel.UserListBean> mySelfList = new ArrayList<>();
    private List<GroupMemberModel.UserListBean> otherList = new ArrayList<>();

    private IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() { // Tutorial Step 1
        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);

        }

        @Override
        public void onFirstRemoteAudioFrame(int uid, int elapsed) {
            super.onFirstRemoteAudioFrame(uid, elapsed);
            EventBus.getDefault().post(new SoundStatusEvent(1,uid));

//            EventBus.getDefault().post(new SoundStatusEvent(1,uid));

        }

        @Override
        public void onUserOffline(final int uid, final int reason) { // Tutorial Step 4
            EventBus.getDefault().post(new SoundStatusEvent(2,uid));

        }

        @Override
        public void onUserMuteAudio(final int uid, final boolean muted) { // Tutorial Step 6

        }

    };
    private SoundChatOtherAdapter otherAdapter;
    private SoundChatMyselfAdapter myselfAdapter;

    @Inject
    public SoundChatPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);

    }

    @Override
    public void initSingleConfig() {
        try {
            mRtcEngine = RtcEngine.create(mActivity.getBaseContext(), mActivity.getString(R.string.agora_app_id), mRtcEventHandler);
            mModel.setRtcEngine(mRtcEngine);
        } catch (Exception e) {
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }

    }

    @Override
    public void initGroupConfig(int cRole) {
        try {
            mRtcEngine = RtcEngine.create(mActivity.getBaseContext(), mActivity.getString(R.string.agora_app_id), mRtcEventHandler);
            mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
            mRtcEngine.enableAudioVolumeIndication(200, 3); // 200 ms
            mRtcEngine.setLogFile(Environment.getExternalStorageDirectory()
                    + File.separator + mActivity.getPackageName() + "/log/agora-rtc.log");
            mRtcEngine.setClientRole(cRole);

            mModel.setRtcEngine(mRtcEngine);

        } catch (Exception e) {
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    @Override
    public void initAdapter(RecyclerView recMySelf, RecyclerView recOther) {
        myselfAdapter = new SoundChatMyselfAdapter(mActivity, mySelfList);
        recMySelf.setLayoutManager(new GridLayoutManager(mActivity, 3));
        recMySelf.setAdapter(myselfAdapter);

        otherAdapter = new SoundChatOtherAdapter(mActivity, otherList);
        recOther.setLayoutManager(new GridLayoutManager(mActivity, 4));
        recOther.setAdapter(otherAdapter);

    }

    @Override
    public void setNotifyList(List<GroupMemberModel.UserListBean> mList) {
        otherList.clear();
        mySelfList.clear();
        if (mList == null) {
            myselfAdapter.notifyDataSetChanged();
            otherAdapter.notifyDataSetChanged();
        } else {
            otherList.addAll(mList);
            mySelfList.addAll(mList);
            myselfAdapter.notifyDataSetChanged();
            otherAdapter.notifyDataSetChanged();
        }

    }


    @Override
    public void onSingleDestroy() {
        if (mRtcEngine != null) {
            mRtcEngine.leaveChannel();//离开频道
            RtcEngine.destroy();
            mRtcEngine = null;
        }
    }
}
