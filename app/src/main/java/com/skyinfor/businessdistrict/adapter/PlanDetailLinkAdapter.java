package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class PlanDetailLinkAdapter extends BaseMyRecyclerVIewAdapter<PlanListModel.PlanResponseBean> {

    public PlanDetailLinkAdapter(Context context, List<PlanListModel.PlanResponseBean> list) {
        super(context, list, R.layout.item_plan_detail_linkage);
    }

    @Override
    protected void covert(PlanListModel.PlanResponseBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        holder.setText(R.id.text_plan_response_title, String.format("联动响应%s", position + 1));
        holder.setText(R.id.text_plan_response_dec, mode.getName());
        holder.setText(R.id.text_plan_response_type, mode.getResponse_type());
        holder.setText(R.id.text_plan_response_action, mode.getResponse_action());
        TextView msg = holder.getView(R.id.text_plan_response_msg);
        View relateView = holder.getView(R.id.lin_plan_link);
        View msgView = holder.getView(R.id.rel_plan_response_msg);

        RecyclerView recyclerView = holder.getView(R.id.rec_plan_link);
        if (mode.getRelation_user() != null && mode.getRelation_user().size() > 0) {
            relateView.setVisibility(View.VISIBLE);
            PlanDetailRelatePerAdapter perAdapter = new PlanDetailRelatePerAdapter(mContext, mode.getRelation_user());
            recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
            recyclerView.setAdapter(perAdapter);
        } else {
            relateView.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(mode.getDetails())) {
            msg.setText(mode.getDetails());
        }
        msgView.setVisibility(TextUtils.isEmpty(mode.getDetails()) ? View.GONE : View.VISIBLE);

    }
}
