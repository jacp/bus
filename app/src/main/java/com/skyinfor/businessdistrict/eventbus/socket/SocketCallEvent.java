package com.skyinfor.businessdistrict.eventbus.socket;

public class SocketCallEvent {

    private int type;
    private String str;

    public SocketCallEvent(int type, String str) {
        this.type = type;
        this.str = str;
    }

    public int getType() {
        return type;
    }

    public String getStr() {
        return str;
    }
}
