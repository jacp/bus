package com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.SelectSexAdapter;
import com.skyinfor.businessdistrict.eventbus.EditPersonEvent;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class EditDetailPresent extends BasePresenter<EditDetailContract.Model>
        implements EditDetailContract.Presenter {

    private HttpDialog dialog;
    private EditDetailApi api;
    private List<String> list = new ArrayList<>();
    private SelectSexAdapter adapter;


    @Inject
    public EditDetailPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(EditDetailApi.class);

    }


    @Override
    public void updateInfo(List<MultipartBody.Part> parts, final int type, final String str) {
        dialog.show();
        addSubscription(api.userInformationUpdate(parts)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        EventBus.getDefault().post(new EditPersonEvent(str, type));
                        dialog.dismiss();
                        mModel.finishAction();
                    }
                });

    }

    @Override
    public void initAdapter(RecyclerView recyclerView,String sex) {
        list.clear();
        list.add("男");
        list.add("女");
        adapter = new SelectSexAdapter(mActivity, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);
        adapter.mPosition=sex.equals("男")?0:1;
        adapter.notifyDataSetChanged();

        adapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                adapter.mPosition = position;
                adapter.notifyDataSetChanged();
                mModel.setSelectResult(list.get(position));
            }
        });
    }
}
