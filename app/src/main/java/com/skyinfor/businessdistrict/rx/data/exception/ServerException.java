package com.skyinfor.businessdistrict.rx.data.exception;

/**
 * Created by wali on 2016/9/18.
 */
public class ServerException extends Exception {
    public int code;
    public String message;

    public ServerException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}