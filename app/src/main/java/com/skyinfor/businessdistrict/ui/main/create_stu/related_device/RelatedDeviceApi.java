package com.skyinfor.businessdistrict.ui.main.create_stu.related_device;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface RelatedDeviceApi {

    @GET(ApiUrl.taskDevice)
    Observable<Response<List<RelatedDeviceModel>>>taskDevice();

}
