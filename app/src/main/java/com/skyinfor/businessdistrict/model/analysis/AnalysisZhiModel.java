package com.skyinfor.businessdistrict.model.analysis;

public class AnalysisZhiModel {


    /**
     * anju : 93
     * shangye : 90
     * zhili : 96
     */

    private int anju;
    private int shangye;
    private int zhili;

    public int getAnju() {
        return anju;
    }

    public void setAnju(int anju) {
        this.anju = anju;
    }

    public int getShangye() {
        return shangye;
    }

    public void setShangye(int shangye) {
        this.shangye = shangye;
    }

    public int getZhili() {
        return zhili;
    }

    public void setZhili(int zhili) {
        this.zhili = zhili;
    }
}
