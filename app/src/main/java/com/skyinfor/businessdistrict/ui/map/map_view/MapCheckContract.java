package com.skyinfor.businessdistrict.ui.map.map_view;

import com.amap.api.maps.model.MarkerOptions;
import com.skyinfor.businessdistrict.model.MapCheckModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.ArrayList;
import java.util.List;

public class MapCheckContract {

    interface Model extends MModel {
        void setResult(ArrayList<MarkerOptions> optionsArrayList);

    }

    interface Presenter extends IPresenter<Model> {
        void deviceList();
        void setMapClick(int id);
    }

}
