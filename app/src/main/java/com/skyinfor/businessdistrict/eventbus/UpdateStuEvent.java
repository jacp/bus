package com.skyinfor.businessdistrict.eventbus;

public class UpdateStuEvent {

    private int type;
    private String str;

    public UpdateStuEvent(int type, String str) {
        this.type = type;
        this.str = str;
    }

    public UpdateStuEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getStr() {
        return str;
    }
}
