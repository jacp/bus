package com.skyinfor.businessdistrict.ui.chat.system_notice;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.SystemNoticeAdapter;
import com.skyinfor.businessdistrict.model.SystemNoticeModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.stu_detail.StatusDetailActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class SystemNoticePresent extends BasePresenter<SystemNoticeContract.Model>
        implements SystemNoticeContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private SystemNoticeApi api;
    private List<SystemNoticeModel> list = new ArrayList<>();
    private SystemNoticeAdapter adapter;
    public HttpDialog dialog;
    private int mPosition;
    private RecyclerView recyclerView;


    @Inject
    public SystemNoticePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(SystemNoticeApi.class);

    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new SystemNoticeAdapter(mActivity, list);
        this.recyclerView = recyclerView;
        this.recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity),
                Color.parseColor("#efeff4")));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

    }

    @Override
    public void getNoticeList(String user_id, final int page, String unit) {
        addSubscription(api.userNoticeList(user_id, String.valueOf(page), unit)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<SystemNoticeModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                        dialog.dismiss();

                    }

                    @Override
                    public void onError(Throwable e) {
                        mModel.compelete();
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(List<SystemNoticeModel> bean) {
                        if (page == 1) {
                            list.clear();
                        }
                        list.addAll(bean);
                        for (SystemNoticeModel mode : list) {
                            mode.setNotice_code(setIndex(mode.getNotice_status()));
                        }

                        if (bean.size() < 10) {
                            mModel.close();
                            mModel.noLoadMode();
                            if (page > 1) {
                                ToashUtils.show(mActivity, "已经是最后一页数据", 2000, Gravity.CENTER);
                            }
                        } else {
                            mModel.defalutMode();
                        }
                        adapter.notifyDataSetChanged();
                    }
                });

    }




    @Override
    public void onItemClick(View view, int position) {
        mPosition = position;

        Bundle data = new Bundle();
        data.putInt("rele_id", list.get(position).getRele_id());
        data.putString("notice_type", list.get(position).getNotice_type());
        data.putInt("type", list.get(position).getNotice_type().equals("task") ? 0 : 1);
        UIHelper.startActivity(mActivity, StatusDetailActivity.class, data);
    }

    private int setIndex(String index) {
        int tmp;
        switch (index) {
            case "未接受":
            case "待处理":
                tmp = 1;
                break;
            case "执行中":
            case "处理中":
                tmp = 2;

                break;
            case "待审核":
                tmp = 3;

                break;
            case "已完成":
                tmp = 4;

                break;
            case "已拒绝":
                tmp = 5;

                break;
            default:
                tmp = 0;
        }
        return tmp;
    }

}
