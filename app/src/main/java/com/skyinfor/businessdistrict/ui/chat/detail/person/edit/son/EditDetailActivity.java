package com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.http.HttpParams;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditDetailActivity extends BaseActivity<EditDetailPresent> implements EditDetailContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_change_name)
    EditText textChangeName;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    @BindView(R.id.rec_sex)
    RecyclerView recSex;
    private UserInfoModel model;
    private int type;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_change_group_name;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        textRightEditText.setVisibility(View.VISIBLE);
        type = getIntent().getIntExtra("type", 0);
        model = (UserInfoModel) getIntent().getSerializableExtra("model");
        String text = getIntent().getStringExtra("str");
        textChangeName.setText(text);
        textChangeName.setSelection(text.length());

        textRightEditText.setText("完成");

        String str = null;


        switch (type) {
            case 1:
                str = "性别";
                textChangeName.setVisibility(View.GONE);
                mPresenter.initAdapter(recSex, model.getSex());
                break;
            case 2:
                str = "邮箱";
                break;
            case 4:
                str = "QQ号";
                break;
            case 5:
                str = "微信号";
                break;
            case 6:
                str = "通讯地址";
                break;
        }
        tvTitle.setText(String.format("修改%s", str));

    }

    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        HttpParams params = new HttpParams();
        params.put("id", model.getId() + "");

        if (type == 1) {
//            initNetData(model.getSex());
            params.put("sex", model.getSex());
            mPresenter.updateInfo(params.setType(), type, model.getSex());
        } else {
            String text = textChangeName.getText().toString().trim();
            if (TextUtils.isEmpty(text)) {
                ToashUtils.show(mContext, "文字不能为空！");

            } else {
                switch (type) {
                    case 2:
                        params.put("email", text);

                        break;
                    case 4:
                        params.put("qq", text);

                        break;
                    case 5:
                        params.put("wechat", text);

                        break;
                    case 6:
                        params.put("address", text);

                        break;
                }
                mPresenter.updateInfo(params.setType(), type, text);
            }

        }
    }


    @Override
    public void setSelectResult(String str) {
        model.setSex(str);
    }

    @Override
    public void finishAction() {
        appManager.finishActivity();
    }
}
