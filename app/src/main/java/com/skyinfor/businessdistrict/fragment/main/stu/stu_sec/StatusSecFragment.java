package com.skyinfor.businessdistrict.fragment.main.stu.stu_sec;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.eventbus.StatusEvent;
import com.skyinfor.businessdistrict.eventbus.TaskEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateStuEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusSecFragment extends BaseFragment<StatusSecPresent> implements StatusSecContract.Model {


    @BindView(R.id.viewpager_status_sec)
    ViewPager viewpagerStatusSec;
    @BindView(R.id.tab_stu)
    TabLayout tabStu;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .appComponent(appComponent)
                .fragmentModule(fragmentModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_status_sec;


    }

    @Override
    protected void initEventAndData() {
        super.initEventAndData();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initData() {
        super.initData();
        mPresenter.initAdapter(getChildFragmentManager(), viewpagerStatusSec, tabStu);



    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void getEvent(StatusEvent event) {
        int type = event.getSelectType();
        mPresenter.setNotify(type);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusEvent(UpdateStuEvent event) {
        mPresenter.setNotify(0);
        if (event.getType()>0&&event.getType()<4){
            mPresenter.setNotify(event.getType()-1);
            mPresenter.setNotify(event.getType());
        }
    }

    public void setNotify(){
        for (int i = 0; i < 5; i++) {
            mPresenter.setNotify(i);
        }
    }

}
