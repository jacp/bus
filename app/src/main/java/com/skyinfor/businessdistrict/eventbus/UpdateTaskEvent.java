package com.skyinfor.businessdistrict.eventbus;

public class UpdateTaskEvent {

    private int type;

    public UpdateTaskEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
