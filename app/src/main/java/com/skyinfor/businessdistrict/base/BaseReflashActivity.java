package com.skyinfor.businessdistrict.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;


import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrDefaultHandler2;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler2;

/**
 * Created by min on 2017/3/25.
 */

public abstract class BaseReflashActivity<T extends IPresenter> extends BaseActivity implements MModel {
//    @BindView(R.id.fragment_rotate_header_with_view_group_frame)
//    protected PtrClassicFrameLayout ptrFrame;
//    @BindView(R.id.rv_recyclerview)
//    protected GiveParentTouchRecyclerView recyclerView;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        initRefresh();
    }

    //初始化刷新
    public void initRefresh() {
        ptrFrame.setPtrHandler(new PtrHandler2() {
            @Override
            public boolean checkCanDoLoadMore(PtrFrameLayout frame, View content, View footer) {
//                return PtrDefaultHandler2.checkContentCanBePulledDown(frame, recyclerView, footer);
                return PtrDefaultHandler2.checkContentCanBePulledUp(frame, recyclerView, footer);
            }

            @Override
            public void onLoadMoreBegin(PtrFrameLayout frame) {
                loadMoreData();
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, recyclerView, header);
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                updateData();
            }
        });
        // the following are default settings
        ptrFrame.setResistance(2.3f);
        ptrFrame.setRatioOfHeaderHeightToRefresh(1.2f);
        ptrFrame.setDurationToClose(150);
        // default is false
        ptrFrame.setPullToRefresh(false);
        // default is true
        ptrFrame.setKeepHeaderWhenRefresh(true);
    }

    /**
     * 刷新调用
     */
    protected abstract void updateData();

    /**
     * 加载更多调用
     */
    protected abstract void loadMoreData();
}
