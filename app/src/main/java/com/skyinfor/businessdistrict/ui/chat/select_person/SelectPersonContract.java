package com.skyinfor.businessdistrict.ui.chat.select_person;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

public class SelectPersonContract {
    interface Model extends MModel {
        void setSelectList(List<GroupMemberModel.UserListBean> personListBeanList);


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView titleRec,RecyclerView recyclerView);
        void getGroupInfo(String uuId);
    }

}
