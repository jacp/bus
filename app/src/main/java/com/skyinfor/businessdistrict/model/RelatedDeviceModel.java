package com.skyinfor.businessdistrict.model;

import java.io.Serializable;
import java.util.List;

public class RelatedDeviceModel implements Serializable {


    /**
     * name : 摄像头
     * list : [{"id":2,"name":"三迪曼哈顿九号楼","ip":"10.1.13.1","type":"720P半球摄像机","nvrip":"10.1.12.51","channel":"2","theone":"761f7b94-3c6e-c002-ce27-d28014bc2c05","status":"","position":"0,0,0"},{"id":3,"name":"三迪曼哈顿南","ip":"10.1.13.3","type":"720P半球摄像机","nvrip":"10.1.12.51","channel":"2","theone":"338f2cb8-f61e-d1bb-cc39-af15c5b5ab9e","status":"","position":"121.24238364618085,31.058409758032177,10.826781962190749"},{"id":4,"name":"信达蓝爵小区","ip":"10.1.15.206","type":"720P半球摄像机","nvrip":"10.1.12.51","channel":"3","theone":"8b81621a-5c4d-deea-fa51-4e3ecb7e9e43","status":"","position":"121.23854089966058,31.061989227862906,11.545586010084117"},{"id":5,"name":"万达广场南","ip":"10.1.15.207","type":"720P半球摄像机","nvrip":"10.1.12.51","channel":"1","theone":"8d2d20a0-fa3e-d40c-522d-a005c776fd67","status":"","position":"121.2395671143636,31.059062954361647,11.387328603602754"},{"id":245,"name":"茸宁路黄渡浜街交叉口","ip":"10.1.13.202","type":"720P半球摄像机","nvrip":"","channel":"","theone":"e7b07070-efdd-d81d-5388-20a0d6ea2d66","status":"","position":"121.24310819908523,31.06089242387471,10.55771133861897"},{"id":246,"name":"上海富悦财富广场A座","ip":"10.1.15.304","type":"720P半球摄像机","nvrip":"","channel":"","theone":"1bca7fe6-fed0-300f-8c22-50fa5b7799c5","status":"","position":"121.24215953441751,31.061055887134426,10.80999307555854"},{"id":247,"name":"茸悦路黄渡浜街交叉口","ip":"10.1.13.106","type":"720P半球摄像机","nvrip":"","channel":"","theone":"199b914d-896a-c3f1-100e-312c06318555","status":"","position":"121.24112133968075,31.060951617223502,10.587609249915678"},{"id":249,"name":"五龙商业广场","ip":"10.1.15.103","type":"720P半球摄像机","nvrip":"","channel":"","theone":"3674f13e-a7a9-27a9-ae89-858526f73f4e","status":"","position":"121.23658293281954,31.05696880525086,12.310172065601137"},{"id":250,"name":"光星路广富林路交叉口","ip":"10.1.14.305","type":"720P半球摄像机","nvrip":"","channel":"","theone":"db3861b8-ad52-abab-3436-0a76cbea66ae","status":"","position":"121.23678285308704,31.059330246920798,10.811829499392266"},{"id":251,"name":"中展璞荟","ip":"10.1.16.104","type":"720P半球摄像机","nvrip":"","channel":"","theone":"7a099724-6bdb-8344-3beb-627db25922ed","status":"","position":"121.23602230557638,31.059580430753915,11.196725995033658"},{"id":252,"name":"万达广场西","ip":"10.1.14.210","type":"720P半球摄像机","nvrip":"","channel":"","theone":"d2ddf4bb-8fa0-a965-2ccd-bf0ede72f462","status":"","position":"121.23690210526507,31.059950602669627,10.768473150177844"},{"id":253,"name":"万科梦想派小区","ip":"10.1.15.107","type":"720P半球摄像机","nvrip":"","channel":"","theone":"40d29d46-fd65-2c47-3ca7-64e9aa6bd90d","status":"","position":"121.23947218760259,31.06332190417165,10.1499802758563"},{"id":254,"name":"上海富悦财富广场南","ip":"10.1.15.210","type":"720P半球摄像机","nvrip":"","channel":"","theone":"d38d420f-f1b6-7bf5-f38c-e40d9ce1a928","status":"","position":"121.24203304427151,31.061109840864585,11.813288357644486"},{"id":255,"name":"茸悦路广富林路交叉口","ip":"10.1.13.103","type":"720P半球摄像机","nvrip":"","channel":"","theone":"d17fd8e5-3af7-151e-3680-37d9117d1214","status":"","position":"121.24100089983578,31.058654032812456,10.979040462218375"},{"id":256,"name":"光星路里陆街交叉路口","ip":"10.1.12.121","type":"720P半球摄像机","nvrip":"","channel":"","theone":"65f164f9-6010-d9ad-b64e-1dff2b099ea0","status":"","position":"121.23686033203158,31.056888666554467,12.218913486430939"}]
     */

    private String name;
    private List<ListBean> list;
    private boolean isExpand=true;

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean implements Serializable{
        /**
         * id : 2
         * name : 三迪曼哈顿九号楼
         * ip : 10.1.13.1
         * type : 720P半球摄像机
         * nvrip : 10.1.12.51
         * channel : 2
         * theone : 761f7b94-3c6e-c002-ce27-d28014bc2c05
         * status :
         * position : 0,0,0
         */

        private int id;
        private String name;
        private String ip;
        private String type;
        private String nvrip;
        private String channel;
        private String theone;
        private String status;
        private String position;
        private boolean isCheck;
        private boolean isShow;
        private String typeName;
        private String address;
        private String ascription;
        private String duty_person_name;
        private String duty_person_phone;
        private String is_import;
        private String nickname;

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getIs_import() {
            return is_import;
        }

        public void setIs_import(String is_import) {
            this.is_import = is_import;
        }

        public String getDuty_person_phone() {
            return duty_person_phone;
        }

        public void setDuty_person_phone(String duty_person_phone) {
            this.duty_person_phone = duty_person_phone;
        }

        public String getDuty_person_name() {
            return duty_person_name;
        }

        public void setDuty_person_name(String duty_person_name) {
            this.duty_person_name = duty_person_name;
        }

        public String getAscription() {
            return ascription;
        }

        public void setAscription(String ascription) {
            this.ascription = ascription;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }

        public boolean isShow() {
            return isShow;
        }

        public void setShow(boolean show) {
            isShow = show;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getNvrip() {
            return nvrip;
        }

        public void setNvrip(String nvrip) {
            this.nvrip = nvrip;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getTheone() {
            return theone;
        }

        public void setTheone(String theone) {
            this.theone = theone;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPosition() {
            return position;
        }

        public void setPosition(String position) {
            this.position = position;
        }
    }
}
