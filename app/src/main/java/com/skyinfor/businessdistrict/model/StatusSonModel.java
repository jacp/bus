package com.skyinfor.businessdistrict.model;

import java.io.Serializable;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusSonModel implements Serializable{


    /**
     * id : 57
     * create_time : 2018-10-26 13:34:18
     * update_time : 2018-10-26 13:34:18
     * name : 吕了
     * type_id : 1
     * origin_type : user
     * origin_id : 101
     * accept_user_id : 26,25,48,46,49,52,54,56,57,97,94,95,100,102,101
     * status_code : 1
     * details :
     * address : 上海市松江区黄渡浜街11栋
     * picture :
     * video :
     * audio :
     * position : 上海市松江区黄渡浜街11栋
     * type_name : 情况类型1
     * level : 一级
     * status_name : 待处理
     * nickname : 舒文强
     * user_role_list_id : 14
     */

    private int id;
    private String create_time;
    private String update_time;
    private String name;
    private int type_id;
    private String origin_type;
    private int origin_id;
    private String accept_user_id;
    private int status_code;
    private String details;
    private String address;
    private String picture;
    private String video;
    private String audio;
    private String position;
    private String type_name;
    private String level;
    private String status_name;
    private String nickname;
    private int user_role_list_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getOrigin_type() {
        return origin_type;
    }

    public void setOrigin_type(String origin_type) {
        this.origin_type = origin_type;
    }

    public int getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(int origin_id) {
        this.origin_id = origin_id;
    }

    public String getAccept_user_id() {
        return accept_user_id;
    }

    public void setAccept_user_id(String accept_user_id) {
        this.accept_user_id = accept_user_id;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getUser_role_list_id() {
        return user_role_list_id;
    }

    public void setUser_role_list_id(int user_role_list_id) {
        this.user_role_list_id = user_role_list_id;
    }
}
