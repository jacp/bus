package com.skyinfor.businessdistrict.ui.main.search_task_or_stu;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.ClearEditText;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class SearchTaskAndStuActivity extends BaseActivity<SearchTaskAndStuPresent>
        implements SearchTaskAndStuContract.Model, TextWatcher {


    @BindView(R.id.edit_search_task)
    ClearEditText editSearchTask;
    @BindView(R.id.text_search_task_cancel)
    TextView textSearchTaskCancel;
    @BindView(R.id.rec_search)
    RecyclerView recSearch;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    private int page = 1;
    // 0: task 我派发的 ，stu 上报我的  1: task 派发我的  , stu 我上报的
    private int setCurrentType;
    //0 ：task 1 : stu
    private int type;
    private String name;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_task_and_stu;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        initRefresh(ptrStu, recSearch);
        getVis();

        setCurrentType = getIntent().getIntExtra("currentType", -1);
        type = getIntent().getIntExtra("type", -1);
        editSearchTask.addTextChangedListener(this);
        switch (type) {
            case 0:
                mPresenter.initTaskAdapter(recSearch);
                break;
            case 1:
                mPresenter.initStatusAdapter(recSearch);
                break;
        }

    }

    @OnClick(R.id.text_search_task_cancel)
    public void onViewClicked() {
        appManager.finishActivity();
    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page = 1;
        getCurrentNet();

    }

    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page++;
        getCurrentNet();
    }


    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }

    private void initTaskData() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", Constants.USER_ID + "");
        params.put("page", page + "");
        params.put("unit", "10");
        params.put("name", name);
        params.put("limit", "10");

        setCurrentType = Constants.TASK_TYPE;
        mPresenter.getTaskList(params, page, setCurrentType);

    }

    private void initStatusData() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", Constants.USER_ID + "");
        params.put("page", page + "");
        params.put("unit", "10");
        params.put("name", name);
        params.put("limit", "10");

        setCurrentType = Constants.STATUS_TYPE;
        mPresenter.getStatusList(params, page, setCurrentType);

    }


    private void getCurrentNet() {
        if (type == 0) {
            initTaskData();

        } else {
            initStatusData();

        }
    }

    private void setEmpNet() {
        if (type == 0) {
            mPresenter.setTaskNotify();
        } else {
            mPresenter.setStatusNotify();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        name = String.valueOf(charSequence);
        getVis();

        if (name.length() > 0) {
            mPresenter.dialog.show();
            getCurrentNet();
        } else {
            setEmpNet();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void getVis(){
        String str = editSearchTask.getText().toString().trim();
        if (!TextUtils.isEmpty(str)){
            ptrFrame.setVisibility(View.VISIBLE);
        }else {
            ptrFrame.setVisibility(View.GONE);
        }
    }
}
