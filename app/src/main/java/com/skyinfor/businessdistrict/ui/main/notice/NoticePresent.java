package com.skyinfor.businessdistrict.ui.main.notice;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.NoticeAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.notice.notice_detail.NoticeDetailActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class NoticePresent extends BasePresenter<NoticeContact.Model> implements NoticeContact.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private List<HomeNoticeModel.DataBean> mList = new ArrayList<>();
    private NoticeAdapter adapter;
    private HttpDialog dialog;
    private NoticeApi api;

    @Inject
    public NoticePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(NoticeApi.class);
        dialog.show();
    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new NoticeAdapter(mActivity, mList);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void noticeList(String page, final String limit) {
        final int pages = Integer.valueOf(page);

        addSubscription(api.noticeList(Constants.USER_ID + "", page, limit)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<HomeNoticeModel>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        mModel.compelete();
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(HomeNoticeModel o) {
                        List<HomeNoticeModel.DataBean> list = o.getData();
                        if (pages == 1) {
                            mList.clear();
                        }
                        mList.addAll(list);

                        if (list.size() < 10) {
                            mModel.close();
                            mModel.noLoadMode();
                            if (pages > 1) {
                                ToashUtils.show(mActivity, "已经是最后一页数据", 2000, Gravity.CENTER);
                            }
                        } else {
                            mModel.defalutMode();
                        }
                        adapter.setNotifyData((ArrayList) mList);

                    }
                });
    }

    @Override
    public void onItemClick(View view, int position) {
        Bundle data = new Bundle();
        data.putString("content", mList.get(position).getContent());
        UIHelper.startActivity(mActivity, NoticeDetailActivity.class, data);
    }
}
