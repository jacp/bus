package com.skyinfor.businessdistrict.fragment.main.home;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.model.HomeTaskCountModel;
import com.skyinfor.businessdistrict.model.HomeThingModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public interface HomeApi {

    @GET(ApiUrl.waitTasksCount)
    Observable<Response<HomeTaskCountModel>> waitTasksCount(@Query("user_id") String user_id);

    @GET(ApiUrl.noticeList)
    Observable<Response<HomeNoticeModel>> noticeList(@Query("user_id") String user_id, @Query("page") String page,
                                                     @Query("unit") String limit);


    @GET(ApiUrl.newTaskList)
    Observable<Response<List<HomeThingModel>>> newTaskList();


    @GET(ApiUrl.dataAnalysis)
    Observable<Response<Object>> dataAnalysis();

}
