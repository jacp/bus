package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class TaskPopAdapter extends BaseMyRecyclerVIewAdapter<String> {
    public int mPosition = 0;
    private int size;

    public TaskPopAdapter(Context context, List<String> list) {
        super(context, list, R.layout.item_task_pop);
        size = list.size();
    }

    @Override
    protected void covert(String mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        TextView div = holder.getView(R.id.text_task_pop_div);
        holder.setText(R.id.text_task_pop, mode);
        View view = holder.getView(R.id.view_task);

        if (mPosition == position) {
            div.setVisibility(View.VISIBLE);
        }
        view.setVisibility(position < size - 1 ? View.VISIBLE : View.GONE);


    }
}
