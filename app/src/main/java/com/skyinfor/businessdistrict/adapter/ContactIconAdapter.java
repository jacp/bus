package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ContactIconAdapter extends BaseMyRecyclerVIewAdapter<PersonListBean> {

    public ContactIconAdapter(Context context, List<PersonListBean> list) {
        super(context, list, R.layout.item_contact_icon);
    }

    @Override
    protected void covert(PersonListBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        IconView img = holder.getView(R.id.img_contact_icon, position);
        img.setBaseInfo(mContext,mode.getNickname(),mode.getAvatar());

    }
}
