package com.skyinfor.businessdistrict.listener;

import android.app.Dialog;

/**
 * Created by Min on 2017/4/26.
 */

public interface OnMessageDialogEvent {
    public void ok(Dialog dialog);
    public void close(Dialog dialog);
}
