package com.skyinfor.businessdistrict.ui.chat.system_notice;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.UpdateStuEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class SystemNoticeActivity extends BaseActivity<SystemNoticePresent> implements SystemNoticeContract.Model {


    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    private int page = 1;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_system_notice;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);
        tvTitle.setText("系统通知");
        initRefresh(ptrStu, rvList);
        mPresenter.initAdapter(rvList);
        mPresenter.dialog.show();
        initNetData();
    }


    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page = 1;
        initNetData();
    }


    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page++;
        initNetData();
    }


    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }

    private void initNetData() {
        mPresenter.getNoticeList(String.valueOf(Constants.USER_ID), page, "10");
//        mPresenter.tmpList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusEvent(UpdateTaskEvent event) {
        page = 1;
        initNetData();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStuEvent(UpdateStuEvent event) {
        page = 1;
        initNetData();
    }

}
