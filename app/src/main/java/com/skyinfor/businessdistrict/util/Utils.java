package com.skyinfor.businessdistrict.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import com.cjt2325.cameralibrary.util.LogUtil;
import com.rey.material.app.TimePickerDialog;
import com.skyinfor.businessdistrict.R;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * Name: Utils
 * Author: FrameJack
 * Email: framejackname@gmail.com
 * Date: 2017-09-06 14:37
 * Desc:
 * Comment: //TODO
 */
public class Utils<T> {
    public static String time;

    public static void setEditEnd(EditText edit) {
        //设置数据
        int length = edit.getText().toString().length();
        edit.setSelection(length);
    }

    /**
     * 软键盘显示和隐藏
     *
     * @param context
     * @param view
     * @param status
     */
    public static void setSoftInputStatus(Context context, EditText view, boolean status) {
        view.requestFocus();
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);

        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (status) {
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        } else {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //强制隐藏键盘
        }
    }

    public static boolean checkChineseCharacter(String str) {
        if (str.getBytes().length == str.length())
            return false;
        return true;
    }


    //功能去重复的数据
    public List<T> removeDuplicate(
            List<T> list) {
        @SuppressWarnings("rawtypes")
        Set set = new LinkedHashSet<>();
        set.addAll(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    /**
     * list集合变成str
     *
     * @param list
     * @param separator
     * @return
     */
    public static String listToString(List list, char separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i)).append(separator);
        }
        return list.isEmpty() ? "" : sb.toString().substring(0, sb.toString().length() - 1);
    }

    //string转list集合，使用，进行分隔
    public static List<String> stringToList(String strs) {
        String str[] = strs.split(",");
        return Arrays.asList(str);
    }

    /**
     * 拨打电话（直接拨打电话）
     *
     * @param phoneNum 电话号码
     */
    public static void callDirectPhone(String phoneNum, Context mContext) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mContext.startActivity(intent);
    }

    /**
     * 拨打电话（跳转到拨号界面，用户手动点击拨打）
     *
     * @param phoneNum 电话号码
     */
    public static void callPhone(String phoneNum, Context mContext) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        mContext.startActivity(intent);
    }

    public static void setDrawablePosition(Context mContext,TextView tv, int drawableId, int position) {
        Drawable leftDraw = null;
        Drawable topDraw = null;
        Drawable rightDraw = null;
        Drawable boomDraw = null;

        switch (position){
            case Gravity.LEFT:
                leftDraw =mContext.getResources().getDrawable(drawableId);
                leftDraw.setBounds(0, 0, leftDraw.getMinimumWidth(), leftDraw.getMinimumHeight());// 设置边界

                break;
            case Gravity.TOP:
                topDraw =mContext.getResources().getDrawable(drawableId);
                topDraw.setBounds(0, 0, leftDraw.getMinimumWidth(), leftDraw.getMinimumHeight());// 设置边界


                break;
            case Gravity.RIGHT:
                rightDraw =mContext.getResources().getDrawable(drawableId);
                rightDraw.setBounds(0, 0, leftDraw.getMinimumWidth(), leftDraw.getMinimumHeight());// 设置边界

                break;
            case Gravity.BOTTOM:
                boomDraw =mContext.getResources().getDrawable(drawableId);
                boomDraw.setBounds(0, 0, leftDraw.getMinimumWidth(), leftDraw.getMinimumHeight());// 设置边界

                break;
        }

        tv.setCompoundDrawables(leftDraw, topDraw, rightDraw, boomDraw);

    }

    /**
     * 获取本地软件版本号名称
     */
    public static String getLocalVersionName(Context ctx) {
        String localVersion = "";
        try {
            PackageInfo packageInfo = ctx.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(ctx.getPackageName(), 0);
            localVersion = packageInfo.versionName;
            LogUtil.d("TAG", "本软件的版本号。。" + localVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return localVersion;
    }

}
