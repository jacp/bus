package com.skyinfor.businessdistrict.model;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by min on 2017/3/1.
 */
public class Response<T> implements Serializable {
    public static final int SUCCESS_CODE = 0;

    public String result;
    public String msg;
    public T data;
    private int code = -2;

    public int code() {
        if (!TextUtils.isEmpty(msg) && code == -2) {
            try {
                code = Integer.valueOf(result);
            } catch (Exception e) {
                code = -1;
            }
        }
        return code;
    }

    public boolean isSuccess() {
        if (code() == SUCCESS_CODE) {
            return true;
        } else {
            return false;
        }
    }
}


