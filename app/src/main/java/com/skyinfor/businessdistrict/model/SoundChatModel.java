package com.skyinfor.businessdistrict.model;

import java.io.Serializable;
import java.util.List;

public class SoundChatModel implements Serializable{


    private String type;
    private String send_id;
    private String receive_id;
    private String action;
    private String detail;

    private String send_name;
    private String send_img;
    private String content;
    private String group_uuid;
    private String channelId;
    private String nickname;
    private String avatar;
    private String groupName;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    private List<GroupMemberModel.UserListBean> user_array;

    public List<GroupMemberModel.UserListBean> getUser_array() {
        return user_array;
    }

    public void setUser_array(List<GroupMemberModel.UserListBean> user_array) {
        this.user_array = user_array;
    }

    public SoundChatModel(String send_name, String send_img) {
        this.send_name = send_name;
        this.send_img = send_img;
    }

    public SoundChatModel(String type, String send_id, String receive_id, String action, String detail,
                          String send_name, String send_img, String content, String group_uuid,String groupName) {
        this.type = type;
        this.send_id = send_id;
        this.receive_id = receive_id;
        this.action = action;
        this.detail = detail;
        this.send_name = send_name;
        this.send_img = send_img;
        this.content = content;
        this.group_uuid = group_uuid;
        this.groupName=groupName;
    }

    public SoundChatModel(String type, String send_id, String receive_id, String channelId) {
        this.type = type;
        this.send_id = send_id;
        this.receive_id = receive_id;
        this.channelId = channelId;
    }

    public String getGroup_uuid() {
        return group_uuid;
    }

    public void setGroup_uuid(String group_uuid) {
        this.group_uuid = group_uuid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSend_id() {
        return send_id;
    }

    public void setSend_id(String send_id) {
        this.send_id = send_id;
    }


    public String getReceive_id() {
        return receive_id;
    }

    public void setReceive_id(String receive_id) {
        this.receive_id = receive_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getSend_name() {
        return send_name;
    }

    public void setSend_name(String send_name) {
        this.send_name = send_name;
    }

    public String getSend_img() {
        return send_img;
    }

    public void setSend_img(String send_img) {
        this.send_img = send_img;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
