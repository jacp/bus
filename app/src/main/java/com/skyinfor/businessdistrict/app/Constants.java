package com.skyinfor.businessdistrict.app;


import android.app.Activity;
import android.text.TextUtils;

import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.eventbus.socket.SocketActionEvent;
import com.skyinfor.businessdistrict.ui.login.LoginActivity;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.SharedHelper;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by min on 2017/3/1.
 */
//G:\OA\OA\app\hctx.jks
public class Constants {
    // ModelType:0:请假单1:加班单2:补卡单详情3:外出单详情4:领用单详情5:采购单详情6:项目立项7:项目变更8:公告9:项目撤回10:工作计划完成申请11:工作协同申请12:工作延期13:工单拆机
    // 14:工单报修 15:财务请款 16.财务报销

    //    public static final String Base_Url = "http://117.131.119.11:8081/HCTX_OA/";//正式
    public static final String Base_Url = "http://www.skyinfor.cc:58080/shangwuqu/api/public/";//测试

    //        public static final String Base_Url = "http://192.168.200.234:8080/HCTX_OA/";//本地测试
    public static final String SHARED_PREFERENCE_NAME_KEY = "LINKTOWN_B_SHARED_PREFERENCE";
    public static final int HTTP_CONNECT_TIMEOUT = 16000;
    public static int STATUS_TYPE;
    public static int TASK_TYPE;

    /**
     * 共享文件名称
     */
    public static final String SHARED_PREFERENCE_NAME = "TABLEPMS_SHARED_PREFERENCE";
    /**
     * SharedPreference 中的字段 保存下载程序的id
     */
    public static final String APPDOWNLOAD_ID = "download_id";


    /**
     * 保存登录的用户邮箱
     */
    public static String USER_EMAIL = "";
    /**
     * 保存登录的用户COOKIE
     */
    public static String USER_COOKIE = "";

    /**
     * 保存登录的用户COOKIE
     */
    public static String USER_TOKEN_KEY = "user_token";
    public static String USER_NAME_KEY = "user_name";
    public static String ICON_KEY = "icon";
    public static String PHONE_KEY = "phone";
    public static String USER_ID_KEY = "user_id";
    public static String LOGIN_NAME_KEY = "login_name";
    public static String LOGIN_PASS_WORD_KEY = "password";

    public static String NICK_NAME_KEY = "nick_name";
    public static String RANK_FILED_KEY = "rank_field";


    public static String USER_TOKEN = "";
    public static String USER_NAME = "";
    public static String ICON = "";
    public static String PHONE = "";
    public static String RANK_FIELD = "";
    public static int USER_ID = -1;
    public static String NICK_NAME = "";

    public static boolean PRIVILEGE_TASK;//派发任务权限
    public static boolean PRIVILEGE_PLAN;//查看预案权限
    public static boolean PRIVILEGE_GROUP;//创建群组权限
    public static boolean PRIVILEGE_DEVICE;//查看设备权限

    public static String LOGIN_NAME;
    public static String LOGIN_PASS_WORD;
    public static boolean IS_LOCK;
    public static boolean IS_DIS_SOCKET;
    public static String CUR_ID;


    public static void readSpUserInfo() {
        Constants.LOGIN_NAME=SharedHelper.getString(Constants.LOGIN_NAME_KEY);
        Constants.LOGIN_PASS_WORD=SharedHelper.getString(Constants.LOGIN_PASS_WORD_KEY);

        Constants.USER_ID = SharedHelper.getI(Constants.USER_ID_KEY, -1);
        Constants.USER_TOKEN = SharedHelper.getString(Constants.USER_TOKEN_KEY);
        Constants.USER_NAME = SharedHelper.getString(Constants.USER_NAME_KEY);
        Constants.NICK_NAME = SharedHelper.getString(Constants.NICK_NAME_KEY);
        Constants.ICON = SharedHelper.getString(Constants.ICON_KEY);
        Constants.PHONE = SharedHelper.getString(Constants.PHONE_KEY);
        Constants.RANK_FIELD = SharedHelper.getString(Constants.RANK_FILED_KEY);
        getEventPer();
    }

    private static void getEventPer() {
        PRIVILEGE_TASK = getIsPer("PRIVILEGE_TASK");
        PRIVILEGE_PLAN = getIsPer("PRIVILEGE_PLAN");
        PRIVILEGE_GROUP = getIsPer("PRIVILEGE_GROUP");
        PRIVILEGE_DEVICE = getIsPer("PRIVILEGE_DEVICE");
    }

    public static void saveUserCookie(String cookie) {
        Constants.USER_COOKIE = cookie;
        SharedHelper.put(Constants.USER_TOKEN_KEY, cookie);
    }

    public static void saveUserInfo(String loginName,String passWord,int userId, String token, String userName, String nickName,
                                    String icon, String phone, String rank_field) {


        SharedHelper.put(Constants.LOGIN_NAME_KEY, loginName);
        SharedHelper.put(Constants.LOGIN_PASS_WORD_KEY, passWord);

        SharedHelper.put(Constants.USER_ID_KEY, userId);
        SharedHelper.put(Constants.USER_TOKEN_KEY, token);
        SharedHelper.put(Constants.USER_NAME_KEY, userName);
        SharedHelper.put(Constants.NICK_NAME_KEY, nickName);

        SharedHelper.put(Constants.ICON_KEY, icon);
        SharedHelper.put(Constants.PHONE_KEY, phone);

        SharedHelper.put(Constants.RANK_FILED_KEY, rank_field);

        Constants.LOGIN_NAME=loginName;
        Constants.LOGIN_PASS_WORD=passWord;
        Constants.USER_ID = userId;
        Constants.USER_TOKEN = token;
        Constants.USER_NAME = userName;
        Constants.NICK_NAME = nickName;
        Constants.ICON = icon;
        Constants.PHONE = phone;
        Constants.RANK_FIELD = rank_field;
        getEventPer();

    }

    /**
     * 是否在线
     *
     * @return
     */
    public static boolean isOnLine() {
        return Constants.USER_ID != -1&&!TextUtils.isEmpty(Constants.USER_TOKEN);
    }


    public static void removeUserInfo() {
        SharedHelper.remove(Constants.LOGIN_NAME_KEY);
        SharedHelper.remove(Constants.LOGIN_PASS_WORD_KEY);

        SharedHelper.remove(Constants.USER_ID_KEY);
        SharedHelper.remove(Constants.USER_TOKEN_KEY);
        SharedHelper.remove(Constants.USER_NAME_KEY);
        SharedHelper.remove(Constants.NICK_NAME_KEY);

        SharedHelper.remove(Constants.ICON_KEY);
        SharedHelper.remove(Constants.PHONE_KEY);

        SharedHelper.remove(Constants.RANK_FILED_KEY);
        Constants.USER_ID = 0;
        Constants.USER_TOKEN = "";
    }

    private static boolean getIsPer(String str) {
        return Constants.RANK_FIELD.contains(str);
    }

    /**
     * 退出登录
     *
     * @param activity
     */
    public static void logout(Activity activity) {
        removeUserInfo();
        XGPushManager.unregisterPush(activity);
        EventBus.getDefault().post(new SocketActionEvent(3));

        if (activity != null) {
            UIHelper.startActivity(activity, LoginActivity.class);
            AppManager.getAppManager().finishOtherTwoActivity(LoginActivity.class, LoginActivity.class);
        }
    }

}
