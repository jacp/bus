package com.skyinfor.businessdistrict.di.component;

import android.app.Activity;


import com.skyinfor.businessdistrict.di.PerActivity;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.ui.chat.detail.group.ChatDetailActivity;
import com.skyinfor.businessdistrict.ui.chat.detail.group.change_group_name.ChangeGroupNameActivity;
import com.skyinfor.businessdistrict.ui.chat.detail.group.member.GroupMemberDecActivity;
import com.skyinfor.businessdistrict.ui.chat.detail.person.PersonInfoActivity;
import com.skyinfor.businessdistrict.ui.chat.detail.person.edit.EditPersonInfoActivity;
import com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son.EditDetailActivity;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.ui.chat.select_person.SelectPersonActivity;
import com.skyinfor.businessdistrict.ui.chat.sound_chat.SoundChatActivity;
import com.skyinfor.businessdistrict.ui.chat.system_notice.SystemNoticeActivity;
import com.skyinfor.businessdistrict.ui.chat.video_chat.VideoChatActivity;
import com.skyinfor.businessdistrict.ui.contact.MsgSearchActivity;
import com.skyinfor.businessdistrict.ui.login.LoginActivity;
import com.skyinfor.businessdistrict.ui.main.MainActivity;
import com.skyinfor.businessdistrict.ui.main.contact.ContactSearchActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.CreateStuOrTaskActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.RelatedDeviceActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info.CameraInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug.FirePlugInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.stu_type.SelectStatusTypeActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select.TaskPersonSelectActivity;
import com.skyinfor.businessdistrict.ui.main.data_aly.BigDataActivity;
import com.skyinfor.businessdistrict.ui.main.notice.NoticeActivity;
import com.skyinfor.businessdistrict.ui.main.plan_check.PlanCheckActivity;
import com.skyinfor.businessdistrict.ui.main.plan_check.plan_detail.PlanDetailActivity;
import com.skyinfor.businessdistrict.ui.main.plan_check.search.PlanCheckSearchActivity;
import com.skyinfor.businessdistrict.ui.main.search_task_or_stu.SearchTaskAndStuActivity;
import com.skyinfor.businessdistrict.ui.main.stu_detail.StatusDetailActivity;
import com.skyinfor.businessdistrict.ui.main.stu_detail.person_dec.PersonDecListActivity;
import com.skyinfor.businessdistrict.ui.main.video_rec.VideoRecActivity;
import com.skyinfor.businessdistrict.ui.map.MapChooseActivity;
import com.skyinfor.businessdistrict.ui.map.map_view.MapCheckActivity;
import com.skyinfor.businessdistrict.ui.map.map_view.search.MapCheckSearchActivity;
import com.skyinfor.businessdistrict.ui.setting.SettingActivity;
import com.skyinfor.businessdistrict.ui.setting.modify_pass.ModifyPassActivity;
import com.skyinfor.businessdistrict.ui.startpager.StartPageActivity;

import dagger.Component;


/**
 * Created by min on 2017/3/2.
 * Activity 注入
 */

@PerActivity
@Component(dependencies = AppComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    Activity getActivity();

    void inject(MainActivity mainActivity);

    //欢迎页
    void inject(StartPageActivity startPageActivity);

    //登录
    void inject(LoginActivity loginActivity);

    //情况详情
    void inject(StatusDetailActivity statusDetailActivity);

    //创建任务
    void inject(CreateStuOrTaskActivity createStuOrTaskActivity);

    //情况类型选择
    void inject(SelectStatusTypeActivity selectStatusTypeActivity);

    //地图选择
    void inject(MapChooseActivity mapChooseActivity);

    //小视频录制
    void inject(VideoRecActivity videoRecActivity);

    //搜索任务
    void inject(SearchTaskAndStuActivity searchTaskAndStuActivity);

    //任务人员选择
    void inject(TaskPersonSelectActivity taskPersonSelectActivity);

    //关联设备
    void inject(RelatedDeviceActivity relatedDeviceActivity);

    //通讯录搜索
    void inject(ContactSearchActivity contactSearchActivity);

    //聊天房间
    void inject(ChatRoomActivity chatRoomActivity);

    //聊天详情
    void inject(ChatDetailActivity chatDetailActivity);

    //修改群名
    void inject(ChangeGroupNameActivity changeGroupNameActivity);

    //群成员列表
    void inject(GroupMemberDecActivity groupMemberDecActivity);

    //个人信息详情
    void inject(PersonInfoActivity personInfoActivity);

    //系统通知
    void inject(SystemNoticeActivity systemNoticeActivity);

    //视频聊天
    void inject(VideoChatActivity videoChatActivity);

    //语音聊天
    void inject(SoundChatActivity soundChatActivity);

    //修改个人资料
    void inject(EditPersonInfoActivity editPersonInfoActivity);

    //修改个人资料详情
    void inject(EditDetailActivity editDetailActivity);

    //设置界面
    void inject(SettingActivity settingActivity);

    //修改密码
    void inject(ModifyPassActivity modifyPassActivity);

    //地图查看
    void inject(MapCheckActivity mapCheckActivity);

    //人员列表详情
    void inject(PersonDecListActivity personDecListActivity);

    //发起视频或语音之前人员选择
    void inject(SelectPersonActivity selectPersonActivity);

    //消息搜索
    void inject(MsgSearchActivity msgSearchActivity);

    //公告
    void inject(NoticeActivity noticeActivity);

    //预案查看
    void inject(PlanCheckActivity planCheckActivity);

    //预案详情
    void inject(PlanDetailActivity planDetailActivity);

    //消防栓信息
    void inject(FirePlugInfoActivity firePlugInfoActivity);

    //相机信息
    void inject(CameraInfoActivity cameraInfoActivity);

    //预案搜索
    void inject(PlanCheckSearchActivity planCheckSearchActivity);

    //设备搜索
    void inject(MapCheckSearchActivity mapCheckSearchActivity);

    //数据统计
    void inject(BigDataActivity bigDataActivity);
}