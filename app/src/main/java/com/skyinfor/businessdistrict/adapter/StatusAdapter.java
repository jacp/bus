package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.util.DateUtil;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusAdapter extends BaseMyRecyclerVIewAdapter<StatusSonModel> {

    public StatusAdapter(Context context, List<StatusSonModel> list) {
        super(context, list, R.layout.item_status);
    }

    @Override
    protected void covert(StatusSonModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        TextView name = holder.getView(R.id.text_status_name);

        TextView process = holder.getView(R.id.text_status_process);

        TextView type = holder.getView(R.id.text_status_type);

        TextView from = holder.getView(R.id.text_status_from);

        TextView date = holder.getView(R.id.text_status_date);

        name.setText(mode.getName());

        process.setText(mode.getStatus_name());

        type.setText(mode.getType_name());

        from.setText(mode.getNickname());

        date.setText(DateUtil.formatDate(DateUtil.strToDate(mode.getCreate_time(), 9), 9));

        int layId = 0;
        switch (mode.getLevel()) {
            case "一级":
                layId = R.mipmap.icon_stu_one;
                break;
            case "二级":
                layId = R.mipmap.icon_stu_two;

                break;

            case "三级":
                layId = R.mipmap.icon_stu_three;

                break;
        }

        Drawable drawable = mContext.getResources().getDrawable(layId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());// 设置边界

        name.setCompoundDrawables(drawable, null, null, null);

        setColor(mode.getStatus_code(), process);
    }

    private void setColor(int index, TextView tv) {
        switch (index) {
            case 1:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
            case 2:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_orange));

                break;

            case 3:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_blue));

                break;
            case 4:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_green));

                break;
        }
    }
}
