package com.skyinfor.businessdistrict.adapter;

import android.content.Context;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class PlanListAdapter extends BaseMyRecyclerVIewAdapter<PlanListModel> {

    public PlanListAdapter(Context context, List<PlanListModel> list) {
        super(context, list, R.layout.item_plan_list);
    }

    @Override
    protected void covert(PlanListModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        holder.setText(R.id.text_plan_name, mode.getName());
        if (mode.getPlan_condition() != null && mode.getPlan_condition().size() > 0) {
            holder.setText(R.id.text_plan_type, String.format("条件类型：%s", mode.getPlan_condition().
                    get(0).getCondition_name()));
        }

    }
}
