package com.skyinfor.businessdistrict.ui.chat.video_chat;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.FrameLayout;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.SoundChatMyselfAdapter;
import com.skyinfor.businessdistrict.adapter.SoundChatOtherAdapter;
import com.skyinfor.businessdistrict.adapter.VideoChatMySelfAdapter;
import com.skyinfor.businessdistrict.eventbus.SoundStatusEvent;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.VideoGroupChatModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class VideoChatPresent extends BasePresenter<VideoChatContract.Model>
        implements VideoChatContract.Presenter {

    private RtcEngine mRtcEngine;
    private List<VideoGroupChatModel> list = new ArrayList<>();

    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() { // Tutorial Step 1


        @Override
        public void onFirstRemoteVideoDecoded(int uid, int width, int height, int elapsed) {
            if (type == 0) {
                EventBus.getDefault().post(new SoundStatusEvent(3, uid));

            } else {
                EventBus.getDefault().post(new SoundStatusEvent(1, uid));
            }
        }

        @Override
        public void onFirstRemoteVideoFrame(int uid, int width, int height, int elapsed) {
            super.onFirstRemoteVideoFrame(uid, width, height, elapsed);
        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            if (type == 0) {
                EventBus.getDefault().post(new SoundStatusEvent(1, uid));
            }
        }

        @Override
        public void onUserOffline(final int uid, final int reason) { // Tutorial Step 4
            EventBus.getDefault().post(new SoundStatusEvent(2, uid));

        }

        @Override
        public void onUserMuteAudio(final int uid, final boolean muted) { // Tutorial Step 6

        }
    };
    private VideoChatMySelfAdapter adapter;
    private SoundChatMyselfAdapter myselfAdapter;
    private List<GroupMemberModel.UserListBean> mySelfList = new ArrayList<>();
    private SoundChatOtherAdapter otherAdapter;
    private List<GroupMemberModel.UserListBean> otherList = new ArrayList<>();
    private int type;

    @Inject
    public VideoChatPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);

    }


    @Override
    public void initSingleConfig(int type) {
        try {
            this.type = type;
            mRtcEngine = RtcEngine.create(mActivity.getBaseContext(), mActivity.getString(R.string.agora_app_id), mRtcEventHandler);
            mModel.setRtcEngine(mRtcEngine);
        } catch (Exception e) {
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }

    }


    @Override
    public void onSingleDestroy() {
        if (mRtcEngine != null) {
            mRtcEngine.leaveChannel();//离开频道
            RtcEngine.destroy();
            mRtcEngine = null;
        }
    }

    @Override
    public void initAdapter(RecyclerView recyclerView, RecyclerView recVideo, RecyclerView recOther) {
        myselfAdapter = new SoundChatMyselfAdapter(mActivity, mySelfList);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, 3));
        recyclerView.setAdapter(myselfAdapter);

        adapter = new VideoChatMySelfAdapter(mActivity, list);
        recVideo.setLayoutManager(new GridLayoutManager(mActivity, 3));
        recVideo.setAdapter(adapter);

        otherAdapter = new SoundChatOtherAdapter(mActivity, otherList);
        recOther.setLayoutManager(new GridLayoutManager(mActivity, 4));
        recOther.setAdapter(otherAdapter);
    }

    @Override
    public void setNotify(List<VideoGroupChatModel> mList) {
        list.clear();
        list.addAll(mList);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void setTitleNotify(List<GroupMemberModel.UserListBean> mList) {
        mySelfList.clear();
        mySelfList.addAll(mList);
        myselfAdapter.notifyDataSetChanged();

        otherList.clear();
        otherList.addAll(mList);
        otherAdapter.notifyDataSetChanged();
    }


}
