package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.util.DateUtil;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class TaskAdapter extends BaseMyRecyclerVIewAdapter<TaskSonModel> {


    private List<TaskSonModel> list;
    private TaskIconAdapter managerAdapter;
    private ImageView injectImg;

    public TaskAdapter(Context context, List<TaskSonModel> list) {
        super(context, list, R.layout.item_task_son);
        this.list = list;
    }

    @Override
    protected void covert(TaskSonModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        holder.setText(R.id.text_task_son_name, mode.getName());
        TextView status = holder.getView(R.id.text_task_son_stu);
        IconView imgManage = holder.getView(R.id.img_task, position);
        RecyclerView recyclerView = holder.getView(R.id.rec_task_exe);
        injectImg = holder.getView(R.id.img_task_inject);
        injectImg.setVisibility(View.GONE);
        View moreAccept = holder.getView(R.id.text_task_end);
        if (mode.getAccept_user_list() != null && mode.getAccept_user_list().size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            moreAccept.setVisibility(View.VISIBLE);

            managerAdapter = new TaskIconAdapter(mContext, mode.getAccept_user_list());
            LinearLayoutManager manager = new LinearLayoutManager(mContext);
            manager.setOrientation(LinearLayout.HORIZONTAL);
            recyclerView.setLayoutManager(manager);
            recyclerView.setAdapter(managerAdapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            moreAccept.setVisibility(View.GONE);
        }

        holder.setText(R.id.text_task_location, mode.getAddress());
        TextView date = holder.getView(R.id.text_task_date);
        date.setText(DateUtil.formatDate(DateUtil.strToDate(mode.getCreate_time(), 9), 9));

        imgManage.setBaseInfo(mContext, mode.getSend_user_nickname(), mode.getSend_user_avatar());

        setColor(Integer.parseInt(mode.getStatus_code()), status);
        status.setText(mode.getStatus_name());

    }

    private void setColor(int index, TextView tv) {
        switch (index) {
            case 1:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
            case 2:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_orange));
                break;

            case 3:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_blue));
                break;
            case 4:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_green));
                break;
            case 5:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                injectImg.setVisibility(View.VISIBLE);

                break;

        }
    }

    public void setNotify(List<TaskSonModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

}
