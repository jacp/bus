package com.skyinfor.businessdistrict.fragment.main.home.plan.plan_list;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface PlanListApi {

    @GET(ApiUrl.planList)
    Observable<Response<List<PlanListModel>>>planList(@Query("page") String page, @Query("unit")String unit
            , @Query("user_id")String user_id);

    @GET(ApiUrl.planLog)
    Observable<Response<List<PlanListModel>>>planLog(@Query("page") String page,@Query("unit")String unit
            ,@Query("user_id")String user_id);

}
