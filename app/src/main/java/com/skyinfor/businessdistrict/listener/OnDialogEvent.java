package com.skyinfor.businessdistrict.listener;

import android.app.Dialog;

/**
 * Created by Min on 2017/4/26.
 */

public interface OnDialogEvent<T> {
    public void ok(Dialog dialog, T data, int arg1, int arg2);
    public void cancel(Dialog dialog);
}
