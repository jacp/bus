package com.skyinfor.businessdistrict.ui.main.stu_detail.person_dec;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonDecListActivity extends BaseActivity<PersonDecListPresent>
        implements PersonDecListContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rec_person_dec_list)
    RecyclerView recPersonDecList;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_person_dec_list;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        tvTitle.setText("人员");
        List<UserIconBean> list = (List<UserIconBean>) getIntent().getSerializableExtra("model");
        mPresenter.initAdapter(recPersonDecList);
        if (list != null && list.size() > 0)
            mPresenter.setNotify(list);
    }
}
