package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ContactPersonAdapter extends BaseMyRecyclerVIewAdapter<PersonListBean> {
    private boolean isSelect;
    private List<PersonListBean> list;
    private boolean isShowSelf = true;

    public ContactPersonAdapter(Context context, List<PersonListBean> list, boolean isSelect) {
        super(context, list, R.layout.item_contact_person);
        this.list = list;
        this.isSelect = isSelect;
    }

    @Override
    protected void covert(final PersonListBean mode, BaseViewHolder holder, final int position) {
        super.covert(mode, holder, position);

        IconView img = holder.getView(R.id.img_contact_person_icon, position);
        img.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());

        holder.setText(R.id.text_contact_person_name, mode.getNickname());

        holder.setText(R.id.text_contact_person_job, mode.getJob());

        TextView box = holder.getView(R.id.checkbox_status);
        box.setVisibility(isSelect == true ? View.VISIBLE : View.GONE);

        box.setSelected(mode.isCheck());


    }

    public void setNotify(List<PersonListBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

}
