package com.skyinfor.businessdistrict.ui.main.plan_check;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SegmentTabLayout;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.ui.main.plan_check.search.PlanCheckSearchActivity;
import com.skyinfor.businessdistrict.util.UIHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlanCheckActivity extends BaseActivity<PlanCheckPresent> implements PlanCheckContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ap_bar_stl)
    SegmentTabLayout apBarStl;
    @BindView(R.id.view_plan_check)
    ViewPager viewPlanCheck;
    @BindView(R.id.img_right)
    ImageView imgRight;
    private String[] str = {"预案列表", "触发记录"};

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plan_check;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        tvTitle.setVisibility(View.GONE);
        apBarStl.setVisibility(View.VISIBLE);
        apBarStl.setTabData(str);
        imgRight.setVisibility(View.VISIBLE);
        imgRight.setImageResource(R.mipmap.icon_task_search);
        mPresenter.initAdapter(getSupportFragmentManager(), apBarStl, viewPlanCheck);

    }


    @OnClick(R.id.img_right)
    public void onViewClicked() {
        Bundle data=new Bundle();
        UIHelper.startActivity(mContext,PlanCheckSearchActivity.class,data);
    }
}
