package com.skyinfor.businessdistrict.eventbus;

public class PositionEvent {

    private String position;

    public PositionEvent(String position) {
        this.position = position;
    }

    public String getPosition() {
        return position;
    }
}
