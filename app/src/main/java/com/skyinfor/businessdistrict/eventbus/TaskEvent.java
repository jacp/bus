package com.skyinfor.businessdistrict.eventbus;

public class TaskEvent {

    private int selectType;

    public TaskEvent(int selectType) {
        this.selectType = selectType;
    }

    public int getSelectType() {
        return selectType;
    }

}
