package com.skyinfor.businessdistrict.eventbus;

import android.graphics.Bitmap;

public class VideoEvent {
    private String url;
    private Bitmap bitmap;

    public VideoEvent(String url, Bitmap bitmap) {
        this.url = url;
        this.bitmap = bitmap;
    }

    public String getUrl() {
        return url;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
