package com.skyinfor.businessdistrict.ui.main.plan_check;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.flyco.tablayout.SegmentTabLayout;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class PlanCheckContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(FragmentManager fm, final SegmentTabLayout apBarStl,
                         final ViewPager viewPager);

    }

}
