package com.skyinfor.businessdistrict.ui.main.stu_detail.person_dec;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

public class PersonDecListContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void setNotify(List<UserIconBean> mList);
    }

}
