package com.skyinfor.businessdistrict.eventbus;

public class ChatNotifyEvent {

    private int type;

    public ChatNotifyEvent() {

    }

    public ChatNotifyEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
