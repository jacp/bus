package com.skyinfor.businessdistrict.fragment.main.home.plan.plan_list;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class PlanListContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();
    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView,int type);
        void gePlanList(int page,int type);

    }

}
