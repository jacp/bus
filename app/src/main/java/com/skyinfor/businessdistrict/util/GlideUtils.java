package com.skyinfor.businessdistrict.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.signature.StringSignature;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.MyApplication;

import java.io.File;


/**
 * Created by wali on 2016/9/22.
 */
public class GlideUtils {

    /**
     * 常用的加载方式
     *
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadImg(Context context, String url, ImageView imageView) {
        try {
            ViewGroup.LayoutParams params = imageView.getLayoutParams();
            int width = params.width;
            int height = params.height;

            Glide.with(context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.mipmap.placeholder)
                    .override(width, height)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                throw new Exception("当前的img 高度为0 请使用 ");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * 外置传入宽和高
     *
     * @param width
     * @param height
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadOutSideImg(int width, int height, Context context, String url, ImageView imageView) {
        try {
            Glide.with(context)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.mipmap.placeholder)
                    .placeholder(R.mipmap.placeholder)
                    .override(width, height)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                throw new Exception("当前的img 高度为0 请使用 ");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }


    /**
     * 加载圆形图片
     *
     * @param width
     * @param height
     * @param context
     * @param url
     * @param imageView
     */
    public static void loadCircleImg(int width, int height, Context context, String url,
                                     ImageView imageView) {
        Glide.with(context)
                .load(url)
                .bitmapTransform(new CircleTransformation(context, CircleTransformation.CropType.CENTER))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.placeholder)
                .placeholder(R.mipmap.placeholder)
                .crossFade()
                .override(width, height)
                .into(imageView);
    }

    /**
     * 加载android本地图片
     *
     * @param url
     * @param imageView
     * @param imgId
     */
    public static void loadImg(String url, ImageView imageView, int imgId) {
        Glide.with(MyApplication.application)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(imgId)
                .error(imgId)
                .into(imageView);
    }

    public static void loadImg(int url, ImageView imageView) {
        Glide.with(MyApplication.application)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    public static void loadImg(Object url, ImageView imageView) {
        Glide.with(MyApplication.application)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    public static void loadCacheUri(String path, int width, int height, ImageView imageView) {
        Glide.with(MyApplication.application)
                //.load(path)
                //配置上下文
                .load(Uri.fromFile(new File(path)))      //设置图片路径(fix #8,文件名包含%符号 无法识别和显示)
                .error(R.mipmap.placeholder)           //设置错误图片
                .placeholder(R.mipmap.placeholder)     //设置占位图片
                .diskCacheStrategy(DiskCacheStrategy.ALL)//缓存全尺寸
                .override(width, height)
                .into(imageView);
    }

    public static void loadCacheUri(String path, ImageView imageView) {
        Glide.with(MyApplication.application)
                //.load(path)
                //配置上下文
                .load(Uri.fromFile(new File(path)))      //设置图片路径(fix #8,文件名包含%符号 无法识别和显示)
                .error(R.mipmap.placeholder)           //设置错误图片
                .placeholder(R.mipmap.placeholder)     //设置占位图片
                .diskCacheStrategy(DiskCacheStrategy.ALL)//缓存全尺寸
                .into(imageView);
    }

    public static void loadCacheLocalUri(int path, ImageView imageView) {
        Glide.with(MyApplication.application)
                //.load(path)
                //配置上下文
                .load(path)      //设置图片路径(fix #8,文件名包含%符号 无法识别和显示)
                .error(R.mipmap.placeholder)           //设置错误图片
                .placeholder(R.mipmap.placeholder)     //设置占位图片
                .diskCacheStrategy(DiskCacheStrategy.ALL)//缓存全尺寸
                .into(imageView);
    }

    public static void loadisPlaceholderImg(String url, ImageView imageView) {
        Glide.with(MyApplication.application)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.mipmap.placeholder)
                .placeholder(R.mipmap.placeholder)
                .into(imageView);
    }


}
