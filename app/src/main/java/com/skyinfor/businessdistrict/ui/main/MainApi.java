package com.skyinfor.businessdistrict.ui.main;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface MainApi {

    @FormUrlEncoded
    @POST(ApiUrl.bindUserId)
    Observable<Response<Object>>bindUserId(@Field("user_id") String user_id
            ,@Field("client_id") String client_id);

    @FormUrlEncoded
    @POST(ApiUrl.userLocationUpload)
    Observable<Response<Object>>userLocationUpload(@Field("user_id") String user_id
            ,@Field("position") String position);

}
