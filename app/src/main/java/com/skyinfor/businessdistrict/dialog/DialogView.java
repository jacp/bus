package com.skyinfor.businessdistrict.dialog;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;


import com.rey.material.app.Dialog;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.listener.OnMessageDialogEvent;

/**
 * Created by min on 2017/4/28.
 */

public class DialogView extends Dialog implements View.OnClickListener {
    private TextView yes;//确定按钮
    private TextView no;//取消按钮
    private TextView titleTv;//消息标题文本
    private TextView messageTv;//消息提示文本
    private String titleStr;//从外界设置的title文本
    private String messageStr;//从外界设置的消息文本
    //确定文本和取消文本的显示内容
    private String yesStr, noStr;

    public OnMessageDialogEvent _mEvent;
    private int strIcon;


    public DialogView(Context context,String title,String content,String yesStr,String noStr) {
        super(context, R.style.CornerDialog);
        this.titleStr=title;
        this.messageStr=content;
        this.yesStr=yesStr;
        this.noStr=noStr;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        setContentView(R.layout.dialog_agreen);
        //按空白处取消动画
        setCanceledOnTouchOutside(true);

        //初始化界面控件
        initView();
        //初始化界面数据
        initData();

    }


    public DialogView setDialogEvent(OnMessageDialogEvent event) {
        this._mEvent = event;
        return this;
    }


    /**
     * 初始化界面控件的显示数据
     */
    private void initData() {
        //如果用户自定了title和message
        if (!TextUtils.isEmpty(titleStr)) {
            titleTv.setText(titleStr);
        }
        if (!TextUtils.isEmpty(messageStr)) {
            messageTv.setText(messageStr);
        }
        //如果设置按钮的文字
        if (!TextUtils.isEmpty(yesStr)) {
            yes.setText(yesStr);
        }
        if (!TextUtils.isEmpty(noStr)) {
            no.setText(noStr);
        }
    }

    /**
     * 初始化界面控件
     */
    private void initView() {
        yes = (TextView) findViewById(R.id.tv_agreen);
        no = (TextView) findViewById(R.id.tv_cancle);
        titleTv = (TextView) findViewById(R.id.tv_prompt);
        messageTv = (TextView) findViewById(R.id.tv_content);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    /**
     * 从外界Activity为Dialog设置标题
     *
     * @param title
     */
    public void setTitle(String title) {
        titleStr = title;
    }

    /**
     * 从外界Activity为Dialog设置dialog的message
     *
     * @param message
     */
    public void setMessage(String message) {
        messageStr = message;
    }


    @Override
    public void onClick(View v) {
        if (_mEvent != null) {
            if (v.equals(yes)) {
                _mEvent.ok(this);
            } else if (v.equals(no)) {
                _mEvent.close(this);
            }
        }
        if(autoDismiss()) {
            this.dismiss();
        }
    }

    protected boolean autoDismiss() {
        return true;
    }
}
