package com.skyinfor.businessdistrict.ui.setting.modify_pass;

import android.app.Activity;

import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.detail.person.edit.EditPersonApi;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.SharedHelper;

import java.util.List;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class ModifyPassPresent extends BasePresenter<ModifyPassContract.Model>
        implements ModifyPassContract.Presenter {

    private HttpDialog dialog;
    private EditPersonApi api;

    @Inject
    public ModifyPassPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(EditPersonApi.class);

    }


    @Override
    public void updateInfo(List<MultipartBody.Part> parts, final String passWord) {
        dialog.show();
        addSubscription(api.userInformationUpdate(parts)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        SharedHelper.put(Constants.LOGIN_PASS_WORD_KEY, passWord);
                        Constants.LOGIN_PASS_WORD = passWord;
                        mModel.setResultSuc();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();

                    }
                });

    }

}
