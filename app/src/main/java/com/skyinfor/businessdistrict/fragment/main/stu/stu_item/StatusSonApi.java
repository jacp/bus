package com.skyinfor.businessdistrict.fragment.main.stu.stu_item;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusSonModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public interface StatusSonApi {


    @GET(ApiUrl.ConditionAcceptList)
    Observable<Response<List<StatusSonModel>>> acceptList(@QueryMap Map<String, String> param);

    @GET(ApiUrl.ConditionSendList)
    Observable<Response<List<StatusSonModel>>> sendList(@QueryMap Map<String, String> param);

    @GET(ApiUrl.conditionAll)
    Observable<Response<List<StatusSonModel>>> conditionAll(@QueryMap Map<String, String> param);
}
