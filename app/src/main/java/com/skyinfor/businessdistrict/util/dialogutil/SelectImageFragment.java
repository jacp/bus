package com.skyinfor.businessdistrict.util.dialogutil;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.util.ImagePickerUtil;
import com.skyinfor.businessdistrict.util.LuBanCompress;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.http.HttpParams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Name: SelectImageFragment
 * Author: FrameJack
 * Email: framejackname@gmail.com
 * Date: 2017-07-24 10:24
 * Desc:图片选择（拍照）
 * Comment: 选择图片DialogFragment 也只能这样写了
 * //TODO
 */
public class SelectImageFragment extends BaseDialogFragment {

    /**
     * 头像名称
     */
    private static final String IMAGE_FILE_NAME = "image.jpg";
    /**
     * 请求码
     */
    private static final int IMAGE_REQUEST_CODE = 0;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int RESULT_REQUEST_CODE = 2;
    private static final int NO_CROP = 3;
    private ImageView imgUserdataHead;
    private selectCompletionListener listener;
    private Context mContext;
    private boolean isCrop = true;
    private int type;//1 拍照 0 相册选取
    private File file;
    public static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_CAMER = 200;
    private int maxImgCount = 1;
    private ArrayList<ImageItem> selImageList = new ArrayList<>(); //当前选择的所有图片
    private ArrayList<ImageItem> images;


    public interface selectCompletionListener {
        void completion(String path);
    }

    /**
     * @param data
     * @param imgUserdataHead 选择后显示图片的控件
     * @param listener        回调
     * @return
     */
    public static SelectImageFragment getInstance(Bundle data, ImageView imgUserdataHead
            , selectCompletionListener listener) {
        SelectImageFragment fragment = new SelectImageFragment();
        fragment.setArguments(data);
        fragment.listener = listener;
        if (imgUserdataHead != null) {
            fragment.imgUserdataHead = imgUserdataHead;
        }
        return fragment;
    }

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_select_image;
    }

    @Override
    protected void initData() {
        super.initData();
        mContext = getActivity();
        isCrop = getArguments().getBoolean("isCrop");

        TextView takePhone = (TextView) view.findViewById(R.id.text_select_take_phone);
        TextView selectPhone = (TextView) view.findViewById(R.id.text_select_take_select);
        TextView cancel = (TextView) view.findViewById(R.id.text_select_cancel);

        takePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                useCamera();
                dismiss();
            }
        });
        selectPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImg();
                dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }


    /**
     * 使用相机
     */
    private void useCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/bus/" + System.currentTimeMillis() + ".jpg");
        file.getParentFile().mkdirs();

        //改变Uri  com.xykj.customview.fileprovider注意和xml中的一致
        Uri uri = FileProvider.getUriForFile(mContext, "com.skyinfor.businessdistrict.fileprovider", file);
        //添加权限
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CODE_CAMER);
    }

    private void selectImg() {
        ImagePickerUtil.initImageMorePicker();
        //打开选择,本次允许选择的数量
        ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
        Intent intent1 = new Intent(mContext, ImageGridActivity.class);
        /* 如果需要进入选择的时候显示已经选中的图片，
         * 详情请查看ImagePickerActivity
         * */
//                                intent1.putExtra(ImageGridActivity.EXTRAS_IMAGES,images);
        startActivityForResult(intent1, REQUEST_CODE_SELECT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == REQUEST_CODE_SELECT) {
            images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                selImageList.clear();
                selImageList.addAll(images);
            }
            if (listener != null) {
                listener.completion(selImageList.get(0).path);
            }

        } else if (requestCode == REQUEST_CODE_CAMER && resultCode == RESULT_OK) {
            file.getAbsolutePath();
            if (listener != null) {
                listener.completion(file.getAbsolutePath());
            }
        }

    }


}
