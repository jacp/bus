package com.skyinfor.businessdistrict.ui.chat.detail.group;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.dialog.DialogView;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.eventbus.ContactEvent;
import com.skyinfor.businessdistrict.listener.OnMessageDialogEvent;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.ui.chat.detail.group.change_group_name.ChangeGroupNameActivity;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.util.UIHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;

public class ChatDetailActivity extends BaseActivity<ChatDetailPresent> implements ChatDetailContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rec_chat_detail)
    RecyclerView recChatDetail;
    @BindView(R.id.text_chat_detail_name)
    TextView textChatDetailName;
    private String group_uuid;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_chat_detail;

    }


    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);

        tvTitle.setText("群聊成员");
        group_uuid = getIntent().getStringExtra("group_uuid");
        mPresenter.initAdapter(recChatDetail);
        mPresenter.getGroupInfo(group_uuid);

    }

    @Override
    public void setResult(GroupMemberModel model) {
        textChatDetailName.setText(model.getGroup_name());
    }

    @Override
    public void exitResult() {
        EventBus.getDefault().post(new ChatNotifyEvent());
        EventBus.getDefault().post(new ContactEvent(1,group_uuid));
        appManager.finishActivity(ChatRoomActivity.class);
        appManager.finishActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void getEvent(ChatNameEvent event) {
        textChatDetailName.setText(event.getName());
    }

    @OnClick(R.id.rel_chat_detail_name)
    public void onViewClicked() {
        String name = textChatDetailName.getText().toString().trim();

        Bundle data = new Bundle();
        data.putString("uuid", group_uuid);
        data.putString("name", name);
        UIHelper.startActivity(mContext, ChangeGroupNameActivity.class, data);
    }

    @OnClick(R.id.text_chat_detail_exit)
    public void onExitClicked() {
        DialogView dialog = new DialogView(mContext, "",
                String.format("是否确认%s?", "群聊"), "确认", "取消");
        dialog.show();

        dialog.setDialogEvent(new OnMessageDialogEvent() {
            @Override
            public void ok(Dialog dialog) {
                mPresenter.exitGroup(group_uuid, String.valueOf(Constants.USER_ID));
            }

            @Override
            public void close(Dialog dialog) {

            }
        });
    }
}
