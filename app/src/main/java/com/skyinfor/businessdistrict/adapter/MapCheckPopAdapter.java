package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.CheckBox;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.MapPopModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class MapCheckPopAdapter extends BaseMyRecyclerVIewAdapter<MapPopModel> {

    private List<MapPopModel> list;

    public MapCheckPopAdapter(Context context, List<MapPopModel> list) {
        super(context, list, R.layout.item_map_type_choose);
        this.list = list;
    }

    @Override
    protected void covert(MapPopModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        CheckBox box = holder.getView(R.id.checkbox_status_one);
        TextView name = holder.getView(R.id.text_map_icon);
        name.setText(mode.getStr());
        int layId;
        switch (mode.getStr()) {
            case "摄像头":
                layId = R.mipmap.icon_map_position_icon_camera;
                break;
            case "消防栓":
                layId = R.mipmap.icon_map_position_xiao_fang;
                break;
            case "人员":
                layId = R.mipmap.icon_map_position_icon_people;
                break;
            default:
                layId = R.mipmap.icon_map_position_icon_people;
                break;
        }
        box.setChecked(mode.isCheck());

        Drawable left = mContext.getResources().getDrawable(layId);
        left.setBounds(0, 0, left.getMinimumWidth(), left.getMinimumHeight());
        name.setCompoundDrawables(left, null, null, null);
    }

    public void setNotify(List<MapPopModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
