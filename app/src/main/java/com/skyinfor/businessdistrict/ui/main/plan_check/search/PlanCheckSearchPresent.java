package com.skyinfor.businessdistrict.ui.main.plan_check.search;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.lzy.imagepicker.view.FolderPopUpWindow;
import com.skyinfor.businessdistrict.adapter.PlanListAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.plan_check.plan_detail.PlanDetailActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class PlanCheckSearchPresent extends BasePresenter<PlanCheckSearchContract.Model> implements PlanCheckSearchContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private List<PlanListModel> list = new ArrayList<>();
    private PlanListAdapter adapter;
    public HttpDialog dialog;
    private int type;
    private PlanCheckSearchApi api;

    @Inject
    public PlanCheckSearchPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        api = retrofit.create(PlanCheckSearchApi.class);
        dialog = new HttpDialog(mActivity);

    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new PlanListAdapter(mActivity, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity),
                Color.parseColor("#efeff4")));

        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

    }


    @Override
    public void gePlanList(String name, final int page, final int type) {
        final String pages = String.valueOf(page);
        this.type = type;

        Observable<Response<List<PlanListModel>>> observable = null;
        switch (type) {
            case 0:
                observable = api.planList(name, pages, "10", String.valueOf(Constants.USER_ID));
                break;
            case 1:
                observable = api.planLog(name, pages, "10", String.valueOf(Constants.USER_ID));
                break;
        }

        addSubscription(observable
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<PlanListModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mModel.compelete();

                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<PlanListModel> bean) {

                        if (page == 1) {
                            list.clear();
                        }
                        list.addAll(bean);

                        if (bean.size() < 10) {
                            mModel.close();
                            mModel.noLoadMode();
                            if (page > 1) {
                                ToashUtils.show(mActivity, "已经是最后一页数据", 2000, Gravity.CENTER);
                            }
                        } else {
                            mModel.defalutMode();
                        }
                        adapter.notifyDataSetChanged();
                    }

                });

    }

    @Override
    public void onItemClick(View view, int position) {
        Bundle data = new Bundle();
        data.putInt("type", type);
        data.putSerializable("model", list.get(position));

        UIHelper.startActivity(mActivity, PlanDetailActivity.class, data);
    }
}
