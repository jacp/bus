package com.skyinfor.businessdistrict.ui.chat.select_person;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.skyinfor.businessdistrict.adapter.ChatDetailMemberAdapter;
import com.skyinfor.businessdistrict.adapter.SelectPersonAdapter;
import com.skyinfor.businessdistrict.adapter.SelectPersonContentAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.SharedHelper;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;
import com.skyinfor.businessdistrict.util.recyclerview.SpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class SelectPersonPresent extends BasePresenter<SelectPersonContract.Model>
        implements SelectPersonContract.Presenter {

    private List<GroupMemberModel.UserListBean> titleList = new ArrayList<>();
    private List<GroupMemberModel.UserListBean> list = new ArrayList<>();
    private HttpDialog dialog;
    private SelectPersonApi api;
    private SelectPersonAdapter titleAdapter;
    private SelectPersonContentAdapter adapter;

    @Inject
    public SelectPersonPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(SelectPersonApi.class);

    }


    @Override
    public void initAdapter(RecyclerView titleRec, RecyclerView recyclerView) {
        GroupMemberModel.UserListBean bean=new GroupMemberModel.UserListBean();
        bean.setAvatar(Constants.ICON);
        bean.setId(Constants.USER_ID);
        bean.setNickname(Constants.NICK_NAME);
        titleList.add(bean);

        //title选中
        titleAdapter = new SelectPersonAdapter(mActivity,titleList);
        LinearLayoutManager iconLin = new LinearLayoutManager(mActivity);
        iconLin.setOrientation(LinearLayout.HORIZONTAL);
        titleRec.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.HORIZONTAL, dp2px(10, mActivity),
                Color.parseColor("#ffffff")));
        titleRec.setLayoutManager(iconLin);
        titleRec.setAdapter(titleAdapter);

        //群员
         adapter= new SelectPersonContentAdapter(mActivity,list,
                true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (titleList.size()<=9){
                    if (Constants.USER_ID!=list.get(position).getId()){
                        boolean check = list.get(position).isCheck();
                        list.get(position).setCheck(!check);
                        adapter.setNotify(list);
                        getIconList(position);
                    }
                }else {
                    ToashUtils.show(mActivity,"最多只能选9个人");
                }

            }
        });
    }

    @Override
    public void getGroupInfo(String uuId) {
        dialog.show();
        addSubscription(api.talkGroupInformation(uuId)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<GroupMemberModel>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(GroupMemberModel groupMemberModel) {
                        list.clear();
                        list.addAll(groupMemberModel.getUser_list());

                        for (GroupMemberModel.UserListBean model:
                                list ) {
                            if (Constants.USER_ID==model.getId()){
                                model.setCheck(true);
                            }
                        }

                        adapter.notifyDataSetChanged();

                    }
                });

    }

    private void getIconList(int position) {
        boolean check = list.get(position).isCheck();
        if (!check) {
            titleList.remove(list.get(position));
        } else {
            titleList.add(list.get(position));
        }
        titleAdapter.notifyDataSetChanged();
        mModel.setSelectList(titleList);
    }

}
