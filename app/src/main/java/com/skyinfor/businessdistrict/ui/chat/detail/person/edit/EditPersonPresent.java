package com.skyinfor.businessdistrict.ui.chat.detail.person.edit;

import android.app.Activity;

import com.skyinfor.businessdistrict.eventbus.EditPersonEvent;
import com.skyinfor.businessdistrict.model.FileModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son.EditDetailApi;
import com.skyinfor.businessdistrict.util.HttpDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class EditPersonPresent extends BasePresenter<EditPersonContract.Model>implements
        EditPersonContract.Presenter{

    private HttpDialog dialog;
    private EditPersonApi api;

    @Inject
    public EditPersonPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(EditPersonApi.class);

    }

    @Override
    public void updateInfo(List<MultipartBody.Part> parts) {
        dialog.show();
        addSubscription(api.userInformationUpdate(parts)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();
                        EventBus.getDefault().post(new EditPersonEvent());
                    }
                });

    }

    @Override
    public void upFile(List<MultipartBody.Part> part) {
        dialog.show();

        addSubscription(api.upFile(part)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<FileModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(FileModel bean) {
                        mModel.setSucImg(bean.getSave_path());
                    }
                });

    }

}
