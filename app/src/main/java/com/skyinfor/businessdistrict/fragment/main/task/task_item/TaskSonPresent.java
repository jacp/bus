package com.skyinfor.businessdistrict.fragment.main.task.task_item;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.Gravity;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.StatusAdapter;
import com.skyinfor.businessdistrict.adapter.TaskAdapter;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.model.TaskModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.stu_detail.StatusDetailActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class TaskSonPresent extends BasePresenter<TaskSonContract.Model> implements TaskSonContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {
    private List<TaskSonModel> list = new ArrayList<>();
    public TaskAdapter adapter;
    private TaskSonApi api;
    public HttpDialog dialog;
    private int mPosition;

    @Inject
    public TaskSonPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        api = retrofit.create(TaskSonApi.class);
        dialog = new HttpDialog(mActivity);

    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new TaskAdapter(mActivity, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
//        SlideInBottomAnimatorAdapter slideInBottomAnimatorAdapter = new SlideInBottomAnimatorAdapter(adapter, recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity), Color.parseColor("#efeff4")));
        adapter.setOnItemClickListener(this);
    }

    @Override
    public void getList(Map<String, String> parts, final int pages, int type) {
        if (adapter != null) {
            Observable<Response<List<TaskSonModel>>> observable = null;

            switch (type) {
                case 0:
                    observable = api.taskSendList(parts);

                    break;
                case 1:
                    observable = api.taskAcceptList(parts);

                    break;
                case 2:
                    observable = api.taskAll(parts);
                    break;

            }

            addSubscription(observable.compose(SchedulersCompat.applyIoSchedulers())
                            .compose(RxResultHelper.handleResult())
                    , new Subscriber<List<TaskSonModel>>() {
                        @Override
                        public void onCompleted() {
                            mModel.compelete();
                            dialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mModel.compelete();
                            dialog.dismiss();
                        }

                        @Override
                        public void onNext(List<TaskSonModel> bean) {
                            if (pages == 1) {
                                list.clear();
                            }
                            list.addAll(bean);

                            if (bean.size() <10) {
                                mModel.close();
                                mModel.noLoadMode();
                                if (pages > 1) {
                                    ToashUtils.show(mActivity, "已经是最后一页数据", 2000, Gravity.CENTER);
                                }
                            } else {
                                mModel.defalutMode();
                            }
                            adapter.notifyItemRangeChanged(0,list.size());
                        }
                    });
        }

    }

    @Override
    public void setNotify() {
        list.remove(list.get(mPosition));
        adapter.notifyItemRangeChanged(0,list.size());
    }


    @Override
    public void onItemClick(View view, int position) {
        mPosition = position;
        Bundle data = new Bundle();
        data.putInt("type", 0);
        data.putSerializable("rele_id", list.get(position).getId());
        UIHelper.startActivity(mActivity, StatusDetailActivity.class, data);

    }

}
