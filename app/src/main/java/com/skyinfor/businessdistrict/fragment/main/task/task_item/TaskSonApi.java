package com.skyinfor.businessdistrict.fragment.main.task.task_item;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.TaskSonModel;

import java.util.List;
import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface TaskSonApi {

    @GET(ApiUrl.taskSendList)
    Observable<Response<List<TaskSonModel>>> taskSendList(@QueryMap Map<String, String> param);

    @GET(ApiUrl.taskAcceptList)
    Observable<Response<List<TaskSonModel>>> taskAcceptList(@QueryMap Map<String, String> param);

    @GET(ApiUrl.taskAll)
    Observable<Response<List<TaskSonModel>>> taskAll(@QueryMap Map<String, String> param);

}
