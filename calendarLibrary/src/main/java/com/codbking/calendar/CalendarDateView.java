package com.codbking.calendar;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import static com.codbking.calendar.CalendarFactory.getMonthOfDayList;

/**
 * Created by codbking on 2016/12/18.
 * email:codbking@gmail.com
 * github:https://github.com/codbking
 * blog:http://www.jianshu.com/users/49d47538a2dd/latest_articles
 */

public class CalendarDateView extends ViewPager implements CalendarTopView {

    HashMap<Integer, CalendarView> views = new HashMap<>();
    private CaledarTopViewChangeListener     mCaledarLayoutChangeListener;
    private CalendarView.OnItemClickListener onItemClickListener;
    private boolean                          defaultDay;

    private LinkedList<CalendarView> cache = new LinkedList();

    public int MAXCOUNT    = Integer.MAX_VALUE;
    public int SELECT_ITEM = Integer.MAX_VALUE / 2;
    public int MOMTH       = CalendarUtil.getYMD(new Date())[1];
    public int YEAR        = CalendarUtil.getYMD(new Date())[0];
    public boolean isToday;

    private int row = 6;

    private CaledarAdapter mAdapter;
    private int calendarItemHeight = 0;

    public void setAdapter(CaledarAdapter adapter) {
        mAdapter = adapter;
        initData();
    }

    public void setAdapter(CaledarAdapter adapter, boolean defaultDay) {
        mAdapter = adapter;
        this.defaultDay = defaultDay;
        initData();
    }

    public void setAdapter(CaledarAdapter adapter, boolean defaultDay, int selectItem) {
        mAdapter = adapter;
        this.defaultDay = defaultDay;

        initData(selectItem);
    }

    public void setAdapter(CaledarAdapter adapter, int MAXCOUNT, int SELECT_ITEM, int YEAR, int MOMTH, boolean isToday) {

        this.MAXCOUNT = MAXCOUNT;
        this.MOMTH = MOMTH;
        this.YEAR = YEAR;
        this.SELECT_ITEM = SELECT_ITEM;
        this.isToday = isToday;
        mAdapter = adapter;
        initData();
    }

    public void setOnItemClickListener(CalendarView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public CalendarDateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CalendarDateView);
        row = a.getInteger(R.styleable.CalendarDateView_cbd_calendar_row, 6);
        a.recycle();

        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int calendarHeight = 0;
        if (getAdapter() != null) {
            CalendarView view = (CalendarView) getChildAt(0);
            if (view != null) {
                calendarHeight = view.getMeasuredHeight();
                calendarItemHeight = view.getItemHeight();
            }
        }
        setMeasuredDimension(widthMeasureSpec, MeasureSpec.makeMeasureSpec(calendarHeight, MeasureSpec.EXACTLY));
    }

    private void init() {
        setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return MAXCOUNT;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int position) {

                CalendarView view;

                if (!cache.isEmpty()) {
                    view = cache.removeFirst();
                } else {
                    view = new CalendarView(container.getContext(), row);
                }

                view.setOnItemClickListener(onItemClickListener);
                view.setAdapter(mAdapter);

                view.setData(getMonthOfDayList(YEAR, MOMTH + position - SELECT_ITEM), position == SELECT_ITEM, defaultDay);
                container.addView(view);
                views.put(position, view);

                return view;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
                cache.addLast((CalendarView) object);
                views.remove(position);
            }
        });

        addOnPageChangeListener(new SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if (onItemClickListener != null) {
                    try {
                        CalendarView view = views.get(position);
                        if (view != null) {
                            Object[] obs = view.getSelect();
                            CalendarBean ob = (CalendarBean) obs[2];
                            Log.e("000", ob.year + "-" + ob.moth + "-" + ob.day);

                            if (!defaultDay) {
                                onItemClickListener.onItemClick((View) obs[0], (int) obs[1], (CalendarBean) obs[2], false);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                mCaledarLayoutChangeListener.onLayoutChange(CalendarDateView.this);
            }
        });
    }


    private void initData() {
        setCurrentItem(SELECT_ITEM, false);
        getAdapter().notifyDataSetChanged();

    }

    private void initData(int select) {
        setCurrentItem(select, false);
        getAdapter().notifyDataSetChanged();

    }

    @Override
    public int[] getCurrentSelectPositon() {
        CalendarView view = views.get(getCurrentItem());
        if (view == null) {
            view = (CalendarView) getChildAt(0);
        }
        if (view != null) {
            return view.getSelectPostion();
        }
        return new int[4];
    }

    @Override
    public int getItemHeight() {
        return calendarItemHeight;
    }

    @Override
    public void setCaledarTopViewChangeListener(CaledarTopViewChangeListener listener) {
        mCaledarLayoutChangeListener = listener;
    }


}
