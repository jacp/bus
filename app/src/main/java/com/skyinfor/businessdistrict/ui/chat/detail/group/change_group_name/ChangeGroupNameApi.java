package com.skyinfor.businessdistrict.ui.chat.detail.group.change_group_name;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface ChangeGroupNameApi {

    @FormUrlEncoded
    @POST(ApiUrl.groupNameModify)
    Observable<Response<Object>>groupNameModify(@Field("group_uuid")String group_uuid
            , @Field("group_name") String group_name );


}
