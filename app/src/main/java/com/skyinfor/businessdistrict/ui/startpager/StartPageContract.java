package com.skyinfor.businessdistrict.ui.startpager;


import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

/**
 * Created by min on 2017/4/15.
 */

public interface StartPageContract {
    interface Model extends MModel {
    }

    interface Presenter extends IPresenter<Model> {

    }
}
