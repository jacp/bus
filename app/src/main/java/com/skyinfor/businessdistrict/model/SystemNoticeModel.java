package com.skyinfor.businessdistrict.model;

public class SystemNoticeModel {


    /**
     * id : 7017
     * user_id : 101
     * notice_name : 弄
     * notice_type : task
     * rele_id : 181
     * related_user : 舒文强
     * notice_status : 已拒绝
     * create_time : 2018-11-08 09:20:39
     * is_read : 1
     */

    private int id;
    private int user_id;
    private String notice_name;
    private String notice_type;
    private int rele_id;
    private String related_user;
    private String notice_status;
    private int notice_code;
    private String create_time;
    private int is_read;

    public int getNotice_code() {
        return notice_code;
    }

    public void setNotice_code(int notice_code) {
        this.notice_code = notice_code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getNotice_name() {
        return notice_name;
    }

    public void setNotice_name(String notice_name) {
        this.notice_name = notice_name;
    }

    public String getNotice_type() {
        return notice_type;
    }

    public void setNotice_type(String notice_type) {
        this.notice_type = notice_type;
    }

    public int getRele_id() {
        return rele_id;
    }

    public void setRele_id(int rele_id) {
        this.rele_id = rele_id;
    }

    public String getRelated_user() {
        return related_user;
    }

    public void setRelated_user(String related_user) {
        this.related_user = related_user;
    }

    public String getNotice_status() {
        return notice_status;
    }

    public void setNotice_status(String notice_status) {
        this.notice_status = notice_status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public int getIs_read() {
        return is_read;
    }

    public void setIs_read(int is_read) {
        this.is_read = is_read;
    }
}
