package com.skyinfor.businessdistrict.ui.chat.detail.group.member;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

public class GroupMemberDecContract {

    interface Model extends MModel {

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void setNotify(List<GroupMemberModel.UserListBean> userListBeanList);
    }
}
