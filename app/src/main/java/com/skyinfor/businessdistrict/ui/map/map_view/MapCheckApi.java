package com.skyinfor.businessdistrict.ui.map.map_view;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.MapCheckModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface MapCheckApi {

    @GET(ApiUrl.deviceList)
    Observable<Response<List<MapCheckModel>>>deviceList();

}
