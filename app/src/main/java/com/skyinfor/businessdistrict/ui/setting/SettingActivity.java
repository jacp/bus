package com.skyinfor.businessdistrict.ui.setting;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.dialog.DialogView;
import com.skyinfor.businessdistrict.eventbus.ClearCacheEvent;
import com.skyinfor.businessdistrict.listener.OnMessageDialogEvent;
import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.ui.setting.modify_pass.ModifyPassActivity;
import com.skyinfor.businessdistrict.util.DataCleanManager;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.skyinfor.businessdistrict.util.Utils.getLocalVersionName;

public class SettingActivity extends BaseActivity<SettingPresent> implements SettingContract.Model {
    @BindView(R.id.text_setting_pass)
    TextView textSettingPass;
    @BindView(R.id.text_setting_clear_cache)
    TextView textSettingClearCache;
    @BindView(R.id.text_setting_version)
    TextView textSettingVersion;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private DataCleanManager manager;
    private HttpDialog dialog;
    private UserInfoModel model;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);

        model = (UserInfoModel) getIntent().getSerializableExtra("model");

        tvTitle.setText("设置");
        textSettingVersion.setText(getLocalVersionName(mContext));
        dialog = new HttpDialog(mContext);

        manager = new DataCleanManager();
        try {
            textSettingClearCache.setText(manager.getTotalCacheSize(mContext));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.rel_setting_pass, R.id.rel_setting_clear_cache,
            R.id.rel_setting_version, R.id.text_setting_exit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rel_setting_pass:
                Bundle data = new Bundle();
                data.putSerializable("model", model);
                UIHelper.startActivity(mContext, ModifyPassActivity.class, data);

                break;
            case R.id.rel_setting_clear_cache:
                clearAction("清除缓存");

                break;
            case R.id.rel_setting_version:

                break;
            case R.id.text_setting_exit:
                Constants.logout(mContext);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void clearCompele(ClearCacheEvent event) {
        if (dialog != null) {
            dialog.dismiss();
            textSettingClearCache.setText("0B");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    private void clearAction(String str) {
        DialogView dialog = new DialogView(mContext, "",
                String.format("是否确认%s?", str), "确认", "取消");
        dialog.show();
        dialog.setDialogEvent(new OnMessageDialogEvent() {
            @Override
            public void ok(Dialog dialog) {
                dialog.show();
                manager.clearAllCache(mContext);
            }

            @Override
            public void close(Dialog dialog) {
                dialog.dismiss();
            }
        });
    }

}
