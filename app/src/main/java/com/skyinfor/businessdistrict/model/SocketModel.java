package com.skyinfor.businessdistrict.model;

public class SocketModel {


    /**
     * type : connect_socket
     * data : {"client_id":"7f0000010b550000021b"}
     */

    private String type;
    private DataBean data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * client_id : 7f0000010b550000021b
         */

        private String client_id;
        private String status_code;
        private String group_uuid;
        private String send_id;
        private String receive_id;
        private String send_name;
        private String send_img;
        private String avatar;
        private String id="0";
        private String nickname;
        private String type_id;
        private String groupName;


        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getSend_name() {
            return send_name;
        }

        public void setSend_name(String send_name) {
            this.send_name = send_name;
        }

        public String getSend_img() {
            return send_img;
        }

        public void setSend_img(String send_img) {
            this.send_img = send_img;
        }

        public String getSend_id() {
            return send_id;
        }

        public void setSend_id(String send_id) {
            this.send_id = send_id;
        }

        public String getReceive_id() {
            return receive_id;
        }

        public void setReceive_id(String receive_id) {
            this.receive_id = receive_id;
        }


        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }


        public String getGroup_uuid() {
            return group_uuid;
        }

        public void setGroup_uuid(String group_uuid) {
            this.group_uuid = group_uuid;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getStatus_code() {
            return status_code;
        }

        public void setStatus_code(String status_code) {
            this.status_code = status_code;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }
    }
}
