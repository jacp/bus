package com.skyinfor.businessdistrict.ui.map;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.util.GCJ2WGS;
import com.skyinfor.businessdistrict.util.ToashUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapChooseActivity extends BaseActivity<MapChoosePresent> implements AMap.OnMapClickListener, LocationSource, AMapLocationListener, GeocodeSearch.OnGeocodeSearchListener, AMap.OnCameraChangeListener, AMap.OnMarkerClickListener, AMap.OnMarkerDragListener {


    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    @BindView(R.id.map_choose)
    MapView mMapView;
    @BindView(R.id.text_map_current_content)
    TextView textMapCurrentContent;
    @BindView(R.id.rel_map_location)
    View locationView;

    private AMapLocationClient mLocationClient;
    private AMapLocationClientOption mLocationOption;
    private boolean isFirstLoc = true;
    private OnLocationChangedListener mListener;
    private GeocodeSearch geoSearch;
    private double longitude;
    private double latitude;
    //    private MarkerOptions markerOption;
    private AMap mAMap;
    private AMapLocationClient mlocationClient;
    private BitmapDescriptor ICON_YELLOW = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
    private BitmapDescriptor ICON_RED = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
    // 中心点坐标
    private LatLng centerLatLng = null;
    // 中心点marker
    private Marker centerMarker;
    private List<Marker> markerList = new ArrayList<>();
    private GeocodeSearch geocoderSearch;
    private String strLag;
    private String address;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_map_choose;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mMapView.onCreate(savedInstanceState);
        textRightEditText.setVisibility(View.VISIBLE);
        textRightEditText.setText("完成");
        int type = getIntent().getIntExtra("type", 0);
//        tvTitle.setText(type == 0 ? "情况地址" : "任务地址");
        tvTitle.setText("地址");

        strLag = getIntent().getStringExtra("strLag");
        address = getIntent().getStringExtra("address");

//        markerOption = new MarkerOptions().draggable(true);
        if (TextUtils.isEmpty(strLag)) {
            if (mAMap == null) {
                mAMap = mMapView.getMap();
                mAMap.getUiSettings().setRotateGesturesEnabled(false);
                setUpMap();
            }
        } else {
            locationView.setVisibility(View.GONE);
            textRightEditText.setVisibility(View.GONE);
            setUpMap(mMapView);
            textMapCurrentContent.setText(address);
        }
    }

    /**
     * 设置一些amap的属性
     */
    private void setUpMap() {
//        mAMap.setOnMapClickListener(this);
        mAMap.setLocationSource(this);// 设置定位监听
        mAMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
        mAMap.getUiSettings().setZoomControlsEnabled(false);//放大缩小按钮是否显示
        // 自定义系统定位蓝点
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        // 自定义定位蓝点图标
        myLocationStyle.myLocationIcon(
                BitmapDescriptorFactory.fromResource(R.mipmap.icon_location));
        // 自定义精度范围的圆形边框颜色
        myLocationStyle.strokeColor(Color.argb(0, 0, 0, 0));
        // 自定义精度范围的圆形边框宽度
        myLocationStyle.strokeWidth(0);
        // 设置圆形的填充颜色
        myLocationStyle.radiusFillColor(Color.argb(0, 0, 0, 0));
        // 将自定义的 myLocationStyle 对象添加到地图上
//        mAMap.setMyLocationStyle(myLocationStyle);
        mAMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        mAMap.setMyLocationType(AMap.LOCATION_TYPE_MAP_FOLLOW );
        mAMap.setOnCameraChangeListener(this);
        //逆地址解析
        geocoderSearch = new GeocodeSearch(getApplicationContext());
        geocoderSearch.setOnGeocodeSearchListener(this);
//        mAMap.setOnMarkerClickListener(this);//map点击事件
//        mAMap.setOnMarkerDragListener(this);
    }

    /**
     * 设置一些amap的属性
     */
    private void setUpMap(MapView mMapView) {
        mAMap = mMapView.getMap();
        mAMap.getUiSettings().setRotateGesturesEnabled(false);
        mAMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
        mAMap.getUiSettings().setZoomControlsEnabled(false);//放大缩小按钮是否显示
        mAMap.setMyLocationEnabled(false);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
//        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        mAMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
        mAMap.moveCamera(CameraUpdateFactory.zoomTo(17));
        mAMap.setOnMapClickListener(this);
        String location[] = strLag.split(",");

        mAMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(Double.valueOf(location[1]), Double.valueOf(location[0]))));
        MarkerOptions markerOption = new MarkerOptions();
        LatLng x = new LatLng(Double.valueOf(location[1]), Double.valueOf(location[0]));
        markerOption.position(x);
        markerOption.title(address);
        markerOption.perspective(false);
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_location));//设置图标
        mAMap.addMarker(markerOption);
    }


    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        String address = textMapCurrentContent.getText().toString().trim();

        if (latitude != 0) {
            GCJ2WGS wg = new GCJ2WGS();
            double[] b = wg.gcj02_To_Gps84(latitude, longitude);
            Intent intent = new Intent();
            Bundle data = new Bundle();

            data.putString("position", +b[1] + "," + b[0]);
            data.putString("address", address);
            intent.putExtras(data);
            setResult(101, intent);
            appManager.finishActivity();
        } else {
            ToashUtils.show(mContext, "地址选择错误，请重新定位");
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
        deactivate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }
        //mLocationClient.stopLocation();//停止定位
        if (mLocationClient != null)
            mLocationClient.onDestroy();//销毁定位客户端。
        //销毁定位客户端之后，若要重新开启定位请重新New一个AMapLocationClient对象。

    }


    @Override
    public void onMapClick(LatLng latLng) {
//        markerOption.icon(ICON_YELLOW);
//        centerLatLng = latLng;
//        addCenterMarker(centerLatLng);
//        longitude = centerLatLng.longitude;
//        latitude = centerLatLng.latitude;
//        ToashUtils.show(mContext,"经纬度"+longitude+"==="+latitude);
//        MyLog.e("","经纬度"+longitude+"==="+latitude);
//        // 第一个参数表示一个Latlng(经纬度)，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
//        RegeocodeQuery query = new RegeocodeQuery(new LatLonPoint(latitude, longitude), 25, GeocodeSearch.AMAP);
//        geocoderSearch.getFromLocationAsyn(query);
    }


    /**
     * 激活定位
     */
    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
        initLocation();
    }

    /**
     * 停止定位
     */
    @Override
    public void deactivate() {
        mListener = null;
        if (mlocationClient != null) {
            mlocationClient.stopLocation();
            mlocationClient.onDestroy();
        }
        mlocationClient = null;
    }

//    private void addCenterMarker(LatLng latlng) {
//        if (null == centerMarker) {
//            centerMarker = mAMap.addMarker(markerOption);
//        }
//        centerMarker.setPosition(latlng);
//
//        markerList.add(centerMarker);
//
//    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {
                //定位成功回调信息，设置相关消息
                aMapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见官方定位类型表
                latitude = aMapLocation.getLatitude();//获取纬度
                longitude = aMapLocation.getLongitude();//获取经度
                aMapLocation.getAccuracy();//获取精度信息
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date(aMapLocation.getTime());
                df.format(date);//定位时间
                aMapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                aMapLocation.getCountry();//国家信息
                aMapLocation.getProvince();//省信息
                aMapLocation.getCity();//城市信息
                aMapLocation.getDistrict();//城区信息
                aMapLocation.getStreet();//街道信息
                aMapLocation.getStreetNum();//街道门牌号信息
                aMapLocation.getCityCode();//城市编码
                aMapLocation.getAdCode();//地区编码

                // 如果不设置标志位，此时再拖动地图时，它会不断将地图移动到当前的位置

                if (isFirstLoc) {
                    //设置缩放级别
                    mAMap.moveCamera(CameraUpdateFactory.zoomTo(17));
                    //将地图移动到定位点
                    mAMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude())));
                    //点击定位按钮 能够将地图的中心移动到定位点
                    mListener.onLocationChanged(aMapLocation);
                    //获取定位信息
                    /*StringBuffer buffer = new StringBuffer();
                    buffer.append(aMapLocation.getCountry() + ""
                            + aMapLocation.getProvince() + ""
                            + aMapLocation.getCity() + ""
                            + aMapLocation.getProvince() + ""
                            + aMapLocation.getDistrict() + ""
                            + aMapLocation.getStreet() + ""
                            + aMapLocation.getStreetNum());
                    Toast.makeText(getApplicationContext(), buffer.toString(), Toast.LENGTH_LONG).show();*/
                    StringBuffer buffer = new StringBuffer();
                    buffer.append(
                            aMapLocation.getProvince() + ""
                                    + aMapLocation.getDistrict() + ""
                                    + aMapLocation.getStreet() + ""
                                    + aMapLocation.getStreetNum());

                    textMapCurrentContent.setText(buffer.toString());
                    isFirstLoc = false;

//                    mAMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(latitude,longitude)));
//                    MarkerOptions markerOption = new MarkerOptions();
//                    LatLng x = new LatLng(latitude, longitude);
//                    markerOption.draggable(true);
//                    markerOption.position(x);
//                    markerOption.title(address);
//                    markerOption.perspective(false);
//                    markerOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_location));//设置图标
//                    mAMap.addMarker(markerOption);
//                    mMapView.invalidate();
                }
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                Log.e("AmapError", "location Error, ErrCode:"
                        + aMapLocation.getErrorCode() + ", errInfo:"
                        + aMapLocation.getErrorInfo());
                Toast.makeText(getApplicationContext(), "定位失败", Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int rCode) {
        String formatAddress = regeocodeResult.getRegeocodeAddress().getFormatAddress();
        Log.e("formatAddress", "formatAddress:" + formatAddress);
        Log.e("formatAddress", "rCode:" + rCode);
        textMapCurrentContent.setText(formatAddress);
    }

    @Override
    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onCameraChangeFinish(CameraPosition cameraPosition) {
        LatLng target = cameraPosition.target;
        geo(target);
        longitude = target.longitude;
        latitude = target.latitude;
    }

    /**
     * 先要执行逆地理编码的搜索
     */
    public void geo(LatLng latlng) {
        // 第一个参数表示一个Latlng(经纬度)，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
        RegeocodeQuery query = new RegeocodeQuery(new LatLonPoint(latlng.latitude, latlng.longitude), 200, GeocodeSearch.AMAP);
        geoSearch.getFromLocationAsyn(query);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        String number = marker.getId().substring(6);
        int posions = 0;
        if (!number.equals("") && number != null) {
            posions = Integer.parseInt(number) - 2;
        }

        Log.e("position", posions + "");

        return true;
    }

    /**
     * 定位获取当前位置
     */
    private void initLocation() {
        if (mlocationClient == null) {
            mlocationClient = new AMapLocationClient(this);
            mLocationOption = new AMapLocationClientOption();
            // 设置定位监听
            mlocationClient.setLocationListener(this);
            // 设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            // 只是为了获取当前位置，所以设置为单次定位
            mLocationOption.setOnceLocation(true);
            // 设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            mlocationClient.startLocation();
        } else {
            mlocationClient.startLocation();
        }
    }


    @OnClick(R.id.rel_map_location)
    public void onLocationClicked() {
        isFirstLoc = true;
        initLocation();
    }


    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        LatLng latLng = marker.getPosition();
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;
        Log.e("latitude", latitude + "");
        Log.e("longitude", longitude + "");
        getAddress(latLng);

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

    /**
     * 根据经纬度得到地址
     */
    public void getAddress(final LatLng latLonPoint) {
        // 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
        RegeocodeQuery query = new RegeocodeQuery(new LatLonPoint(latLonPoint.latitude,latLonPoint.longitude), 50, GeocodeSearch.AMAP);
        geocoderSearch.getFromLocationAsyn(query);// 设置同步逆地理编码请求

    }

}
