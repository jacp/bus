package com.skyinfor.businessdistrict.ui.login;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.UserBean;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/22.
 */

public interface LoginApi {

    @FormUrlEncoded
    @POST(ApiUrl.Logoin)
    Observable<Response<Object>>login(@Field("name") String name
            , @Field("password") String password);


}
