package com.skyinfor.businessdistrict.eventbus;

import java.io.Serializable;

/**
 * 通用的消息基类
 */

public class Event_Base<T extends Event_Base> implements Serializable {
    public Object obj;

    public int what = -1;

    public EType action = EType.Defalut;

    public T Action(EType action) {
        this.action = action;
        return (T) this;
    }

    public T What(int what) {
        this.what = what;
        return (T) this;
    }

    public T Obj(Object obj) {
        this.obj = obj;
        return (T) this;
    }

    /**
     * 判断obj是否为空
     *
     * @return
     */
    public boolean objIsNotNull() {
        return obj != null;
    }

    /**
     * 判断obj是否允许转换为某个class
     * 也可作为判断obj的对象内容是否为某个class的子类或本身
     *
     * @param cls
     * @return
     */
    public boolean objIsShiftClass(Class cls) {
        if (objIsNotNull()) {
            Class objClass = obj.getClass();
            return objClass.isAssignableFrom(cls);
        }
        return false;
    }

    /**
     * 通用的Action枚举
     */
    public enum EType {
        Refresh,
        Del,
        Add,
        LoactionSuccess,// 获取地址成功
        LocationFailure,// 获取地址失败
        Remove,
        Defalut,
        Login,
        RefreshLog,// 更新日志
        RefrshAttendance,  //更新考勤显示
        RefrshDateLast,  // 上一天
        RefrshDateNext,  //下一天
        UPDATE_GROUP_MEMBER//更新群成员列表
        , UPDATE_WORK_LIST//更新工作列表
        , UPDATE_PLAN_LIST, UPDATE_API
        ,UPDATE_TOOL_NUM,UPDATE_TOOL_LIST
    }
}
