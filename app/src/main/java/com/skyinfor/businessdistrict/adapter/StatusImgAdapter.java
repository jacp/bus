package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class StatusImgAdapter extends BaseMyRecyclerVIewAdapter<String> {
    private ArrayList<ImageItem> listItem = new ArrayList<>();

    public StatusImgAdapter(Context context, List<String> list) {
        super(context, list, R.layout.list_item_image);
    }

    @Override
    protected void covert(final String mode, BaseViewHolder holder, final int position) {
        super.covert(mode, holder, position);
        ImageView img = holder.getView(R.id.iv_img, position);
        GlideUtils.loadImg(mContext,mode, img);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listItem.clear();
                ImageItem item = new ImageItem();
                item.path = mode;
                listItem.add(item);

                //打开预览
                Intent intentPreview = new Intent(mContext, ImagePreviewDelActivity.class);
                intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, listItem);
                intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, position);
                intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
                intentPreview.putExtra(ImagePicker.DELETE_IS_SHOW, true);
                mContext.startActivity(intentPreview);
            }
        });


    }
}
