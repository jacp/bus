package com.skyinfor.businessdistrict.model;

import java.io.Serializable;

public class PersonListBean implements Serializable{

    /**
     * id : 23
     * nickname : 系统演示
     * phone : 13525698742
     * job : 演示
     * avatar : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png
     * theone : a1faf36b-c096-f81b-9176-3aaaa2dc0bc2
     */

    private int id;
    private String nickname;
    private String phone;
    private String job;
    private String avatar;
    private String theone;
    private boolean check;

    public PersonListBean() {
    }

    public PersonListBean(int id) {
        this.id = id;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTheone() {
        return theone;
    }

    public void setTheone(String theone) {
        this.theone = theone;
    }

}
