package com.skyinfor.businessdistrict.model;

import java.io.Serializable;
import java.util.List;

public class PlanListModel implements Serializable{


    /**
     * id : 58
     * create_time : 2018-11-29 11:23:22
     * update_time : 2018-11-29 11:23:22
     * name : 迪丽热巴
     * details : null
     * created_user_id : 95
     * plan_condition : [{"id":66,"create_time":"2018-11-29 11:23:07","update_time":"2018-11-29 11:23:07","plan_name":"迪丽热巴","type_id":"10","target_id":"127","name":null,"condition_name":"社会治安","relation_user":[{"id":127,"nickname":"Staff","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bff53dbd3763.png"}]}]
     * plan_response : [{"id":67,"create_time":"2018-11-29 11:23:21","update_time":"2018-11-29 11:23:21","plan_name":"迪丽热巴","response_type":"平台响应","response_action":"镜头切换","response_actor":"1","name":"迪丽热巴来了","details":null,"relation_user":[]}]
     */

    private int id;
    private String create_time;
    private String update_time;
    private String name;
    private Object details;
    private int created_user_id;
    private String address;
    private String nickname;
    private String avatar;

    private List<PlanConditionBean> plan_condition;
    private List<PlanResponseBean> plan_response;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public int getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(int created_user_id) {
        this.created_user_id = created_user_id;
    }

    public List<PlanConditionBean> getPlan_condition() {
        return plan_condition;
    }

    public void setPlan_condition(List<PlanConditionBean> plan_condition) {
        this.plan_condition = plan_condition;
    }

    public List<PlanResponseBean> getPlan_response() {
        return plan_response;
    }

    public void setPlan_response(List<PlanResponseBean> plan_response) {
        this.plan_response = plan_response;
    }

    public static class PlanConditionBean implements Serializable{
        /**
         * id : 66
         * create_time : 2018-11-29 11:23:07
         * update_time : 2018-11-29 11:23:07
         * plan_name : 迪丽热巴
         * type_id : 10
         * target_id : 127
         * name : null
         * condition_name : 社会治安
         * relation_user : [{"id":127,"nickname":"Staff","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bff53dbd3763.png"}]
         */

        private int id;
        private String create_time;
        private String update_time;
        private String plan_name;
        private String type_id;
        private String target_id;
        private String name;
        private String condition_name;
        private List<PlanRelatedModel> relation_user;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }

        public String getPlan_name() {
            return plan_name;
        }

        public void setPlan_name(String plan_name) {
            this.plan_name = plan_name;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getTarget_id() {
            return target_id;
        }

        public void setTarget_id(String target_id) {
            this.target_id = target_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCondition_name() {
            return condition_name;
        }

        public void setCondition_name(String condition_name) {
            this.condition_name = condition_name;
        }

        public List<PlanRelatedModel> getRelation_user() {
            return relation_user;
        }

        public void setRelation_user(List<PlanRelatedModel> relation_user) {
            this.relation_user = relation_user;
        }


    }

    public static class PlanResponseBean implements Serializable{
        /**
         * id : 67
         * create_time : 2018-11-29 11:23:21
         * update_time : 2018-11-29 11:23:21
         * plan_name : 迪丽热巴
         * response_type : 平台响应
         * response_action : 镜头切换
         * response_actor : 1
         * name : 迪丽热巴来了
         * details : null
         * relation_user : []
         */

        private int id;
        private String create_time;
        private String update_time;
        private String plan_name;
        private String response_type;
        private String response_action;
        private String response_actor;
        private String name;
        private String details;
        private List<PlanRelatedModel> relation_user;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }

        public String getPlan_name() {
            return plan_name;
        }

        public void setPlan_name(String plan_name) {
            this.plan_name = plan_name;
        }

        public String getResponse_type() {
            return response_type;
        }

        public void setResponse_type(String response_type) {
            this.response_type = response_type;
        }

        public String getResponse_action() {
            return response_action;
        }

        public void setResponse_action(String response_action) {
            this.response_action = response_action;
        }

        public String getResponse_actor() {
            return response_actor;
        }

        public void setResponse_actor(String response_actor) {
            this.response_actor = response_actor;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }

        public List<PlanRelatedModel> getRelation_user() {
            return relation_user;
        }

        public void setRelation_user(List<PlanRelatedModel> relation_user) {
            this.relation_user = relation_user;
        }
    }
}
