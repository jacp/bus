package com.skyinfor.businessdistrict.base;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.listener.RItemClickListener;
import com.skyinfor.businessdistrict.listener.RItemRemoveListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by min
 * on 2016/4/21.
 */
public abstract class BaseRecyclrAdapter<T> extends RecyclerView.Adapter<BaseRecyclrAdapter.SparseArrayViewHolder> implements View.OnClickListener {
    private  List<T> list;
    protected int itemLayoutId;
    protected  Context mContext;
    private  LayoutInflater _mLayoutInflater;
    public static final  int TYPE_HEADER  = 0;
    public static final  int TYPE_NORMAL  = 1;
    private static final int TYPE_HEARDER = 2;
    private View mHeaderView;

    public BaseRecyclrAdapter(int itemLayoutId, List<T> list, Context context) {
        this.itemLayoutId = itemLayoutId;
        this.list = list;
        if (list == null) {
            this.list = new ArrayList<>();
        } else {
            this.list = list;
        }
        mContext = context;
        _mLayoutInflater = LayoutInflater.from(context);
    }

    public BaseRecyclrAdapter setList(List<T> list) {
        this.list = list;
        notifyDataSetChanged();
        return this;
    }

    /**
     * 设置头部布局
     * @param headerView
     */
    public void setHeaderView(View headerView) {
        mHeaderView = headerView;
        notifyItemInserted(0);
    }

    public boolean existHeadView() {
        return mHeaderView != null;
    }

    @Override
    public BaseRecyclrAdapter.SparseArrayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater.inflate(itemLayoutId, parent, false);
        itemView.setOnClickListener(this);

        if (mHeaderView != null && viewType == TYPE_HEADER)
            return new SparseArrayViewHolder(mHeaderView);
        //        View layout = LayoutInflater.from(parent.getContext()).inflate(itemLayoutId, parent, false);
        return new SparseArrayViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BaseRecyclrAdapter.SparseArrayViewHolder holder, int position) {
        int cIndex = position;
        boolean isHeadView = getItemViewType(position) == TYPE_HEADER;
        if (isHeadView)
            return;
        if (mHeaderView != null) {
            cIndex--;
        }
        holder.itemView.setTag(R.id.RItemPostion, cIndex);
        convert(holder, getItem(cIndex), cIndex);//position -
    }

    public List<T> list() {
        return list;
    }

    public T getItem(int position) {
        return list.get(position);
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (mHeaderView == null)
            return TYPE_NORMAL;
        if (position == 0)
            return TYPE_HEADER;
        return TYPE_NORMAL;
    }

    /**
     * @param holder
     * @param item
     * @param position
     */
    public abstract void convert(BaseRecyclrAdapter.SparseArrayViewHolder holder, T item, int position);

    @Override
    public int getItemCount() {

        return list.size() + (mHeaderView != null ? 1 : 0);
    }

    //  点击事件
    protected RItemClickListener mRitemClick;
    // 移除
    protected RItemRemoveListener rItemRemoveListener;


    public RItemClickListener getRitemClick() {
        return mRitemClick;
    }

    public void setRitemClick(RItemClickListener RitemClick) {
        this.mRitemClick = RitemClick;
    }

    public RItemRemoveListener getRitemRemove() {
        return rItemRemoveListener;
    }

    public void setRitemRemove(RItemRemoveListener ritemRemove) {
        this.rItemRemoveListener = ritemRemove;
    }

    @Override
    public void onClick(View v) {
        Object obj = v.getTag(R.id.RItemPostion);
        if (obj != null && obj instanceof Integer) {
            if (mRitemClick != null) {
                mRitemClick.onRItemClick(this, v, (Integer) obj);
            }
        }
    }


    public static class SparseArrayViewHolder extends RecyclerView.ViewHolder {
        private final SparseArray<View> views;

        public SparseArrayViewHolder(View itemView) {
            super(itemView);
            views = new SparseArray<View>();
        }

        public <T extends View> T getView(int id) {
            View view = views.get(id);
            if (view == null) {
                view = itemView.findViewById(id);
                views.put(id, view);
            }
            return (T) view;
        }

        public SparseArrayViewHolder setText(int viewId, String value) {
            TextView view = getView(viewId);
            view.setText(value);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setTextColor(int viewId, int textColor) {
            TextView view = getView(viewId);
            view.setTextColor(textColor);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setImageResource(int viewId, int imageResId) {
            ImageView view = getView(viewId);
            view.setImageResource(imageResId);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setBackgroundColor(int viewId, int color) {
            View view = getView(viewId);
            view.setBackgroundColor(color);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setBackgroundResource(int viewId, int backgroundRes) {
            View view = getView(viewId);
            view.setBackgroundResource(backgroundRes);
            return SparseArrayViewHolder.this;
        }


        public SparseArrayViewHolder setVisible(int viewId, boolean visible) {
            View view = getView(viewId);
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
            return SparseArrayViewHolder.this;
        }


        public SparseArrayViewHolder setOnClickListener(int viewId, View.OnClickListener listener) {
            View view = getView(viewId);
            view.setOnClickListener(listener);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setOnTouchListener(int viewId, View.OnTouchListener listener) {
            View view = getView(viewId);
            view.setOnTouchListener(listener);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setOnLongClickListener(int viewId, View.OnLongClickListener listener) {
            View view = getView(viewId);
            view.setOnLongClickListener(listener);
            return SparseArrayViewHolder.this;
        }

        public SparseArrayViewHolder setTag(int viewId, Object tag) {
            View view = getView(viewId);
            view.setTag(tag);
            return SparseArrayViewHolder.this;
        }

    }

    /**
     * GiveParentTouchRecyclerView 移动到当前位置，
     * @param manager
     *         设置RecyclerView对应的manager
     * @param n
     *         要跳转的位置
     */
    public static void MoveToPosition(LinearLayoutManager manager, int n) {
        manager.scrollToPositionWithOffset(n, 0);
        manager.setStackFromEnd(true);
    }
}