package com.skyinfor.businessdistrict.ui.chat.select_person;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.GroupMemberModel;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectPersonActivity extends BaseActivity<SelectPersonPresent> implements SelectPersonContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    @BindView(R.id.rec_select_title)
    RecyclerView recSelectTitle;
    @BindView(R.id.rec_select_person)
    RecyclerView recSelectPerson;
    private List<GroupMemberModel.UserListBean> list;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_person;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        tvTitle.setText("选择人员");
        textRightEditText.setText("完成");
        String uuId=getIntent().getStringExtra("uuid");
        textRightEditText.setVisibility(View.VISIBLE);
        mPresenter.initAdapter(recSelectTitle, recSelectPerson);
        mPresenter.getGroupInfo(uuId);
    }

    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        if (list != null && list.size() > 0) {
            Intent intent = new Intent();
            Bundle data = new Bundle();
            data.putSerializable("data", (Serializable) list);
            intent.putExtras(data);
            setResult(101, intent);
            appManager.finishActivity();
        }
    }

    @Override
    public void setSelectList(List<GroupMemberModel.UserListBean> personListBeanList) {
        list = personListBeanList;
    }
}
