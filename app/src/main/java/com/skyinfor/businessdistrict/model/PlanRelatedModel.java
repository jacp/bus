package com.skyinfor.businessdistrict.model;

import java.io.Serializable;

public class PlanRelatedModel implements Serializable{
    /**
     * id : 127
     * nickname : Staff
     * avatar : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bff53dbd3763.png
     */

    private int id;
    private String nickname;
    private String avatar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
