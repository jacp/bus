package com.skyinfor.businessdistrict.fragment.main.contact;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.skyinfor.businessdistrict.adapter.ContactDepartAdapter;
import com.skyinfor.businessdistrict.adapter.ContactGroupAdapter;
import com.skyinfor.businessdistrict.adapter.ContactIconAdapter;
import com.skyinfor.businessdistrict.adapter.ContactPersonAdapter;
import com.skyinfor.businessdistrict.adapter.ContactTitleAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.model.ContactAllModel;
import com.skyinfor.businessdistrict.model.ContactGroupModel;
import com.skyinfor.businessdistrict.model.ContactPersonFullModel;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;
import com.skyinfor.businessdistrict.util.recyclerview.WrapContentLinearLayoutManager;
import com.skyinfor.businessdistrict.util.recyclerview.manager.ScrollLinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.agora.AgoraAPI;
import io.agora.AgoraAPIOnlySignal;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class ContactPresent extends BasePresenter<ContactContract.Model> implements ContactContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private ContactApi api;
    public HttpDialog dialog;
    List<ContactAllModel> list = new ArrayList<>();
    private int level = 0;
    private List<String> titleList = new ArrayList<>();
    private List<PersonListBean> iconList = new ArrayList<>();
    private List<PersonListBean> personList = new ArrayList<>();
    private List<ContactAllModel> departList = new ArrayList<>();
    private List<ContactGroupModel.GroupListBean> groupList = new ArrayList<>();
    private List<String> tmpList = new ArrayList<>();
    private ContactTitleAdapter titleAdapter;
    private ContactDepartAdapter departAdapter;
    private ContactPersonAdapter personAdapter;
    private ContactIconAdapter iconAdapter;
    private List<PersonListBean> getList;
    private boolean isShowSelf;
    private ContactGroupAdapter contactGroupAdapter;

    @Inject
    public ContactPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        api = retrofit.create(ContactApi.class);
        dialog = new HttpDialog(mActivity);
    }

    /**
     * 通讯录
     */
    @Override
    public void getContactList(List<PersonListBean> getList) {

        this.getList = getList;

        addSubscription(api.userList()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<ContactAllModel>>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<ContactAllModel> object) {
                        Log.e("sd", object + "");
                        for (int i = 0; i < object.size(); i++) {
                            ContactAllModel model = object.get(i);
                            getList(model);
                        }

                        getCurrentList(level,list.get(0).getName());

                    }
                });

    }


    /**
     * 完整的组织架构和群聊
     *
     * @param getList 对选中的list
     */
    @Override
    public void getFullList(List<PersonListBean> getList,int type) {
        this.getList = getList;

        addSubscription(api.getFullList(String.valueOf(Constants.USER_ID))
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<Object>>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<Object> object) {
                        Log.e("sfd", object + "");

                        if (object != null && object.size() > 0) {
                            Gson gson = new Gson();

                            ContactPersonFullModel contactAllModel = gson.fromJson(gson.toJson(object.get(0)), new TypeToken<ContactPersonFullModel>() {
                            }.getType());

                            ContactGroupModel groupModel = gson.fromJson(gson.toJson(object.get(1)), new TypeToken<ContactGroupModel>() {
                            }.getType());

                            if (type==0){
                                //person
                                for (int i = 0; i < contactAllModel.getList().size(); i++) {
                                    ContactAllModel model = contactAllModel.getList().get(i);
                                    getList(model);
                                }
                                getCurrentList(level,list.get(0).getName());
                            }

                            //chat

                            groupList.clear();
                            if (groupModel != null && groupModel.getList() != null && groupModel.getList().size() > 0) {
                                groupList.addAll(groupModel.getList());
                                contactGroupAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });

    }


    @Override
    public void initAdapter(RecyclerView iconRec, RecyclerView titleRec, RecyclerView personRec, RecyclerView sonRec
            , RecyclerView chatRec, final boolean isSelect, boolean isShowSelf) {
        this.isShowSelf = isShowSelf;

        iconAdapter = new ContactIconAdapter(mActivity, iconList);
        LinearLayoutManager iconLin = new LinearLayoutManager(mActivity);
        iconLin.setOrientation(LinearLayout.HORIZONTAL);
        iconRec.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.HORIZONTAL, dp2px(10, mActivity),
                Color.parseColor("#ffffff")));

        iconRec.setLayoutManager(iconLin);
        iconRec.setAdapter(iconAdapter);

        //标题
        titleAdapter = new ContactTitleAdapter(mActivity, titleList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        titleRec.setLayoutManager(linearLayoutManager);
        titleRec.setAdapter(titleAdapter);
        titleAdapter.setOnItemClickListener(this);

        //人员
        personAdapter = new ContactPersonAdapter(mActivity, personList, isSelect);
        ((SimpleItemAnimator) personRec.getItemAnimator()).setSupportsChangeAnimations(false);
        personRec.setLayoutManager( new LinearLayoutManager(mActivity));
        personRec.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        personRec.setAdapter(personAdapter);

        personAdapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isSelect) {
                    boolean check = personList.get(position).isCheck();
                    personList.get(position).setCheck(!check);
                    personAdapter.notifyItemChanged(position);
                    getIconList(position);

                } else {
                    String userName = personList.get(position).getNickname();
                    String userId = String.valueOf(personList.get(position).getId());
                    String avatar = personList.get(position).getAvatar();

                    Bundle data = new Bundle();
                    data.putString("name", userName);
                    data.putString("id", userId);
                    data.putString("avatar", avatar);
                    UIHelper.startActivity(mActivity, ChatRoomActivity.class, data);
                }
            }
        });

        //部门
        departAdapter = new ContactDepartAdapter(mActivity, departList);
        ScrollLinearLayoutManager departLin = new ScrollLinearLayoutManager(mActivity);
        sonRec.setLayoutManager(departLin);
        sonRec.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        sonRec.setAdapter(departAdapter);
        departAdapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                titleList.add(departList.get(position).getName());
                titleAdapter.setNotify(titleList);
                level += 1;
                getCurrentList(level,departList.get(position).getName());

            }
        });

        contactGroupAdapter = new ContactGroupAdapter(mActivity, groupList);
        chatRec.setLayoutManager(new LinearLayoutManager(mActivity));
        chatRec.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        chatRec.setAdapter(contactGroupAdapter);
        contactGroupAdapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String userName = groupList.get(position).getGroup_name();
                int groupId = groupList.get(position).getId();
                String groupUuid = groupList.get(position).getGroup_uuid();

                Bundle data = new Bundle();
                data.putString("name", userName);
                data.putInt("group_id", groupId);
                data.putString("group_uuid", groupUuid);
                UIHelper.startActivity(mActivity, ChatRoomActivity.class, data);
            }
        });
    }

    @Override
    public void delGroup(String uuid) {
        for (int i = 0; i < groupList.size(); i++) {
            if (uuid.equals(groupList.get(i).getGroup_uuid())){
                groupList.remove(groupList.get(i));
                break;
            }
        }
        contactGroupAdapter.notifyDataSetChanged();

    }

    /**
     * 对返回的列表数据处理成需要的list集合
     *
     * @param model 递归的数据源
     */
    private void getList(ContactAllModel model) {
        list.add(model);

        for (int i = 0; i < model.getChildren().size(); i++) {
            ContactAllModel data = model.getChildren().get(i);
            if (data.getChildren().size() > 0) {
                getList(data);
            } else {
                list.add(data);
            }
        }

    }

    /**
     * 获取level的数据
     *
     * @param level 选择的层级
     */
    private void getCurrentList(int level,String name) {
        personList.clear();
        departList.clear();
        int size=personList.size();

        for (int i = 0; i < list.size(); i++) {
            ContactAllModel model = list.get(i);
            if (model.getLevel() == level&&name.contains(model.getName())) {
                personList.addAll(model.getPerson_list());
                departList.addAll(model.getChildren());
            }
        }
        if (level <= 0) {
            titleList.add(list.get(level).getName());
            titleAdapter.setNotify(titleList);
        }

        if (getList != null) {
            for (int i = 0; i < getList.size(); i++) {
                PersonListBean bean = getList.get(i);

                for (int j = 0; j < personList.size(); j++) {
                    PersonListBean data = personList.get(j);

                    if (data.getId() == bean.getId()) {
                        data.setCheck(true);
                        getIconList(j);
                    }
                }
            }

        }
        if (!isShowSelf) {
            for (int i = 0; i < personList.size(); i++) {
                PersonListBean bean = personList.get(i);
                if (Constants.USER_ID == bean.getId()) {
                    personList.remove(personList.get(i));
                    break;
                }
            }
        }
        if (level!=0){
            personAdapter.notifyDataSetChanged();
        }else {
            personAdapter.notifyItemRangeChanged(size,personList.size());
        }
        personAdapter.notifyDataSetChanged();

        departAdapter.notifyDataSetChanged();
    }

    /**
     * title点击事件
     *
     * @param view
     * @param position
     */
    @Override
    public void onItemClick(View view, int position) {
        tmpList.clear();
        level = list.get(position).getLevel();
        getCurrentList(level,titleList.get(position));
        tmpList.addAll(titleList.subList(0, level + 1));
        titleList.clear();
        titleList.addAll(tmpList);
        titleAdapter.setNotify(titleList);

    }

    private void getIconList(int position) {
        boolean check = personList.get(position).isCheck();
        if (!check) {
            iconList.remove(personList.get(position));
        } else {
            iconList.add(personList.get(position));
        }
        iconAdapter.notifyDataSetChanged();
        mModel.setSelectList(iconList);
    }


}
