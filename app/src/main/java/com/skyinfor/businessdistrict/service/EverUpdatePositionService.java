package com.skyinfor.businessdistrict.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.skyinfor.businessdistrict.eventbus.PositionEvent;
import com.skyinfor.businessdistrict.util.GCJ2WGS;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

public class EverUpdatePositionService extends Service implements AMapLocationListener {
    private AMapLocationClient mLocationClient;
    private AMapLocationClientOption mLocationOption;
    private Timer timer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getPositioning();

        if (timer==null){
            timer = new Timer();
        }
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                if (timer==null){
                    cancel();
                    System.gc();
                }else {
                    getPositioning();
                }
            }
        };

        timer.schedule(timerTask, 0, 1800000);

        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopSelf();
        if (timer!=null){
            timer.cancel();
            timer.purge();
            timer=null;

        }
    }

    // 高德定位
    public void getPositioning() {
        //初始化定位
        mLocationClient = new AMapLocationClient(this);
        //设置定位回调监听
        mLocationClient.setLocationListener(this);
        //初始化AMapLocationClientOption对象
        mLocationOption = new AMapLocationClientOption();
        //获取一次定位结果：
        //该方法默认为false。
        mLocationOption.setOnceLocation(true);
        //获取最近3s内精度最高的一次定位结果：
        //设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
        mLocationOption.setOnceLocationLatest(true);
        //设置是否返回地址信息（默认返回地址信息）
        mLocationOption.setNeedAddress(true);
        //设置是否允许模拟位置,默认为true，允许模拟位置
        mLocationOption.setMockEnable(true);
        //给定位客户端对象设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        //设置定位模式为AMapLocationMode.Hight_Accuracy，高精度模式。
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        //启动定位
        mLocationClient.startLocation();
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation.getLatitude()==0.0){

        }else {
            GCJ2WGS wg = new GCJ2WGS();
            double[] b = wg.gcj02_To_Gps84(aMapLocation.getLatitude(), aMapLocation.getLongitude());
            String position = b[1] + "," + b[0];
            EventBus.getDefault().post(new PositionEvent(position));
        }
    }
}
