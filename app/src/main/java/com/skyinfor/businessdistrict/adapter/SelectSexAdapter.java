package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class SelectSexAdapter extends BaseMyRecyclerVIewAdapter<String> {

    public int mPosition=0;

    public SelectSexAdapter(Context context, List<String> list) {
        super(context, list, R.layout.item_select_sex);
    }

    @Override
    protected void covert(String mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        TextView box = holder.getView(R.id.checkbox_status_one);
        box.setText(mode);
        box.setSelected(position==mPosition);

    }
}
