package com.skyinfor.businessdistrict.ui.contact;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.ClearEditText;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;

import butterknife.BindView;
import butterknife.OnClick;

public class MsgSearchActivity extends BaseActivity<MsgSearchPresent>
        implements MsgSearchContract.Model, TextWatcher {
    @BindView(R.id.edit_search_task)
    ClearEditText editSearchTask;
    @BindView(R.id.rec_contact_search)
    RecyclerView recContactSearch;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_contact_search;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mPresenter.initAdapter(recContactSearch);
        editSearchTask.addTextChangedListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (!TextUtils.isEmpty(charSequence)) {
            mPresenter.getConversationList(String.valueOf(Constants.USER_ID),
                    charSequence.toString());
        } else {
            mPresenter.setNotify();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    @OnClick(R.id.text_search_task_cancel)
    public void onViewClicked() {
        appManager.finishActivity();

    }
}
