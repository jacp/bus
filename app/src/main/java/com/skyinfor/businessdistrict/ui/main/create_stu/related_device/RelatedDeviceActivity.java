package com.skyinfor.businessdistrict.ui.main.create_stu.related_device;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.ui.map.map_view.search.MapCheckSearchActivity;
import com.skyinfor.businessdistrict.util.UIHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RelatedDeviceActivity extends BaseActivity<RelatedDevicePresent> implements
        RelatedDeviceContract.Model {
    @BindView(R.id.rec_relate_device)
    RecyclerView recRelateDevice;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    @BindView(R.id.img_right)
    ImageView imgRight;
    private ArrayList<RelatedDeviceModel.ListBean> deviceList;
    private List<String> deviceIdList;
    private int type;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_related_device;
    }

    @Override
    protected void initView() {
        super.initView();
        type = getIntent().getIntExtra("type", 0);
        mPresenter.initAdapter(recRelateDevice);

        switch (type) {
            case 0:
                List<RelatedDeviceModel> list = (List<RelatedDeviceModel>) getIntent()
                        .getSerializableExtra("model");
                tvTitle.setText("设备管理");
                mPresenter.setNotify(list);

                break;
            case 1:
            case 3:
                deviceIdList = (List<String>) getIntent().getSerializableExtra("model");
                mPresenter.taskDevice(deviceIdList, type);
                tvTitle.setText(type == 1 ? "关联设备" : "设备管理");
                if (type==3){
                    imgRight.setVisibility(View.VISIBLE);
                    imgRight.setImageResource(R.mipmap.icon_task_search);
                }else{
                    textRightEditText.setVisibility(type == 1 ? View.VISIBLE : View.GONE);
                    textRightEditText.setText("完成");
                }
                break;
        }

    }


    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        if (deviceList != null && deviceList.size() > 0) {
            Intent intent = new Intent();
            Bundle data = new Bundle();
            data.putSerializable("model", deviceList);
            intent.putExtras(data);
            setResult(101, intent);
            appManager.finishActivity();
        }
    }

    @Override
    public void setResultList(ArrayList<RelatedDeviceModel.ListBean> deviceList) {
        this.deviceList = deviceList;
    }


    @OnClick(R.id.img_right)
    public void onSearchClicked() {
        Bundle data=new Bundle();
        data.putInt("type",1);
        UIHelper.startActivity(mContext,MapCheckSearchActivity.class,data);
    }
}
