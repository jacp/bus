package com.skyinfor.businessdistrict.ui.main.search_task_or_stu;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;

import java.util.List;
import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface SearchTaskAndStuApi {

    //status start
    @GET(ApiUrl.ConditionAcceptList)
    Observable<Response<List<StatusSonModel>>> acceptList(@QueryMap Map<String, String> param);

    @GET(ApiUrl.ConditionSendList)
    Observable<Response<List<StatusSonModel>>> sendList(@QueryMap Map<String, String> param);
    //status end

    //task start
    @GET(ApiUrl.taskSendList)
    Observable<Response<List<TaskSonModel>>> taskSendList(@QueryMap Map<String, String> param);

    @GET(ApiUrl.taskAcceptList)
    Observable<Response<List<TaskSonModel>>> taskAcceptList(@QueryMap Map<String, String> param);
    //task end

    @GET(ApiUrl.taskAll)
    Observable<Response<List<TaskSonModel>>> taskAll(@QueryMap Map<String, String> param);

    @GET(ApiUrl.conditionAll)
    Observable<Response<List<StatusSonModel>>> conditionAll(@QueryMap Map<String, String> param);
}
