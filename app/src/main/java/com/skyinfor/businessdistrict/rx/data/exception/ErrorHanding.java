package com.skyinfor.businessdistrict.rx.data.exception;

import android.app.Activity;
import android.content.Context;

import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.base.MyBaseActivity;
import com.skyinfor.businessdistrict.eventbus.MeaasgeEvent;
import com.skyinfor.businessdistrict.model.UserBean;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.login.LoginActivity;
import com.skyinfor.businessdistrict.ui.login.LoginApi;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.MyLog;
import com.skyinfor.businessdistrict.util.SharedHelper;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;

import org.greenrobot.eventbus.EventBus;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;


/**
 * Created by min on 2017/3/1.
 */
public class ErrorHanding {
    private static String TAG = "loginEmChat";
    private static Context mContext;

    //    static HttpDialog myHttpDialog;
    public static void handleError(Throwable throwable, Object msg, Context context) {
        throwable.printStackTrace();
        mContext = context;
        //        myHttpDialog = new HttpDialog(context);
        if (!MyApplication.ExtImpl.g().isAvailable()) {
            ToashUtils.show(context, "您已经处于无网络的异次元");
        } else if (throwable instanceof HttpException) {
            ToashUtils.show(context, ((ServerException) throwable).message);
        } else if (throwable instanceof ApiException) {
            ToashUtils.show(context, ((ApiException) throwable).message);
        } else if (throwable instanceof ServerException) {

            ServerException exception = (ServerException) throwable;
            MyLog.e("exception.code", exception.code + "");
            switch (exception.code) {
                case 99999:
                    if (msg != null) {
                        if (msg instanceof String) {
                            ToashUtils.show(context, "" + msg);
                        }
                    } else {
                        ToashUtils.show(context, "系统繁忙，请稍后重试");
                    }
                    break;

                case 11:
                case 12:
                    /**
                     * 登录超时
                     * 自动登录
                     */
                    //                    Log.d("minmin","登录超时");
//                    simulationLogin();
                    Activity activity = AppManager.getAppManager().getActivity(LoginActivity.class);
                    if (activity == null) {
                        Constants.USER_TOKEN = "";
                        UIHelper.startActivity(mContext, LoginActivity.class);
                    }
                    break;
                case 00002:
                    ToashUtils.show(context, "帐号或密码不正确");
                    break;
                case 10000:
                    ToashUtils.show(context, "获取数据成功");
                    break;
                case 3:
                    ToashUtils.show(context, "账号不存在");
                    break;
                case 4:
                    ToashUtils.show(context, "无访问权限");
                    break;
                case 10012:
                    ToashUtils.show(context, "不正确的时间范围");
                    break;
                case 10023:
                    ToashUtils.show(context, "当天日志已创建");
                    break;
                case 10024:
                    ToashUtils.show(context, "未来日志不能创建");
                    break;
                case 10028:
                    ToashUtils.show(context, "你已经确认本月考勤");
                    break;
                default:
                    if (msg != null) {
                        if (msg instanceof String) {
                            if (!"所查数据不存在".equals(msg)) {
                                ToashUtils.show(context, "" + msg);
                            }

                        }
                    } else {
                        ToashUtils.show(context, "系统繁忙，请稍后重试");
                    }
                    break;
            }
        }
    }

    /**
     * 模拟登录
     */
    private static void simulationLogin() {
        //  dialog.show();
        //        myHttpDialog.show();
        //        Log.d("minmin","simulationLogin");
        final Activity activity = AppManager.getAppManager().getCurActivity();
        if (activity != null) {
//            //            ToashUtils.show(activity, "身份校验过期，为您进行自动登录");
            if (activity != null) {
                IPresenter cache = null;

                if (activity instanceof BaseActivity) {
                    cache = (((BaseActivity) activity).p());
                } else if (activity instanceof MyBaseActivity) {
                    cache = (((MyBaseActivity) activity).p());
                }

                if (cache != null && cache instanceof BasePresenter) {
                    LoginApi loginApi = ((BasePresenter) cache).retrofit().create(LoginApi.class);
                    final String userName = SharedHelper.getString(Constants.LOGIN_NAME_KEY);
                    String passWord = SharedHelper.getString(Constants.LOGIN_PASS_WORD_KEY);

                    ((BasePresenter) cache).addSubscription(
                            loginApi.login(userName, passWord)
                                    .compose(SchedulersCompat.applyIoSchedulers())
                                    .compose(RxResultHelper.handleResult()), new Subscriber<UserBean>() {

                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {
                                    //                                Log.e("error", e.getMessage());
                                    UIHelper.startActivity(AppManager.getAppManager().getLastActivity(), LoginActivity.class);
                                    //                            ToashUtils.show(activity, "onError");
                                    //                            Log.d("minmin","onError");
                                }

                                @Override
                                public void onNext(UserBean o) {
//                                    EventBus.getDefault().post(new MeaasgeEvent().Action(Login));
//                                    EventBus.getDefault().post(new MeaasgeEvent().What(1234));
                                    UIHelper.startActivity(AppManager.getAppManager().getLastActivity(), LoginActivity.class);
                                }

                            });
                }
            }
        }
    }


}
