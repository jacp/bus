package com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class CameraInfoContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {


    }

}
