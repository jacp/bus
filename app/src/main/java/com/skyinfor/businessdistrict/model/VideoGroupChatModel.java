package com.skyinfor.businessdistrict.model;

import android.view.SurfaceView;

public class VideoGroupChatModel {

    private int uid;
    private SurfaceView surfaceView;
    private boolean isFullScreen = false;
    private boolean mIsMuteStream = true;
    private boolean mIsUserSelf = false;
    private String name;
    private String iconUrl;
    private boolean isAdd;

    public VideoGroupChatModel(int uid, SurfaceView surfaceView, boolean isFullScreen,
                               String iconUrl,String name,boolean isAdd) {
        this.uid = uid;
        this.surfaceView = surfaceView;
        this.isFullScreen = isFullScreen;
        this.mIsMuteStream = mIsMuteStream;
        this.mIsUserSelf = mIsUserSelf;
        this.iconUrl=iconUrl;
        this.name=name;
        this.isAdd=isAdd;
    }

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean add) {
        isAdd = add;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public SurfaceView getSurfaceView() {
        return surfaceView;
    }

    public void setSurfaceView(SurfaceView surfaceView) {
        this.surfaceView = surfaceView;
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        isFullScreen = fullScreen;
    }

    public boolean ismIsMuteStream() {
        return mIsMuteStream;
    }

    public void setmIsMuteStream(boolean mIsMuteStream) {
        this.mIsMuteStream = mIsMuteStream;
    }

    public boolean ismIsUserSelf() {
        return mIsUserSelf;
    }

    public void setmIsUserSelf(boolean mIsUserSelf) {
        this.mIsUserSelf = mIsUserSelf;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
