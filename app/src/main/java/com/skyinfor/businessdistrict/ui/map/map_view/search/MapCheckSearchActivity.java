package com.skyinfor.businessdistrict.ui.map.map_view.search;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.ClearEditText;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;

import butterknife.BindView;
import butterknife.OnClick;

public class MapCheckSearchActivity extends BaseActivity<MapCheckSearchPresent> implements MapCheckSearchContract.Model, TextWatcher {
    @BindView(R.id.edit_search_task)
    ClearEditText editSearchTask;
    @BindView(R.id.rec_contact_search)
    RecyclerView recContactSearch;
    private int type;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_contact_search;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        type = getIntent().getIntExtra("type",0);
        getVis();
        mPresenter.initAdapter(recContactSearch);
        editSearchTask.addTextChangedListener(this);
    }


    @OnClick(R.id.text_search_task_cancel)
    public void onViewClicked() {
        appManager.finishActivity();

    }

    private void getVis() {
        String str = editSearchTask.getText().toString().trim();
        if (!TextUtils.isEmpty(str)) {
            recContactSearch.setVisibility(View.VISIBLE);
        } else {
            recContactSearch.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        String name = String.valueOf(charSequence);
        getVis();

        if (name.length() > 0) {
            mPresenter.dialog.show();
            mPresenter.taskDevice(name,type);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
