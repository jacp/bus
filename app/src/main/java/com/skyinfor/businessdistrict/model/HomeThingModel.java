package com.skyinfor.businessdistrict.model;

public class HomeThingModel {


    /**
     * id : 243
     * create_time : 2018-11-23 17:29:30
     * update_time : 2018-11-23 17:29:30
     * name : 幕花园bb
     * type_id : 2
     * send_user_id : 117
     * accept_user_id : 116
     * status_code : 1
     * relation_device : 21
     * details : 保存
     * address : 上海市松江区黄渡浜街636号
     * position : 121.24144386729319,31.060904522522943
     * picture :
     * video :
     * audio :
     * list_type : task
     * format_time : 11/23|17:29
     * origin_type : user
     * origin_id : 46
     */

    private int id;
    private String create_time;
    private String update_time;
    private String name;
    private String type_id;
    private String send_user_id;
    private String accept_user_id;
    private String status_code;
    private String relation_device;
    private String details;
    private String address;
    private String position;
    private String picture;
    private String video;
    private String audio;
    private String list_type;
    private String format_time;
    private String origin_type;
    private int origin_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getSend_user_id() {
        return send_user_id;
    }

    public void setSend_user_id(String send_user_id) {
        this.send_user_id = send_user_id;
    }

    public String getAccept_user_id() {
        return accept_user_id;
    }

    public void setAccept_user_id(String accept_user_id) {
        this.accept_user_id = accept_user_id;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getRelation_device() {
        return relation_device;
    }

    public void setRelation_device(String relation_device) {
        this.relation_device = relation_device;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getList_type() {
        return list_type;
    }

    public void setList_type(String list_type) {
        this.list_type = list_type;
    }

    public String getFormat_time() {
        return format_time;
    }

    public void setFormat_time(String format_time) {
        this.format_time = format_time;
    }

    public String getOrigin_type() {
        return origin_type;
    }

    public void setOrigin_type(String origin_type) {
        this.origin_type = origin_type;
    }

    public int getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(int origin_id) {
        this.origin_id = origin_id;
    }
}
