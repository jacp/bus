package com.skyinfor.businessdistrict.model;

public class HomeTaskCountModel {
    private int condition;
    private int task;

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getTask() {
        return task;
    }

    public void setTask(int task) {
        this.task = task;
    }


}
