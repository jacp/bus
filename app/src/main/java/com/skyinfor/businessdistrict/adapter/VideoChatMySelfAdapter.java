package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.VideoGroupChatModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class VideoChatMySelfAdapter extends BaseMyRecyclerVIewAdapter<VideoGroupChatModel> {

    public VideoChatMySelfAdapter(Context context, List<VideoGroupChatModel> list) {
        super(context, list, R.layout.item_video_chat);
    }

    @Override
    protected void covert(VideoGroupChatModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        RelativeLayout container = holder.getView(R.id.rel_video_chat_container);
        View view = holder.getView(R.id.rel_video_item);

        if (container.getChildCount() != 0) {
            container.removeAllViews();
        }

        if (mode.getSurfaceView() != null) {
            if (mode.getSurfaceView().getParent() != null) {
                ((RelativeLayout) mode.getSurfaceView().getParent()).removeAllViews();
            }
            container.addView(mode.getSurfaceView());
        }

        ImageView img = holder.getView(R.id.img_sound_chat_self, position);
        TextView text = holder.getView(R.id.text_sound_self_name);

        view.setVisibility(mode.isAdd() == false ? View.VISIBLE : View.GONE);
        GlideUtils.loadImg(mContext,mode.getIconUrl(), img);
        text.setText(mode.getName());
    }
}
