package com.skyinfor.businessdistrict.ui.main.data_aly;

import com.skyinfor.businessdistrict.model.analysis.AnalysisCountModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisOnlineModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisZhiModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.HashMap;
import java.util.List;

public class BigDataContract {

    interface Model extends MModel {
        void setCountNum(AnalysisCountModel model);

        void setIndexResult(AnalysisZhiModel model);

        void setLineResult(List<HashMap<String, Integer>> lineList);

        void setDeviceResult(AnalysisOnlineModel model);

        void setPeopleResult(HashMap<String, String> map);

    }

    interface Presenter extends IPresenter<Model> {
        void dataAnalysis();

    }

}
