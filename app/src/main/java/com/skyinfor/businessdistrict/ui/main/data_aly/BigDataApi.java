package com.skyinfor.businessdistrict.ui.main.data_aly;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface BigDataApi {

    @GET(ApiUrl.dataAnalysis)
    Observable<Response<Object>> dataAnalysis();

}
