package com.skyinfor.businessdistrict.eventbus;

public class TimeCountEvent {

    private String str;

    public TimeCountEvent(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }
}
