package com.skyinfor.businessdistrict.ui.startpager;

import android.os.Bundle;
import android.os.Handler;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.ui.login.LoginActivity;
import com.skyinfor.businessdistrict.ui.main.MainActivity;
import com.skyinfor.businessdistrict.util.PackageUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.tencent.android.tpush.XGPushClickedResult;
import com.tencent.android.tpush.XGPushManager;

/**
 * Created by min on 2017/4/18.
 */

public class StartPageActivity extends BaseActivity<StartPagePresent> implements StartPageContract.Model {

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_start_page;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        // 判断是否从推送通知栏打开的
        XGPushClickedResult click = XGPushManager.onActivityStarted(this);
        if (click != null) {
            //从推送通知栏打开Service打开Activity会重新执行Laucher流程  //查看是不是全新打开的面板
            appManager.finishActivity(StartPageActivity.this);//如果有面板存在则关闭当前的面板
        }else {
            IsFIrstLogin();
        }
    }

    private void IsFIrstLogin() {

        new Handler().postDelayed(new Runnable() {
            public void run() {
                /**
                 * 判断是否在线，在线则跳转
                 */
                if (!Constants.isOnLine()) {
                    UIHelper.startActivity(StartPageActivity.this, LoginActivity.class);
                } else {
                    UIHelper.startActivity(StartPageActivity.this, MainActivity.class);
//                    appManager.finishActivity();
                }
                appManager.finishActivity(StartPageActivity.class);
            }

        }, 1500);
    }

    /**
     * 动态获取状态栏高度
     */
    @Override
    protected int _topBarAdaptiveVH() {
        return R.id.comm_topBarSteep;
    }

}

