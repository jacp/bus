package com.skyinfor.businessdistrict.fragment.main.stu.stu_sec;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.skyinfor.businessdistrict.adapter.BaseFragmentAdapter;
import com.skyinfor.businessdistrict.fragment.main.stu.stu_item.StatusSonFragment;
import com.skyinfor.businessdistrict.fragment.main.task.task_item.TaskSonFragment;
import com.skyinfor.businessdistrict.presenter.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.skyinfor.businessdistrict.util.UiUtils.reflex;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusSecPresent extends BasePresenter<StatusSecContract.Model> implements StatusSecContract.Presenter {
    private List<String> titleList = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();


    @Inject
    public StatusSecPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }

    @Override
    public void initAdapter(FragmentManager fm, ViewPager viewPager, TabLayout tabLayout) {
        titleList.add("全部");
        titleList.add("待处理");
        titleList.add("处理中");
        titleList.add("待审核");
        titleList.add("已完成");
        for (int i = 0; i < 5; i++) {
            StatusSonFragment fragment = StatusSonFragment.getInstance();
            fragment.setmFragmentTitle(i + "");
            fragments.add(fragment);
        }
        BaseFragmentAdapter adapter = new BaseFragmentAdapter(fm, titleList, fragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(5);

        reflex(tabLayout, 15);
    }

    @Override
    public void setNotify(int selectType) {
        StatusSonFragment fragment = (StatusSonFragment) fragments.get(selectType);
        fragment.setNotify(selectType);

    }

}
