package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.custom.chat.MsgImgView;
import com.skyinfor.businessdistrict.custom.chat.MsgTextView;
import com.skyinfor.businessdistrict.custom.chat.MsgVideoView;
import com.skyinfor.businessdistrict.custom.chat.MsgSoundView;
import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ChatContentAdapter extends BaseMyRecyclerVIewAdapter<ChatContentModel> {

    private List<ChatContentModel> list;

    public ChatContentAdapter(Context context, List<ChatContentModel> list) {
        super(context, list);
        this.list = list;

    }


    @Override
    protected BaseViewHolder getViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                return new leftViewHolder(setInflater(R.layout.item_chat_left, parent));

            case 1:
                return new rightViewHolder(setInflater(R.layout.item_chat_right, parent));
        }
        return null;
    }


    @Override
    protected int setViewType(int position, boolean isHead) {
        if (list.get(position).getSend_id() == Constants.USER_ID) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * type 1 文字 2 图片 3 语音通话 4 视频通话
     */
    class leftViewHolder extends viewHolder {

        public leftViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void viewCovert(ChatContentModel mode, BaseViewHolder holder, Context context, int position, int viewType) {
            super.viewCovert(mode, holder, context, position, viewType);
            IconView img = holder.getView(R.id.left_chat_head, position);
            mode.setNickname(mode.getNickname());
            mode.setAvatar(mode.getAvatar());
            img.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());

            LinearLayout layout = holder.getView(R.id.lin_left);
            setInfo(mode, layout, 0);

        }
    }

    class rightViewHolder extends viewHolder {


        public rightViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        protected void viewCovert(ChatContentModel mode, BaseViewHolder holder, Context context, int position, int viewType) {
            super.viewCovert(mode, holder, context, position, viewType);
            IconView img = holder.getView(R.id.right_chat_head, position);
            mode.setNickname(Constants.NICK_NAME);
            mode.setAvatar(Constants.ICON);
            img.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());
            LinearLayout layout = holder.getView(R.id.lin_left);

            setInfo(mode, layout, 1);

        }
    }

    private void setInfo(ChatContentModel mode, LinearLayout layout, int type) {
        layout.removeAllViews();

        switch (mode.getType()) {
            case 1:
                layout.addView(new MsgTextView(mContext, type, mode));

                break;
            case 2:
                layout.addView(new MsgImgView(mContext, type, mode));

                break;
            case 3:
                layout.addView(new MsgSoundView(mContext, type, mode));

                break;
            case 4:
                layout.addView(new MsgVideoView(mContext, type, mode));

                break;
        }

    }
}
