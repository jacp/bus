package com.skyinfor.businessdistrict.ui.main.create_stu.stu_type;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.SelectStatusAdapter;
import com.skyinfor.businessdistrict.model.StatusTypeModel;
import com.skyinfor.businessdistrict.model.TaskModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class SelectStatusTypePresent extends BasePresenter<SelectStatusTypeContract.Model>
        implements SelectStatusTypeContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private SelectStatusTypeApi api;
    private HttpDialog dialog;
    private List<StatusTypeModel> stuList = new ArrayList<>();
    private List<TaskModel> taskList = new ArrayList<>();
    private SelectStatusAdapter adapter;
    private int type;

    @Inject
    public SelectStatusTypePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        api = retrofit.create(SelectStatusTypeApi.class);
        dialog = new HttpDialog(mActivity);

    }


    @Override
    public void getStatusList(final int position) {
        dialog.show();
        addSubscription(api.conditionTypeList()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<StatusTypeModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<StatusTypeModel> model) {
                        stuList.addAll(model);
                        adapter.mPosition=position;
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                        mModel.setGetList(stuList);
                    }
                });

    }

    @Override
    public void getTaskTypeList(final int position) {
        dialog.show();
        addSubscription(api.taskTypeList()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<TaskModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<TaskModel> model) {
                        taskList.addAll(model);
                        adapter.mPosition=position;
                        adapter.notifyDataSetChanged();
                        dialog.dismiss();
                        mModel.setGetTaskList(taskList);
                    }
                });
    }

    @Override
    public void initAdapter(RecyclerView recyclerView, int type) {
        this.type=type;

        if (type == 0) {
            adapter = new SelectStatusAdapter(mActivity, stuList, type);
        } else {
            adapter = new SelectStatusAdapter(mActivity, taskList, type);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity), Color.parseColor("#efeff4")));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
//        adapter.setOnChecklistener(new SelectListener() {
//            @Override
//            public void selectCurItem(boolean isCheck, int position) {
//                adapter.mPosition = position;
//                adapter.notifyItemChanged(position);
//                mModel.setSelectResult(position);
//            }
//        });

    }

    @Override
    public void onItemClick(View view, int position) {
        adapter.mPosition = position;
        adapter.notifyDataSetChanged();
        mModel.setSelectResult(position);
    }


}
