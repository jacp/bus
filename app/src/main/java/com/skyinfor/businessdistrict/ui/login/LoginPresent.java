package com.skyinfor.businessdistrict.ui.login;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.model.UserBean;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.MainActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

/**
 * Created by SKYINFOR on 2018/10/22.
 */

public class LoginPresent extends BasePresenter<LoginContract.Model> implements LoginContract.Presenter {

    private LoginApi api;
    private HttpDialog dialog;

    @Inject
    public LoginPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(activity);
        api = retrofit.create(LoginApi.class);

    }


    @Override
    public void LoginAction(String name, String pass) {
        dialog.show();

        addSubscription(api.login(name, pass)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {
                        mModel.setResultAction();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        if (o != null) {
                            Gson gson = new Gson();

                            UserBean bean = gson.fromJson(gson.toJson(o), new TypeToken<UserBean>() {
                            }.getType());

                            Constants.saveUserInfo(bean.getLogin_name(), bean.getPassword(),
                                    bean.getId(), bean.getToken_user(),
                                    bean.getLogin_name(), bean.getNickname(), bean.getAvatar()
                                    , bean.getPhone(), bean.getRank_field());
                        }
                        dialog.dismiss();
                    }
                });

    }
}
