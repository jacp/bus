package com.skyinfor.businessdistrict.model;

import java.util.List;

public class ContactPersonFullModel {

    private String name;
    private List<ContactAllModel>list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ContactAllModel> getList() {
        return list;
    }

    public void setList(List<ContactAllModel> list) {
        this.list = list;
    }
}
