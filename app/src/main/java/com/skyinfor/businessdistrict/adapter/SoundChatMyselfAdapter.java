package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class SoundChatMyselfAdapter extends BaseMyRecyclerVIewAdapter<GroupMemberModel.UserListBean>{

    public SoundChatMyselfAdapter(Context context, List<GroupMemberModel.UserListBean> list) {
        super(context, list, R.layout.item_sound_chat_self_person);
    }

    @Override
    protected void covert(GroupMemberModel.UserListBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        ImageView img=holder.getView(R.id.img_sound_chat_self,position);
        TextView text=holder.getView(R.id.text_sound_self_name);

        if (TextUtils.isEmpty(mode.getAvatar())){
            text.setText(mode.getNickname());
            if (!TextUtils.isEmpty(mode.getNickname())){
                text.setText(mode.getNickname().substring(mode.getNickname().length() - 2,
                        mode.getNickname().length()));
            }
            img.setBackgroundColor(mContext.getResources().getColor(R.color.colorMain));

        }else {
            DisplayMetrics dm = mContext.getResources().getDisplayMetrics();
            int screenWidth = dm.widthPixels/3;

            ViewGroup.LayoutParams params = img.getLayoutParams();
            int height = params.height;
            GlideUtils.loadOutSideImg(screenWidth,height,mContext,mode.getAvatar(),img);
        }
    }
}
