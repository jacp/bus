package com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug;

import android.app.Activity;

import com.skyinfor.businessdistrict.presenter.BasePresenter;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class FirePlugInfoPresent extends BasePresenter<FirePlugInfoContract.Model>implements
        FirePlugInfoContract.Presenter {

    @Inject
    public FirePlugInfoPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }



}
