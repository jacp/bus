package com.skyinfor.businessdistrict.model;

import com.skyinfor.businessdistrict.adapter.UserIconBean;

import java.io.Serializable;
import java.util.List;

public class StatusTypeModel implements Serializable {


    /**
     * id : 1
     * name : 情况类型1
     * level : 一级
     * handle_role : 13
     * manage_role : 14
     * handle_user : [{"nickname":"刘丹丹","avatar":""},{"nickname":"李永飞","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png"},{"nickname":"尚崇俊","avatar":""},{"nickname":"李宁处置","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png"},{"nickname":"刘骁","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png"},{"nickname":"柴志俊","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png"},{"nickname":"陈磊","avatar":""}]
     * manage_user : [{"nickname":"App演示","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bce93222d0c8.png"},{"nickname":"陈磊","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bce81fe12b06.jpg"},{"nickname":"李宁","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png"},{"nickname":"武强","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bd013ded5e17.png"},{"nickname":"熊玉兵","avatar":""},{"nickname":"李佳","avatar":""},{"nickname":"曹忠德","avatar":""},{"nickname":"舒文强","avatar":""}]
     */

    private int id;
    private String name;
    private String level;
    private String handle_role;
    private String manage_role;
    private List<UserIconBean> handle_user;
    private List<UserIconBean> manage_user;
    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getHandle_role() {
        return handle_role;
    }

    public void setHandle_role(String handle_role) {
        this.handle_role = handle_role;
    }

    public String getManage_role() {
        return manage_role;
    }

    public void setManage_role(String manage_role) {
        this.manage_role = manage_role;
    }

    public List<UserIconBean> getHandle_user() {
        return handle_user;
    }

    public void setHandle_user(List<UserIconBean> handle_user) {
        this.handle_user = handle_user;
    }

    public List<UserIconBean> getManage_user() {
        return manage_user;
    }

    public void setManage_user(List<UserIconBean> manage_user) {
        this.manage_user = manage_user;
    }


}
