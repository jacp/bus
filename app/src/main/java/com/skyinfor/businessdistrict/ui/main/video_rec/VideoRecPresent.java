package com.skyinfor.businessdistrict.ui.main.video_rec;

import android.app.Activity;

import com.skyinfor.businessdistrict.presenter.BasePresenter;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class VideoRecPresent extends BasePresenter<VideoRecContract.Model>implements VideoRecContract.Presenter {

    @Inject
    public VideoRecPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }
}
