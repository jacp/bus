package com.skyinfor.businessdistrict.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;

@SuppressLint("AppCompatCustomView")
public class NoExecuteView extends TextView {

    private Rect rect = new Rect();
    private Paint paint;

    public NoExecuteView(Context context) {
        super(context);
    }

    public NoExecuteView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        paint = new Paint();
        paint.setColor(getResources().getColor(R.color.color_f40f0f));
        paint.setStyle(Paint.Style.FILL);
        rect = new Rect(60, 60, 60, 60);
        canvas.drawRect(rect, paint);

        paint.setColor(getResources().getColor(R.color.color_f40f0f));
        paint.setStyle(Paint.Style.STROKE);
        String text=getText().toString();
        canvas.drawText(text,0,text.length(),paint);

    }
}
