package com.skyinfor.businessdistrict.fragment.main.contact;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;
import com.skyinfor.businessdistrict.util.HttpDialog;

import java.util.List;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class ContactContract {

    interface Model extends MModel {
        void setSelectList(List<PersonListBean> personListBeanList);
        void setDialog(HttpDialog dialog);

    }

    interface Presenter extends IPresenter<Model> {
        void getContactList(List<PersonListBean> getList);
        void getFullList(List<PersonListBean> getList,int type);

        void initAdapter(RecyclerView iconRec,RecyclerView titleRec,RecyclerView personRec,RecyclerView sonRec
                ,RecyclerView chatRec,boolean isSelect, boolean isShowSelf);
        void delGroup(String uuid);

    }

}
