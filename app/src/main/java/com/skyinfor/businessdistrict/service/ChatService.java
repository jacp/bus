package com.skyinfor.businessdistrict.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.eventbus.SoundStatusEvent;
import com.skyinfor.businessdistrict.model.SocketModel;
import com.skyinfor.businessdistrict.model.SoundChatModel;
import com.skyinfor.businessdistrict.ui.chat.sound_chat.SoundChatActivity;
import com.skyinfor.businessdistrict.ui.chat.video_chat.VideoChatActivity;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.TipHelper;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.tencent.android.tpush.XGLocalMessage;
import com.tencent.android.tpush.XGPushManager;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.agora.AgoraAPI;
import io.agora.AgoraAPIOnlySignal;

public class ChatService extends Service {

    private AgoraAPIOnlySignal agoraAPI;
    private TipHelper helper;
    private HashMap<String, Object> map;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        initChat();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        agoraAPI = null;
        helper.destroy();
    }

    private void initChat() {
        helper = new TipHelper(ChatService.this);

        agoraAPI = MyApplication.getInstance().getmAgoraAPI();

        if (agoraAPI == null) {
            return;
        }

        agoraAPI.callbackSet(new AgoraAPI.CallBack() {

            @Override
            public void onChannelUserJoined(String account, int uid) {
                super.onChannelUserJoined(account, uid);

            }

            //用户离开
            @Override
            public void onChannelUserLeaved(String account, int uid) {

            }

            //登出
            @Override
            public void onLogout(final int i) {

            }

            //查询用户在线状态
            @Override
            public void onQueryUserStatusResult(final String name, final String status) {

            }

            //普通消息接受
            @Override
            public void onMessageInstantReceive(final String account, int uid, final String msg) {
                Gson gson = new Gson();
                SoundChatModel model = gson.fromJson(msg, SoundChatModel.class);

                int type = Integer.parseInt(model.getType());

                if (type < 5) {
                    if (Constants.IS_LOCK) {
                        helper.Vibrate(200);
                    }
//                    helper.playvoid(0);
                }
                if (Constants.IS_DIS_SOCKET){
                    initLocalMessage(model);
                }

                EventBus.getDefault().post(new ChatNotifyEvent(type));

                Bundle data = new Bundle();

                switch (type) {
                    case 33:
                    case 3:
                        data.putString("str", msg);
                        data.putInt("type", 1);
                        data.putInt("channelType", type);
                        if (model.getUser_array() != null) {
                            data.putSerializable("model", (Serializable) model.getUser_array());
                        }
                        data.putString("user_id", model.getSend_id());

                        data.putString("channel_id", TextUtils.isEmpty(model.getAction()) ? model.getReceive_id() : model.getAction());
                        UIHelper.startActivity(ChatService.this, SoundChatActivity.class, data);

                        break;
                    case 44:
                    case 4:
                        data.putString("str", msg);
                        data.putInt("type", 1);
                        data.putInt("channelType", type);
                        if (model.getUser_array() != null) {
                            data.putSerializable("model", (Serializable) model.getUser_array());
                        }
                        data.putString("user_id", model.getSend_id());

                        data.putString("channel_id", TextUtils.isEmpty(model.getAction()) ? model.getReceive_id() : model.getAction());
                        UIHelper.startActivity(ChatService.this, VideoChatActivity.class, data);
                        break;
                    case 99:
                        AppManager appManager = AppManager.getAppManager();

                        if (appManager.getCurActivity() instanceof SoundChatActivity) {
                            AppManager.getAppManager().finishActivity(SoundChatActivity.class);
                        } else if (appManager.getCurActivity() instanceof VideoChatActivity) {
                            AppManager.getAppManager().finishActivity(VideoChatActivity.class);
                        }
                        break;
                    case 88:
                        EventBus.getDefault().post(new SoundStatusEvent(1));

                        break;

                }

            }

            //平道信息接收
            @Override
            public void onMessageChannelReceive(String channelID, final String account, int uid, final String msg) {
//                if (!account.equals(String.valueOf(Constants.USER_ID))) {
//                    EventBus.getDefault().post(new ChatNotifyEvent());
//                }

            }

            //信息发送成功
            @Override
            public void onMessageSendSuccess(String messageID) {

            }


            //信息发送失败
            @Override
            public void onMessageSendError(String messageID, int ecode) {

            }


            @Override
            public void onInviteReceived(String channelID, String account, int uid, String extra) {
                super.onInviteReceived(channelID, account, uid, extra);


            }

            @Override
            public void onInviteAcceptedByPeer(String channelID, String account, int uid, String extra) {
                super.onInviteAcceptedByPeer(channelID, account, uid, extra);


            }

            //信息错误
            @Override
            public void onError(String name, int ecode, String desc) {
                Log.i("ChatService", "onError  name = " + name + " ecode = " + ecode + " desc = " + desc);
            }

            //日志
            @Override
            public void onLog(String txt) {
                super.onLog(txt);
            }
        });
    }

    // 设置本地消息
    private void initLocalMessage(SoundChatModel msg) {
        //设置消息信息
        SocketModel data = new SocketModel();
        data.setType("new_message");
        SocketModel.DataBean mode = new SocketModel.DataBean();
        mode.setGroup_uuid(msg.getGroup_uuid());
        mode.setSend_id(msg.getSend_id());
        mode.setReceive_id(msg.getReceive_id());
        mode.setSend_name(msg.getSend_name());
        mode.setNickname(msg.getNickname());
        mode.setSend_img(msg.getSend_img());
        mode.setGroupName(msg.getGroupName());
        data.setData(mode);
        Gson gson = new Gson();
        String str = gson.toJson(data);

        try {
            map = parseJSONObject(new JSONObject(str));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        XGLocalMessage local_msg = new XGLocalMessage();

        // 设置本地消息类型，1:通知，2:消息
        local_msg.setType(1);
        // 设置消息标题
        local_msg.setTitle("你有新消息");
        // 设置消息内容
        local_msg.setContent(msg.getContent());
        local_msg.setCustomContent(map);
        // 设置消息日期，格式为：20140502
        local_msg.setDate("20140930");
        // 设置消息触发的小时(24小时制)，例如：22代表晚上10点
        local_msg.setHour("19");
        // 获取消息触发的分钟，例如：05代表05分
        local_msg.setMin("31");
        local_msg.setIcon_res("right");
        XGPushManager.addLocalNotification(this, local_msg);
    }

    public static HashMap<String, Object> parseJSONObject(JSONObject jsonobj) {
        JSONArray a_name = jsonobj.names();
        HashMap<String, Object> map = new HashMap<String, Object>();
        if (a_name != null) {
            int i = 0;
            while (i < a_name.length()) {
                String key;
                try {
                    key = a_name.getString(i);
                    Object obj = jsonobj.get(key);
                    map.put(key, parseUnknowObjectToJson(obj));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                i++;
            }
        }
        return map;
    }

    private static Object parseUnknowObjectToJson(Object o) {
        if (o instanceof JSONObject) {
            return parseJSONObject((JSONObject) o);
        } else if (o instanceof JSONArray) {
            return parseJSONArray((JSONArray) o);
        }
        return o;
    }

    public static ArrayList<Object> parseJSONArray(JSONArray jsonarr) {
        ArrayList<Object> list = new ArrayList<Object>();
        int len = jsonarr.length();
        for (int i = 0; i < len; i++) {
            Object o;
            try {
                o = jsonarr.get(i);
                list.add(parseUnknowObjectToJson(o));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

}
