package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class PlanDetailOperationAdapter extends BaseMyRecyclerVIewAdapter<String> {

    public PlanDetailOperationAdapter(Context context, List<String> list) {
        super(context, list, R.layout.item_plan_detail_operation);
    }

    @Override
    protected void covert(String mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        holder.setText(R.id.text_plan_operation_name, mode);
        ImageView img = holder.getView(R.id.img_plan_operation_next);

        img.setVisibility(mList.size() - 1 == position ? View.GONE : View.VISIBLE);
    }
}
