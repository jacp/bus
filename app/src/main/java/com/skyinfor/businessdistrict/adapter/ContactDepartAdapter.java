package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.ContactAllModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ContactDepartAdapter extends BaseMyRecyclerVIewAdapter<ContactAllModel> {

    public ContactDepartAdapter(Context context, List<ContactAllModel> list) {
        super(context, list, R.layout.item_contact_department);
    }

    @Override
    protected void covert(ContactAllModel mode, BaseViewHolder holder, final int position) {
        super.covert(mode, holder, position);
        holder.setText(R.id.text_contact_depart_content, mode.getName());



    }

}
