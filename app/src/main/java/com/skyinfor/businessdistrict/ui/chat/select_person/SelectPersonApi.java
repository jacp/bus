package com.skyinfor.businessdistrict.ui.chat.select_person;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.Response;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface SelectPersonApi {

    @FormUrlEncoded
    @POST(ApiUrl.talkGroupInformation)
    Observable<Response<GroupMemberModel>> talkGroupInformation(@Field("group_uuid") String group_uuid);

}
