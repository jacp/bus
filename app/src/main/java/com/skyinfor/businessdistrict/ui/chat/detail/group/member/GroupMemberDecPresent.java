package com.skyinfor.businessdistrict.ui.chat.detail.group.member;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.ChatDetailMemberAdapter;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.util.recyclerview.SpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class GroupMemberDecPresent extends BasePresenter<GroupMemberDecContract.Model>
        implements GroupMemberDecContract.Presenter {

    private ChatDetailMemberAdapter adapter;
    private List<GroupMemberModel.UserListBean> list = new ArrayList<>();


    @Inject
    public GroupMemberDecPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new ChatDetailMemberAdapter(mActivity, list, false);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, 5));
        recyclerView.addItemDecoration(new SpaceItemDecoration(dp2px(16, mActivity)));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setNotify(List<GroupMemberModel.UserListBean> userListBeanList) {
        list.clear();
        list.addAll(userListBeanList);
        adapter.notifyDataSetChanged();

    }
}
