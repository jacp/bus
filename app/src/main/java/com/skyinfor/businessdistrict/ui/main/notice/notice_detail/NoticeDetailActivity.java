package com.skyinfor.businessdistrict.ui.main.notice.notice_detail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.util.HttpDialog;

import java.util.ArrayList;

import butterknife.BindView;

public class NoticeDetailActivity extends BaseActivity {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.web_view_notice)
    WebView webViewNotice;
    String css = "<style type=\"text/css\" >    img{        width: 100%;        height: auto;    }    body{        padding-right: 5px;        padding-left: 5px;        padding-top: 5px;        font-size: 45px !important;        word-wrap:break-word;    }    div{        font-size:35px !important;    }</style>";
    private HttpDialog dialog;
    private ArrayList<ImageItem> listItem = new ArrayList<>();


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_notice_detail;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        tvTitle.setText("公告详情");
        String content = getIntent().getStringExtra("content");
        dialog = new HttpDialog(mContext);
        dialog.show();
        initWebConfig(content);
    }

    private String setWebData(String content) {
        String html = "<html><head>" + css + "</head>" + content + "</html>";
        return html;
    }

    @SuppressLint("JavascriptInterface")
    private void initWebConfig(String content){
        webViewNotice .setLayerType(View.LAYER_TYPE_HARDWARE,null);//开启硬件加速
        webViewNotice.getSettings().setUseWideViewPort(true);//关键点
        webViewNotice.setVerticalScrollBarEnabled(false);
        webViewNotice.setHorizontalScrollBarEnabled(false);
        webViewNotice.getSettings().setSupportZoom(true); // 支持缩放
        webViewNotice.getSettings().setLoadWithOverviewMode(true);
        webViewNotice.getSettings().setBuiltInZoomControls(true); // 设置显示缩放按钮
        webViewNotice.getSettings().setTextZoom(100);
        //不显示webview缩放按钮
        webViewNotice.getSettings().setDisplayZoomControls(false);

        webViewNotice.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView webView, int i) {
                if (i == 100) {
                    dialog.dismiss();
                }
            }
        });
        WebSettings settings = webViewNotice.getSettings();
        settings.setDisplayZoomControls(false);
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        webViewNotice.addJavascriptInterface(new JavaInterface(this), "imagelistner");

        webViewNotice.loadDataWithBaseURL(null, setWebData(content), "text/html", "utf-8", null);
        webViewNotice.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                view.getSettings().setJavaScriptEnabled(true);
                super.onPageFinished(view, url);
                // html加载完成之后，添加监听图片的点击js函数
                addImageClickListner();

            }
        });

    }

    // js通信接口
    public class JavaInterface {

        private Context context;

        public JavaInterface(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void openImage(String img) {
            System.out.println(img);
            listItem.clear();
            ImageItem item = new ImageItem();
            item.path = img;
            listItem.add(item);

            //打开预览
            Intent intentPreview = new Intent(mContext, ImagePreviewDelActivity.class);
            intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, listItem);
//            intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, position);
            intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
            intentPreview.putExtra(ImagePicker.DELETE_IS_SHOW, true);
            mContext.startActivity(intentPreview);

        }
    }

    // 注入js函数监听
    private void addImageClickListner() {
        // 这段js函数的功能就是，遍历所有的img几点，并添加onclick函数，函数的功能是在图片点击的时候调用本地java接口并传递url过去
        webViewNotice.loadUrl("javascript:(function(){" +
                "var objs = document.getElementsByTagName(\"img\"); " +
                "for(var i=0;i<objs.length;i++)  " +
                "{"
                + "    objs[i].onclick=function()  " +
                "    {  "
                + "        window.imagelistner.openImage(this.src);  " +
                "    }  " +
                "}" +
                "})()");
    }

}
