package com.skyinfor.businessdistrict.rx.data.download;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;


import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.util.AbAppUtil;
import com.skyinfor.businessdistrict.util.ContentUtils;

import java.io.File;

public class DownloaderReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		String action = intent.getAction();
		if (action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
			long id = ContentUtils.getSharePreLong(context,
					Constants.SHARED_PREFERENCE_NAME, Constants.APPDOWNLOAD_ID);
			long did = intent
					.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
			if (id > 0 && id == did) {
				AbAppUtil.installApk(context, new File(
						Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
						, "hctx_oa" + ".apk"));
			}
		}
	}

}
