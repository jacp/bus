package com.skyinfor.businessdistrict.base;

/**
 * Created by min on 2017/3/8.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;
import com.skyinfor.businessdistrict.util.AppManager;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by min on 2017/3/1.
 */

public abstract class MainBaseActivity<T extends IPresenter> extends AppCompatActivity implements MModel {
    @Inject
    protected T mPresenter;
    protected Activity mContext;
    protected Context context;
    protected Unbinder unbinder;
    public AppManager appManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getLayout();

        // 判断当前的Activity是堆栈中是否存在
        appManager = AppManager.getAppManager();
        Activity activity = appManager.getActivity(this.getClass());
        if (activity != null) {
            activity.finish();
        }
        appManager.addActivity(this);

        ButterKnife.bind(this);
//        unbinder=ButterKnife.bind(this);
        mContext = this;
        setupActivityComponent(MyApplication.getAppComponent(), new ActivityModule(this));
        mPresenter.attachView(this);
        initView();
        initData(savedInstanceState);
        bindEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unbinder.unbind();//解绑
        if (mPresenter != null) mPresenter.detachView();

    }
//    @OnClick(R.id.iv_back)
//    protected void back(){
//        appManager.finishActivity();
//    }

    /**
     * 依赖注入的入口
     *
     * @param appComponent appComponent
     */
    protected abstract void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule);


    protected abstract void getLayout();

    protected abstract void initData(Bundle savedInstanceState);

    protected void initView() {

    }
    protected void bindEvent() {

    }

}

