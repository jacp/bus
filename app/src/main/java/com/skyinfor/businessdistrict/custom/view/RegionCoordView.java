package com.skyinfor.businessdistrict.custom.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;


/**
 * Created by Administrator on 2017/4/26 0026.
 */

public class RegionCoordView extends ImageView {
    public  Context mContext;
    private int     width;
    private int     height;

    public RegionCoordView(Context context) {
        this(context, null);
    }

    public RegionCoordView(Context context, AttributeSet attrs) {
        this(context, attrs,0);

    }

    public RegionCoordView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

    }




    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            float x = event.getX();
            float y = event.getY();

            if (x >width/2-20 && x < width/2+20 && y > height/2-20 && y < height/2+20) {
             //   ((PunchOutActivity) mContext).showClickArea();
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
    }
}
