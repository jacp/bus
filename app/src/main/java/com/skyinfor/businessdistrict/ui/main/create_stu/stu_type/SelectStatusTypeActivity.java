package com.skyinfor.businessdistrict.ui.main.create_stu.stu_type;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.StatusTypeModel;
import com.skyinfor.businessdistrict.model.TaskModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectStatusTypeActivity extends BaseActivity<SelectStatusTypePresent>
        implements SelectStatusTypeContract.Model {


    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rec_status_type)
    RecyclerView recStatusType;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    private int position;
    private List<StatusTypeModel> list;
    private List<TaskModel> taskList;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_select_status_type;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        int type = getIntent().getIntExtra("type", 0);

        tvTitle.setText(type == 0 ? "情况类型" : "任务类型");
        textRightEditText.setVisibility(View.VISIBLE);
        textRightEditText.setText("完成");
        position = getIntent().getIntExtra("position", 0);

        mPresenter.initAdapter(recStatusType, type);
        if (type == 0) {
            mPresenter.getStatusList(position);
        } else {
            getIntent().getSerializableExtra("model");

            mPresenter.getTaskTypeList(position);
        }

    }


    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        if (list != null) {
            StatusTypeModel model = list.get(position);
            Intent intent = new Intent();
            Bundle data = new Bundle();
            data.putSerializable("model", model);
            data.putInt("position",position);
            intent.putExtras(data);
            setResult(101, intent);
            appManager.finishActivity();
        } else {
            TaskModel model = taskList.get(position);
            Intent intent = new Intent();
            Bundle data = new Bundle();
            data.putSerializable("model", model);
            data.putInt("position",position);
            intent.putExtras(data);
            setResult(101, intent);
            appManager.finishActivity();
        }

    }

    @Override
    public void setSelectResult(int id) {
        position = id;
    }

    @Override
    public void setGetList(List<StatusTypeModel> list) {
        this.list = list;

    }

    @Override
    public void setGetTaskList(List<TaskModel> list) {
        taskList = list;

    }
}
