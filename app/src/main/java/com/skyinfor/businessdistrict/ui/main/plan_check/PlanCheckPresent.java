package com.skyinfor.businessdistrict.ui.main.plan_check;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.skyinfor.businessdistrict.adapter.BaseFragmentAdapter;
import com.skyinfor.businessdistrict.fragment.main.home.plan.plan_list.PlanListFragment;
import com.skyinfor.businessdistrict.presenter.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class PlanCheckPresent extends BasePresenter<PlanCheckContract.Model> implements
        PlanCheckContract.Presenter {

    private List<String> strList = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();


    @Inject
    public PlanCheckPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }

    @Override
    public void initAdapter(FragmentManager fm, final SegmentTabLayout apBarStl, final ViewPager viewPager) {
        strList.add("预案列表");
        strList.add("触发记录");

        for (int i = 0; i < 2; i++) {
            PlanListFragment fragment = new PlanListFragment();
            fragment.setmFragmentTitle(i + "");
            fragments.add(fragment);
        }

        BaseFragmentAdapter adapter = new BaseFragmentAdapter(fm, strList, fragments);
        viewPager.setAdapter(adapter);

        apBarStl.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                viewPager.setCurrentItem(position);

            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                apBarStl.setCurrentTab(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}
