package com.skyinfor.businessdistrict.adapter;

import java.io.Serializable;

public class UserIconBean implements Serializable{

    /**
     * nickname : 刘丹丹
     * avatar :
     */

    private String nickname;
    private String avatar;
    private int user_id;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
