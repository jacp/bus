package com.skyinfor.businessdistrict.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;


import com.skyinfor.businessdistrict.R;

import java.util.List;


/**
 * Author : Haily
 * Date   : ${DATE}${TIME}
 * Email  : zhanghailei55@gmail.com
 * Desc   :
 * Comment: //TODO
 */

public abstract class DialogCenterView<T> extends Dialog {
    public List<T> list;
    Context context;


    public DialogCenterView(Context context, List<T> list) {
        super(context, R.style.MoodDialog);
        this.list = list;
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = initView();
        setContentView(view);
        initData(list, view);

        setCanceledOnTouchOutside(true);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    /**
     * 初始化界面控件的显示数据
     * @param list
     * @param view
     */
    public abstract void initData(List<T> list, View view);

    /**
     * 初始化界面控件
     */
    public abstract View initView();
}
