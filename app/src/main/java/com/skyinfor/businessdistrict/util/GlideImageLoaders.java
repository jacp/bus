package com.skyinfor.businessdistrict.util;

import android.content.Context;
import android.widget.ImageView;

import com.youth.banner.loader.ImageLoader;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class GlideImageLoaders extends ImageLoader {


    @Override
    public void displayImage(Context context, Object path, ImageView imageView) {
        GlideUtils.loadImg(path,imageView);
    }
}
