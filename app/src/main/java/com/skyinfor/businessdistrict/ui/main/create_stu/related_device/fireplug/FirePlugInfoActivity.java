package com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.dialog.DialogView;
import com.skyinfor.businessdistrict.listener.OnMessageDialogEvent;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.ui.map.MapChooseActivity;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;

import butterknife.BindView;
import butterknife.OnClick;

import static com.skyinfor.businessdistrict.util.Utils.callPhone;

public class FirePlugInfoActivity extends BaseActivity<FirePlugInfoPresent> implements FirePlugInfoContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_fire_type)
    TextView textFireType;
    @BindView(R.id.text_fire_belong)
    TextView textFireBelong;
    @BindView(R.id.text_fire_manager)
    TextView textFireManager;
    @BindView(R.id.text_fire_manager_phone)
    TextView textFireManagerPhone;
    @BindView(R.id.text_fire_name_is_important)
    TextView textFireNameIsImportant;
    @BindView(R.id.text_fire_name)
    TextView textFireName;
    @BindView(R.id.text_fire_position)
    TextView textFirePosition;
    private String address;
    private String position;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_fire_plug;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);

        RelatedDeviceModel.ListBean model = (RelatedDeviceModel.ListBean) getIntent()
                .getSerializableExtra("model");
        setInfo(model);
    }

    private void setInfo(RelatedDeviceModel.ListBean model) {
        if (model != null) {
            tvTitle.setText(model.getName());
            textFireName.setText(model.getName());
            textFirePosition.setText(model.getAddress());
            textFireType.setText(model.getType());
            textFireBelong.setText(model.getAscription());
            textFireManager.setText(model.getDuty_person_name());
            textFireManagerPhone.setText(model.getDuty_person_phone() + "");
            textFireNameIsImportant.setText(model.getIs_import());
            address = model.getAddress();
            position = model.getPosition();
        }

    }

    @OnClick({R.id.rel_fire_position, R.id.rel_fire_call})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rel_fire_position:
                Bundle data = new Bundle();
                data.putString("strLag", position);
                data.putString("address", address);
                UIHelper.startActivity(mContext, MapChooseActivity.class, data);

                break;
            case R.id.rel_fire_call:
                final String str = textFireManagerPhone.getText().toString().trim();
                if (TextUtils.isEmpty(str)) {
                    ToashUtils.show(mContext, "号码暂未提供！");
                } else {
                    DialogView dialog = new DialogView(mContext, "",
                            String.format("是否确认拨打%s?", str), "拨打", "取消");
                    dialog.show();
                    dialog.setDialogEvent(new OnMessageDialogEvent() {
                        @Override
                        public void ok(Dialog dialog) {
                            callPhone(str, mContext);
                        }

                        @Override
                        public void close(Dialog dialog) {
                            dialog.dismiss();
                        }
                    });
                }

                break;
        }
    }

}
