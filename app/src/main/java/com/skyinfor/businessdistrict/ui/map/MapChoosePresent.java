package com.skyinfor.businessdistrict.ui.map;

import android.app.Activity;

import com.skyinfor.businessdistrict.presenter.BasePresenter;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class MapChoosePresent extends BasePresenter<MapChooseContract.Model>
        implements MapChooseContract.Presenter {

    @Inject
    public MapChoosePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }


}
