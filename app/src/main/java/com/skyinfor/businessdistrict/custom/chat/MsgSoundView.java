package com.skyinfor.businessdistrict.custom.chat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.eventbus.ChatActionEvent;
import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.model.SoundChatModel;

import org.greenrobot.eventbus.EventBus;

import io.agora.AgoraAPIOnlySignal;

public class MsgSoundView extends RelativeLayout {


    public MsgSoundView(Context context, int type, ChatContentModel mode) {
        super(context);

        initData(context, type, mode);
    }

    public MsgSoundView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initData(Context context, int type, final ChatContentModel mode) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sound, null);
        TextView text = view.findViewById(R.id.text_sound);
        if (type == 0) {
            text.setTextColor(context.getResources().getColor(R.color.color_333333));
            Drawable left = getResources().getDrawable(R.mipmap.icon_news_chat_icon_hangup);
            left.setBounds(0, 0, left.getMinimumWidth(), left.getMinimumHeight());
            text.setCompoundDrawables(left, null, null, null);
        } else {
            text.setTextColor(context.getResources().getColor(R.color.colorWhite));
            Drawable right = getResources().getDrawable(R.mipmap.icon_news_chat_icon_hangup_02);
            right.setBounds(0, 0, right.getMinimumWidth(), right.getMinimumHeight());
            text.setCompoundDrawables(null, null, right, null);
        }
        text.setText(mode.getContent());
        text.setBackgroundResource(type==0?R.drawable.icon_bg_talk_left:R.drawable.icon_bg_talk_right);

        addView(view);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new ChatActionEvent(3));
            }
        });
    }


}
