package com.skyinfor.businessdistrict.ui.chat.detail.group;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.Response;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface ChatDetailApi {

    @FormUrlEncoded
    @POST(ApiUrl.talkGroupInformation)
    Observable<Response<GroupMemberModel>> talkGroupInformation(@Field("group_uuid") String group_uuid);


    @FormUrlEncoded
    @POST(ApiUrl.groupSignOut)
    Observable<Response<Object>> groupSignOut(@Field("group_uuid") String group_uuid,
                                              @Field("user_id") String user_id );

}
