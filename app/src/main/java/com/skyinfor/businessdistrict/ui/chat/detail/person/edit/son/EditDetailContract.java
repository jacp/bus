package com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

import okhttp3.MultipartBody;

public class EditDetailContract {

    interface Model extends MModel {
        void setSelectResult(String str);
        void finishAction();
    }

    interface Presenter extends IPresenter<Model> {
        void updateInfo(List<MultipartBody.Part> parts,int type,String str);
        void initAdapter(RecyclerView recyclerView,String sex);
    }

}
