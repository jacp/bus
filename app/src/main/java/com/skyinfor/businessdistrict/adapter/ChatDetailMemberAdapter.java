package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ChatDetailMemberAdapter extends BaseMyRecyclerVIewAdapter<GroupMemberModel.UserListBean> {


    private boolean isMore;

    public ChatDetailMemberAdapter(Context context, List<GroupMemberModel.UserListBean> list,
                                   boolean isMore) {
        super(context, list, R.layout.item_chat_detail_member);
        this.isMore = isMore;
    }

    @Override
    protected void covert(GroupMemberModel.UserListBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        IconView img = holder.getView(R.id.img_chat_round_head, position);
        if (isMore) {
            if (position == 14) {
                img.setImgResource(R.mipmap.icon_people_more_icon);
                holder.setText(R.id.text_chat_round_name, "更多");
            } else {
                img.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());
                holder.setText(R.id.text_chat_round_name, mode.getNickname());
            }
        } else {
            img.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());
            holder.setText(R.id.text_chat_round_name, mode.getNickname());
        }

    }

    @Override
    protected void onRecyLoadAgain(RecyclerView.ViewHolder holder) {
        super.onRecyLoadAgain(holder);
        BaseViewHolder ho = (BaseViewHolder) holder;
        IconView img = ho.getView(R.id.img_chat_round_head);
    }
}
