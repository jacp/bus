package com.skyinfor.businessdistrict.model;

public class ChatContentModel {


    /**
     * id : 278
     * content : 52525
     * type : 1
     * send_id : 94
     * group_uuid : 6a13ee00-6085-6ed1-e5fe-e9e83a7f1ea6
     * action : 6a13ee00-6085-6ed1-e5fe-e9e83a7f1ea6
     * time : 2018-11-15 13:29:02
     * detail :
     * avatar :
     * nickname : 刘骁
     */

    private int id;
    private String content;
    private int type;
    private int send_id;
    private String group_uuid;
    private String action;
    private String time;
    private String detail;
    private String avatar;
    private String nickname;

    public ChatContentModel(String content, int type, int send_id, String group_uuid
            , String avatar, String nickname) {
        this.content = content;
        this.type = type;
        this.send_id = send_id;
        this.group_uuid = group_uuid;
        this.avatar = avatar;
        this.nickname = nickname;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSend_id() {
        return send_id;
    }

    public void setSend_id(int send_id) {
        this.send_id = send_id;
    }

    public String getGroup_uuid() {
        return group_uuid;
    }

    public void setGroup_uuid(String group_uuid) {
        this.group_uuid = group_uuid;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
