package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class StatusIconAdapter extends BaseMyRecyclerVIewAdapter<UserIconBean> {

    public StatusIconAdapter(Context context, List<UserIconBean> list) {
        super(context, list, R.layout.item_status_icon_list);
    }

    @Override
    protected void covert(UserIconBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        IconView icon=holder.getView(R.id.img_status_icon,position);
        icon.setBaseInfo(mContext,mode.getNickname(),mode.getAvatar());

    }
}
