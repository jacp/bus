package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class RelatedDeviceSonAdapter extends BaseMyRecyclerVIewAdapter<RelatedDeviceModel.ListBean> {

    private String name;
    private List<RelatedDeviceModel.ListBean> list;

    public RelatedDeviceSonAdapter(Context context, String name, List<RelatedDeviceModel.ListBean> list) {
        super(context, list, R.layout.item_related_device_son);
        this.list=list;
        this.name = name;
    }

    @Override
    protected void covert(RelatedDeviceModel.ListBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        TextView text = holder.getView(R.id.text_relate_device_content);
        text.setText(TextUtils.isEmpty(mode.getName())?mode.getNickname():mode.getName());
        View view=holder.getView(R.id.div_select);

        TextView box = holder.getView(R.id.checkbox_status);
        box.setSelected(mode.isCheck());

        int layId;
        switch (name) {
            case "摄像头":
                layId = R.mipmap.icon_map_position_icon_camera;
                break;
            case "消防栓":
                layId = R.mipmap.icon_map_position_xiao_fang;
                break;
            case "人员":
                layId = R.mipmap.icon_map_position_icon_people;
                break;
            default:
                layId = R.mipmap.icon_map_position_icon_camera;
                break;
        }

        Drawable drawableLeft = mContext.getResources().getDrawable(
                layId);

        text.setCompoundDrawablesWithIntrinsicBounds(drawableLeft,
                null, null, null);

        box.setVisibility(mode.isShow() == false ? View.VISIBLE : View.GONE);
        view.setVisibility(mList.size()-1==position?View.GONE:View.VISIBLE);
    }

    public void setNotify(List<RelatedDeviceModel.ListBean> list) {
//        this.name=name;
//        this.list = list;
        notifyDataSetChanged();
    }
}
