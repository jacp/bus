package com.skyinfor.businessdistrict.fragment.main.home;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisZhiModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class HomeContract {

    interface Model extends MModel {
        void setBannerResult(List<HomeNoticeModel.DataBean> list);

        void setTaskCount(int stuSize, int taskSize);

        void setIndexResult(AnalysisZhiModel model);

        void setLineResult(List<HashMap<String, Integer>> lineList);

        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();
    }

    interface Presenter extends IPresenter<Model> {
        void waitTasksCount(String userId);

        void noticeList(String page, String limit);

        void newTaskList();

        void initAdapter(RecyclerView recyclerView);

        void dataAnalysis();
    }

}
