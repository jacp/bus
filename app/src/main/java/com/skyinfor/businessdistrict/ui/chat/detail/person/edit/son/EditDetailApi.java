package com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface EditDetailApi {

    @Multipart
    @POST(ApiUrl.userInformationUpdate)
    Observable<Response<Object>> userInformationUpdate(@Part List<MultipartBody.Part> parts);

}
