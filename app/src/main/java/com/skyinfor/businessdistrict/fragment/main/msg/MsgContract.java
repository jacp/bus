package com.skyinfor.businessdistrict.fragment.main.msg;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class MsgContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);

        void getConversationList(String userId,int type);

    }

}
