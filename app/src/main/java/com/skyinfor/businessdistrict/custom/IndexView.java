package com.skyinfor.businessdistrict.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.skyinfor.businessdistrict.R;

public class IndexView extends View {
    private float mRoundWidth = 25;//圆弧宽度
    private float mRadius; // 圆弧半径
    private Paint mPaint;//画笔
    private int mCenterY;//Y轴中心值
    private int mCenterX;//宽度中心值
    private RectF mRectF;
    private String mProgressStr = "0%";//进度值
    private String mContentStr = "安居指数";//内容
    private float mProgressTextSize = 50;//进度值字体大小
    private float mContentTextSize = 30;//底下内容字体大小
    private float mDegreeWidth = 10;//刻度宽度
    private int centerWidth = 20;
    private float mInCludeAngle = 0;
    private int mIndexColor;


    public IndexView(Context context) {
        this(context, null);
    }

    public IndexView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IndexView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = getResources().obtainAttributes(attrs, R.styleable.IndexView);
        mProgressStr = a.getString(R.styleable.IndexView_IndexProgressStr);
        mContentStr = a.getString(R.styleable.IndexView_IndexDec);
        a.recycle();//释放资源
        init();
    }

    /**
     * 当layout大小变化后会回调次方法
     * 通过这方法获取宽高
     *
     * @param w
     * @param h
     * @param oldw
     * @param oldh
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mCenterX = w / 2;//控宽的中心点
        mCenterY = h / 2;//控件高的中心点
        //防止宽高不一致
        int min = Math.min(mCenterX, mCenterY);
        //半径
        mRadius = min - mRoundWidth / 2;
        //为画圆弧准备
        mRectF = new RectF(mCenterX - mRadius, mCenterY - mRadius, mCenterX + mRadius, mCenterY + mRadius);

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setStrokeWidth(mRoundWidth);
        mPaint.setColor(getResources().getColor(R.color.color_status_red));
        canvas.drawArc(mRectF, 180, 45, false, mPaint);

        mPaint.setColor(getResources().getColor(R.color.color_status_orange));
        canvas.drawArc(mRectF, 225, 90, false, mPaint);

        mPaint.setColor(getResources().getColor(R.color.colorMain));
        canvas.drawArc(mRectF, 315, 45, false, mPaint);

        mPaint.setColor(getResources().getColor(R.color.colorWhite));
        mPaint.setStrokeWidth(mDegreeWidth);
        setDegreeData(canvas);

        //画进度值文字
        mPaint.setColor(getResources().getColor(R.color.color_333333));
        mPaint.setTextSize(mProgressTextSize);
        mPaint.setStrokeWidth(0);//如果不设置回0，很难看
        float progressWidth = mPaint.measureText(mProgressStr);
        canvas.drawText(mProgressStr, mCenterX - progressWidth / 2, mCenterY + 80, mPaint);

        //画底部文字内容
        mPaint.setColor(getResources().getColor(R.color.color_search_text_gray));
        mPaint.setTextSize(mContentTextSize);
        mPaint.setStrokeWidth(0);//如果不设置回0，很难看
        float contentWidth = mPaint.measureText(mContentStr);
        canvas.drawText(mContentStr, mCenterX - contentWidth / 2, mCenterY + 130, mPaint);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        Path path = new Path();
        path.moveTo(mCenterX, mCenterY + centerWidth);
        path.lineTo(mCenterX - centerWidth, mCenterY);
        path.lineTo(mCenterX, mCenterY - mRadius + mRoundWidth);
        path.lineTo(mCenterX + centerWidth, mCenterY);
        path.moveTo(mCenterX, mCenterY + mRadius);
        path.close();

        path.setFillType(Path.FillType.WINDING);

        float index;
        if (mInCludeAngle < 3) {
            index = mInCludeAngle;
        } else if (mInCludeAngle>7f){
            index = mInCludeAngle +2;
        }else {
            index = mInCludeAngle +1;
        }

        canvas.rotate(index * 15 - 90, mCenterX, mCenterY);
        paint.setStrokeWidth(0);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(getResources().getColor(setZhiColor()));
        canvas.drawPath(path, paint);

        paint.setColor(Color.WHITE);
        canvas.drawOval(mCenterX - centerWidth / 2, mCenterY - centerWidth / 2, mCenterX + centerWidth / 2, mCenterY + centerWidth / 2, paint);

    }

    /**
     * 初始化画笔
     */
    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mRoundWidth);
    }

    /**
     * 设置进度值文字
     *
     * @param mProgressStr
     */
    public void setProgressStr(int mProgressStr) {
        this.mProgressStr = mProgressStr + "%";
        postInvalidate();

    }

    /**
     * 设置内容
     *
     * @param mContentStr
     */
    public void setContentStr(String mContentStr) {
        this.mContentStr = mContentStr;
        postInvalidate();
    }

    /**
     * 设置所有文字信息
     *
     * @param mProgressStr
     * @param mContentStr
     */
    public void setBaseStr(int mProgressStr, String mContentStr) {
        this.mProgressStr = mProgressStr + "%";
        this.mContentStr = mContentStr;
        postInvalidate();
    }

    /**
     * 设置刻度值
     *
     * @param canvas
     */
    private void setDegreeData(Canvas canvas) {
        int index = 180;
        for (int i = 0; i < 10; i++) {
            if (i == 2 || i == 7) {
                index = index + 30;
            } else {
                index += 15;
            }
            canvas.drawArc(mRectF, index, 1, false, mPaint);

        }
        mInCludeAngle = Float.valueOf(mProgressStr.split("%")[0]) / 10f;

    }

    private int setZhiColor() {
        int mIndexColor;
        if (mInCludeAngle <= 2f) {
            mIndexColor = R.color.color_status_red;
        } else if (mInCludeAngle > 7f) {
            mIndexColor = R.color.colorMain;
        } else {
            mIndexColor = R.color.color_status_orange;
        }
        return mIndexColor;
    }
}
