package com.skyinfor.businessdistrict.custom.chat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.eventbus.ChatActionEvent;
import com.skyinfor.businessdistrict.model.ChatContentModel;

import org.greenrobot.eventbus.EventBus;

public class MsgVideoView extends RelativeLayout {
    public MsgVideoView(Context context, int type, ChatContentModel mode) {
        super(context);

        initData(context, type, mode);
    }

    public MsgVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initData(Context context, int type, ChatContentModel mode) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sound, null);
        TextView text = view.findViewById(R.id.text_sound);
        if (type == 0) {
            text.setTextColor(context.getResources().getColor(R.color.color_333333));
            Drawable left = getResources().getDrawable(R.mipmap.icon_news_chat_icon_video_01_left);
            left.setBounds(0, 0, left.getMinimumWidth(), left.getMinimumHeight());
            text.setCompoundDrawables(left, null, null, null);
        } else {
            text.setTextColor(context.getResources().getColor(R.color.colorWhite));
            Drawable right = getResources().getDrawable(R.mipmap.icon_news_chat_icon_video_02_right);
            right.setBounds(0, 0, right.getMinimumWidth(), right.getMinimumHeight());
            text.setCompoundDrawables(null, null, right, null);
        }
        text.setText(mode.getContent());
        text.setBackgroundResource(type==0?R.drawable.icon_bg_talk_left:R.drawable.icon_bg_talk_right);

        addView(view);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new ChatActionEvent(4));
            }
        });
    }
}
