package com.skyinfor.businessdistrict.ui.main.stu_detail;

import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.TextureMapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.AudioPlayerView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.dialog.DialogView;
import com.skyinfor.businessdistrict.eventbus.BitmapEvent;
import com.skyinfor.businessdistrict.eventbus.TaskUpdateModel;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;
import com.skyinfor.businessdistrict.eventbus.socket.UpdateStuDetailEvent;
import com.skyinfor.businessdistrict.listener.OnMessageDialogEvent;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.ui.main.create_stu.CreateStuOrTaskActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.RelatedDeviceActivity;
import com.skyinfor.businessdistrict.ui.main.stu_detail.person_dec.PersonDecListActivity;
import com.skyinfor.businessdistrict.ui.main.video_play.VideoPlayActivity;
import com.skyinfor.businessdistrict.ui.map.MapChooseActivity;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.ImagePickerUtil;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.audio.AudioManage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusDetailActivity extends BaseActivity<StatusDetailPresent>
        implements StatusDetailContract.Model, AMap.OnMapClickListener {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_status_name)
    TextView textStatusName;
    @BindView(R.id.text_status_process)
    TextView textStatusProcess;
    @BindView(R.id.text_stu_detail_name)
    TextView textStuDetailName;
    @BindView(R.id.text_stu_detail_type)
    TextView textStuDetailType;
    @BindView(R.id.text_stu_detail_from)
    TextView textStuDetailFrom;
    @BindView(R.id.text_stu_detail_level)
    TextView textStuDetailLevel;
    @BindView(R.id.text_stu_detail_location)
    TextView textStuDetailLocation;
    @BindView(R.id.rel_stu_detail_location)
    RelativeLayout relStuDetailLocation;
    @BindView(R.id.text_stu_detail_date)
    TextView textStuDetailDate;
    @BindView(R.id.text_task_detail_name)
    TextView textTaskDetailName;
    @BindView(R.id.text_task_detail_type)
    TextView textTaskDetailType;
    @BindView(R.id.rel_task_detail_type)
    RelativeLayout relTaskDetailType;
    @BindView(R.id.text_task_detail_create_person)
    TextView textTaskDetailCreatePerson;
    @BindView(R.id.rel_task_detail_create_person)
    RelativeLayout relTaskDetailCreatePerson;
    @BindView(R.id.img_task_detail_manager)
    TextView imgTaskDetailManager;
    @BindView(R.id.rec_accept_list)
    RecyclerView recAcceptList;
    @BindView(R.id.rel_task_detail_manager)
    RelativeLayout relTaskDetailManager;
    @BindView(R.id.text_task_detail_device)
    TextView textTaskDetailDevice;
    @BindView(R.id.rel_task_detail_device)
    RelativeLayout relTaskDetailDevice;
    @BindView(R.id.text_task_detail_location)
    TextView textTaskDetailLocation;
    @BindView(R.id.rel_task_detail_location)
    RelativeLayout relTaskDetailLocation;
    @BindView(R.id.text_task_detail_date)
    TextView textTaskDetailDate;
    @BindView(R.id.rel_task_detail_date)
    RelativeLayout relTaskDetailDate;
    @BindView(R.id.rec_status_img_list)
    RecyclerView recStatusImgList;
    @BindView(R.id.lin_stu_img)
    LinearLayout linStuImg;
    @BindView(R.id.text_stu_voice)
    TextView textStuVoice;
    @BindView(R.id.img_stu_video)
    ImageView imgStuVideo;
    @BindView(R.id.layout_status)
    View layoutStu;
    @BindView(R.id.layout_task)
    View layoutTask;
    @BindView(R.id.text_stu_detail_remark)
    TextView textStuDetailRemark;
    @BindView(R.id.map_stu)
    TextureMapView mapStu;
    @BindView(R.id.map_task)
    TextureMapView mapTask;
    @BindView(R.id.text_status_btn_one)
    TextView textStatusBtnOne;
    @BindView(R.id.text_status_btn_two)
    TextView textStatusBtnTwo;
    @BindView(R.id.text_status_btn_three)
    TextView textStatusBtnThree;
    @BindView(R.id.lin_status_btn)
    LinearLayout linStatusBtn;
    @BindView(R.id.rel_video)
    View relVideo;
    @BindView(R.id.text_status_sound_end)
    AudioPlayerView textStatusSoundEnd;


    private List<UserIconBean> userIconBeans = new ArrayList<>();

    private int type;// 0 任务  1 情况
    private AMap mAMap;
    private String position;
    private String address;
    private String id;
    private String videoPath;
    private boolean isPlay;
    private String soundUrl;
    private AudioManage manage;
    private int rele_id;
    private String noticeType;
    private List<RelatedDeviceModel> deviceList = new ArrayList<>();
    private MediaMetadataRetriever retriever;
    private Thread thread;
    private String path;
    private String videoName;
    private TaskSonModel model;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_status_detail;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        if (type == 0) {
            mapTask.onCreate(savedInstanceState);

        } else {
            mapStu.onCreate(savedInstanceState);
        }
    }

    @Override
    protected void initView() {
        super.initView();
        EventBus.getDefault().register(this);
        ImagePickerUtil.initImageMorePicker();

        type = getIntent().getIntExtra("type", 0);
        rele_id = getIntent().getIntExtra("rele_id", 0);
        noticeType = getIntent().getStringExtra("notice_type");

        mPresenter.initAdapter(recStatusImgList);

        switch (type) {
            case 0:
                noticeType = "task";
                mPresenter.initAcceptAdapter(recAcceptList);
                layoutStu.setVisibility(View.GONE);
                layoutTask.setVisibility(View.VISIBLE);

                break;
            case 1:
                noticeType = "condition";
                layoutStu.setVisibility(View.VISIBLE);
                layoutTask.setVisibility(View.GONE);

                break;
        }

        if (rele_id != 0) {
            linStatusBtn.setVisibility(View.GONE);
            mPresenter.getNoticeAction(String.valueOf(rele_id), noticeType, false);
        }

        tvTitle.setText(type == 0 ? "任务详情" : "情况详情");
    }

    @Override
    public void setTaskData(TaskSonModel model) {
        this.model = model;
        userIconBeans.clear();
        textStatusName.setText(model.getName());//名称
        textTaskDetailName.setText(model.getName());
        textTaskDetailDate.setText(model.getCreate_time());//日期
        textTaskDetailType.setText(model.getType_name());
        textTaskDetailCreatePerson.setText(model.getSend_user_nickname());
        if (model.getAccept_user_list() != null && model.getAccept_user_list().size() > 0) {
            for (TaskSonModel.AcceptUserListBean userListBean :
                    model.getAccept_user_list()) {
                UserIconBean item = new UserIconBean();
                item.setAvatar(userListBean.getAvatar());
                item.setNickname(userListBean.getNickname());
                userIconBeans.add(item);
            }
            mPresenter.setAcceptNotify(userIconBeans);
        }

        if (!TextUtils.isEmpty(model.getPicture())) {
            mPresenter.setImgNotify(model.getPicture());
        }

        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < model.getRelation_device().size(); i++) {
            TaskSonModel.RelationDeviceBean data = model.getRelation_device().get(i);
            List<TaskSonModel.RelationDeviceBean.deviceBean> beanList = data.getList();
            RelatedDeviceModel relatedDeviceModel = new RelatedDeviceModel();
            relatedDeviceModel.setName(data.getType());
            List<RelatedDeviceModel.ListBean> dataList = new ArrayList<>();
            dataList.clear();

            for (int j = 0; j < beanList.size(); j++) {
                buffer.append(String.format("%s,", beanList.get(j).getDevice_name()));

                RelatedDeviceModel.ListBean listBean = new RelatedDeviceModel.ListBean();
                listBean.setShow(true);
                listBean.setName(beanList.get(j).getDevice_name());//设置二级数据list
                dataList.add(listBean);
            }
            relatedDeviceModel.setExpand(true);
            relatedDeviceModel.setList(dataList);
            deviceList.add(relatedDeviceModel);
        }
        if (!TextUtils.isEmpty(buffer.toString())) {
            textTaskDetailDevice.setText(buffer.toString());
        }

        textTaskDetailLocation.setText(model.getAddress());

        textStatusProcess.setText(model.getStatus_name());

        setColor(Integer.parseInt(model.getStatus_code()), textStatusProcess);
        textStuDetailRemark.setText(model.getDetails());
        position = model.getPosition();
        address = model.getAddress();
        if (!TextUtils.isEmpty(model.getPosition())) {
            final String location[] = model.getPosition().split(",");
            setUpMap(mapTask, location);
        }
        setTaskBtnStatus(Integer.parseInt(model.getStatus_code()));
        videoPath = model.getVideo();
        relVideo.setVisibility(TextUtils.isEmpty(videoPath) == true ? View.GONE : View.VISIBLE);

        id = String.valueOf(model.getId());
        if (!TextUtils.isEmpty(model.getVideo())) {
            createVideoThumbnail(model.getVideo());
        }

        if (!TextUtils.isEmpty(model.getAudio())) {
            textStatusSoundEnd.setVisibility(View.VISIBLE);
            soundUrl = model.getAudio();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    textStatusSoundEnd.setUrl(soundUrl);
                }
            }).start();
        }

    }

    @Override
    public void setStatusData(StatusSonModel model) {
        textStatusName.setText(model.getName());//名称
        setStuIconLevel(model.getLevel());
        textStuDetailName.setText(model.getName());//名称
        textStuDetailType.setText(model.getType_name());//类型
        textStuDetailLevel.setText(model.getLevel());//等级
        textStuDetailLocation.setText(model.getAddress());//地址
        textStuDetailDate.setText(model.getCreate_time());//日期
        textStuDetailRemark.setText(model.getDetails());//备注


        if (!TextUtils.isEmpty(model.getPicture())) {
            mPresenter.setImgNotify(model.getPicture());
        }
        textStatusProcess.setText(model.getStatus_name());
        setColor(model.getStatus_code(), textStatusProcess);
        textStuDetailFrom.setText(model.getNickname());
        position = model.getPosition();
        address = model.getAddress();
        if (!TextUtils.isEmpty(model.getPosition())) {
            final String location[] = model.getPosition().split(",");
            setUpMap(mapStu, location);
        }
        setStuBtnStatus(model.getStatus_code());
        videoPath = model.getVideo();

        id = String.valueOf(model.getId());
        relVideo.setVisibility(TextUtils.isEmpty(videoPath) == true ? View.GONE : View.VISIBLE);

        if (!TextUtils.isEmpty(model.getVideo())) {
            createVideoThumbnail(model.getVideo());
        }

        if (!TextUtils.isEmpty(model.getAudio())) {
            textStatusSoundEnd.setVisibility(View.VISIBLE);
            soundUrl = model.getAudio();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    textStatusSoundEnd.setUrl(soundUrl);
                }
            }).start();
        }

    }

    /**
     * 改变状态之后，更新新界面值
     */
    @Override
    public void setNotifyView() {
        mPresenter.getNoticeAction(String.valueOf(rele_id), noticeType, true);
    }

    @Override
    public void finishAction() {
        appManager.finishActivity();

    }

    @Override
    public void setNotifyStu(int status, String name) {
        if (noticeType.equals("task")) {
            setTaskBtnStatus(status);
        } else {
            setStuBtnStatus(status);
        }
        textStatusProcess.setText(name);
        setColor(status, textStatusProcess);
    }

    private void setColor(int index, TextView tv) {
        tv.setCompoundDrawables(null, null, null, null);

        switch (index) {
            case 1:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
            case 2:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_orange));
                break;

            case 3:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_blue));
                break;
            case 4:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_green));

                break;

            case 5:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                tv.setText("");
                int layId = R.mipmap.icon_task_inject;
                Drawable drawable = mContext.getResources().getDrawable(layId);
                drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());// 设置边界
                tv.setCompoundDrawables(drawable, null, null, null);
                break;

        }
    }

    private void setStuIconLevel(String level) {
        int layId = 0;
        switch (level) {
            case "一级":
                layId = R.mipmap.icon_stu_one;
                break;
            case "二级":
                layId = R.mipmap.icon_stu_two;

                break;

            case "三级":
                layId = R.mipmap.icon_stu_three;

                break;
        }

        Drawable drawable = mContext.getResources().getDrawable(layId);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());// 设置边界
        textStatusName.setCompoundDrawables(drawable, null, null, null);
    }

    /**
     * 设置一些amap的属性
     */
    private void setUpMap(TextureMapView mMapView, String[] position) {
        mAMap = mMapView.getMap();
        mAMap.getUiSettings().setAllGesturesEnabled(false);
        mAMap.getUiSettings().setZoomGesturesEnabled(false);
        mAMap.getUiSettings().setRotateGesturesEnabled(false);
        mAMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
        mAMap.getUiSettings().setZoomControlsEnabled(false);//放大缩小按钮是否显示
        mAMap.setMyLocationEnabled(false);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
//        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        mAMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
        mAMap.moveCamera(CameraUpdateFactory.zoomTo(17));
        mAMap.setOnMapClickListener(this);
        //将地图移动到定位点
        mAMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(Double.valueOf(position[1]), Double.valueOf(position[0]))));

        MarkerOptions markerOption = new MarkerOptions();
        LatLng x = new LatLng(Double.valueOf(position[1]), Double.valueOf(position[0]));
        markerOption.position(x);
//        markerOption.title(name);
        markerOption.perspective(false);
        markerOption.draggable(false);
        markerOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.icon_location));//设置图标
        mAMap.addMarker(markerOption);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Bundle data = new Bundle();
        data.putString("strLag", position);
        data.putString("address", address);
        UIHelper.startActivity(mContext, MapChooseActivity.class, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (type == 0) {
            if (mapTask != null)
                mapTask.onResume();

        } else {
            if (mapStu != null)
                mapStu.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (type == 0) {
            if (mapTask != null)
                mapTask.onPause();

        } else {
            if (mapStu != null)
                mapStu.onPause();
        }
        if (!TextUtils.isEmpty(soundUrl)){
            textStatusSoundEnd.stopPlay();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getEvent(BitmapEvent event) {
        GlideUtils.loadImg(event.getLocalStr(), imgStuVideo);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getSocketUpdataEvent(UpdateStuDetailEvent event) {
        noticeType = event.getType();
        if (event.getStatus() == 6) {
            ToashUtils.show(mContext, "当前任务已被删除！");
            EventBus.getDefault().post(new UpdateTaskEvent(Integer.valueOf(3)));
            appManager.finishActivity();
        } else {
            setNotifyView();
        }

    }

    @Subscribe
    public void updateStatus(TaskUpdateModel event) {
        mPresenter.statusChange(id, event.getStatus() + "", 0);

    }


    @Override
    protected void onDestroy() {
        if (!TextUtils.isEmpty(soundUrl)) {
            try {
                textStatusSoundEnd.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (type == 0) {
            if (mapTask != null)
                mapTask.onDestroy();

        } else {
            if (mapStu != null)
                mapStu.onDestroy();
        }
        EventBus.getDefault().unregister(this);
        super.onDestroy();

    }

    @OnClick(R.id.video_status_video)
    public void onViewClicked() {
        if (!TextUtils.isEmpty(videoPath)) {
            Bundle data = new Bundle();
            data.putString("uri", videoPath);
            data.putString("title", videoName);
            data.putString("imgUrl", path);
            UIHelper.startActivity(mContext, VideoPlayActivity.class, data);
        }
    }

    @OnClick({R.id.text_status_btn_one, R.id.text_status_btn_two, R.id.text_status_btn_three
            , R.id.rel_task_detail_device, R.id.rel_task_detail_manager})
    public void onViewClicked(View view) {
        String oneStr = textStatusBtnOne.getText().toString();
        String twoStr = textStatusBtnTwo.getText().toString();
        String threeStr = textStatusBtnThree.getText().toString();

        switch (view.getId()) {
            case R.id.text_status_btn_one:
                setOnClickOneAction(oneStr);

                break;
            case R.id.text_status_btn_two:
                setOnClickTwoAction(twoStr);

                break;
            case R.id.text_status_btn_three:
                setOnClickThreeClick(threeStr);

                break;
            case R.id.rel_task_detail_device:
                Bundle deviceData = new Bundle();
                deviceData.putInt("type", 0);
                deviceData.putSerializable("model", (Serializable) deviceList);
                UIHelper.startActivityForResult(mContext, RelatedDeviceActivity.class, 500, deviceData);

                break;
            case R.id.rel_task_detail_manager:
                Bundle data = new Bundle();
                data.putSerializable("model", (Serializable) userIconBeans);
                UIHelper.startActivity(mContext, PersonDecListActivity.class, data);

                break;
        }
    }

    /**
     * 管理员只做审核
     *
     * @param status
     */
    private void setStuBtnStatus(int status) {
        linStatusBtn.setVisibility(View.VISIBLE);

        switch (status) {
            //待处理
            case 1:
                if (Constants.STATUS_TYPE == 0) {
                    textStatusBtnOne.setVisibility(View.GONE);
                    textStatusBtnTwo.setVisibility(View.GONE);
                    if (!Constants.PRIVILEGE_TASK) {
                        textStatusBtnThree.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setText("开始处理");
                    } else {
                        linStatusBtn.setVisibility(View.GONE);
                    }

                } else {
                    linStatusBtn.setVisibility(View.GONE);
                }

                break;
            //处理中
            case 2:
                textStatusBtnOne.setVisibility(View.GONE);
                textStatusBtnTwo.setVisibility(View.GONE);
                if (Constants.STATUS_TYPE == 0) {
                    if (!Constants.PRIVILEGE_TASK) {
                        textStatusBtnThree.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setText("处理完成");
                    } else {
                        linStatusBtn.setVisibility(View.GONE);
                    }

                } else {
                    linStatusBtn.setVisibility(View.GONE);
                }

                break;
            //待审核
            case 3:
                if (Constants.STATUS_TYPE == 0) {
                    if (Constants.PRIVILEGE_TASK) {
                        textStatusBtnOne.setVisibility(View.VISIBLE);
                        textStatusBtnTwo.setVisibility(View.VISIBLE);
                        linStatusBtn.setVisibility(View.VISIBLE);
                        textStatusBtnOne.setText("派发任务");
                        textStatusBtnTwo.setText("重新执行");
                        textStatusBtnThree.setText("通过审核");
                    } else {
                        linStatusBtn.setVisibility(View.GONE);
                    }
                } else {
                    linStatusBtn.setVisibility(View.GONE);
                }
                break;

            //完成
            case 4:
                linStatusBtn.setVisibility(View.GONE);

                break;
        }

    }

    /**
     * 管理员：派发权限  处置者：无
     * <p>
     * 我派发的：待审核 已完成
     * <p>
     * 派发我的：待处理 处理中
     *
     * @param status
     */
    private void setTaskBtnStatus(int status) {
        linStatusBtn.setVisibility(View.VISIBLE);

        switch (status) {
            //待处理
            case 1:
                if (Constants.TASK_TYPE == 1) {
                    if (Constants.PRIVILEGE_TASK) {
                        linStatusBtn.setVisibility(View.GONE);
                    } else {
                        textStatusBtnOne.setVisibility(View.VISIBLE);
                        textStatusBtnTwo.setVisibility(View.GONE);
                        textStatusBtnThree.setVisibility(View.VISIBLE);
                        textStatusBtnOne.setText("拒绝任务");
                        textStatusBtnThree.setText("接受任务");
                    }

                } else {
                    linStatusBtn.setVisibility(View.GONE);
                }

                break;
            //处理中
            case 2:
                if (Constants.TASK_TYPE == 1) {
                    if (Constants.PRIVILEGE_TASK) {
                        linStatusBtn.setVisibility(View.GONE);
                    } else {
                        textStatusBtnOne.setVisibility(View.GONE);
                        textStatusBtnTwo.setVisibility(View.GONE);
                        textStatusBtnThree.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setText("完成任务");
                    }
                } else {
                    linStatusBtn.setVisibility(View.GONE);

                }

                break;
            //待审核
            case 3:
                if (Constants.TASK_TYPE == 1) {
                    linStatusBtn.setVisibility(View.GONE);
                } else {
                    if (Constants.PRIVILEGE_TASK) {
                        textStatusBtnOne.setVisibility(View.VISIBLE);
                        textStatusBtnTwo.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setText("删除任务");
                        textStatusBtnTwo.setText("重新执行");
                        textStatusBtnThree.setText("通过审核");
                    } else {
                        linStatusBtn.setVisibility(View.GONE);
                    }
                }

                break;
            //已完成
            case 4:
                linStatusBtn.setVisibility(View.GONE);

                break;
            //已拒绝
            case 5:
                if (Constants.TASK_TYPE == 1) {
                    linStatusBtn.setVisibility(View.GONE);
                } else {
                    if (Constants.PRIVILEGE_TASK) {
                        textStatusBtnOne.setVisibility(View.VISIBLE);
                        textStatusBtnTwo.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setVisibility(View.VISIBLE);
                        textStatusBtnThree.setText("删除任务");
                        textStatusBtnTwo.setText("继续执行");
                        textStatusBtnThree.setText("重新分配");
                    } else {
                        linStatusBtn.setVisibility(View.GONE);
                    }
                }

                break;
        }
    }

    /**
     * 设置Btn One点击事件
     *
     * @param oneStr
     */
    private void setOnClickOneAction(String oneStr) {
        switch (type) {
            case 0:
                if (oneStr.equals("拒绝任务")) {
                    setDialogShow(oneStr, 5);
                } else if (oneStr.equals("删除任务")) {
                    setDialogShow(oneStr, 6);
                }
                break;
            case 1:
                if (oneStr.equals("派发任务")) {
                    UIHelper.startActivity(mContext, CreateStuOrTaskActivity.class);
                }
                break;
        }
    }

    /**
     * 设置Btn Two点击事件
     *
     * @param twoStr
     */
    private void setOnClickTwoAction(String twoStr) {
        switch (type) {
            case 0:
                if (twoStr.equals("重新执行") || twoStr.equals("继续执行")) {
                    setDialogShow(twoStr, 2);
                }

                break;
            case 1:
                if (twoStr.equals("重新执行")) {
                    setDialogShow(twoStr, 2);
                }
                break;
        }
    }


    /**
     * 设置Btn three点击事件
     *
     * @param threeStr
     */
    private void setOnClickThreeClick(String threeStr) {
        switch (type) {
            case 0:
                if (threeStr.equals("接受任务")) {
                    setDialogShow(threeStr, 2);
                } else if (threeStr.equals("完成任务")) {
                    setDialogShow(threeStr, 3);

                } else if (threeStr.equals("重新分配")) {
//                    setDialogShow(threeStr, 2);
                    Bundle data = new Bundle();
                    data.putSerializable("model", model);
                    data.putInt("type", 3);
                    UIHelper.startActivity(mContext, CreateStuOrTaskActivity.class, data);

                } else if (threeStr.equals("通过审核")) {
                    setDialogShow(threeStr, 4);
                }

                break;
            case 1:
                if (threeStr.equals("开始处理")) {
                    setDialogShow(threeStr, 2);

                } else if (threeStr.equals("派发任务")) {
                    UIHelper.startActivity(mContext, CreateStuOrTaskActivity.class);
                } else if (threeStr.equals("处理完成")) {
                    setDialogShow(threeStr, 3);

                } else if (threeStr.equals("通过审核")) {
                    setDialogShow(threeStr, 4);
                }
                break;

        }
    }


    private void setDialogShow(String str, final int dataType) {
        DialogView dialog = new DialogView(mContext, "",
                String.format("是否确认%s?", str), "确认", "取消");
        dialog.show();
        dialog.setDialogEvent(new OnMessageDialogEvent() {
            @Override
            public void ok(Dialog dialog) {
                switch (dataType) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5: //拒绝
                        mPresenter.statusChange(id, dataType + "", type);
                        break;
                    case 6://删除
                        mPresenter.deleteTask(id);
                        break;
                }
            }

            @Override
            public void close(Dialog dialog) {
                dialog.dismiss();
            }
        });
    }

    private void createVideoThumbnail(final String filePath) {
        String data[] = filePath.split("/");
        path = "/sdcard/" + data[data.length - 1] + ".jpg";
        File file = new File(path);

        if (file.exists()) {
            GlideUtils.loadImg(path, imgStuVideo);

        } else {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    setFFemg(filePath);

                }
            });
            thread.start();
        }
    }
//
//    @OnClick(R.id.text_status_sound_end)
//    public void onSoundClicked() {
////        if (manage == null) {
////            manage = new AudioManage();
////        }
////        isPlay = !isPlay;
////        if (isPlay) {
////            manage.startPlay(mContext, soundUrl);
////        } else {
////            manage.stopPlay();
////        }
//
//
//    }

    private void setFFemg(String filePath) {
        String data[] = filePath.split("/");
        path = "/sdcard/" + data[data.length - 1] + ".jpg";
        videoName = data[data.length - 1];
        String cmd = String.format("-i %s -y -f image2 -t 0.001 -s %s %s "
                , filePath, "704X576", path);

        String[] command = cmd.split(" ");
        try {
            FFmpeg.getInstance(mContext).execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.e("失败", s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.e("成功", s);
                }

                @Override
                public void onStart() {
                    Log.e("开始", "1");
                }

                @Override
                public void onFinish() {
                    Log.e("完成", "3");
                    EventBus.getDefault().post(new BitmapEvent(path));
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }

}
