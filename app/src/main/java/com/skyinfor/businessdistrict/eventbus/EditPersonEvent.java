package com.skyinfor.businessdistrict.eventbus;

public class EditPersonEvent {
    private String str;
    private int type;

    public EditPersonEvent() {
    }

    public EditPersonEvent(String str, int type) {
        this.str = str;
        this.type = type;
    }

    public String getStr() {
        return str;
    }

    public int getType() {
        return type;
    }
}
