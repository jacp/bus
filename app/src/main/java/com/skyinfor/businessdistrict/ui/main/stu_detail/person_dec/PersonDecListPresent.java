package com.skyinfor.businessdistrict.ui.main.stu_detail.person_dec;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.PersonDecListAdapter;
import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.util.recyclerview.SpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class PersonDecListPresent extends BasePresenter<PersonDecListContract.Model>
        implements PersonDecListContract.Presenter {

    private List<UserIconBean> list=new ArrayList<>();
    private PersonDecListAdapter adapter;

    @Inject
    public PersonDecListPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new PersonDecListAdapter(mActivity,list);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, 5));
        recyclerView.addItemDecoration(new SpaceItemDecoration(dp2px(16, mActivity)));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setNotify(List<UserIconBean> mList) {
        list.addAll(mList);
        adapter.notifyDataSetChanged();
    }

}
