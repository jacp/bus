package com.skyinfor.businessdistrict.eventbus;

public class ChatActionEvent {

    private int type;

    public ChatActionEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
