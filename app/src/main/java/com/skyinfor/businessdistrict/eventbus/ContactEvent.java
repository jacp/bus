package com.skyinfor.businessdistrict.eventbus;

public class ContactEvent {
    private int type;
    private String uuid;

    public ContactEvent(int type, String uuid) {
        this.type = type;
        this.uuid = uuid;
    }

    public ContactEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getUuid() {
        return uuid;
    }
}
