package com.skyinfor.businessdistrict.ui.setting;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class SettingContract {

    interface Model extends MModel {
    }

    interface Presenter extends IPresenter<Model> {

    }

}
