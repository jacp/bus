package com.skyinfor.businessdistrict.util;

import android.view.View;
import android.view.ViewGroup;

import com.codbking.calendar.CalendarBean;

/**
 * Created by min on 2017/4/19.
 */

public interface CalederInterface {
    View getView(View convertView, ViewGroup parentView, CalendarBean bean);

}
