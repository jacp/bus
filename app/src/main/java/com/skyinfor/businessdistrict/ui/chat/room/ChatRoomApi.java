package com.skyinfor.businessdistrict.ui.chat.room;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.model.FileModel;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface ChatRoomApi {

    @Multipart
    @POST(ApiUrl.talkList)
    Observable<Response<List<ChatContentModel>>> talkList(@Part List<MultipartBody.Part> part);

    @Multipart
    @POST(ApiUrl.talkGroupList)
    Observable<Response<List<ChatContentModel>>> talkGroupList(@Part List<MultipartBody.Part> part);

    @Multipart
    @POST(ApiUrl.talkRecordSave)
    Observable<Response<Object>> talkRecordSave(@Part List<MultipartBody.Part> part);

    @Multipart
    @POST(ApiUrl.talkGroupRecordSave)
    Observable<Response<Object>> talkGroupRecordSave(@Part List<MultipartBody.Part> part);


    @FormUrlEncoded
    @POST(ApiUrl.talkRecordStatus)
    Observable<Response<Object>> talkRecordStatus(@Field("user_id") String user_id,
                                                  @Field("target_id") String target_id );

    @Multipart
    @POST(ApiUrl.commonUpload)
    Observable<Response<FileModel>> upFile(@Part List<MultipartBody.Part> part);

    @FormUrlEncoded
    @POST(ApiUrl.talkGroupInformation)
    Observable<Response<GroupMemberModel>> talkGroupInformation(@Field("group_uuid") String group_uuid);

}
