package com.skyinfor.businessdistrict.ui.main.contact;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;
import com.skyinfor.businessdistrict.ui.main.MainContract;

public class ContactSearchContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void searchUser(String name);
        void setNotify();
    }

}
