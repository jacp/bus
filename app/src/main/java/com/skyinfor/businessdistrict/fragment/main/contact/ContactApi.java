package com.skyinfor.businessdistrict.fragment.main.contact;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.ContactAllModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public interface ContactApi {

    @GET(ApiUrl.userList)
    Observable<Response<List<ContactAllModel>>>userList();

    @GET(ApiUrl.getFullList)
    Observable<Response<List<Object>>>getFullList(@Query("user_id") String user_id);


}
