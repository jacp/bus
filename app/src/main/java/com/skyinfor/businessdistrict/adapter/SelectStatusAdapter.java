package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.StatusTypeModel;
import com.skyinfor.businessdistrict.model.TaskModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class SelectStatusAdapter<T> extends BaseMyRecyclerVIewAdapter<T> {

    public int mPosition = 0;
    private int type;

    public SelectStatusAdapter(Context context, List<T> list, int type) {
        super(context, list, R.layout.item_select_status_type);
        this.type = type;
        mList = list;
    }

    @Override
    protected void covert(T mode, BaseViewHolder holder, final int position) {
        super.covert(mode, holder, position);

        final TextView box = holder.getView(R.id.checkbox_status);
        TextView text = holder.getView(R.id.text_status_level);
        box.setActivated(position == mPosition);
        text.setVisibility(type == 0 ? View.VISIBLE : View.GONE);

        switch (type) {
            case 0:
                StatusTypeModel bean = (StatusTypeModel) mode;

                box.setText(bean.getName());
                text.setText(bean.getLevel());
                break;
            case 1:
                TaskModel data = (TaskModel) mode;
                box.setText(data.getName());

                break;
        }

        box.setSelected(mPosition == position);

    }

}
