package com.skyinfor.businessdistrict.fragment.main.contact;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.eventbus.ContactEvent;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.ui.main.contact.ContactSearchActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class ContactFragment extends BaseFragment<ContactPresent> implements ContactContract.Model {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_back)
    TextView tvBack;
    @BindView(R.id.rec_contact_department)
    RecyclerView recContactDepartment;
    @BindView(R.id.rec_contact_list)
    RecyclerView recContactList;
    @BindView(R.id.rec_contact_son)
    RecyclerView recContactSon;
    @BindView(R.id.rec_contact_chat)
    RecyclerView recContactChat;
    @BindView(R.id.title_bar_contact)
    View title;
    @BindView(R.id.edit_contact)
    View editContact;
    @BindView(R.id.rec_contact_icon)
    RecyclerView recContactIcon;
    @BindView(R.id.lin_contact_chat)
    LinearLayout linContactChat;
    private boolean isSelect;
    private List<PersonListBean> personListBeanList;
    private boolean isShowSelf = true;
    private HttpDialog dialog;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .appComponent(appComponent)
                .fragmentModule(fragmentModule)
                .build()
                .inject(this);

    }

    public static ContactFragment getInstance(boolean isSelect) {
        ContactFragment fragment = new ContactFragment();
        fragment.isSelect = isSelect;
        return fragment;
    }

    public static ContactFragment getInstance(boolean isSelect, List<PersonListBean> personList) {
        ContactFragment fragment = new ContactFragment();
        fragment.isSelect = isSelect;
        fragment.personListBeanList = personList;
        return fragment;
    }

    public static ContactFragment getInstance(boolean isSelect, List<PersonListBean> personList, boolean isShowSelf) {
        ContactFragment fragment = new ContactFragment();
        fragment.isSelect = isSelect;
        fragment.personListBeanList = personList;
        fragment.isShowSelf = isShowSelf;
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_contact;
    }

    @Override
    protected void initEventAndData() {
        super.initEventAndData();
        initContact();
    }

    @Override
    public void setSelectList(List<PersonListBean> personListBeanList) {
        this.personListBeanList = personListBeanList;
    }

    @Override
    public void setDialog(HttpDialog dialog) {
        this.dialog=dialog;
    }

    public List<PersonListBean> getPersonListBeanList() {
        return personListBeanList;
    }

    public void setPersonListBeanList(List<PersonListBean> personListBeanList) {
        this.personListBeanList = personListBeanList;
    }


    @OnClick(R.id.edit_contact)
    public void onViewClicked() {
        UIHelper.startActivity(mContext, ContactSearchActivity.class);
    }

    private void initContact(){
        super.initData();
        title.setVisibility(isSelect == true ? View.GONE : View.VISIBLE);
        editContact.setVisibility(isSelect == true ? View.GONE : View.VISIBLE);
        recContactIcon.setVisibility(isSelect == false ? View.GONE : View.VISIBLE);
        linContactChat.setVisibility(isSelect == false ? View.VISIBLE : View.GONE);

        tvBack.setVisibility(View.GONE);
        tvTitle.setText("通讯录");
        mPresenter.dialog.show();

        mPresenter.initAdapter(recContactIcon, recContactDepartment, recContactList, recContactSon, recContactChat, isSelect, isShowSelf);
        if (!isSelect) {
            EventBus.getDefault().register(this);
            mPresenter.getFullList(personListBeanList,0);
        } else {
            mPresenter.getContactList(personListBeanList);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void getEvent(ContactEvent event){
        if (event.getType()==0){
            mPresenter.getFullList(personListBeanList,1);
        }else {
            mPresenter.delGroup(event.getUuid());
        }
    }

}
