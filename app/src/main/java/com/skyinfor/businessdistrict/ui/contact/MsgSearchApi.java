package com.skyinfor.businessdistrict.ui.contact;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.ConversationModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

public interface MsgSearchApi {

    @FormUrlEncoded
    @POST(ApiUrl.talkUserList)
    Observable<Response<List<ConversationModel>>> talkUserList(@Field("user_id") String user_id,
                                                               @Field("keyword")String keyword);

    @FormUrlEncoded
    @POST(ApiUrl.talkRecordStatus)
    Observable<Response<Object>> talkRecordStatus(@Field("user_id") String user_id,
                                                  @Field("target_id") String target_id );


}
