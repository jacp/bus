package com.skyinfor.businessdistrict.ui.main.notice;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.model.Response;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface NoticeApi {

    @GET(ApiUrl.noticeList)
    Observable<Response<HomeNoticeModel>> noticeList(@Query("user_id") String user_id, @Query("page") String page,
                                                     @Query("unit") String limit);

}
