package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.ContactGroupModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ContactGroupAdapter extends BaseMyRecyclerVIewAdapter<ContactGroupModel.GroupListBean> {

    public ContactGroupAdapter(Context context, List list) {
        super(context, list, R.layout.item_contact_group);
    }

    @Override
    protected void covert(ContactGroupModel.GroupListBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        IconView iconView=holder.getView(R.id.text_contact_group);
        iconView.setBaseInfo(mContext,"群聊","");
        holder.setText(R.id.text_contact_group_name, mode.getGroup_name());
    }
}
