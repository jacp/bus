package com.skyinfor.businessdistrict.base;


import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.google.gson.Gson;
import com.litesuits.orm.LiteOrm;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerAppComponent;
import com.skyinfor.businessdistrict.di.module.AppModule;
import com.skyinfor.businessdistrict.net.Network;
import com.skyinfor.businessdistrict.util.Ext;
import com.skyinfor.businessdistrict.util.ScreenListener;
import com.skyinfor.businessdistrict.util.ViewUtils;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.smtt.sdk.QbSdk;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import io.agora.AgoraAPIOnlySignal;

import static com.skyinfor.businessdistrict.cache.CacheLoader.getApplication;


/**
 * Created by min on 2017/3/1.
 */

public class MyApplication extends Application {
    private String TAG = MyApplication.class.getSimpleName();
    private static MyApplication mInstance;
    private static final String realmName = "hf.realm";
    public static LiteOrm liteOrm;
    public Gson gson;

    //依赖注入
    private static AppComponent mAppComponent;
    public static int USER_TYPE;
    public static String USER_ID = "";
    public static String appToken = "123456"; // 用户登录token
    public static String APPID = "ANDROID-1.0.0";//appid
    public static Application application;

    public static Context applicationContext;
    private static MyApplication instance;
    public static String currentUserNick = "";
    private AgoraAPIOnlySignal m_agoraAPI;


    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        MultiDex.install(this);


      /*  if (BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(
                    new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
        }*/
        /**
         *  极光推送
         */
        mInstance = this;
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        dagger();
        initData();

        //        GlideCacheUtil.getInstance().clearImageAllCache(getApplication());
        // bugly 配置  初始化Bugly
        CrashReport.initCrashReport(this, "05682fcaa1", false);
        Context context = this;
        // 获取当前包名
        String packageName = context.getPackageName();
        // 获取当前进程名
        String processName = getProcessName(android.os.Process.myPid());
        // 设置是否为上报进程
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(context);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        initTencentWebView(); //初始化tencent WebView
        setupAgoraEngine();
        setScreenAction();
        initFFmpegBinary(this);
    }


    private void initTencentWebView() {
        //初始化X5内核
        QbSdk.initX5Environment(getApplication(), new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {
                //x5内核初始化完成回调接口，此接口回调并表示已经加载起来了x5，有可能特殊情况下x5内核加载失败，切换到系统内核。

            }

            @Override
            public void onViewInitFinished(boolean b) {
                //x5內核初始化完成的回调，为true表示x5内核加载成功，否则表示x5内核加载失败，会自动切换到系统内核。
                Log.e("@@", "加载内核是否成功:" + b);
            }
        });

    }


    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    private void initData() {
        Constants.readSpUserInfo();
//        Constants.readSpUserInfoModel();
        if (liteOrm == null) {
            liteOrm = LiteOrm.newSingleInstance(getApplication(), "liteorm.db");
        }
        liteOrm.setDebugged(true); // open the log
        gson = new Gson();
    }

    private void dagger() {
        initExtension();
    }

    private List<Object> getModules() {
        return Arrays.<Object>asList(new AppModule(this));
    }

    /**
     * 当前实例
     *
     * @return
     */
    public static MyApplication getInstance() {
        return mInstance;
    }


    //获取当前版本信息
    private void initExtension() {
        Ext.init(getApplication(), new ExtImpl());
    }

    public static final class ExtImpl extends Ext {

        @Override
        public String getCurOpenId() {
            return null;
        }

        @Override
        public String getDeviceInfo() {
            return null;
        }

        @Override
        public String getPackageNameForResource() {
            return "com.skyinfor.businessdistrict";
        }

        @Override
        public int getScreenHeight() {
            return ViewUtils.getScreenHeight();
        }

        @Override
        public int getScreenWidth() {
            return ViewUtils.getScreenWidth();
        }

        @Override
        public boolean isAvailable() {
            return Network.isAvailable();
        }

        @Override
        public boolean isWap() {
            return Network.isWap();
        }

        @Override
        public boolean isMobile() {
            return Network.isMobile();
        }

        @Override
        public boolean is2G() {
            return Network.is2G();
        }

        @Override
        public boolean is3G() {
            return Network.is3G();
        }

        @Override
        public boolean isWifi() {
            return Network.isWifi();
        }

        @Override
        public boolean isEthernet() {
            return Network.isEthernet();
        }
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }

    public AgoraAPIOnlySignal getmAgoraAPI() {
        return m_agoraAPI;
    }


    private void setupAgoraEngine() {
        String appID = getString(R.string.agora_app_id);

        try {
            m_agoraAPI = AgoraAPIOnlySignal.getInstance(this, appID);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));

            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    private void setScreenAction() {
        ScreenListener screenListener = new ScreenListener(this);
        screenListener.begin(new ScreenListener.ScreenStateListener() {
            @Override
            public void onScreenOn() {
                Constants.IS_LOCK = false;
            }

            @Override
            public void onScreenOff() {
                Constants.IS_LOCK = true;
            }

            @Override
            public void onUserPresent() {
                Constants.IS_LOCK = false;
            }
        });

    }


    private void initFFmpegBinary(Context context) {

        try {
            FFmpeg.getInstance(context).loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                }
            });

        } catch (FFmpegNotSupportedException e) {
            e.printStackTrace();
        }
    }

}
