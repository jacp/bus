package com.skyinfor.businessdistrict.model;

import java.io.Serializable;

public class TaskModel implements Serializable{


    /**
     * id : 1
     * name : 日常任务
     */

    private int id;
    private String name;
    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
