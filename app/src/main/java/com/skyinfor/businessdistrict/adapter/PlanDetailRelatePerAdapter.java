package com.skyinfor.businessdistrict.adapter;

import android.content.Context;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.model.PlanRelatedModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class PlanDetailRelatePerAdapter extends BaseMyRecyclerVIewAdapter<PlanRelatedModel> {

    public PlanDetailRelatePerAdapter(Context context, List<PlanRelatedModel> list) {
        super(context, list, R.layout.item_plan_detail_per);

    }

    @Override
    protected void covert(PlanRelatedModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        holder.setText(R.id.text_plan_per_name,mode.getNickname());
        IconView iconView=holder.getView(R.id.icon_plan_per,position);
        iconView.setBaseInfo(mContext,mode.getNickname(),mode.getAvatar());
    }
}
