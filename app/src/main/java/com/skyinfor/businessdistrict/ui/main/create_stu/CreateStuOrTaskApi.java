package com.skyinfor.businessdistrict.ui.main.create_stu;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.FileModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusSonModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/24.
 */

public interface CreateStuOrTaskApi {

    @Multipart
    @POST(ApiUrl.conditionUpload)
    Observable<Response<Object>> acceptList(@Part List<MultipartBody.Part> part);

    @Multipart
    @POST(ApiUrl.conditionSendList)
    Observable<Response<Object>> conditionSendList(@Part List<MultipartBody.Part> part);

    @Multipart
    @POST(ApiUrl.commonUpload)
    Observable<Response<FileModel>> upFile(@Part List<MultipartBody.Part> part);


}
