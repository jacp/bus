package com.skyinfor.businessdistrict.model.analysis;

public class AnalysisOnlineModel {

    /**
     * online_device_num : 93
     * online_user_num : 14
     */

    private int online_device_num;
    private int online_user_num;

    public int getOnline_device_num() {
        return online_device_num;
    }

    public void setOnline_device_num(int online_device_num) {
        this.online_device_num = online_device_num;
    }

    public int getOnline_user_num() {
        return online_user_num;
    }

    public void setOnline_user_num(int online_user_num) {
        this.online_user_num = online_user_num;
    }
}
