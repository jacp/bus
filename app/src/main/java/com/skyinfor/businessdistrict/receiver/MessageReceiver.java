package com.skyinfor.businessdistrict.receiver;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.eventbus.MeaasgeEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateStuEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;
import com.skyinfor.businessdistrict.eventbus.socket.SocketCallEvent;
import com.skyinfor.businessdistrict.eventbus.socket.UpdateStuDetailEvent;
import com.skyinfor.businessdistrict.model.SocketModel;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.ui.main.MainActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.CreateStuOrTaskActivity;
import com.skyinfor.businessdistrict.ui.main.stu_detail.StatusDetailActivity;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.tencent.android.tpush.XGPushBaseReceiver;
import com.tencent.android.tpush.XGPushClickedResult;
import com.tencent.android.tpush.XGPushRegisterResult;
import com.tencent.android.tpush.XGPushShowedResult;
import com.tencent.android.tpush.XGPushTextMessage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageReceiver extends XGPushBaseReceiver {
    public static final String LogTag = "MessageReceiver";

//    private void show(Context context, String text) {
//        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
//    }

    // 通知展示
    @Override
    public void onNotifactionShowedResult(Context context,
                                          XGPushShowedResult message) {
        if (context == null || message == null) {
            return;
        }
        Log.d("LC", message.toString());
        EventBus.getDefault().post(new ChatNotifyEvent());

    }

    //反注册的回调
    @Override
    public void onUnregisterResult(Context context, int errorCode) {
        if (context == null) {
            return;
        }
        String text = "";
        if (errorCode == XGPushBaseReceiver.SUCCESS) {
            text = "反注册成功";
        } else {
            text = "反注册失败" + errorCode;
        }
        Log.d(LogTag, text);

    }

    //设置tag的回调
    @Override
    public void onSetTagResult(Context context, int errorCode, String tagName) {
        if (context == null) {
            return;
        }
        String text = "";
        if (errorCode == XGPushBaseReceiver.SUCCESS) {
            text = "\"" + tagName + "\"设置成功";
        } else {
            text = "\"" + tagName + "\"设置失败,错误码：" + errorCode;
        }
        Log.d(LogTag, text);

    }

    //删除tag的回调
    @Override
    public void onDeleteTagResult(Context context, int errorCode, String tagName) {
        if (context == null) {
            return;
        }
        String text = "";
        if (errorCode == XGPushBaseReceiver.SUCCESS) {
            text = "\"" + tagName + "\"删除成功";
        } else {
            text = "\"" + tagName + "\"删除失败,错误码：" + errorCode;
        }
        Log.d(LogTag, text);

    }

    // 通知点击回调 actionType=1为该消息被清除，actionType=0为该消息被点击。此处不能做点击消息跳转，详细方法请参照官网的Android常见问题文档
    @Override
    public void onNotifactionClickedResult(Context context,
                                           XGPushClickedResult message) {
        Log.e("LC", "+++++++++++++++ 通知被点击 跳转到指定页面。");
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
        if (context == null || message == null) {
            return;
        }
        String text = "";
        if (message.getActionType() == XGPushClickedResult.NOTIFACTION_CLICKED_TYPE) {
            // 通知在通知栏被点击啦。。。。。
            // APP自己处理点击的相关动作
            // 这个动作可以在activity的onResume也能监听，请看第3点相关内容
            text = "通知被打开 :" + message;
            String type;
            int id;
            String customContent = message.getCustomContent();

            Gson gson = new Gson();
            SocketModel mode = gson.fromJson(customContent, new TypeToken<SocketModel>() {
            }.getType());
            type = mode.getType();
            Bundle data = new Bundle();
            switch (type) {
                case "new_task":
                    id = Integer.parseInt(mode.getData().getId());
                    data.putInt("rele_id", id);
                    data.putInt("type", 0);
                    UIHelper.startActivity(context, StatusDetailActivity.class, data);

                    break;

                case "new_condition":
                    id = Integer.parseInt(mode.getData().getId());
                    data.putInt("rele_id", id);
                    data.putInt("type", 1);
                    UIHelper.startActivity(context, StatusDetailActivity.class, data);

                    break;

                case "new_message":
                    AppManager appManager = AppManager.getAppManager();
                    Activity activity = appManager.getActivity(ChatRoomActivity.class);
                    if (activity != null) {
                        appManager.finishActivity(ChatRoomActivity.class);
                    }

                    SocketModel.DataBean modeData = mode.getData();
                    String str = TextUtils.isEmpty(modeData.getGroupName()) ?
                            modeData.getNickname() : modeData.getGroupName();

                    data.putString("name", TextUtils.isEmpty(str) ? modeData.getSend_name() : str);
                    data.putString("group_uuid", modeData.getGroup_uuid());
                    data.putString("id", modeData.getSend_id());
//                    data.putString("avatar",modeData.getSend_img());
                    UIHelper.startActivity(context, ChatRoomActivity.class, data);
//                    UIHelper.startActivity(context, MainActivity.class);

                    break;
                default:
//                    UIHelper.startActivity(context, MainActivity.class);

                    break;
            }
        } else if (message.getActionType() == XGPushClickedResult.NOTIFACTION_DELETED_TYPE) {
            // 通知被清除啦。。。。
            // APP自己处理通知被清除后的相关动作
            text = "通知被清除 :" + message;
        }

    }

    //注册的回调
    @Override
    public void onRegisterResult(Context context, int errorCode,
                                 XGPushRegisterResult message) {
        // TODO Auto-generated method stub
        if (context == null || message == null) {
            return;
        }
        String text = "";
        if (errorCode == XGPushBaseReceiver.SUCCESS) {
            text = message + "注册成功";
            EventBus.getDefault().post(new MeaasgeEvent());
            // 在这里拿token
            String token = message.getToken();
        } else {
            text = message + "注册失败错误码：" + errorCode;
        }
        Log.d(LogTag, text);
    }

    // 消息透传的回调
    @Override
    public void onTextMessage(Context context, XGPushTextMessage message) {
        // TODO Auto-generated method stub
        String text = "收到消息:" + message.toString();
        // 获取自定义key-value
        String customContent = message.getCustomContent();
        if (customContent != null && customContent.length() != 0) {
            try {
                JSONObject obj = new JSONObject(customContent);
                // key1为前台配置的key
                if (!obj.isNull("key")) {
                    String value = obj.getString("key");
                    Log.d(LogTag, "get custom value:" + value);
                }
                // ...
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.d("LC", "++++++++++++++++透传消息");
        // APP自主处理消息的过程...
        Log.d(LogTag, text);
    }

}
