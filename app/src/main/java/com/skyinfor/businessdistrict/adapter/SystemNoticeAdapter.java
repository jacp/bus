package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.SystemNoticeModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class SystemNoticeAdapter extends BaseMyRecyclerVIewAdapter<SystemNoticeModel> {

    public SystemNoticeAdapter(Context context, List<SystemNoticeModel> list) {
        super(context, list, R.layout.item_system_notice);
    }

    @Override
    protected void covert(SystemNoticeModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        TextView typeName = holder.getView(R.id.text_notice_status);
        TextView name = holder.getView(R.id.text_notice_name);

        //create Name start
        TextView createPerName = holder.getView(R.id.text_notice_create_per_name);
        TextView createPer = holder.getView(R.id.text_notice_create_per);
        //create Name end

        //create Time start
        TextView createTimeName = holder.getView(R.id.text_notice_create_time_name);
        TextView createTime = holder.getView(R.id.text_notice_create_time);
        //create Time end

        TextView status = holder.getView(R.id.text_notice_cur_status);

        boolean isTask = mode.getNotice_type().endsWith("task");

        typeName.setText(isTask ? "任务" : "情况");
        name.setText(mode.getNotice_name());

        createPerName.setText(isTask ? "创建人" : "来源");
        createPer.setText(mode.getRelated_user());

        createTimeName.setText(isTask ? "创建时间" : "上报时间");
        createTime.setText(mode.getCreate_time());

        Drawable drawable = mContext.getResources().getDrawable(isTask ? R.mipmap.icon_news_notice_icon_task : R.mipmap.icon_news_notice_icon_situation);

        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());// 设置边界
        typeName.setCompoundDrawables(drawable, null, null, null);

        status.setText(mode.getNotice_status());

        if (mode.getNotice_code()!=0){
            setColor(mode.getNotice_code(),status);
        }else {
            setColor(mode.getNotice_status(), status);
        }

    }

    /**
     * 待处理
     * 处理中 /执行中
     * 待审核
     * 已完成
     *
     * @param index
     * @param tv
     */
    private void setColor(String index, TextView tv) {
        switch (index) {
            case "待处理":
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
            case "执行中":
            case "处理中":
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_orange));
                break;
            case "待审核":
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_blue));
                break;
            case "已完成":
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_green));
                break;
            case "已拒绝":
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
            default:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
        }
    }


    private void setColor(int index, TextView tv) {
        switch (index) {
            case 1:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));
                break;
            case 2:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_orange));
                break;

            case 3:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_blue));
                break;
            case 4:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_green));
                break;
            case 5:
                tv.setTextColor(mContext.getResources().getColor(R.color.color_status_red));

                break;

        }
    }

}
