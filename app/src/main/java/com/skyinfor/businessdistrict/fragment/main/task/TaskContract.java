package com.skyinfor.businessdistrict.fragment.main.task;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;

import com.flyco.tablayout.SegmentTabLayout;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class TaskContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(FragmentManager fm, final ViewPager viewPager,
                         final SegmentTabLayout apBarStl);
        void setNotify(int position);
    }

}
