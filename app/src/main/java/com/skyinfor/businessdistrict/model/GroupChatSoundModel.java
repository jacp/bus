package com.skyinfor.businessdistrict.model;

import java.util.List;

public class GroupChatSoundModel {

    private List<GroupMemberModel.UserListBean> user_array;

    private String action;

    private int type;

    private String groupName;


    public GroupChatSoundModel(List<GroupMemberModel.UserListBean> user_array, String action, int type) {
        this.user_array = user_array;
        this.action = action;
        this.type = type;
    }

    public GroupChatSoundModel(List<GroupMemberModel.UserListBean> user_array, String action, int type, String groupName) {
        this.user_array = user_array;
        this.action = action;
        this.type = type;
        this.groupName = groupName;

    }

    public GroupChatSoundModel(String action, int type) {
        this.action = action;
        this.type = type;
    }

    public List<GroupMemberModel.UserListBean> getUser_array() {
        return user_array;
    }

    public void setUser_array(List<GroupMemberModel.UserListBean> user_array) {
        this.user_array = user_array;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
