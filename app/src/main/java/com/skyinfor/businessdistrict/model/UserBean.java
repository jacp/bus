package com.skyinfor.businessdistrict.model;

/**
 * Created by LWY on 2018/7/20.
 */

public class UserBean {


    /**
     * id : 101
     * create_time : 2018-10-23 10:17:33
     * update_time : 2018-10-23 10:17:33
     * login_name : user-shuwq
     * password : e10adc3949ba59abbe56e057f20f883e
     * nickname : 舒文强
     * email :
     * phone :
     * department :
     * job :
     * avatar :
     * theone : 5543291f-9896-d4a0-cbb8-1bd588670955
     * token_user : 6921c621355e068faf731a83ec066b07
     * sex : 男
     * birthday : 2000-01-01
     * qq :
     * wechat :
     * address :
     * user_role_list_id : 14
     * user_group_list_id : 4
     * delete_status : 0
     * status : 0
     * client_id : null
     * rank_field : PRIVILEGE_PVE_LOGIN;PRIVILEGE_PME_LOGIN;PRIVILEGE_APP_LOGIN;PRIVILEGE_TASK;PRIVILEGE_PLAN;PRIVILEGE_GROUP;PRIVILEGE_DEVICE;PRIVILEGE_PAE_LOGIN
     */

    private int id;
    private String create_time;
    private String update_time;
    private String login_name;
    private String password;
    private String nickname;
    private String email;
    private String phone;
    private String department;
    private String job;
    private String avatar;
    private String theone;
    private String token_user;
    private String sex;
    private String birthday;
    private String qq;
    private String wechat;
    private String address;
    private int user_role_list_id;
    private int user_group_list_id;
    private int delete_status;
    private String status;
    private Object client_id;
    private String rank_field;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getLogin_name() {
        return login_name;
    }

    public void setLogin_name(String login_name) {
        this.login_name = login_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTheone() {
        return theone;
    }

    public void setTheone(String theone) {
        this.theone = theone;
    }

    public String getToken_user() {
        return token_user;
    }

    public void setToken_user(String token_user) {
        this.token_user = token_user;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getUser_role_list_id() {
        return user_role_list_id;
    }

    public void setUser_role_list_id(int user_role_list_id) {
        this.user_role_list_id = user_role_list_id;
    }

    public int getUser_group_list_id() {
        return user_group_list_id;
    }

    public void setUser_group_list_id(int user_group_list_id) {
        this.user_group_list_id = user_group_list_id;
    }

    public int getDelete_status() {
        return delete_status;
    }

    public void setDelete_status(int delete_status) {
        this.delete_status = delete_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getClient_id() {
        return client_id;
    }

    public void setClient_id(Object client_id) {
        this.client_id = client_id;
    }

    public String getRank_field() {
        return rank_field;
    }

    public void setRank_field(String rank_field) {
        this.rank_field = rank_field;
    }
}
