package com.skyinfor.businessdistrict.ui.login;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.dialog.DialogIconView;
import com.skyinfor.businessdistrict.listener.OnMessageDialogEvent;
import com.skyinfor.businessdistrict.ui.main.MainActivity;
import com.skyinfor.businessdistrict.util.CallPhoneUtils;
import com.skyinfor.businessdistrict.util.MD5Utils;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by SKYINFOR on 2018/10/22.
 */

public class LoginActivity extends BaseActivity<LoginPresent> implements LoginContract.Model, OnMessageDialogEvent {

    @BindView(R.id.edit_login_name)
    EditText editLoginName;
    @BindView(R.id.edit_login_pass)
    EditText editLoginPass;
    private String phoneNum;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }


    @OnClick({R.id.text_login_forget_pass, R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_login_forget_pass:
                phoneNum = "1234569";
                Dialog dialog = new DialogIconView(mContext, "提示", "请联系管理员进行修改", "呼叫", "取消", phoneNum).setDialogEvent(this);
                dialog.show();


                break;
            case R.id.btn_login:
                String name = editLoginName.getText().toString().trim();
                String pass = editLoginPass.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    ToashUtils.show(mContext, "请输入用户名");
                } else if (TextUtils.isEmpty(pass)) {
                    ToashUtils.show(mContext, "请输入密码");
                } else {
                    pass = MD5Utils.getMd5(pass);
                    mPresenter.LoginAction(name, pass);
                }

                break;
        }
    }

    @Override
    public void ok(Dialog dialog) {
        CallPhoneUtils.LastCallPhone(mContext, phoneNum);
    }

    @Override
    public void close(Dialog dialog) {
        dialog.dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            appManager.finishAllActivity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void setResultAction() {
        UIHelper.startActivity(mContext, MainActivity.class);

        appManager.finishActivity(LoginActivity.class);
    }
}
