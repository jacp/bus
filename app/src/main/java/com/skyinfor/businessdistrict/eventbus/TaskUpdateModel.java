package com.skyinfor.businessdistrict.eventbus;

public class TaskUpdateModel {

    private int status;

    public TaskUpdateModel(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
