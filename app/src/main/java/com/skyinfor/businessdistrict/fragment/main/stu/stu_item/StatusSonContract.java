package com.skyinfor.businessdistrict.fragment.main.stu.stu_item;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusSonContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void getList(Map<String, String> parts, int pages, int type);
        void initAdapter(RecyclerView recyclerView);

    }

}
