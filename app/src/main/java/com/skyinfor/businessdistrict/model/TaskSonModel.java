package com.skyinfor.businessdistrict.model;

import java.io.Serializable;
import java.util.List;

public class TaskSonModel implements Serializable{

    /**
     * id : 92
     * create_time : 2018-10-30 18:39:47
     * update_time : 2018-10-31 15:39:19
     * name : 空
     * type_id : 2
     * send_user_id : 101
     * accept_user_id : 48,46,51
     * status_code : 3
     * relation_device : 4
     * details : 记录
     * address : 上海市松江区黄渡浜街11栋
     * position : 121.24159390145047,31.060590723711833
     * picture : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bd834f2e63da.png
     * video :
     * audio :
     * type_name : 突发任务
     * status_name : 待审核
     * accept_user_list : [{"nickname":"李宁","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","phone":"18516070553","job":"经理"},{"nickname":"陈磊","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bce81fe12b06.jpg","phone":"18356070598","job":"员工"},{"nickname":"朱中响","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bd2c038e5842.png","phone":"18225819193","job":"员工"}]
     * send_user_avatar :
     */

    private int id;
    private String create_time;
    private String update_time;
    private String name;
    private String type_id;
    private String send_user_id;
    private String accept_user_id;
    private String status_code;
    private List<RelationDeviceBean> relation_device;
    private String details;
    private String address;
    private String position;
    private String picture;
    private String video;
    private String audio;
    private String type_name;
    private String status_name;
    private String send_user_avatar;
    private String send_user_nickname;
    private List<AcceptUserListBean> accept_user_list;

    public String getSend_user_nickname() {
        return send_user_nickname;
    }

    public void setSend_user_nickname(String send_user_nickname) {
        this.send_user_nickname = send_user_nickname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getSend_user_id() {
        return send_user_id;
    }

    public void setSend_user_id(String send_user_id) {
        this.send_user_id = send_user_id;
    }

    public String getAccept_user_id() {
        return accept_user_id;
    }

    public void setAccept_user_id(String accept_user_id) {
        this.accept_user_id = accept_user_id;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public List<RelationDeviceBean> getRelation_device() {
        return relation_device;
    }

    public void setRelation_device(List<RelationDeviceBean> relation_device) {
        this.relation_device = relation_device;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getSend_user_avatar() {
        return send_user_avatar;
    }

    public void setSend_user_avatar(String send_user_avatar) {
        this.send_user_avatar = send_user_avatar;
    }

    public List<AcceptUserListBean> getAccept_user_list() {
        return accept_user_list;
    }

    public void setAccept_user_list(List<AcceptUserListBean> accept_user_list) {
        this.accept_user_list = accept_user_list;
    }

    public static class AcceptUserListBean implements Serializable {
        /**
         * nickname : 李宁
         * avatar : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png
         * phone : 18516070553
         * job : 经理
         */

        private String nickname;
        private String avatar;
        private String phone;
        private String job;
        private int user_id;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }
    }

    public static class RelationDeviceBean implements Serializable{

        /**
         * type : 摄像头
         * list : [{"device_name":"信达蓝爵小区"},{"device_name":"万达广场南"},{"device_name":"茸宁路黄渡浜街交叉口"},{"device_name":"上海富悦财富广场A座"},{"device_name":"茸悦路黄渡浜街交叉口"}]
         */

        private String type;
        private List<deviceBean> list;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<deviceBean> getList() {
            return list;
        }

        public void setList(List<deviceBean> list) {
            this.list = list;
        }

        public static class deviceBean implements Serializable{
            /**
             * device_name : 信达蓝爵小区
             */

            private String device_name;
            private String theone ;

            public String getTheone() {
                return theone;
            }

            public void setTheone(String theone) {
                this.theone = theone;
            }

            public String getDevice_name() {
                return device_name;
            }

            public void setDevice_name(String device_name) {
                this.device_name = device_name;
            }
        }
    }

}
