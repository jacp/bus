package com.skyinfor.businessdistrict.eventbus.socket;

public class SocketActionEvent {

    private int action;
    private byte[] msg;

    public SocketActionEvent(int action) {
        this.action = action;
    }

    public SocketActionEvent(int action, byte[] msg) {
        this.action = action;
        this.msg = msg;
    }

    public int getAction() {
        return action;
    }

    public byte[] getMsg() {
        return msg;
    }
}
