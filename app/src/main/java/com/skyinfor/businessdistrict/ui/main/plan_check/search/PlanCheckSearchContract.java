package com.skyinfor.businessdistrict.ui.main.plan_check.search;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class PlanCheckSearchContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void gePlanList(String name,final int page, final int type);

    }

}
