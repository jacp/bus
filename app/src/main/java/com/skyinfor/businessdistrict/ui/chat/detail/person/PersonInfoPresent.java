package com.skyinfor.businessdistrict.ui.chat.detail.person;

import android.app.Activity;

import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;

import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

public class PersonInfoPresent extends BasePresenter<PersonInfoContract.Model>
        implements PersonInfoContract.Presenter {

    private HttpDialog dialog;
    private PersonInfoApi api;

    @Inject
    public PersonInfoPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(PersonInfoApi.class);
    }


    @Override
    public void getUserInfo(String userId) {
        dialog.show();

        addSubscription(api.userInfo(userId)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<UserInfoModel>>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(List<UserInfoModel> userInfoModel) {
                        mModel.setResult(userInfoModel.get(0));

                    }
                });
    }
}
