package com.skyinfor.businessdistrict.ui.chat.system_notice;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.SystemNoticeModel;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface SystemNoticeApi {

    @GET(ApiUrl.userNoticeList)
    Observable<Response<List<SystemNoticeModel>>>userNoticeList(@Query("user_id") String user_id , @Query("page")
            String page, @Query("unit") String unit);

}
