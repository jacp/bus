package com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.CreateGroupModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

public interface TaskPersonSelectApi {

    @FormUrlEncoded
    @POST(ApiUrl.talkGroupCreate)
    Observable<Response<CreateGroupModel>> talkGroupCreate(@Field("group_name") String group_name,
                                                           @Field("user_id") String user_id,
                                                           @Field("group_owner_id") String group_owner_id );

    @Multipart
    @POST(ApiUrl.talkGroupRecordSave)
    Observable<Response<Object>> talkGroupRecordSave(@Part List<MultipartBody.Part> part);


}
