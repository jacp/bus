package com.skyinfor.businessdistrict.ui.chat.system_notice;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class SystemNoticeContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void getNoticeList(String user_id ,int page,String unit);
    }
}
