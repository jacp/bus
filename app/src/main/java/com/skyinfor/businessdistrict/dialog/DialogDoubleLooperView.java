package com.skyinfor.businessdistrict.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.UiUtils;
import com.weigan.loopview.LoopView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Author     : HaiLy
 * Date       : 2018/3/30 13:43
 * Email      : zhanghailei55@gmail.com
 * Desc       :
 * Comment    : /TODO
 * Class      : DialogDoubleLooperView
 */


public class DialogDoubleLooperView<T> extends Dialog implements View.OnClickListener {
    private View     _view;
    public  TextView tv_dialog_yes;
    public  TextView tv_dialog_cencel;
    public  LoopView loopViewYear;
    public  LoopView loopViewMonth;

    public OnDoubleDialogEvent<T> _mEvent;
    public List<T>                list;

    public List<String> list_str;
    public  List<String> mMonthList = new ArrayList<>();
    private int          initYear   = -1;
    private int          initMonth  = 0;


    /**
     * 使用arraysResId初始化
     * @param context
     * @param listRes
     * @param o
     *         无用
     */
    public DialogDoubleLooperView(Context context, int listRes, Object o) {
        this(context, context.getResources().getStringArray(listRes));
    }

    public DialogDoubleLooperView(Context context, String... lists) {
        this(context, (List<T>) Arrays.asList(lists), null, 0, 0);
    }

    public DialogDoubleLooperView(Context context, List<T> lists, List<String> monthList, int initYear, int initMonth) {
        this(context);
        this.initYear = initYear;
        this.initMonth = initMonth;
        mMonthList.clear();
        list_str = new ArrayList<>();
        list = lists;
        if (monthList != null) {
            mMonthList = monthList;

        }
        if (list == null) {
            list = new ArrayList<>();
        }
        /**
         * 迭代
         */
        for (T data : list) {
            list_str.add(tConverString(data));
        }
        dialogInit(context);
    }

    protected String tConverString(T data) {
        if (data instanceof String) {
            return (String) data;
        }
        return "";
    }

    private DialogDoubleLooperView(Context context) {
        super(context, R.style.ActionSheetDialogStyle);
    }

    private DialogDoubleLooperView(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    private DialogDoubleLooperView(Context context, int themeResId) {
        super(context, themeResId);
    }

    /**
     * 对dialog进行赋值和操作
     * @param context
     */
    private void dialogInit(Context context) {
        _view = getLayoutInflater().inflate(R.layout.dialog_double_loop, null);
        tv_dialog_yes = (TextView) _view.findViewById(R.id.tv_submit);
        tv_dialog_cencel = (TextView) _view.findViewById(R.id.tv_close);
        // 内容区域
        loopViewYear = (LoopView) _view.findViewById(R.id.loopView_year);
        loopViewMonth = (LoopView) _view.findViewById(R.id.loopView_month);


        //设置是否循环播放
        loopViewYear.setNotLoop();
        loopViewMonth.setNotLoop();

        //获取当前Activity所在的窗体
        Window dialogWindow = this.getWindow();
        //设置原始数据
        loopViewYear.setItems(list_str);
        loopViewMonth.setItems(mMonthList);

        //设置Dialog从窗体底部弹出
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.getDecorView().setPadding(0, 0, 0, 0);

        WindowManager windowManager = dialogWindow.getWindowManager();
        Display       display       = windowManager.getDefaultDisplay();
        //获得window窗口的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //设置窗口宽度为充满全屏
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; //设置宽度
        //设置窗口高度为包裹内容
        //TODO 修改了空白处消失操作 FrameJack 2017-10-30 15:33 操作
        lp.height = UiUtils.dp2px(240, context);
        //将设置好的属性set回去
        dialogWindow.setAttributes(lp);
        if (initYear != 0) {
            loopViewYear.setCurrentPosition(initYear);
        }else {
            Calendar c = Calendar.getInstance();
            initYear = c.get(Calendar.YEAR) - 2003;
            loopViewYear.setCurrentPosition(initYear);
        }

        if (initMonth == 0) {
            //获取当前月份
            Calendar c = Calendar.getInstance();
            loopViewMonth.setCurrentPosition(c.get(Calendar.MONTH) + 1);
        } else if (initMonth == -1) {
            loopViewMonth.setCurrentPosition(1);
            Log.e("当前initMonth", initMonth + "");
        } else if (initMonth == -2) {
            loopViewMonth.setCurrentPosition(0);

        } else {
            loopViewMonth.setCurrentPosition(initMonth + 1);
        }

        setContentView(_view);
        setCanceledOnTouchOutside(true);

        tv_dialog_yes.setOnClickListener(this);
        tv_dialog_cencel.setOnClickListener(this);
    }

    public DialogDoubleLooperView setDialogEvent(OnDoubleDialogEvent<T> event) {
        this._mEvent = event;
        return this;
    }

    public T getItem(int position) {
        return list.get(position);
    }

    public String getItemStr(int position) {
        return list_str.get(position);
    }

    public int getCurrIndex() {
        return loopViewYear.getSelectedItem();
    }

    @Override
    public void onClick(View v) {
        if (_mEvent != null) {
            if (v.equals(tv_dialog_yes)) {
                _mEvent.ok(this, loopViewYear.getSelectedItem(), loopViewMonth.getSelectedItem());
            } else if (v.equals(tv_dialog_cencel)) {
                _mEvent.cancel(this);
            }
        }
        if (autoDismiss()) {
            this.dismiss();
        }
    }

    protected boolean autoDismiss() {
        return true;
    }

    public interface OnDoubleDialogEvent<T> {
        void ok(Dialog dialog, int year, int month);

        void cancel(Dialog dialog);
    }


}
