package com.skyinfor.businessdistrict.util;

import com.skyinfor.businessdistrict.eventbus.TimeCountEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class TimeCountUtil {
    private static TimeCountUtil util;
    private int cnt;
    private Timer timer;

    public static TimeCountUtil getInstance() {
        if (util == null) {
            util = new TimeCountUtil();
        }
        return util;
    }

    public TimeCountUtil startTimeCount() {
        cnt = 0;
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                if (timer==null){
                    cancel();
                    System.gc();
                }else {
                    EventBus.getDefault().post(new TimeCountEvent(getStringTime(cnt++)));
                }
            }
        };

        timer.schedule(timerTask, 0, 1000);
        return this;
    }

    private String getStringTime(int cnt) {
        int hour = cnt / 3600;
        int min = cnt % 3600 / 60;
        int second = cnt % 60;
        return String.format(Locale.CHINA, "%02d:%02d:%02d", hour, min, second);
    }

    public void releaseDestroyTimeSource() {
        if (timer != null) {
            timer.purge();
            timer.cancel();
            timer = null;
            util = null;
        }
    }


}
