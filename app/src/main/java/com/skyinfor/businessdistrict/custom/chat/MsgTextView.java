package com.skyinfor.businessdistrict.custom.chat;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.ChatContentModel;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;
import static com.skyinfor.businessdistrict.util.UiUtils.px2dp;

public class MsgTextView extends RelativeLayout {
    private int type;
    private ChatContentModel mode;
    private Paint mPaint;


    public MsgTextView(Context context, int type, ChatContentModel mode) {
        super(context);
        this.type = type;
        this.mode = mode;
        initData(context);
    }

    public MsgTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    private void initData(Context context) {
        mPaint = new Paint();
        View view = LayoutInflater.from(context).inflate(R.layout.item_text_left, null);
        TextView text = view.findViewById(R.id.left_chat_content);

        text.setText(mode.getContent());
        text.setTextColor(context.getResources().getColor(type == 0 ? R.color.color_333333 : R.color.colorWhite));
        text.setBackgroundResource(type == 0 ? R.drawable.icon_bg_talk_left : R.drawable.icon_bg_talk_right);

        addView(view);
    }


}
