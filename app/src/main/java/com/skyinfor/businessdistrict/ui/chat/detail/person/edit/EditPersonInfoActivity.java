package com.skyinfor.businessdistrict.ui.chat.detail.person.edit;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.custom.TimePickView;
import com.skyinfor.businessdistrict.custom.roundedImage.RoundedImageView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.EditPersonEvent;
import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.ui.chat.detail.person.edit.son.EditDetailActivity;
import com.skyinfor.businessdistrict.util.DateUtil;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.ImagePickerUtil;
import com.skyinfor.businessdistrict.util.LuBanCompress;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.dialogutil.SelectImageFragment;
import com.skyinfor.businessdistrict.util.http.HttpParams;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditPersonInfoActivity extends BaseActivity<EditPersonPresent> implements EditPersonContract.Model, SelectImageFragment.selectCompletionListener {
    @BindView(R.id.text_person_sex)
    TextView textPersonSex;
    @BindView(R.id.text_person_email)
    TextView textPersonEmail;
    @BindView(R.id.text_person_bir)
    TextView textPersonBir;
    @BindView(R.id.text_person_qq)
    TextView textPersonQq;
    @BindView(R.id.text_person_wexin)
    TextView textPersonWexin;
    @BindView(R.id.text_person_address)
    TextView textPersonAddress;
    @BindView(R.id.img_person_head)
    IconView imgPersonHead;
    public static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_CAMER = 200;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private ArrayList<ImageItem> images;
    private ArrayList<ImageItem> selImageList = new ArrayList<>(); //当前选择的所有图片
    private int maxImgCount = 1;
    private String path;
    private UserInfoModel model;
    private TimePickView pvDate;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_edit_person_info;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        model = (UserInfoModel) getIntent().getSerializableExtra("model");
        tvTitle.setText("编辑信息");
        EventBus.getDefault().register(this);

        setInfo(model);
    }

    private void setInfo(UserInfoModel model) {
        imgPersonHead.setTag(R.id.img_person_head);
        imgPersonHead.setBaseInfo(mContext,model.getNickname(),model.getAvatar());
        textPersonSex.setText(model.getSex());
        textPersonEmail.setText(model.getEmail());
        textPersonBir.setText(model.getBirthday());
        textPersonQq.setText(model.getQq());
        textPersonWexin.setText(model.getWechat());
        textPersonAddress.setText(model.getAddress());

    }

    @OnClick({R.id.rel_text_person_head, R.id.rel_person_sex, R.id.rel_person_email,
            R.id.rel_person_bir, R.id.rel_person_qq,
            R.id.rel_person_wexin, R.id.rel_person_address})

    public void onViewClicked(View view) {
        Bundle data = new Bundle();
        data.putSerializable("model", model);

        switch (view.getId()) {
            case R.id.rel_text_person_head:
                selectImg();

                break;
            case R.id.rel_person_sex:
                setEditData(data, 1,textPersonSex.getText().toString());

                break;
            case R.id.rel_person_email:
                setEditData(data, 2,textPersonEmail.getText().toString());

                break;
            case R.id.rel_person_bir:
                initDate();

                break;
            case R.id.rel_person_qq:
                setEditData(data, 4,textPersonQq.getText().toString());

                break;
            case R.id.rel_person_wexin:
                setEditData(data, 5,textPersonWexin.getText().toString());

                break;
            case R.id.rel_person_address:
                setEditData(data, 6,textPersonAddress.getText().toString());

                break;
        }
    }

    @Override
    public void completion(String path) {
        Log.e("path", path);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null && requestCode == REQUEST_CODE_SELECT) {
            images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
            if (images != null) {
                selImageList.clear();
                selImageList.addAll(images);
            }
            path = selImageList.get(0).path;

            Uri uri = Uri.fromFile(new File(path));
            imgPersonHead.setImgUri(uri);

            HttpParams params = new HttpParams();
            params.put("file", LuBanCompress.compressWithLs(new File(images.get(0).path), mContext));
            mPresenter.upFile(params.setMoreImgType());
        }
    }

    private void selectImg() {
        selImageList.clear();
        ImagePickerUtil.initImageMorePicker();
        //打开选择,本次允许选择的数量
        ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
        Intent intent1 = new Intent(mContext, ImageGridActivity.class);
        /* 如果需要进入选择的时候显示已经选中的图片，
         * 详情请查看ImagePickerActivity
         * */
//                                intent1.putExtra(ImageGridActivity.EXTRAS_IMAGES,images);
        startActivityForResult(intent1, REQUEST_CODE_SELECT);
    }

    @Subscribe
    public void getEvent(EditPersonEvent event) {
        switch (event.getType()) {
            case 1:

                model.setSex(event.getStr());
                textPersonSex.setText(event.getStr());
                break;
            case 2:
                textPersonEmail.setText(event.getStr());

                break;

            case 4:
                textPersonQq.setText(event.getStr());

                break;
            case 5:
                textPersonWexin.setText(event.getStr());

                break;
            case 6:
                textPersonAddress.setText(event.getStr());

                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    private void setEditData(Bundle data, int type,String str) {
        data.putInt("type", type);
        data.putString("str", str);
        UIHelper.startActivity(mContext, EditDetailActivity.class, data);
    }

    /**
     *
     */
    private void initNetData() {
        HttpParams params = new HttpParams();
        params.put("id", model.getId() + "");
        params.put("nickname", model.getNickname() + "");
        params.put("password", Constants.LOGIN_PASS_WORD + "");
        params.put("email", model.getEmail() + "");
        params.put("qq", model.getQq() + "");
        params.put("wechat", model.getWechat() + "");
        params.put("address", model.getAddress());
        params.put("avatar", model.getAvatar());
        params.put("birthday",model.getBirthday());
        mPresenter.updateInfo(params.setType());
    }

    @Override
    public void setSucImg(String path) {
        model.setAvatar(path);
        initNetData();
    }

    private void initDate() {
        //时间选择器
        pvDate = new TimePickView(mContext, TimePickView.Type.YEAR_MONTH_DAY);
        pvDate.setTime(new Date());
        pvDate.setCyclic(false);
        pvDate.setCancelable(true);
        pvDate.show();
        pvDate.setOnTimeSelectListener(new TimePickView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date) {
                String startTimeText = DateUtil.getTime1(date);
                textPersonBir.setText(startTimeText);
                model.setBirthday(startTimeText);
                initNetData();
            }
        });
    }
}
