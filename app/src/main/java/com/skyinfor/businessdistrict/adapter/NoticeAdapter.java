package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class NoticeAdapter extends BaseMyRecyclerVIewAdapter<HomeNoticeModel.DataBean> {
    
    public NoticeAdapter(Context context, List<HomeNoticeModel.DataBean> list) {
        super(context, list, R.layout.item_notice);
    }

    @Override
    protected void covert(HomeNoticeModel.DataBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        TextView name = holder.getView(R.id.text_banner_name);
        TextView from = holder.getView(R.id.text_banner_from);
        TextView date = holder.getView(R.id.text_banner_date);
        ImageView img = holder.getView(R.id.text_banner_img,position);
        View div=holder.getView(R.id.text_notice_div);

        name.setText(mode.getTitle());
        from.setText(mode.getNickname());
        date.setText(mode.getCreate_time().substring(0, mode.getCreate_time().length() - 3));
        if (!TextUtils.isEmpty(mode.getPicture())) {
            img.setVisibility(View.VISIBLE);
            GlideUtils.loadImg(mContext,mode.getPicture(), img);
        } else {
            img.setVisibility(View.GONE);
        }

        div.setVisibility(mList.size()-1==position?View.GONE:View.VISIBLE);
        
    }
}
