package com.skyinfor.businessdistrict.ui.setting;

import android.app.Activity;

import com.skyinfor.businessdistrict.presenter.BasePresenter;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class SettingPresent extends BasePresenter<SettingContract.Model>
        implements SettingContract.Presenter {

    @Inject
    public SettingPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }


}
