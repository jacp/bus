package com.skyinfor.businessdistrict.model;

import java.io.Serializable;

public class UserInfoModel implements Serializable {


    /**
     * id : 25
     * nickname : App演示
     * email : 123@qq. iOS
     * phone :
     * department :
     * job : 软件开发
     * avatar : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bda96e660448.png
     * sex : 男
     * birthday : 1960-12-14
     * qq : 11222
     * wechat : 3333
     * address : 33333
     */

    private int id;
    private String nickname;
    private String email;
    private String phone;
    private String department;
    private String job;
    private String avatar;
    private String sex;
    private String birthday;
    private String qq;
    private String wechat;
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
