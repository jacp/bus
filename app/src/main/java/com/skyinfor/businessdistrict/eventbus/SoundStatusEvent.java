package com.skyinfor.businessdistrict.eventbus;

public class SoundStatusEvent {

    private int status;
    private int uuid;

    public SoundStatusEvent(int status, int uuid) {
        this.status = status;
        this.uuid = uuid;
    }

    public SoundStatusEvent(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public int getUuid() {
        return uuid;
    }
}
