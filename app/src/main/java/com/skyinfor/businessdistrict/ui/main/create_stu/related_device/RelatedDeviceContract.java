package com.skyinfor.businessdistrict.ui.main.create_stu.related_device;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.ArrayList;
import java.util.List;

public class RelatedDeviceContract {

    interface Model extends MModel {
        void setResultList(ArrayList<RelatedDeviceModel.ListBean> deviceList);

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void taskDevice(List<String> deviceIdList,int type);
        void setNotify(List<RelatedDeviceModel> newList);
    }
}
