package com.skyinfor.businessdistrict.ui.main.plan_check.plan_detail;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class PlanDetailContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recPer, RecyclerView RecOperation, RecyclerView recLink,int type);
        void setNotify(PlanListModel model);

    }

}
