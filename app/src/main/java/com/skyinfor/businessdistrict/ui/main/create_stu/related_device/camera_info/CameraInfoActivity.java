package com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info;

import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.ui.map.MapChooseActivity;
import com.skyinfor.businessdistrict.util.UIHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraInfoActivity extends BaseActivity<CameraInfoPresent> implements CameraInfoContract.Model {
    @BindView(R.id.text_camera_name)
    TextView textCameraName;
    @BindView(R.id.text_camera_position)
    TextView textCameraPosition;
    @BindView(R.id.rel_camera_position)
    RelativeLayout relCameraPosition;
    @BindView(R.id.text_camera_ip)
    TextView textCameraIp;
    @BindView(R.id.text_camera_type)
    TextView textCameraType;
    @BindView(R.id.text_camera_current_status)
    TextView textCameraCurrentStatus;
    @BindView(R.id.text_camera_belong_nv)
    TextView textCameraBelongNv;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private String address;
    private String position;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_camera_info;
    }


    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        RelatedDeviceModel.ListBean model = (RelatedDeviceModel.ListBean) getIntent()
                .getSerializableExtra("model");

        setInfo(model);
    }

    private void setInfo(RelatedDeviceModel.ListBean model) {
        if (model != null) {
            tvTitle.setText(model.getName());
            textCameraName.setText(model.getName());
            textCameraPosition.setText(model.getAddress());
            textCameraIp.setText(model.getIp());
            textCameraType.setText(model.getType());
            textCameraCurrentStatus.setText(model.getStatus());
            textCameraBelongNv.setText(model.getNvrip());
            address = model.getAddress();
            position = model.getPosition();
        }
    }


    @OnClick(R.id.rel_camera_position)
    public void onViewClicked() {
        Bundle data = new Bundle();
        data.putString("strLag", position);
        data.putString("address", address);
        UIHelper.startActivity(mContext, MapChooseActivity.class, data);

    }

}
