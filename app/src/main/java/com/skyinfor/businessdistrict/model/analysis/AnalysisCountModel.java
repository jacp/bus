package com.skyinfor.businessdistrict.model.analysis;

public class AnalysisCountModel {


    /**
     * condition_num : 0
     * task_num : 0
     * device_num : 14
     */

    private int condition_num;
    private int task_num;
    private int device_num;

    public int getCondition_num() {
        return condition_num;
    }

    public void setCondition_num(int condition_num) {
        this.condition_num = condition_num;
    }

    public int getTask_num() {
        return task_num;
    }

    public void setTask_num(int task_num) {
        this.task_num = task_num;
    }

    public int getDevice_num() {
        return device_num;
    }

    public void setDevice_num(int device_num) {
        this.device_num = device_num;
    }
}
