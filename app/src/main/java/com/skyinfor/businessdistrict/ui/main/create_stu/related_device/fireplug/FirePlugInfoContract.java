package com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class FirePlugInfoContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {


    }

}
