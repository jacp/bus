package com.skyinfor.businessdistrict.ui.map.map_view;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.MapCheckPopAdapter;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.MapPopModel;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info.CameraInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug.FirePlugInfoActivity;
import com.skyinfor.businessdistrict.ui.map.map_view.search.MapCheckSearchActivity;
import com.skyinfor.businessdistrict.util.PopupWindowUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

/**
 * AMap aMap = mapView.getMap();
 * aMap.clear();
 * <p>
 * list.add(marker);
 * <p>
 * for(int i=0;i<list.size();i++){
 * Marker marker = list.get(i);
 * marker.setvisiable(xxx);
 * }
 */
public class MapCheckActivity extends BaseActivity<MapCheckPresent> implements MapCheckContract.Model, LocationSource, AMapLocationListener, AMap.OnMarkerClickListener {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.map_view_check)
    MapView mapViewCheck;
    @BindView(R.id.img_right)
    ImageView imgRight;
    @BindView(R.id.img_right_search)
    ImageView imSearch;
    @BindView(R.id.title_search)
    EditText titleSearch;
    @BindView(R.id.t_title)
    View title;

    private AMap mAMap;
    private GeocodeSearch geocoderSearch;
    private AMapLocationClient mlocationClient;
    private OnLocationChangedListener mListener;
    private AMapLocationClientOption mLocationOption;
    private boolean isFirstLoc = true;
    private PopupWindow popup;
    private List<MapPopModel> list;
    private List<MapPopModel> selectList = new ArrayList<>();
    private MapCheckPopAdapter adapter;
    private ArrayList<Marker> markers;
    private View popView;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_map_check;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        tvTitle.setText("地图查看");
        imgRight.setVisibility(View.VISIBLE);
        imSearch.setVisibility(View.VISIBLE);
        imgRight.setImageResource(R.mipmap.icon_map_icon_screen);

        mapViewCheck.onCreate(savedInstanceState);
        mAMap = mapViewCheck.getMap();
        setUpMap();
        mPresenter.deviceList();
    }

    /**
     * 设置一些amap的属性
     */
    private void setUpMap() {
        mAMap.setLocationSource(this);// 设置定位监听
        mAMap.getUiSettings().setMyLocationButtonEnabled(false);// 设置默认定位按钮是否显示
        mAMap.getUiSettings().setZoomControlsEnabled(false);//放大缩小按钮是否显示
        // 自定义系统定位蓝点
        MyLocationStyle myLocationStyle = new MyLocationStyle();
        // 自定义定位蓝点图标
        myLocationStyle.myLocationIcon(
                BitmapDescriptorFactory.fromResource(R.mipmap.icon_map_position_icon_people));
//        // 自定义精度范围的圆形边框颜色
//        myLocationStyle.strokeColor(Color.argb(0, 0, 0, 0));
//        // 自定义精度范围的圆形边框宽度
//        myLocationStyle.strokeWidth(50);
//        // 设置圆形的填充颜色
//        myLocationStyle.radiusFillColor(Color.argb(0, 0, 0, 0));
        // 将自定义的 myLocationStyle 对象添加到地图上
        mAMap.setMyLocationStyle(myLocationStyle);
        mAMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
        mAMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
        mAMap.setOnMarkerClickListener(this);
    }

    @Override
    public void activate(OnLocationChangedListener listener) {
        this.mListener = listener;
        initLocation();
    }

    @Override
    public void deactivate() {

    }

    /**
     * 定位获取当前位置
     */
    private void initLocation() {
        if (mlocationClient == null) {
            mlocationClient = new AMapLocationClient(this);
            mLocationOption = new AMapLocationClientOption();
            // 设置定位监听
            mlocationClient.setLocationListener(this);
            // 设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            // 只是为了获取当前位置，所以设置为单次定位
            mLocationOption.setOnceLocation(true);
            // 设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            mlocationClient.startLocation();
        } else {
            mlocationClient.startLocation();
        }
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {
                // 如果不设置标志位，此时再拖动地图时，它会不断将地图移动到当前的位置
                if (isFirstLoc) {
                    //设置缩放级别
                    mAMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                    //将地图移动到定位点
                    mAMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude())));
                    //点击定位按钮 能够将地图的中心移动到定位点
                    mListener.onLocationChanged(aMapLocation);

                    StringBuffer buffer = new StringBuffer();
                    buffer.append(
                            aMapLocation.getProvince() + ""
                                    + aMapLocation.getDistrict() + ""
                                    + aMapLocation.getStreet() + ""
                                    + aMapLocation.getStreetNum());
                    isFirstLoc = false;
                }
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                Log.e("AmapError", "location Error, ErrCode:"
                        + aMapLocation.getErrorCode() + ", errInfo:"
                        + aMapLocation.getErrorInfo());
                Toast.makeText(getApplicationContext(), "定位失败", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void setResult(ArrayList<MarkerOptions> optionsArrayList) {
        markers = mAMap.addMarkers(optionsArrayList, false);
        mPresenter.dialog.dismiss();
    }

    @OnClick({R.id.img_right, R.id.img_right_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_right:
                setFilter();
                break;
            case R.id.img_right_search:
                UIHelper.startActivity(mContext, MapCheckSearchActivity.class);

                break;
        }
    }

    private void setFilter() {
        list = new ArrayList<>();
        list.add(new MapPopModel("摄像头", true));
        list.add(new MapPopModel("消防栓", true));
        list.add(new MapPopModel("人员", true));

        for (int i = 0; i < selectList.size(); i++) {
            boolean sCheck = selectList.get(i).isCheck();

            for (int j = 0; j < list.size(); j++) {
                if (i == j) {
                    list.get(j).setCheck(sCheck);
                }
            }
        }

        popView = getLayoutInflater().inflate(R.layout.view_map_pop, null);
        RecyclerView recyclerView = popView.findViewById(R.id.rec_map_pop);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL, dp2px(1, mContext),
                Color.parseColor("#efeff4")));

        final MapCheckPopAdapter adapter = new MapCheckPopAdapter(mContext, list);
        recyclerView.setAdapter(adapter);

        PopupWindowUtils.getInstance(mContext).showPopup(true,
                popView, imgRight, 0, 4);

        adapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                boolean check = list.get(position).isCheck();
                list.get(position).setCheck(!check);
                adapter.setNotify(list);
                selectList.clear();
                selectList.addAll(list);

                String str = list.get(position).getStr();
                for (int i = 0; i < markers.size(); i++) {
                    Marker marker = markers.get(i);
                    if (marker.getTitle().equals(str)) {
                        marker.setVisible(!check);
                    }
                }

            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int id = Integer.valueOf(marker.getSnippet());
        mPresenter.setMapClick(id);
        return true;
    }
}
