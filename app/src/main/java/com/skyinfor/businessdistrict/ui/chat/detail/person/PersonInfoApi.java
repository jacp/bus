package com.skyinfor.businessdistrict.ui.chat.detail.person;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.UserInfoModel;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface PersonInfoApi {

    @GET(ApiUrl.userInfo)
    Observable<Response<List<UserInfoModel>>> userInfo(@Query("user_id") String user_id);

}
