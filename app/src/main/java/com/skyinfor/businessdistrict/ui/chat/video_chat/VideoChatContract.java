package com.skyinfor.businessdistrict.ui.chat.video_chat;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.VideoGroupChatModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

import io.agora.rtc.RtcEngine;

public class VideoChatContract {

    interface Model extends MModel {
        void setRtcEngine(RtcEngine mRtcEngine);

    }

    interface Presenter extends IPresenter<Model> {
        void initSingleConfig(int type);
        void onSingleDestroy();
        void initAdapter(RecyclerView recyclerView,RecyclerView recVideo,RecyclerView recOther);
        void setNotify(List<VideoGroupChatModel> mList);
        void setTitleNotify(List<GroupMemberModel.UserListBean>  mList);

    }


}
