package com.skyinfor.businessdistrict.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.cjt2325.cameralibrary.util.LogUtil;
import com.skyinfor.businessdistrict.adapter.BaseFragmentAdapter;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.fragment.main.contact.ContactFragment;
import com.skyinfor.businessdistrict.fragment.main.home.HomeFragment;
import com.skyinfor.businessdistrict.fragment.main.msg.MsgFragment;
import com.skyinfor.businessdistrict.fragment.main.stu.StatusFragment;
import com.skyinfor.businessdistrict.fragment.main.task.TaskFragment;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.service.ChatService;
import com.skyinfor.businessdistrict.util.HttpDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.agora.AgoraAPI;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

/**
 * Created by SKYINFOR on 2018/10/22.
 */

public class MainPresent extends BasePresenter<MainContract.Model> implements MainContract.Presenter {
    public HttpDialog dialog;
    private String TAG = MainPresent.class.getSimpleName();
    private final MainApi api;


    @Inject
    public MainPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(activity);
        api = retrofit.create(MainApi.class);
        dialog.show();
    }

    @Override
    public void addLoginBack() {
        MyApplication.getInstance().getmAgoraAPI().callbackSet(new AgoraAPI.CallBack() {

            @Override
            public void onLoginSuccess(int i, int i1) {
                LogUtil.i(TAG, "onLoginSuccess " + i + "  " + i1);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(mActivity, ChatService.class);
                        mActivity.startService(intent);

                    }
                });
            }

            @Override
            public void onLoginFailed(final int i) {
                Log.i(TAG, "onLoginFailed " + i);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }


            @Override
            public void onError(String s, int i, String s1) {
                LogUtil.i(TAG, "onError s:" + s + " s1:" + s1);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }

        });
    }

    @Override
    public void bindUserId(String user_id, String client_id) {
        addSubscription(api.bindUserId(user_id, client_id)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();

                    }
                });

    }

    @Override
    public void userLocationUpload(String user_id, String position) {
        addSubscription(api.userLocationUpload(user_id, position)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });

    }

}
