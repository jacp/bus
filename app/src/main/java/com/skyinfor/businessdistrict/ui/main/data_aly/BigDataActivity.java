package com.skyinfor.businessdistrict.ui.main.data_aly;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flyco.tablayout.SegmentTabLayout;
import com.github.mikephil.charting.charts.LineChart;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.IndexView;
import com.skyinfor.businessdistrict.custom.RadarView;
import com.skyinfor.businessdistrict.custom.RoundProgress;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.analysis.AnalysisCountModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisOnlineModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisZhiModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BigDataActivity extends BaseActivity<BigDataPresent> implements BigDataContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.progress_one)
    RoundProgress progressOne;
    @BindView(R.id.progress_two)
    RoundProgress progressTwo;
    @BindView(R.id.radar_big_data)
    RadarView radarBigData;
    @BindView(R.id.chart)
    LineChart chart;
    @BindView(R.id.comm_topBarSteep)
    View commTopBarSteep;
    @BindView(R.id.tv_close)
    TextView tvClose;
    @BindView(R.id.tv_back)
    TextView tvBack;
    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.iv_exit)
    ImageView ivExit;
    @BindView(R.id.rl_exit)
    RelativeLayout rlExit;
    @BindView(R.id.ap_bar_stl)
    SegmentTabLayout apBarStl;
    @BindView(R.id.tv_back_top)
    TextView tvBackTop;
    @BindView(R.id.iv_common)
    com.rey.material.widget.ImageView ivCommon;
    @BindView(R.id.im_text)
    TextView imText;
    @BindView(R.id.iv_user)
    ImageView ivUser;
    @BindView(R.id.tv_user)
    TextView tvUser;
    @BindView(R.id.rl_user)
    RelativeLayout rlUser;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    @BindView(R.id.img_right_search)
    ImageView imgRightSearch;
    @BindView(R.id.img_right)
    ImageView imgRight;
    @BindView(R.id.title_search)
    EditText titleSearch;
    @BindView(R.id.t_title)
    Toolbar tTitle;
    @BindView(R.id.title_bar)
    LinearLayout titleBar;
    @BindView(R.id.text_data_stu_count)
    TextView textDataStuCount;
    @BindView(R.id.lin_data_stu_count)
    LinearLayout linDataStuCount;
    @BindView(R.id.text_data_task_count)
    TextView textDataTaskCount;
    @BindView(R.id.lin_data_task_count)
    LinearLayout linDataTaskCount;
    @BindView(R.id.text_data_device_count)
    TextView textDataDeviceCount;
    @BindView(R.id.lin_data_device_count)
    LinearLayout linDataDeviceCount;
    @BindView(R.id.index_one)
    IndexView indexOne;
    @BindView(R.id.index_two)
    IndexView indexTwo;
    @BindView(R.id.index_three)
    IndexView indexThree;

    private List<String> radarTitleList = new ArrayList<>();
    private List<Double> radarValueList = new ArrayList<>();


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_big_data;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        tvTitle.setText("数据分析");
        textRightEditText.setVisibility(View.VISIBLE);
        textRightEditText.setText("商务区");
        Drawable drawable = getResources().getDrawable(R.mipmap.icon_data_head_icon_position);
        textRightEditText.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null,
                null, null);

        mPresenter.dataAnalysis();
    }


    @Override
    public void setCountNum(AnalysisCountModel model) {
        textDataStuCount.setText(model.getCondition_num() + "");
        textDataTaskCount.setText(model.getTask_num() + "");
        textDataDeviceCount.setText(model.getDevice_num() + "");
    }

    @Override
    public void setIndexResult(AnalysisZhiModel model) {
        indexOne.setProgressStr(model.getAnju());
        indexTwo.setProgressStr(model.getShangye());
        indexThree.setProgressStr(model.getZhili());
    }

    @Override
    public void setLineResult(List<HashMap<String, Integer>> lineList) {
        mPresenter.generateDataLine(chart, lineList);
    }

    @Override
    public void setDeviceResult(AnalysisOnlineModel model) {
        progressOne.setProgress(model.getOnline_device_num(), "设备在线率");
        progressTwo.setProgress(model.getOnline_user_num(), "人员在线率");
    }

    @Override
    public void setPeopleResult(HashMap<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            radarTitleList.add(entry.getKey());
            radarValueList.add(Double.valueOf(entry.getValue()));
        }
        radarBigData.setMaxValue(6000);
        radarBigData.setCount(radarTitleList.size());
        radarBigData.setTitles((ArrayList<String>) radarTitleList);
        radarBigData.setData(radarValueList);
    }


}
