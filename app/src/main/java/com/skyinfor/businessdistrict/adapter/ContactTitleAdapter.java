package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class ContactTitleAdapter extends BaseMyRecyclerVIewAdapter<String> {
    private int size;
    private List<String> list;

    public ContactTitleAdapter(Context context, List<String> list) {
        super(context, list, R.layout.item_contact_title);
        this.list = list;

    }

    @Override
    protected void covert(String mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        holder.setText(R.id.text_contact, mode);
        TextView title=holder.getView(R.id.text_contact);

        if (position <size-1 ||position==0) {
            title.setTextColor(mContext.getResources().getColor(R.color.color_2c96f6));
        }else {
            title.setTextColor(mContext.getResources().getColor(R.color.color_search_text_gray));
        }

    }

    public void setNotify(List<String> list) {
        this.list=list;
        size = list.size();
        notifyDataSetChanged();
    }
}
