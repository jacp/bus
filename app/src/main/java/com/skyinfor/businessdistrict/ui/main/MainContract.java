package com.skyinfor.businessdistrict.ui.main;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.RadioGroup;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

/**
 * Created by SKYINFOR on 2018/10/22.
 */

public class MainContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void addLoginBack();

        void bindUserId(String user_id, String client_id);

        void userLocationUpload(String user_id, String position);

    }

}
