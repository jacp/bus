package com.skyinfor.businessdistrict.ui.map.map_view.search;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.RelatedDeviceAdapter;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.detail.person.PersonInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.camera_info.CameraInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.fireplug.FirePlugInfoActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class MapCheckSearchPresent extends BasePresenter<MapCheckSearchContract.Model> implements MapCheckSearchContract.Presenter {

    private List<RelatedDeviceModel> list = new ArrayList<>();
    private MapCheckSearchApi api;
    public HttpDialog dialog;
    private RelatedDeviceAdapter adapter;

    @Inject
    public MapCheckSearchPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(MapCheckSearchApi.class);

    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new RelatedDeviceAdapter(mActivity, list);
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void taskDevice(String name, int type) {
        Observable<Response<List<RelatedDeviceModel>>> observable = null;

        switch (type) {
            case 0:
                observable = api.deviceUserSearch(name);

                break;
            case 1:
                observable = api.deviceSearch(name);

                break;
        }

        addSubscription(observable
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<RelatedDeviceModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<RelatedDeviceModel> relatedDeviceModels) {
                        list.clear();
                        list.addAll(relatedDeviceModels);
                        for (RelatedDeviceModel mode :
                                list) {
                            mode.setExpand(true);
                            List<RelatedDeviceModel.ListBean> data = mode.getList();
                            for (RelatedDeviceModel.ListBean bean :
                                    data) {
                                bean.setShow(true);
                                bean.setTypeName(mode.getName());
                            }
                        }

                        dialog.dismiss();
                        adapter.notifyDataSetChanged();
                    }
                });

    }

}
