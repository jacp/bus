package com.skyinfor.businessdistrict.ui.chat.detail.person.edit;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

import okhttp3.MultipartBody;

public class EditPersonContract {

    interface Model extends MModel {
        void setSucImg(String path);

    }

    interface Presenter extends IPresenter<Model> {
        void updateInfo(List<MultipartBody.Part> parts);
        void upFile(List<MultipartBody.Part> part);

    }

}
