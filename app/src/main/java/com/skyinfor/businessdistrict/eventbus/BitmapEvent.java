package com.skyinfor.businessdistrict.eventbus;

public class BitmapEvent {

  private String localStr;

    public BitmapEvent(String localStr) {
        this.localStr = localStr;
    }

    public String getLocalStr() {
        return localStr;
    }

}
