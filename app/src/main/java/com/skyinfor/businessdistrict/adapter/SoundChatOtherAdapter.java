package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.ImageView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class SoundChatOtherAdapter extends BaseMyRecyclerVIewAdapter<GroupMemberModel.UserListBean> {

    public SoundChatOtherAdapter(Context context, List<GroupMemberModel.UserListBean> list) {
        super(context, list, R.layout.item_sound_chat_self_other);
    }

    @Override
    protected void covert(GroupMemberModel.UserListBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        IconView img = holder.getView(R.id.img_sound_chat_other, position);
        img.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());

    }
}
