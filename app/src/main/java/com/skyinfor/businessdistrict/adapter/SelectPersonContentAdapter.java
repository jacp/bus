package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class SelectPersonContentAdapter extends BaseMyRecyclerVIewAdapter<GroupMemberModel.UserListBean> {
    private boolean isSelect;
    private List<GroupMemberModel.UserListBean> list;
    private boolean isShowSelf = true;

    public SelectPersonContentAdapter(Context context, List<GroupMemberModel.UserListBean> list, boolean isSelect) {
        super(context, list, R.layout.item_contact_person);
        this.list = list;
        this.isSelect = isSelect;
    }

    @Override
    protected void covert(final GroupMemberModel.UserListBean mode, BaseViewHolder holder, final int position) {
        super.covert(mode, holder, position);

        IconView img = holder.getView(R.id.img_contact_person_icon, position);
        img.setBaseInfo(mContext,mode.getNickname(),mode.getAvatar());

        holder.setText(R.id.text_contact_person_name, mode.getNickname());

        holder.setText(R.id.text_contact_person_job, mode.getJob());

        final TextView box = holder.getView(R.id.checkbox_status);
        box.setVisibility(isSelect == true ? View.VISIBLE : View.GONE);

        box.setSelected(mode.isCheck());

        box.setEnabled(false);

    }

    public void setNotify(List<GroupMemberModel.UserListBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

}
