package com.skyinfor.businessdistrict.listener;

import android.support.v7.widget.RecyclerView;
import android.view.View;


/**
 * RecyclerItemClickListener 监听
 * Created by Min on 2017/3/12.
 */

public interface RItemClickListener {
    void onRItemClick(RecyclerView.Adapter adapter, View view, int position);
}
