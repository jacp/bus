package com.skyinfor.businessdistrict.ui.main.stu_detail;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.skyinfor.businessdistrict.adapter.StatusIconAdapter;
import com.skyinfor.businessdistrict.adapter.StatusImgAdapter;
import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.eventbus.UpdateStuEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusDetailPresent extends BasePresenter<StatusDetailContract.Model>
        implements StatusDetailContract.Presenter {
    private List<String> imgList = new ArrayList<>();
    private StatusImgAdapter adapter;
    private StatusIconAdapter handlerAdapter;
    private List<UserIconBean> handlerList = new ArrayList<>();
    private StatusDetailApi api;
    private HttpDialog dialog;


    @Inject
    public StatusDetailPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        api = retrofit.create(StatusDetailApi.class);
        dialog = new HttpDialog(mActivity);

    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new StatusImgAdapter(mActivity, imgList);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, 3));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setImgNotify(String str) {
        imgList.clear();
        String[] tmpData = str.split(",");
        for (String s :
                tmpData) {
            imgList.add(s);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void initAcceptAdapter(RecyclerView recyclerView) {
        handlerAdapter = new StatusIconAdapter(mActivity, handlerList);
        LinearLayoutManager manager = new LinearLayoutManager(mActivity);
        manager.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(handlerAdapter);

    }

    @Override
    public void setAcceptNotify(List<UserIconBean> list) {
        handlerList.clear();
        handlerList.addAll(list);
        handlerAdapter.notifyDataSetChanged();
    }

    @Override
    public void statusChange(String id, final String status_code, final int type) {
        dialog.show();
        Observable<Response<Object>> observable = null;

        switch (type) {
            case 0:
                observable = api.taskStatusChange(id, status_code);
                break;
            case 1:
                observable = api.conditionStatusChange(id, status_code);
                break;
        }

        addSubscription(observable.compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();
                        if (type==0){
                            EventBus.getDefault().post(new UpdateTaskEvent(Integer.valueOf(status_code)));
                        }else {
                            EventBus.getDefault().post(new UpdateStuEvent(Integer.valueOf(status_code)));

                        }

                        mModel.setNotifyView();

                    }
                });

    }

    @Override
    public void deleteTask(String id) {
        dialog.show();

        addSubscription(api.taskDelete(id)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();
                        EventBus.getDefault().post(new UpdateTaskEvent(Integer.valueOf(3)));
                        mModel.finishAction();

                    }
                });

    }

    @Override
    public void getNoticeAction(String rele_id, final String notice_type, final boolean isNotify) {
        dialog.show();

        addSubscription(api.noticeDetail(rele_id, notice_type)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        Gson gson = new Gson();
                        if (notice_type.equals("task")) {
                            TaskSonModel sonModel = gson.fromJson(gson.toJson(o), new TypeToken<TaskSonModel>() {
                            }.getType());
                            if (isNotify){
                                mModel.setNotifyStu(Integer.parseInt(sonModel.getStatus_code()),sonModel.getStatus_name());
                            }else {
                                mModel.setTaskData(sonModel);
                            }
                        } else {
                            StatusSonModel statusSonModel = gson.fromJson(gson.toJson(o), new TypeToken<StatusSonModel>() {
                            }.getType());
                            if (isNotify){
                                mModel.setNotifyStu(Integer.parseInt(String.valueOf(statusSonModel.getStatus_code())),statusSonModel.getStatus_name());
                            }else {
                                mModel.setStatusData(statusSonModel);
                            }
                        }
                    }
                });
    }
}
