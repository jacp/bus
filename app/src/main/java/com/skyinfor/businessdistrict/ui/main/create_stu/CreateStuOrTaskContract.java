package com.skyinfor.businessdistrict.ui.main.create_stu;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;

/**
 * Created by SKYINFOR on 2018/10/24.
 */

public class CreateStuOrTaskContract {

    interface Model extends MModel {
        void setSucImg(int position,String path);
        void setSucVoice(int position,String path);
        void setSucVideo(int position,String path);
        void finishAc();
    }

    interface Presenter extends IPresenter<Model> {
        void initManagerAdapter(RecyclerView recyclerView);
        void initExecuteAdapter(RecyclerView recyclerView);
        void setNotifyManager(List<UserIconBean> list);
        void setNotifyHandler(List<UserIconBean> list);
        void uploadStuData(List<MultipartBody.Part> part);
        void uploadTaskData(List<MultipartBody.Part> part,int type);
        void upFile(List<MultipartBody.Part> part,int position,int type);
    }

}
