package com.skyinfor.businessdistrict.util;

import com.skyinfor.businessdistrict.model.ContactAllModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class JsonUtils {

//    /**
//     * 根据参数rule的规则提取json数据
//     * @param jsonString 待解析的json数据
//     * @param rule jsonString的逐层key值，如，rule1="data,tagLibs[]";表示获取jsonString下data里tagLibs数组的某一个。[]标识获取JSONArray，否则获取JSONObject
//     * @return 返回JSONObject的toString格式
//     * @throws JSONException
//     */
//    public String dealWithRule(String jsonString, String rule) throws JSONException {
//        String[] rules = rule.split(",");
//
//        for (String key :
//                rules) {
//            if (!key.contains("[]")){
//                jsonString = parseJson(jsonString,0,key);//递归
//            }else {
//                jsonString = parseJson(jsonString,1,key.substring(0,(key.length()-2)));
//            }
//        }
//        return jsonString;
//    }
//
//    /**
//     * 根据key值获取数据
//     * @param jsonStr
//     * @param type 0标识获取JSONObject，1标识获取JSONArray
//     * @param key JSONObject的key值
//     * @return   无论是JSONObject还是JSONArray，都在最后toString,因为还不知道泛型怎么用
//     * @throws JSONException
//     */
//    public String parseJson(String jsonStr,int type,String key) throws JSONException {
//        JSONObject jsonObject = new JSONObject(jsonStr);
//        if (type==0){
//            JSONObject jsonObject1= (JSONObject) jsonObject.get(key);
//            return jsonObject1.toString();
//        }else {
//            JSONArray jsonObject1=  jsonObject.getJSONArray(key);
//            int length = jsonObject1.length();
//            Random rand = new Random();
//            int random = rand.nextInt(length);
//            MyLog.i("JsonUtils","random="+random);
//            JSONObject jsonObject2 = (JSONObject) jsonObject1.get(random);
//            return jsonObject2.toString();
//        }
//    }


    @SuppressWarnings("rawtypes")
    public static List<ContactAllModel> analysisJson(Object objJson, ContactAllModel menu, List<ContactAllModel> list) {
        // 如果obj为json数组
        if (objJson instanceof JSONArray) {
            //将接收到的对象转换为JSONArray数组
            JSONArray objArray = (JSONArray) objJson;
            //对JSONArray数组进行循环遍历
            for (int i = 0; i < objArray.length(); i++) {
                //新建menu对象
                ContactAllModel menu1 = new ContactAllModel();
                //设置menu属性
                try {

                    ((JSONObject) objArray.get(i)).get("name").toString();
//                    //将该级菜单对象存进list集合中
//                    list.add(menu1);
                    //调用回调方法
                    analysisJson(objArray.get(i), menu1, list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            // 如果为json对象
        } else if (objJson instanceof JSONObject) {
            //将Object对象转换为JSONObject对象
            JSONObject jsonObject = (JSONObject) objJson;
            //迭代多有的Key值
            Iterator it = jsonObject.keys();
            //遍历每个Key值
            while (it.hasNext()) {
                //将key值转换为字符串
                String key = it.next().toString();
                //根据key获取对象
                Object object = null;
                try {
                    object = jsonObject.get(key);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // 如果得到的是数组
                if (object instanceof JSONArray) {
                    //将Object对象转换为JSONObject对象
                    JSONArray objArray = (JSONArray) object;
                    //调用回调方法
                    analysisJson(objArray, menu, list);
                }
                // 如果key中是一个json对象
                else if (object instanceof JSONObject) {
                    //调用回调方法
                    analysisJson((JSONObject) object, menu, list);
                }
            }
        }
        return list;
    }


}
