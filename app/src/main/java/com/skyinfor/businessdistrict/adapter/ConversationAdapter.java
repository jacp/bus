package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.model.ConversationModel;
import com.skyinfor.businessdistrict.util.DateUtil;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.skyinfor.businessdistrict.util.TimeUtil.getBaseTimeFormatText;

public class ConversationAdapter extends BaseMyRecyclerVIewAdapter<ConversationModel> {

    public ConversationAdapter(Context context, List<ConversationModel> list) {
        super(context, list, R.layout.item_conversation);
    }

    @Override
    protected void covert(ConversationModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);
        setTypeInfo(mode.getList_type(), holder, mode);
    }

    /**
     * 会话列表新增加了一个type：系统通知，字段名：list_type，字段值：system_notice；
     * 现有三种type：single（单聊），group（群聊），system_notice（系统通知）；
     * 系统通知：notice_name（通知名称），notice_type（通知类型，包括情况和任务两种）
     *
     * @param str
     * @return
     */
    private void setTypeInfo(String str, BaseViewHolder holder, ConversationModel mode) {
        TextView name = holder.getView(R.id.text_con_name);
        TextView content = holder.getView(R.id.text_con_content);
        TextView date = holder.getView(R.id.text_con_date);
        TextView num = holder.getView(R.id.text_con_num);

        num.setVisibility(mode.getNew_msg_num() > 0||mode.getUnread_num() > 0 ? View.VISIBLE : View.INVISIBLE);
        IconView iconView = holder.getView(R.id.icon_conversation);
        num.setText(mode.getNew_msg_num() + "");

        switch (str) {
            case "single":
                name.setText(mode.getNickname());
                content.setText(mode.getContent());
                iconView.setBaseInfo(mContext, mode.getNickname(), mode.getAvatar());

                break;
            case "group":
                iconView.setBaseInfo(mContext, "群聊", "");

                name.setText(mode.getGroup_name());
                content.setText(String.format("%s：%s", mode.getNickname(), mode.getContent()));

                break;
            case "system_notice":
                num.setText(mode.getUnread_num() + "");

                iconView.setBaseInfo(mContext, "通知","",R.drawable.shape_icon_view_bac_orange);
                name.setText("通知");

                String contentStr = String.format("[%s][%s]%s", mode.getNotice_status(),
                        mode.getNotice_type().equals("condition") ? "情况" : "任务",
                        TextUtils.isEmpty(mode.getNotice_name()) == true ? "" : mode.getNotice_name());
                content.setText(contentStr);
                setStatusIndex(contentStr, mode.getNotice_status(), content);
                break;
        }

        if (!TextUtils.isEmpty(mode.getTime())) {
            date.setText(getBaseTimeFormatText(DateUtil.strToDate(mode.getTime(), 9)));
        }

    }

    private void setStatusIndex(String str, String noticeStatus, TextView tv) {
        String re = "^\\[\\w+]";
        int start = 0;
        int end = 0;

        Pattern pattern = Pattern.compile(re);
        Matcher mt = pattern.matcher(str);
        while (mt.find()) {
            end = mt.group(0).length();
            break;
        }

        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new ForegroundColorSpan(mContext.getResources()
                        .getColor(getColor(noticeStatus))),
                start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(spannableString);

    }

    /**
     * 待处理
     * 处理中 /执行中
     * 待审核
     * 已完成
     *
     * @param index
     */
    private int getColor(String index) {
        int color = 0;

        switch (index) {
            case "待处理":
                color = R.color.color_status_red;
                break;
            case "执行中":
            case "处理中":
                color = R.color.color_status_orange;
                break;
            case "待审核":
                color = R.color.color_status_blue;
                break;
            case "已完成":
                color = R.color.color_status_green;
                break;
            case "已拒绝":
                color = R.color.color_status_red;
                break;
            default:
                color = R.color.color_status_red;
                break;
        }
        return color;
    }

}
