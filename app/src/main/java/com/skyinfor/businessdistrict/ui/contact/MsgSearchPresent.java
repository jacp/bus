package com.skyinfor.businessdistrict.ui.contact;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.ConversationAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.ContactAllModel;
import com.skyinfor.businessdistrict.model.ConversationModel;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.ui.chat.system_notice.SystemNoticeActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class MsgSearchPresent extends BasePresenter<MsgSearchContract.Model>
        implements MsgSearchContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private MsgSearchApi api;
    private HttpDialog dialog;
    private List<ConversationModel> list = new ArrayList<>();
    private ConversationAdapter adapter;


    @Inject
    public MsgSearchPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(MsgSearchApi.class);


    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new ConversationAdapter(mActivity, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(1, mActivity),
                Color.parseColor("#efeff4")));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

    }

    @Override
    public void getConversationList(String userId, String keyword) {
        dialog.show();

        addSubscription(api.talkUserList(userId, keyword)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<ConversationModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(List<ConversationModel> conversationModels) {
                        list.clear();
                        list.addAll(conversationModels);
                        adapter.notifyDataSetChanged();

                        dialog.dismiss();

                    }
                });

    }

    @Override
    public void onItemClick(View view, int position) {
        if (list.get(position).getList_type().equals("system_notice")) {
            UIHelper.startActivity(mActivity, SystemNoticeActivity.class);

        } else {
            Bundle data = new Bundle();
            data.putSerializable("model", list.get(position));
            data.putString("avatar", list.get(position).getAvatar());
            UIHelper.startActivity(mActivity, ChatRoomActivity.class, data);
            if (list.get(position).getList_type().equals("single")) {
                talkRecordStatus(Constants.USER_ID + "",
                        list.get(position).getUser_id() + "");
            }
        }
    }

    @Override
    public void talkRecordStatus(String user_id, String target_id) {

        addSubscription(api.talkRecordStatus(user_id, target_id)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {

                    }
                });

    }

    @Override
    public void setNotify() {
        list.clear();
        adapter.notifyDataSetChanged();
    }

}
