package com.skyinfor.businessdistrict.custom.view;

import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;

/**
 * 重写事件分发，让划出来也可以响应事件
 * Created by
 * on 2017/3/9.
 */

public class DrawerLayoutEx extends DrawerLayout {
    public DrawerLayoutEx(Context context) {
        super(context);
    }

    public DrawerLayoutEx(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawerLayoutEx(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev){
        /**
         * 原理很简单，根据按下的位置，是在contentView内还是外，
         * 在COntentView区域内则返回false，这次消息本宝宝不处理了，之后将直接交由对应区域的子view处理
         * 然后解决了
         */
        switch(ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                final float x = ev.getX();
                final float y = ev.getY();
                final View touchedView = findTopChildUnder((int) x, (int) y);
                if (touchedView != null && isContentView(touchedView)
                        && this.isDrawerOpen(GravityCompat.START)) {
                    return false;
                }
                break;

            default:
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    /**
     * 判断点击位置是否位于相应的View内
     * @param x
     * @param y
     * @return
     */
    public View findTopChildUnder(int x, int y) {
        final int childCount = getChildCount();
        for (int i = childCount - 1; i >= 0; i--) {
            final View child = getChildAt(i);
            if (x >= child.getLeft() && x < child.getRight() &&
                    y >= child.getTop() && y < child.getBottom()) {
                return child;
            }
        }
        return null;
    }

    /**
     * 判断点击触摸点的View是否是ContentView(即是主页面的View)
     * @param child
     * @return
     */
    boolean isContentView(View child) {
        return ((LayoutParams) child.getLayoutParams()).gravity == Gravity.NO_GRAVITY;
    }
}
