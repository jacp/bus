package com.skyinfor.businessdistrict.app;

/**
 * Created by min on 2017/4/12.
 * 接口地址
 */

public class ApiUrl {
    //登录
    public static final String Logoin = "app/v1/login";

    //我收到的情况列表
    public static final String ConditionAcceptList = "app/v1/conditionAcceptList";

    //我上报的情况列表
    public static final String ConditionSendList = "app/v1/conditionSendList";

    //情况类型列表
    public static final String conditionTypeList = "app/v1/conditionTypeList";

    //上报的情况
    public static final String conditionUpload = "app/v1/conditionUpload";

    //上报的任务
    public static final String conditionSendList = "app/v1/taskUpload";

    //上传文件
    public static final String commonUpload = "commonUpload";

    //情况类型列表
    public static final String taskTypeList = "app/v1/taskTypeList";

    //通讯录
    public static final String userList = "app/v1/userList";

    //通讯录
    public static final String getFullList = "app/v1/userAddressList";

    //任务关联的设备列表
    public static final String taskDevice = "app/v1/taskDevice";

    //我派发的任务列表
    public static final String taskSendList = "app/v1/taskSendList";

    //我接收的任务列表
    public static final String taskAcceptList = "app/v1/taskAcceptList";

    //通讯录搜索
    public static final String searchUser = "app/v1/searchUser";

    //任务状态值改变
    public static final String taskStatusChange = "app/v1/taskStatusChange";

    //情况状态值改变
    public static final String conditionStatusChange = "app/v1/conditionStatusChange";

    //删除任务
    public static final String taskDelete = "app/v1/taskDelete";

    //所有任务
    public static final String taskAll = "app/v1/taskAll";

    //所有情况
    public static final String conditionAll = "app/v1/conditionAll";

    //会话列表
    public static final String talkUserList = "app/v1/talkUserList";

    //群员列表
    public static final String talkGroupInformation = "app/v1/talkGroupInformation";

    //更改群昵称
    public static final String groupNameModify = "app/v1/groupNameModify";

    //退出群聊
    public static final String groupSignOut = "app/v1/groupSignOut";

    //个人信息详情
    public static final String userInfo = "app/v1/userInfo";

    //创建群聊
    public static final String talkGroupCreate = "app/v1/talkGroupCreate";

    //系统通知列表
    public static final String userNoticeList = "app/v1/userNoticeList";

    //群聊天记录
    public static final String talkGroupList = "app/v1/talkGroupList";

    //聊天记录
    public static final String talkList = "app/v1/talkList";

    //通知详情
    public static final String noticeDetail = "app/v1/noticeDetail";

    //聊天记录存储
    public static final String talkRecordSave = "app/v1/talkRecordSave";

    //群聊天记录存储
    public static final String talkGroupRecordSave = "app/v1/talkGroupRecordSave";

    //聊天记录已读未读状态改变
    public static final String talkRecordStatus = "app/v1/talkRecordStatus";

    //修改个人信息
    public static final String userInformationUpdate = "app/v1/userInformationUpdate";

    //地图查看
    public static final String deviceList = "app/v1/deviceList";

    //地图查看
    public static final String bindUserId = "bindUserId";

    //公告列表banner
    public static final String noticeList = "app/v1/noticeList";

    //未处理情况和任务数量
    public static final String waitTasksCount = "app/v1/waitTasksCount";

    //最新事件
    public static final String newTaskList = "app/v1/newTaskList";

    //预案列表
    public static final String planList = "app/v1/planList";

    //触发记录
    public static final String planLog = "app/v1/planLog";

    //设备搜索
    public static final String deviceUserSearch = "app/v1/deviceUserSearch";

    //设备搜索
    public static final String deviceSearch = "app/v1/deviceSearch";

    //数据分析
    public static final String dataAnalysis = "app/v1/dataAnalysis";

    //位置上传
    public static final String userLocationUpload="app/v1/userLocationUpload";

}
