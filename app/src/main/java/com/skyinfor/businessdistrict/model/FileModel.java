package com.skyinfor.businessdistrict.model;

public class FileModel {


    /**
     * save_path : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bd2bcacf4056.jpg
     */

    private String save_path;

    public String getSave_path() {
        return save_path;
    }

    public void setSave_path(String save_path) {
        this.save_path = save_path;
    }
}
