package com.skyinfor.businessdistrict.ui.main.plan_check.plan_detail;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.PlanListModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlanDetailActivity extends BaseActivity<PlanDetailPresent> implements PlanDetailContract.Model {
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_plan_title)
    TextView textPlanTitle;
    @BindView(R.id.text_plan_info_rec_name)
    TextView textPlanInfoRecName;
    @BindView(R.id.text_plan_info_rec_address)
    TextView textPlanInfoRecAddress;
    @BindView(R.id.text_plan_info_rec_time)
    TextView textPlanInfoRecTime;
    @BindView(R.id.text_plan_info_rec_type)
    TextView textPlanInfoRecType;
    @BindView(R.id.icon_plan_info_rec_from)
    IconView iconPlanInfoRecFrom;
    @BindView(R.id.text_plan_info_rec_from)
    TextView textPlanInfoRecFrom;
    @BindView(R.id.lin_plan_rec_info)
    LinearLayout linPlanRecInfo;
    @BindView(R.id.text_plan_info_name)
    TextView textPlanInfoName;
    @BindView(R.id.text_plan_info_time)
    TextView textPlanInfoTime;
    @BindView(R.id.text_plan_info_type)
    TextView textPlanInfoType;
    @BindView(R.id.rec_plan_info_related_person)
    RecyclerView recPlanInfoRelatedPerson;
    @BindView(R.id.lin_plan_info)
    LinearLayout linPlanInfo;
    @BindView(R.id.rec_plan_operation_step)
    RecyclerView recPlanOperationStep;
    @BindView(R.id.rec_plan_linkage)
    RecyclerView recPlanLinkage;
    @BindView(R.id.lin_plan_operation_step)
    LinearLayout linPlanOperationStep;

    private int type;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plan_detail;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        type = getIntent().getIntExtra("type", 0);
        PlanListModel model = (PlanListModel) getIntent().getSerializableExtra("model");

        linPlanInfo.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
        linPlanRecInfo.setVisibility(type == 0 ? View.GONE : View.VISIBLE);
        tvTitle.setText(type == 0 ? "预案详情" : "触发详情");
        mPresenter.initAdapter(recPlanInfoRelatedPerson, recPlanOperationStep, recPlanLinkage, type);

        recPlanLinkage.setFocusable(false);

        textPlanTitle.requestFocus();
        textPlanTitle.setFocusable(true);
        textPlanTitle.setFocusableInTouchMode(true);

        if (type == 0) {
            setPlanInfo(model);
        } else {
            setRecordInfo(model);
        }

    }

    private void setPlanInfo(PlanListModel model) {
        if (model != null) {
            textPlanTitle.setText(model.getName());
            textPlanInfoName.setText(model.getName());
            textPlanInfoTime.setText(model.getCreate_time());
            if (model.getPlan_condition() != null && model.getPlan_condition().size() > 0) {
                textPlanInfoType.setText(model.getPlan_condition().get(0).getCondition_name());

            }
            linPlanOperationStep.setVisibility(model.getPlan_condition() != null
                    && model.getPlan_condition().size() > 0 ? View.VISIBLE : View.GONE);

            mPresenter.setNotify(model);

        }

    }

    private void setRecordInfo(PlanListModel model) {
        if (model != null) {
            textPlanTitle.setText(model.getName());
            textPlanInfoRecName.setText(model.getName());
            textPlanInfoRecAddress.setText(model.getAddress());
            textPlanInfoRecTime.setText(model.getCreate_time());
            if (model.getPlan_condition() != null && model.getPlan_condition().size() > 0) {
                textPlanInfoRecType.setText(model.getPlan_condition().get(0).getCondition_name());
                textPlanInfoRecFrom.setText(model.getNickname());
                iconPlanInfoRecFrom.setTag(R.id.icon_plan_info_rec_from);
                iconPlanInfoRecFrom.setBaseInfo(mContext, model.getNickname(), model.getAvatar());
            }
            linPlanOperationStep.setVisibility(model.getPlan_condition() != null
                    && model.getPlan_condition().size() > 0 ? View.VISIBLE : View.GONE);

            mPresenter.setNotify(model);

        }

    }

}
