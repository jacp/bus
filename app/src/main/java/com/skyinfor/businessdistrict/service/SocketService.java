package com.skyinfor.businessdistrict.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.eventbus.UpdateStuEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;
import com.skyinfor.businessdistrict.eventbus.socket.SocketActionEvent;
import com.skyinfor.businessdistrict.eventbus.socket.SocketCallEvent;
import com.skyinfor.businessdistrict.eventbus.socket.UpdateStuDetailEvent;
import com.skyinfor.businessdistrict.model.SocketModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class SocketService extends Service {
    private String TAG = SocketService.class.getSimpleName();
    private WebSocketClient mWebSocketClient;
    private String address = "ws://skyinfor.cc:58888";
    private URI uri;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        initSocket();
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * socket初始化和监听
     */
    private void initSocket() {
        try {
            uri = new URI(address);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        if (null == mWebSocketClient) {
            mWebSocketClient = new WebSocketClient(uri) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    Log.i(TAG, "onOpen: ");
                    Constants.IS_DIS_SOCKET=true;
                }

                @Override
                public void onMessage(String s) {
                    Log.i(TAG, "onMessage: " + s);
                    if (s.contains("client_id")) {

                    }

                    Gson gson = new Gson();
                    SocketModel mode = gson.fromJson(s, new TypeToken<SocketModel>() {
                    }.getType());

                    switch (mode.getType()){
                        case "connect_socket":
                            EventBus.getDefault().post(new SocketCallEvent(1, mode.getData().getClient_id()));
                            break;
                        case "task":
                            EventBus.getDefault().post(new UpdateTaskEvent(Integer.valueOf(mode.getData().getStatus_code())));
                            EventBus.getDefault().post(new UpdateStuDetailEvent(mode.getType(),Integer.valueOf(mode.getData().getStatus_code())));

                            break;
                        case "condition":
                            EventBus.getDefault().post(new UpdateStuEvent(Integer.valueOf(mode.getData().getStatus_code())));
                            EventBus.getDefault().post(new UpdateStuDetailEvent(mode.getType(),Integer.valueOf(mode.getData().getStatus_code())));

                            break;
                    }

                }

                @Override
                public void onClose(int i, String s, boolean b) {
                    Log.i(TAG, "onClose: ");
                }

                @Override
                public void onError(Exception e) {
                    Log.i(TAG, "onError: ");
                    Constants.IS_DIS_SOCKET=false;
                }

                @Override
                public void closeConnection(int code, String message) {
                    super.closeConnection(code, message);
                    Log.i(TAG, "closeConnection:"+message);
                    Constants.IS_DIS_SOCKET=false;

                    mWebSocketClient.reconnect();
                }
            };
            mWebSocketClient.connect();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (mWebSocketClient != null) {
            mWebSocketClient = null;
        }
    }

    /**
     * event.getAction()
     *
     * @param event
     */
    @Subscribe
    public void getEventAction(SocketActionEvent event) {
        switch (event.getAction()) {
            case 1://发送信息
                mWebSocketClient.send(event.getMsg());

                break;
            case 2://重连
                if (!mWebSocketClient.isConnecting()) {
                    mWebSocketClient.reconnect();
                }
                break;
            case 3://断开连接
                mWebSocketClient.close();
                break;
        }
    }


}
