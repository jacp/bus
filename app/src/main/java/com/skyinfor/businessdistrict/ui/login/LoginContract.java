package com.skyinfor.businessdistrict.ui.login;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

/**
 * Created by SKYINFOR on 2018/10/22.
 */

public class LoginContract {

    interface Model extends MModel {
        void setResultAction();

    }

    interface Presenter extends IPresenter<Model> {
        void LoginAction(String name,String pass);

    }

}
