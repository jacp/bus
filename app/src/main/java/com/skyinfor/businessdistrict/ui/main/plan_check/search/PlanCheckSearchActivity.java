package com.skyinfor.businessdistrict.ui.main.plan_check.search;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.ClearEditText;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class PlanCheckSearchActivity extends BaseActivity<PlanCheckSearchPresent> implements PlanCheckSearchContract.Model, TextWatcher {
    @BindView(R.id.edit_search_task)
    ClearEditText editSearchTask;
    @BindView(R.id.text_search_task_cancel)
    TextView textSearchTaskCancel;
    @BindView(R.id.rec_search)
    RecyclerView recSearch;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    private int page;
    private int type;
    private String name;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_search_task_and_stu;
    }


    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        initRefresh(ptrStu, recSearch);
        getVis();

        type = getIntent().getIntExtra("type", 0);
        editSearchTask.addTextChangedListener(this);
        mPresenter.initAdapter(recSearch);
    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page = 1;
        initNetData();
    }

    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page++;
        initNetData();
    }

    private void initNetData() {
        mPresenter.gePlanList(name, page, type);
    }


    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrStu != null)
            ptrStu.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrStu != null) {
            ptrStu.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrStu != null) {
            ptrStu.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrStu != null) {
            ptrStu.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }


    @OnClick(R.id.text_search_task_cancel)
    public void onViewClicked() {
        appManager.finishActivity();

    }


    private void getVis() {
        String str = editSearchTask.getText().toString().trim();
        if (!TextUtils.isEmpty(str)) {
            ptrStu.setVisibility(View.VISIBLE);
        } else {
            ptrStu.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        name = String.valueOf(charSequence);
        getVis();

        if (name.length() > 0) {
            mPresenter.dialog.show();
            initNetData();
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
