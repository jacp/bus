package com.skyinfor.businessdistrict.ui.chat.detail.group;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.skyinfor.businessdistrict.adapter.ChatDetailMemberAdapter;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.chat.detail.group.member.GroupMemberDecActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.SpaceItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class ChatDetailPresent extends BasePresenter<ChatDetailContract.Model> implements
        ChatDetailContract.Presenter, BaseMyRecyclerVIewAdapter.setOnItemClickListener {

    private ChatDetailApi api;
    private HttpDialog dialog;
    private List<GroupMemberModel.UserListBean> list = new ArrayList<>();
    private ChatDetailMemberAdapter adapter;
    private List<GroupMemberModel.UserListBean> moreList = new ArrayList<>();

    @Inject
    public ChatDetailPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(ChatDetailApi.class);

    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new ChatDetailMemberAdapter(mActivity, list, true);
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity, 5));
        recyclerView.addItemDecoration(new SpaceItemDecoration(dp2px(16, mActivity)));
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);

    }

    @Override
    public void getGroupInfo(String uuId) {
        dialog.show();
        addSubscription(api.talkGroupInformation(uuId)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<GroupMemberModel>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(GroupMemberModel groupMemberModel) {
                        list.clear();
                        if (groupMemberModel.getUser_list().size() > 15) {
                            list.addAll(groupMemberModel.getUser_list().subList(0, 15));
                        } else {
                            list.addAll(groupMemberModel.getUser_list());
                        }
                        moreList.addAll(groupMemberModel.getUser_list());
                        adapter.notifyDataSetChanged();
                        mModel.setResult(groupMemberModel);

                    }
                });

    }

    @Override
    public void exitGroup(String group_uuid, String user_id) {
        dialog.show();

        addSubscription(api.groupSignOut(group_uuid, user_id)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        mModel.exitResult();
                        dialog.dismiss();
                    }
                });

    }

    @Override
    public void onItemClick(View view, int position) {
        if (position == 14) {
            Bundle data = new Bundle();
            data.putSerializable("model", (Serializable) moreList);
            UIHelper.startActivity(mActivity, GroupMemberDecActivity.class, data);

        } else {

        }

    }
}
