package com.skyinfor.businessdistrict.fragment.main.task.task_sec;

import android.app.Activity;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.skyinfor.businessdistrict.adapter.BaseFragmentAdapter;
import com.skyinfor.businessdistrict.fragment.main.stu.stu_item.StatusSonFragment;
import com.skyinfor.businessdistrict.fragment.main.task.task_item.TaskSonFragment;
import com.skyinfor.businessdistrict.presenter.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.skyinfor.businessdistrict.util.UiUtils.reflex;

public class TaskSecPresent extends BasePresenter<TaskSecContract.Model>
        implements TaskSecContract.Presenter {

    private List<String> titleList = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();

    @Inject
    public TaskSecPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }

    @Override
    public void initAdapter(FragmentManager fm, ViewPager viewPager, TabLayout tabLayout) {
        titleList.add("全部");
        titleList.add("待处理");
        titleList.add("执行中");
        titleList.add("待审核");
        titleList.add("已完成");
        for (int i = 0; i < 5; i++) {
            TaskSonFragment fragment = TaskSonFragment.getInstance();
            fragment.setmFragmentTitle(i + "");
            fragments.add(fragment);
        }
        BaseFragmentAdapter adapter = new BaseFragmentAdapter(fm, titleList, fragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(5);

        reflex(tabLayout, 15);
    }

    @Override
    public void setNotify(int selectType) {
        TaskSonFragment fragment = (TaskSonFragment) fragments.get(selectType);
        fragment.setNotify(selectType);

    }

}
