package com.skyinfor.businessdistrict.ui.chat.room;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.util.Log;

import com.skyinfor.businessdistrict.adapter.ChatContentAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.model.FileModel;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.agora.AgoraAPI;
import io.agora.AgoraAPIOnlySignal;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class ChatRoomPresent extends BasePresenter<ChatRoomContract.Model> implements
        ChatRoomContract.Presenter {

    private String TAG = ChatRoomPresent.class.getSimpleName();
    private HttpDialog dialog;
    private ChatRoomApi api;
    private List<ChatContentModel> list = new ArrayList<>();
    private ChatContentAdapter adapter;
    private AgoraAPIOnlySignal agoraAPI;
    private int type;
    private RecyclerView recyclerView;
    private List<GroupMemberModel.UserListBean>groupList=new ArrayList<>();

    @Inject
    public ChatRoomPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(ChatRoomApi.class);
        dialog.show();

    }


    @Override
    public void initAdapter(RecyclerView recyclerView) {
        adapter = new ChatContentAdapter(mActivity, list);
        this.recyclerView = recyclerView;
        this.recyclerView.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity),
                Color.parseColor("#efeff4")));
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(adapter);
    }

    /**
     * @param part
     * @param type 0 单聊 1 q群聊
     * @param page
     */
    @Override
    public void getConversationList(List<MultipartBody.Part> part, int type, final int page
            , final String nickName, final String ava) {
        this.type = type;

        Observable<Response<List<ChatContentModel>>> observable = null;
        switch (type) {
            case 0:
                observable = api.talkList(part);
                break;
            case 1:
                observable = api.talkGroupList(part);
                break;
        }

        addSubscription(observable
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<ChatContentModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mModel.compelete();
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(List<ChatContentModel> chatContentModels) {
                        int size=chatContentModels.size();

                        if (page == 1) {
                            list.clear();
                        }

                        if (chatContentModels != null) {
                            list.addAll(chatContentModels);
                            ListSort(list);
                            if (!TextUtils.isEmpty(nickName)) {
                                for (ChatContentModel mode :
                                        list) {
                                    if (TextUtils.isEmpty(mode.getGroup_uuid())) {
                                        mode.setNickname(nickName);
                                        if (!TextUtils.isEmpty(ava)){
                                            mode.setAvatar(ava);
                                        }
                                    }
                                }
                            }
                        }

                        mModel.close();
                        mModel.noLoadMode();
                        if (page!=1){
                            adapter.notifyDataSetChanged();
                        }else {
                            adapter.notifyItemRangeChanged(size,list.size());
                        }
                        adapter.notifyDataSetChanged();

                        if (page == 1) {
                            recyclerView.scrollToPosition(adapter.getItemCount() - 1);
                        }

                    }

                });
    }


    @Override
    public void talkRecordSave(List<MultipartBody.Part> part, int type, final String nickName) {
        Observable<Response<Object>> observable;
        if (type == 0) {
            observable = api.talkRecordSave(part);
        } else {
            observable = api.talkGroupRecordSave(part);
        }

        addSubscription(observable
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(Object o) {
                        EventBus.getDefault().post(new ChatNameEvent(""));
                    }
                });

    }

    @Override
    public void setNotifySend(ChatContentModel model) {
        list.add(model);
        adapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);

    }

    private List<ChatContentModel> ListSort(List<ChatContentModel> list) {
        Collections.sort(list, new Comparator<ChatContentModel>() {
            @Override
            public int compare(ChatContentModel o1, ChatContentModel o2) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date dt1 = format.parse(o1.getTime());
                    Date dt2 = format.parse(o2.getTime());
                    if (dt1.getTime() > dt2.getTime()) {
                        return 1;
                    } else if (dt1.getTime() < dt2.getTime()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return 0;
            }
        });
        return list;
    }

    public void talkRecordStatus(String user_id, final String target_id, final String name) {
        addSubscription(api.talkRecordStatus(user_id, target_id)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Object o) {
                        Pattern pattern = Pattern.compile("[0-9]");
                        Matcher matcher = pattern.matcher(target_id);

                        //刷新消息列表的未读状态
                        EventBus.getDefault().post(new ChatNameEvent(""));

                    }
                });

    }

    @Override
    public void upFile(List<MultipartBody.Part> part, final int position) {
        dialog.show();

        addSubscription(api.upFile(part)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<FileModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(FileModel bean) {
                        mModel.setSucImg(bean.getSave_path());
                    }
                });

    }

    @Override
    public void getGroupInfo(String uuId) {
        dialog.show();
        addSubscription(api.talkGroupInformation(uuId)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<GroupMemberModel>() {
                    @Override
                    public void onCompleted() {
                        dialog.dismiss();

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNext(GroupMemberModel groupMemberModel) {
                        groupList.clear();
                        groupList.addAll(groupMemberModel.getUser_list());
                        mModel.setGroupList(groupList);
                    }
                });

    }

}
