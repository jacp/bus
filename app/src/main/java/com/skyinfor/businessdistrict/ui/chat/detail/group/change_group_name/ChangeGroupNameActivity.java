package com.skyinfor.businessdistrict.ui.chat.detail.group.change_group_name;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.util.ToashUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;

public class ChangeGroupNameActivity extends BaseActivity<ChangeGroupNamePresent>
        implements ChangeGroupNameContract.Model {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.text_change_name)
    EditText textChangeName;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    private String uuid;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_change_group_name;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        textRightEditText.setVisibility(View.VISIBLE);
        textRightEditText.setText("完成");
        tvTitle.setText("群名修改");
        uuid = getIntent().getStringExtra("uuid");
        String name = getIntent().getStringExtra("name");
        textChangeName.setText(name);
        textChangeName.setSelection(name.length());

    }

    @Override
    public void setResult(String name) {
        EventBus.getDefault().post(new ChatNameEvent(name));
        EventBus.getDefault().post(new ChatNotifyEvent());
        appManager.finishActivity();
    }

    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        String text = textChangeName.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            ToashUtils.show(mContext, "文字不能为空！");
        } else {
            mPresenter.ChangeNameAction(uuid, text);
        }
    }


}
