package com.skyinfor.businessdistrict.ui.main.notice;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class NoticeActivity extends BaseActivity<NoticePresent> implements NoticeContact.Model {

    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    private int page = 1;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_notice;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        initRefresh(ptrStu, rvList);
        tvTitle.setText("信息公告");
        mPresenter.initAdapter(rvList);
        initNetData();

    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page = 1;
        initNetData();
    }

    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page++;
        initNetData();
    }

    private void initNetData() {
        mPresenter.noticeList(String.valueOf(page), "10");

    }


    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }
}
