package com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.fragment.main.contact.ContactFragment;
import com.skyinfor.businessdistrict.model.PersonListBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.skyinfor.businessdistrict.util.Utils.listToString;

public class TaskPersonSelectActivity extends BaseActivity<TaskPersonSelectPresent>
        implements TaskPersonSelectContract.Model {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.fl_task_person)
    FrameLayout flTaskPerson;
    @BindView(R.id.text_right_edit_text)
    TextView textRightEditText;
    private ContactFragment fragment;
    private List<PersonListBean> personListBeanList;
    private List<String> mIdList = new ArrayList<>();
    private List<String> mNameList = new ArrayList<>();
    private int type;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_task_person;
    }

    @Override
    protected void initView() {
        super.initView();
        type = getIntent().getIntExtra("type", 0);

        personListBeanList = (List<PersonListBean>) getIntent().getSerializableExtra("model");
        tvTitle.setText(type == 0 ? "任务接收人" : "选择人员");
        textRightEditText.setVisibility(View.VISIBLE);
        textRightEditText.setText("完成");

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction bg = fm.beginTransaction();
        fragment = ContactFragment.getInstance(true, personListBeanList,false);
        bg.add(R.id.fl_task_person, fragment);
        bg.commitAllowingStateLoss();
    }

    @OnClick(R.id.text_right_edit_text)
    public void onViewClicked() {
        if (type == 0) {
            List<PersonListBean> list = fragment.getPersonListBeanList();
            if (list != null && list.size() > 0) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("model", (Serializable) list);
                intent.putExtras(bundle);
                setResult(101, intent);
                appManager.finishActivity();
            }
        } else {
            List<PersonListBean> list = fragment.getPersonListBeanList();
            mIdList.clear();
            mNameList.clear();
            if (list!=null){
                for (PersonListBean model : list) {
                    mIdList.add(String.valueOf(model.getId()));
                    mNameList.add(model.getNickname());
                }
                mIdList.add(String.valueOf(Constants.USER_ID));
                mNameList.add(Constants.NICK_NAME);

                String idStr = listToString(mIdList, ',');
                String nameStr = listToString(mNameList, ',');
                mPresenter.createGroup(nameStr, idStr,
                        String.valueOf(Constants.USER_ID));
            }
        }
    }

    @Override
    public void setResult() {
        appManager.finishActivity();

    }
}
