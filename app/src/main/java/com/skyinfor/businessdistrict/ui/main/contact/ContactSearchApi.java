package com.skyinfor.businessdistrict.ui.main.contact;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ContactSearchApi {

    @GET(ApiUrl.searchUser)
    Observable<Response<List<PersonListBean>>> searchUser(@Query("keyword") String keyword);

}
