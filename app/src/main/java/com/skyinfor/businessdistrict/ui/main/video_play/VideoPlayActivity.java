package com.skyinfor.businessdistrict.ui.main.video_play;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.xiao.nicevideoplayer.NiceVideoPlayer;
import com.xiao.nicevideoplayer.NiceVideoPlayerManager;
import com.xiao.nicevideoplayer.TxVideoPlayerController;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayActivity extends BaseActivity {

    @BindView(R.id.video_play)
    NiceVideoPlayer videoPlay;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_play;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        String uri = getIntent().getStringExtra("uri");
        String title = getIntent().getStringExtra("title");
        String imgUrl = getIntent().getStringExtra("imgUrl");
        initPlay(uri, title, imgUrl);
    }

    private void initPlay(String uri, String title, String imgUrl) {
        videoPlay.setPlayerType(NiceVideoPlayer.TYPE_IJK); // or NiceVideoPlayer.TYPE_NATIVE
        videoPlay.setUp(uri, null);

        TxVideoPlayerController controller = new TxVideoPlayerController(this);
        controller.setTitle(title);
        controller.setNetImage(imgUrl);
        videoPlay.setController(controller);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 在onStop时释放掉播放器
        NiceVideoPlayerManager.instance().releaseNiceVideoPlayer();
    }

    @Override
    public void onBackPressed() {
        // 在全屏或者小窗口时按返回键要先退出全屏或小窗口，
        // 所以在Activity中onBackPress要交给NiceVideoPlayer先处理。
        if (NiceVideoPlayerManager.instance().onBackPressd()) return;
        super.onBackPressed();
    }

}
