package com.skyinfor.businessdistrict.eventbus;

public class StatusEvent {

    private int selectType;

    public StatusEvent(int selectType) {
        this.selectType = selectType;
    }

    public int getSelectType() {
        return selectType;
    }

}
