package com.skyinfor.businessdistrict.ui.main.plan_check.plan_detail;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.PlanDetailLinkAdapter;
import com.skyinfor.businessdistrict.adapter.PlanDetailOperationAdapter;
import com.skyinfor.businessdistrict.adapter.PlanDetailRelatePerAdapter;
import com.skyinfor.businessdistrict.model.PlanListModel;
import com.skyinfor.businessdistrict.model.PlanRelatedModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.util.recyclerview.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class PlanDetailPresent extends BasePresenter<PlanDetailContract.Model> implements
        PlanDetailContract.Presenter {

    private List<PlanRelatedModel> perList = new ArrayList<>();
    private PlanDetailRelatePerAdapter perAdapter;
    private List<String> operationList = new ArrayList<>();
    private PlanDetailOperationAdapter operationAdapter;
    private List<PlanListModel.PlanResponseBean> msgList = new ArrayList<>();
    private PlanDetailLinkAdapter linkAdapter;
    private int type;

    @Inject
    public PlanDetailPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
    }


    @Override
    public void initAdapter(RecyclerView recPer, RecyclerView RecOperation, RecyclerView recLink, int type) {
        this.type = type;
        if (type == 0) {
            perAdapter = new PlanDetailRelatePerAdapter(mActivity, perList);
            recPer.setLayoutManager(new GridLayoutManager(mActivity, 3));
            recPer.setAdapter(perAdapter);
        }
        operationAdapter = new PlanDetailOperationAdapter(mActivity
                , operationList);
        RecOperation.setLayoutManager(new LinearLayoutManager(mActivity));
        RecOperation.setAdapter(operationAdapter);

        linkAdapter = new PlanDetailLinkAdapter(mActivity, msgList);
        recLink.setLayoutManager(new LinearLayoutManager(mActivity));
        recLink.addItemDecoration(new DividerItemDecoration(mActivity,
                LinearLayoutManager.VERTICAL, dp2px(10, mActivity),
                Color.parseColor("#efeff4")));
        recLink.setAdapter(linkAdapter);

    }

    @Override
    public void setNotify(PlanListModel model) {
        perList.clear();
        if (type == 0) {
            if (model.getPlan_condition() != null && model.getPlan_condition().size() > 0) {
                if (model.getPlan_condition().get(0) != null) {
                    perList.addAll(model.getPlan_condition().get(0).getRelation_user());
                    perAdapter.notifyDataSetChanged();
                }
            }
        }

        for (int i = 0; i < model.getPlan_response().size(); i++) {
            operationList.add(model.getPlan_response().get(i).getName());
        }
        operationAdapter.notifyDataSetChanged();

        if (model.getPlan_response() != null && model.getPlan_response().size() > 0) {
            msgList.addAll(model.getPlan_response());
            linkAdapter.notifyDataSetChanged();
        }
    }


}
