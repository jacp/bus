package com.skyinfor.businessdistrict.fragment.main.msg;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.ConversationModel;
import com.skyinfor.businessdistrict.model.Response;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public interface MsgApi {

    @FormUrlEncoded
    @POST(ApiUrl.talkUserList)
    Observable<Response<List<ConversationModel>>> talkUserList(@Field("user_id") String user_id);

}
