package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.HomeThingModel;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class HomeThingAdapter extends BaseMyRecyclerVIewAdapter<HomeThingModel> {

    public HomeThingAdapter(Context context, List<HomeThingModel> list) {
        super(context, list, R.layout.item_home_new_thing);
    }

    @Override
    protected void covert(HomeThingModel mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        String[] str = mode.getCreate_time().split(" ");
        String date = "";
        String time = "";
        if (str.length != 0) {
            date = str[0].substring(5, str[0].length());
            time = str[1].substring(0, str[1].length() - 3);
        }

        holder.setText(R.id.text_home_thing_time, time);
        holder.setText(R.id.text_home_thing_date, date.replace("-", "/"));
        TextView content = holder.getView(R.id.text_home_thing_content);
        String data = String.format("[%s] %s", mode.getList_type().equals("task") ? "任务" : "情况", mode.getName());
        SpannableString spannableString = new SpannableString(data);
        spannableString.setSpan(new ForegroundColorSpan(mContext.getResources().
                getColor(R.color.colorMain)), 0, 4, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        content.setText(spannableString);

    }
}
