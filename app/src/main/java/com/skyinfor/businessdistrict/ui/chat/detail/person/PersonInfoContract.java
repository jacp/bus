package com.skyinfor.businessdistrict.ui.chat.detail.person;

import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class PersonInfoContract {

    interface Model extends MModel {
        void setResult(UserInfoModel model);
    }

    interface Presenter extends IPresenter<Model> {
        void getUserInfo(String userId);

    }

}
