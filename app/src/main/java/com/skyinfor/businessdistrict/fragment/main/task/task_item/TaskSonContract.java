package com.skyinfor.businessdistrict.fragment.main.task.task_item;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.Map;

public class TaskSonContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void getList(Map<String, String> parts, final int pages, int type);
        void setNotify();

    }

}
