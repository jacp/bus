package com.skyinfor.businessdistrict.adapter;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;
import com.skyinfor.businessdistrict.util.recyclerview.BaseViewHolder;

import java.util.List;

public class PersonDecListAdapter extends BaseMyRecyclerVIewAdapter<UserIconBean> {

    public PersonDecListAdapter(Context context, List<UserIconBean> list) {
        super(context, list, R.layout.item_chat_detail_member);
    }

    @Override
    protected void covert(UserIconBean mode, BaseViewHolder holder, int position) {
        super.covert(mode, holder, position);

        IconView img=holder.getView(R.id.img_chat_round_head,position);

        TextView name=holder.getView(R.id.text_chat_round_name);

        img.setBaseInfo(mContext,mode.getNickname(),mode.getAvatar());
        name.setText(mode.getNickname());
    }
}
