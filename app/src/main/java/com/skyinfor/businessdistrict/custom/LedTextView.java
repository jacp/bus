package com.skyinfor.businessdistrict.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.skyinfor.businessdistrict.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LedTextView extends View {

    private Rect textRect = new Rect();
    private List<String> list = new ArrayList<>();
    private String FONTS_FOLDER = "fonts";//字体dir
    //字体
    private String FONT_DIGITAL_7 = FONTS_FOLDER
            + File.separator + "digital-7.ttf";

    private Paint mPaint;
    private String mTextStr;
    private int cHeight;
    private RectF mRect;
    private int mWidth;//测量宽度
    private int mHeight;//测量宽度
    private float mBoxWidth;//box的宽度
    private int mSpace;//间距
    private int mTextSize;//文字大小
    private int mAlpha;//透明度


    public LedTextView(Context context) {
        this(context, null);

    }

    public LedTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LedTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = getResources().obtainAttributes(attrs, R.styleable.LedTextView);
        mBoxWidth = array.getDimensionPixelSize(R.styleable.LedTextView_ledBoxWidth, dp2px(50, context));
        mTextSize = array.getDimensionPixelSize(R.styleable.LedTextView_ledTextSize, dp2px(20, context));
        mSpace = array.getDimensionPixelSize(R.styleable.LedTextView_ledSpace, dp2px(6, context));
        mAlpha = array.getInt(R.styleable.LedTextView_ledBoxAlpha, 102);
        mTextStr = array.getString(R.styleable.LedTextView_ledText);
        if (TextUtils.isEmpty(mTextStr)) {
            mTextStr = "0";
        }
        array.recycle();

        init();

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        list.clear();
        int width;
        int height;

        int spaceCount = mTextStr.length() - 1;
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        int widthSpec = MeasureSpec.getMode(widthMeasureSpec);
        mHeight = MeasureSpec.getSize(heightMeasureSpec);
        height = mHeight;

        if (widthSpec == MeasureSpec.EXACTLY) {
            width = mWidth;
            mBoxWidth = (mWidth - mSpace * spaceCount) / (Float.valueOf(mTextStr.length()));
        } else {
            width = getPaddingLeft() + getPaddingRight() + (int) mBoxWidth * (mTextStr.length()) + mSpace * spaceCount;
            if (width > mWidth) {
                mBoxWidth = ((mWidth - mSpace * spaceCount) / (Float.valueOf(mTextStr.length())));
            }
        }

        cHeight = Math.min(mHeight, height);

        for (int i = 0; i < mTextStr.length(); i++) {
            String str = String.valueOf(mTextStr.charAt(i));
            list.add(str);
        }
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mRect = new RectF(0, 0, getWidth(), getHeight());
        float x;
        int y;
        for (int i = 0; i < list.size(); i++) {
            String str = list.get(i);
            mPaint.setColor(Color.BLACK);
            mPaint.setAlpha(mAlpha);
            mPaint.setTextSize(mTextSize);
            mPaint.getTextBounds(str, 0, str.length(), textRect);

            Typeface typeface = Typeface.createFromAsset(getResources().getAssets(), FONT_DIGITAL_7);
            mPaint.setTypeface(typeface);
            mRect = new RectF(0, 0, mBoxWidth, cHeight);
            canvas.drawRect(mRect, mPaint);
            x = mBoxWidth / 2 - textRect.width() / 2;
            y = cHeight / 2 + textRect.height() / 2;
            mPaint.setColor(Color.WHITE);
            canvas.drawText(str, x, y, mPaint);

            canvas.translate(mSpace + mBoxWidth, 0);
        }

    }

    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStrokeWidth(0);
    }

    //dp-->px
    private int dp2px(int dp, Context context) {
        //  dp  * 设备密度 = px
        float density = context.getResources().getDisplayMetrics().density;//屏幕密度
        // 1920*1080     ppi  440
        return (int) (dp * density + 0.5f);
    }

    public void setTextStr(String str) {
        mTextStr = str;
        requestLayout();
        postInvalidate();
    }

}
