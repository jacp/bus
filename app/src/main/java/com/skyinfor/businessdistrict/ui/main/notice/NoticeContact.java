package com.skyinfor.businessdistrict.ui.main.notice;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class NoticeContact {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void noticeList(String page, final String limit);

    }

}
