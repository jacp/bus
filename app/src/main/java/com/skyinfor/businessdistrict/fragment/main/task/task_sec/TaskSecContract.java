package com.skyinfor.businessdistrict.fragment.main.task.task_sec;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class TaskSecContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(FragmentManager fm, ViewPager viewPager, TabLayout tabLayout);
        void setNotify(int selectType);
    }

}
