package com.skyinfor.businessdistrict.eventbus;

public class ChatNameEvent {

    private String name;

    public ChatNameEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
