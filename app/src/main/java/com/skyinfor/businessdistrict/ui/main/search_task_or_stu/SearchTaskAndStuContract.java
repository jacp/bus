package com.skyinfor.businessdistrict.ui.main.search_task_or_stu;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.Map;

public class SearchTaskAndStuContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

    }

    interface Presenter extends IPresenter<Model> {
        void initTaskAdapter(RecyclerView recyclerView);
        void initStatusAdapter(RecyclerView recyclerView);
        void getTaskList(Map<String, String> parts, final int pages, int type);
        void getStatusList(Map<String, String> parts, final int pages, int type);
        void setTaskNotify();
        void setStatusNotify();

    }

}
