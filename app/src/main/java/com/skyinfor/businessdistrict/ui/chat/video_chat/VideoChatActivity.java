package com.skyinfor.businessdistrict.ui.chat.video_chat;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.custom.roundedImage.RoundedImageView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.SoundStatusEvent;
import com.skyinfor.businessdistrict.eventbus.TimeCountEvent;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.SoundChatModel;
import com.skyinfor.businessdistrict.model.VideoGroupChatModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.TimeCountUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.agora.AgoraAPIOnlySignal;
import io.agora.rtc.Constants;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

public class VideoChatActivity extends BaseActivity<VideoChatPresent> implements VideoChatContract.Model {

    @BindView(R.id.comm_topBarSteep)
    View commTopBarSteep;
    @BindView(R.id.img_video_chat)
    RoundedImageView imgVideoChat;
    @BindView(R.id.text_video_name)
    TextView textVideoName;
    @BindView(R.id.text_video_status)
    TextView textVideoStatus;
    @BindView(R.id.text_video_chat_cancel)
    TextView textVideoChatCancel;
    @BindView(R.id.text_video_chat_accept)
    TextView textVideoChatAccept;
    @BindView(R.id.lin_video_begin)
    LinearLayout linVideoBegin;
    @BindView(R.id.rel_video_myself)
    FrameLayout relVideoMyself;
    @BindView(R.id.fr_video_view_other)
    RelativeLayout frVideoViewOther;
    @BindView(R.id.fr_video_view_start)
    FrameLayout frVideoViewStart;
    @BindView(R.id.rel_single_video)
    RelativeLayout relSingleVideo;
    @BindView(R.id.rec_sound_chat_self)
    RecyclerView recSoundChatSelf;
    @BindView(R.id.text_sound_group_sound)
    TextView textSoundGroupSound;
    @BindView(R.id.text_video_group_chat_cancel_self)
    TextView textVideoGroupChatCancelSelf;
    @BindView(R.id.text_sound_group_large)
    TextView textSoundGroupLarge;
    @BindView(R.id.rel_chat_group_self)
    RelativeLayout relChatGroupSelf;
    @BindView(R.id.img_sound_group_chat_other)
    RoundedImageView imgSoundGroupChatOther;
    @BindView(R.id.text_sound_group_chat_name_other)
    TextView textSoundGroupChatNameOther;
    @BindView(R.id.text_sound_group_chat_name_status_other)
    TextView textSoundGroupChatNameStatusOther;
    @BindView(R.id.text_sound_main)
    TextView textSoundMain;
    @BindView(R.id.rec_sound_list)
    RecyclerView recSoundList;
    @BindView(R.id.text_video_group_chat_cancel)
    TextView textVideoGroupChatCancel;
    @BindView(R.id.text_video_group_chat_accept)
    TextView textVideoGroupChatAccept;
    @BindView(R.id.lin_video_group_begin)
    LinearLayout linVideoGroupBegin;
    @BindView(R.id.rel_chat_group_other)
    LinearLayout relChatGroupOther;
    @BindView(R.id.rec_sound_chat_sur)
    RecyclerView recSoundChatSur;
    @BindView(R.id.text_sound_group_time)
    TextView textSoundGroupTime;
    private String channelId;
    private RtcEngine mRtcEngine;
    private String userId;
    private int initWindth;
    private int initHight;
    private boolean isCloseSound;
    private boolean isLargeSound;
    private List<VideoGroupChatModel> dataList = new ArrayList<>();
    private final int MODE_ONE_BIG = 0;
    private final int MODE_SHOW_FOUR = 1;
    private int stateMode = MODE_SHOW_FOUR; // 0 : one big with 16 small, 1: 17 small, show 4
    private List<GroupMemberModel.UserListBean> userListBeanList;
    private int channelType;
    private TimeCountUtil timeCountUtil;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);

    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_video_chat;
    }


    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);

        String str = getIntent().getStringExtra("str");
        int type = getIntent().getIntExtra("type", 0);
        channelId = getIntent().getStringExtra("channel_id");
        userId = getIntent().getStringExtra("user_id");
        userListBeanList = (List<GroupMemberModel.UserListBean>)
                getIntent().getSerializableExtra("model");
        channelType = getIntent().getIntExtra("channelType", 0);

        mPresenter.initSingleConfig(channelType > 5 ? 1 : 0);

        if (userListBeanList == null || userListBeanList.size() == 0) {
            channelType = 4;
        } else {
            channelType = 44;
        }

        if (channelType < 5) {
            setBaseInfo(type, str);
            setupLocalVideo();

        } else {
            relSingleVideo.setVisibility(View.GONE);
            relChatGroupSelf.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
            relChatGroupOther.setVisibility(type == 0 ? View.GONE : View.VISIBLE);

            setupGroupLocalVideo();
            if (type == 0) {
                setInitVideoInfo();
            }else {
                mRtcEngine.joinChannel(null, String.format("%s%s", com.skyinfor.businessdistrict.app.Constants.USER_ID,
                        UUID.randomUUID()), "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you
            }
            mPresenter.initAdapter(recSoundChatSelf, recSoundChatSur, recSoundList);
            mPresenter.setTitleNotify(userListBeanList);

            SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
            surfaceView.setZOrderMediaOverlay(true);

        }
    }

    @OnClick({R.id.text_video_chat_cancel, R.id.text_video_chat_accept
            , R.id.text_video_group_chat_cancel_self, R.id.text_video_group_chat_cancel,
            R.id.text_video_group_chat_accept, R.id.text_sound_group_sound, R.id.text_sound_group_large})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.text_video_chat_accept:
                mRtcEngine.leaveChannel();
                acceptSuc();
                setStatusCallBack(88);

                mRtcEngine.joinChannel(null, channelId, "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you

                break;

            case R.id.text_sound_group_sound://静音
                isCloseSound = !isCloseSound;
                mRtcEngine.muteLocalAudioStream(isCloseSound);
//                isCloseSound == true ?R.mipmap.icon_sound_mute_close?R.mipmap.icon_sound_mute
                Drawable drawableTop = getResources().getDrawable(isCloseSound == false ?
                        R.mipmap.icon_sound_mute : R.mipmap.icon_sound_mute_close);
                textSoundGroupSound.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);

                break;
            case R.id.text_sound_group_large://扩音
                isLargeSound = !isLargeSound;
                mRtcEngine.setEnableSpeakerphone(isLargeSound);
                Drawable drawableTopMore = getResources().getDrawable(isLargeSound == false
                        ? R.mipmap.icon_sound_large_close : R.mipmap.icon_sound_large);

                textSoundGroupLarge.setCompoundDrawablesWithIntrinsicBounds(null,
                        drawableTopMore, null, null);

                break;

            case R.id.text_video_chat_cancel:
            case R.id.text_sound_chat_close://挂断
            case R.id.text_video_group_chat_cancel_self://群聊挂断
            case R.id.text_video_group_chat_cancel://
                setStatusCallBack(99);
                appManager.finishActivity();

                break;
            case R.id.text_video_group_chat_accept://群聊接受
                relChatGroupSelf.setVisibility(View.VISIBLE);
                relChatGroupOther.setVisibility(View.GONE);

                recSoundChatSelf.setVisibility(View.GONE);
                textSoundGroupSound.setVisibility(View.VISIBLE);
                textSoundGroupLarge.setVisibility(View.VISIBLE);
                textVideoGroupChatCancelSelf.setText("挂断");

                mRtcEngine.leaveChannel();

                if (stateMode == MODE_ONE_BIG) {
                    mRtcEngine.setRemoteVideoStreamType(com.skyinfor.businessdistrict.app.Constants.USER_ID, Constants.VIDEO_STREAM_LOW);
                } else if (stateMode == MODE_SHOW_FOUR) {
                    mRtcEngine.setRemoteVideoStreamType(com.skyinfor.businessdistrict.app.Constants.USER_ID, Constants.VIDEO_STREAM_HIGH);
                }

                setInitVideoInfo();
                break;
        }
    }

    private void setBaseInfo(int type, String str) {
        textVideoChatAccept.setVisibility(type == 0 ? View.GONE : View.VISIBLE);
        textVideoStatus.setText(type == 0 ? "正在呼叫" : "邀请你视频通话");
        //自己先加入
        if (type == 0) {

            mRtcEngine.joinChannel(null, channelId, "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you
//            MyApplication.getInstance().getmAgoraAPI().channelInviteUser(channelId, userId, 0);
        } else {
            mRtcEngine.joinChannel(null, String.format("%s%s", com.skyinfor.businessdistrict.app.Constants.USER_ID, UUID.randomUUID()), "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you

            frVideoViewStart.setVisibility(View.VISIBLE);
            frVideoViewOther.setVisibility(View.GONE);
        }

        Gson gson = new Gson();
        SoundChatModel model = gson.fromJson(str, SoundChatModel.class);

        if (model != null) {
            GlideUtils.loadImg(mContext,model.getSend_img(), imgVideoChat);
            textVideoName.setText(model.getSend_name());
        }
    }

    private void acceptSuc() {
        textVideoChatAccept.setVisibility(View.GONE);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getEvent(SoundStatusEvent event) {
        switch (event.getStatus()) {
            case 1:
                if (channelType < 5) {
                    acceptSuc();
                    frVideoViewStart.setVisibility(View.GONE);
                    frVideoViewOther.setVisibility(View.VISIBLE);

                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                            initWindth,
                            initHight);
                    lp.setMargins(0, dp2px(16, mContext), dp2px(16, mContext), dp2px(16, mContext));
                    lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                    frVideoViewOther.setLayoutParams(lp);
                } else {
                    textSoundGroupSound.setVisibility(View.VISIBLE);
                    textSoundGroupLarge.setVisibility(View.VISIBLE);
                    textVideoGroupChatCancelSelf.setText("挂断");
                    changeGroupStatus(event.getUuid(), event.getStatus());

                }
                recSoundChatSelf.setVisibility(View.GONE);
                mRtcEngine.stopAudioMixing();
                if (timeCountUtil==null){
                    timeCountUtil = TimeCountUtil.getInstance().startTimeCount();
                }

                break;
            case 2:
                if (channelType > 5) {
                    changeGroupStatus(event.getUuid(), event.getStatus());

                } else {
                    textVideoStatus.setText("通话结束");
                    appManager.finishActivity(VideoChatActivity.class);
                }

                break;
            case 3:
                //第一次接通
//                setupRemoteVideo(event.getUuid());
                mRtcEngine.stopAudioMixing();

                if (channelType < 5) {
                    setupRemoteVideo(event.getUuid());
                } else {
                    recSoundChatSelf.setVisibility(View.GONE);
                }

                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCurContactTime(TimeCountEvent event) {
        int type = channelType > 5 ? 1 : 0;
        if (type == 0) {
            textVideoStatus.setText(event.getStr());
        } else {
            textSoundGroupTime.setText(event.getStr());
        }

    }

    @Override
    public void setRtcEngine(RtcEngine mRtcEngine) {
        this.mRtcEngine = mRtcEngine;
        playMusic();

    }

    private void setupLocalVideo() {
        mRtcEngine.enableVideo();
//      mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_360P, false); // Earlier than 2.3.0
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(VideoEncoderConfiguration.VD_640x360, VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));

        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        surfaceView.setZOrderMediaOverlay(true);
        frVideoViewOther.addView(surfaceView);
        mRtcEngine.setupLocalVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, 0));

        ViewGroup.LayoutParams layoutParams = frVideoViewOther.getLayoutParams();
        initWindth = layoutParams.width;
        initHight = layoutParams.height;

        layoutParams.width = RelativeLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = RelativeLayout.LayoutParams.MATCH_PARENT;
        frVideoViewOther.setLayoutParams(layoutParams);

    }

    private void setupGroupLocalVideo() {

        mRtcEngine.enableVideo();
//      mRtcEngine.setVideoProfile(Constants.VIDEO_PROFILE_360P, false); // Earlier than 2.3.0
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(VideoEncoderConfiguration.VD_640x360, VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));

        if (stateMode == MODE_ONE_BIG) {
            mRtcEngine.setRemoteVideoStreamType(com.skyinfor.businessdistrict.app.Constants.USER_ID, Constants.VIDEO_STREAM_LOW);
        } else if (stateMode == MODE_SHOW_FOUR) {
            mRtcEngine.setRemoteVideoStreamType(com.skyinfor.businessdistrict.app.Constants.USER_ID, Constants.VIDEO_STREAM_HIGH);
        }

        mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_LIVE_BROADCASTING);
        mRtcEngine.setLogFile(Environment.getExternalStorageDirectory()
                + File.separator + mContext.getPackageName() + "/log/agora-rtc.log");
        mRtcEngine.enableDualStreamMode(true);
        mRtcEngine.setClientRole(Constants.CLIENT_ROLE_BROADCASTER);
        mRtcEngine.setParameters("{\"che.audio.live_for_comm\":true}");
        mRtcEngine.setParameters("{\"che.video.moreFecSchemeEnable\":true}");
        mRtcEngine.setParameters("{\"che.video.lowBitRateStreamParameter\":{\"width\":240,\"height\":320,\"frameRate\":15,\"bitRate\":140}}");

    }

    private void setupRemoteVideo(int uid) {

        if (relVideoMyself.getChildCount() >= 1) {
            return;
        }

        SurfaceView surfaceView = RtcEngine.CreateRendererView(getBaseContext());
        relVideoMyself.addView(surfaceView);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceView, VideoCanvas.RENDER_MODE_HIDDEN, uid));
        surfaceView.setTag(uid); // for mark purpose
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onSingleDestroy();
        if (timeCountUtil!=null){
            timeCountUtil.releaseDestroyTimeSource();
        }

        EventBus.getDefault().unregister(this);
    }

    private void changeGroupStatus(int uuid, int type) {
        if (type == 1) {
            SurfaceView surfaceV = RtcEngine.CreateRendererView(getApplicationContext());
            surfaceV.setZOrderMediaOverlay(true);
            mRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceV,
                    VideoCanvas.RENDER_MODE_HIDDEN, uuid));
//                    mRtcEngine.muteRemoteAudioStream(uuid, true);

            if (stateMode == MODE_ONE_BIG) {
                mRtcEngine.setRemoteVideoStreamType(uuid, Constants.VIDEO_STREAM_LOW);
            } else if (stateMode == MODE_SHOW_FOUR) {
                mRtcEngine.setRemoteVideoStreamType(uuid, Constants.VIDEO_STREAM_HIGH);
            }

            for (int j = 0; j < dataList.size(); j++) {
                if (uuid == dataList.get(j).getUid()) {
                    dataList.get(j).setSurfaceView(surfaceV);
                    dataList.get(j).setAdd(true);
                    break;
                }
            }

        } else if (type == 2) {
            for (int j = 0; j < dataList.size(); j++) {
                if (uuid == dataList.get(j).getUid()) {
                    dataList.get(j).setAdd(false);
                    break;
                }
            }
        }

        mPresenter.setNotify(dataList);
    }

    private void playMusic() {
        String p="/assets/voip_calling_ring.mp3";
        mRtcEngine.startAudioMixing(p, true, true, -1);
    }

    private void setInitVideoInfo() {

        if (stateMode == MODE_ONE_BIG) {
            mRtcEngine.setRemoteVideoStreamType(com.skyinfor.businessdistrict.app.Constants.USER_ID, Constants.VIDEO_STREAM_LOW);
        } else if (stateMode == MODE_SHOW_FOUR) {
            mRtcEngine.setRemoteVideoStreamType(com.skyinfor.businessdistrict.app.Constants.USER_ID, Constants.VIDEO_STREAM_HIGH);
        }

        for (GroupMemberModel.UserListBean data :
                userListBeanList) {
            if (data.getId() == com.skyinfor.businessdistrict.app.Constants.USER_ID) {
                SurfaceView surfaceV = RtcEngine.CreateRendererView(getApplicationContext());
                surfaceV.setZOrderMediaOverlay(true);
                mRtcEngine.setupRemoteVideo(new VideoCanvas(surfaceV,
                        VideoCanvas.RENDER_MODE_HIDDEN, com.skyinfor.businessdistrict.app.Constants.USER_ID));
//                mRtcEngine.muteRemoteAudioStream(com.skyinfor.businessdistrict.app.Constants.USER_ID, true);
                mRtcEngine.setupLocalVideo(new VideoCanvas(surfaceV, VideoCanvas.RENDER_MODE_HIDDEN, 0));

                dataList.add(new VideoGroupChatModel(com.skyinfor.businessdistrict.app.Constants.USER_ID, surfaceV,
                        true, com.skyinfor.businessdistrict.app.Constants.ICON
                        , com.skyinfor.businessdistrict.app.Constants.NICK_NAME, true));
            } else {
                dataList.add(new VideoGroupChatModel(data.getId(), null,
                        true, data.getAvatar()
                        , data.getNickname(), false));
            }
        }

        mRtcEngine.joinChannel(null, channelId, "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID);

    }

    private void setStatusCallBack(int num) {
        AgoraAPIOnlySignal agoraAPI = MyApplication.getInstance().getmAgoraAPI();
        agoraAPI.messageInstantSend(userId, 0, setMsgInfo(num), "");

    }

    private String setMsgInfo(int num) {
        Gson gson = new Gson();
        SoundChatModel model = new SoundChatModel(num + "",
                com.skyinfor.businessdistrict.app.Constants.USER_ID + "",
                userId, channelId);

        return gson.toJson(model);
    }

}
