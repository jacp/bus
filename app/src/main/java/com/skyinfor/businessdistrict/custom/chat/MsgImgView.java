package com.skyinfor.businessdistrict.custom.chat;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.util.GlideUtils;

import java.util.ArrayList;

public class MsgImgView extends RelativeLayout {
    private ArrayList<ImageItem> listItem = new ArrayList<>();

    public MsgImgView(Context context, int type, ChatContentModel mode) {
        super(context);
        initData(context, type, mode);
    }

    public MsgImgView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initData(final Context context, int type, final ChatContentModel mode) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_img, null);
        ImageView img = view.findViewById(R.id.img_chat);
        GlideUtils.loadImg(mode.getContent(), img,R.mipmap.icon_create_task_img_default);

        LinearLayout rel=view.findViewById(R.id.rel_img);
        rel.setBackgroundResource(type==0?R.drawable.icon_bg_talk_left:R.drawable.icon_bg_talk_right);

        addView(view);

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listItem.clear();
                ImageItem item = new ImageItem();
                item.path = mode.getContent();
                listItem.add(item);

                //打开预览
                Intent intentPreview = new Intent(context, ImagePreviewDelActivity.class);
                intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, listItem);
                intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, 0);
                intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
                intentPreview.putExtra(ImagePicker.DELETE_IS_SHOW, true);
                context.startActivity(intentPreview);
            }
        });
    }

}
