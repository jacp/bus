package com.skyinfor.businessdistrict.model;

import java.io.Serializable;
import java.util.List;

public class GroupMemberModel implements Serializable{


    /**
     * group_name : 系统演示,App演示,陈磊,李宁,朱中响,张战韬,许文琪,李宁处置,xyb,pve演示,柴志俊,舒文强,曹忠德,李永飞,刘振强,
     * user_list : [{"id":23,"nickname":"系统演示","email":"skyinfor@skyinfor.com","phone":"13525698742","job":"演示","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","theone":"a1faf36b-c096-f81b-9176-3aaaa2dc0bc2"},{"id":24,"nickname":"pve演示","email":"skyinfor@skyinfor.com","phone":"18765874159","job":"pve演示","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","theone":"a9c57245-58a0-caa6-07cc-393a3ffcad04"},{"id":25,"nickname":"App演示","email":"123@qq. iOS ","phone":"","job":"软件开发","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bda96e660448.png","theone":"0f11ae2b-dd4b-64d7-26op-9d83c8f99982"},{"id":48,"nickname":"陈磊","email":"chenlei@skyinfor.com","phone":"18356070598","job":"员工","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bda975057fd8.jpg","theone":"c7d730cd-efb7-f710-f994-6eeccaad9002"},{"id":46,"nickname":"李宁","email":"lining@skyinfor.com","phone":"18516070553","job":"经理","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bda5d895614c.png","theone":"0f11ae2b-dd4b-64d7-26op-9d83c8f99983"},{"id":53,"nickname":"刘振强","email":"liuzhenqiang@skyinfor.com","phone":"18655373650","job":"员工","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","theone":"ad852125-e1cf-5343-28e5-91c913ecedcf"},{"id":49,"nickname":"武强","email":"得到","phone":"15821377082","job":"员工","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bdbfa81a674b.png","theone":"0f11ae2b-dd4b-64d7-26op-9d83c8f99984"},{"id":51,"nickname":"朱中响","email":"zhuzhongxiang@skyinfor.com","phone":"18225819193","job":"员工","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5be24e8bc02d3.png","theone":"0f11ae2b-dd4b-64d7-26op-9d83c8f99985"},{"id":56,"nickname":"李永飞","email":"liyongfei@skyinfor.com","phone":"18521496367","job":"员工","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","theone":"0f11ae2b-dd4b-64d7-26op-9d83c8f99986"},{"id":58,"nickname":"张战韬","email":"cto@skyinfor.com","phone":"18516666838","job":"总监","avatar":"","theone":"4a99680b-629f-73b1-9283-27ed0148678e"},{"id":59,"nickname":"许文琪","email":"coo@skyinfor.com","phone":"18658887677","job":"总监","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","theone":"19326f29-b3c3-52a8-20f4-5daa9c40cd37"},{"id":97,"nickname":"李宁处置","email":"","phone":"","job":"处置人","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png","theone":"0f11ae2b-dd4b-64d7-26op-9d83c8f99981"},{"id":95,"nickname":"柴志俊","email":"chaizhijun@skyinfor.com","phone":"","job":"测试工程师","avatar":"","theone":"4fabf6b5-99b7-b088-b0bf-3fadd07c8abe"},{"id":100,"nickname":"曹忠德","email":"czd@skyinfor.com","phone":"15254879654","job":"测试工程师","avatar":"http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5bd95f0b93230.png","theone":"ca031bd5-6555-e24d-4947-d9247177d866"},{"id":103,"nickname":"xyb","email":"","phone":"","job":"","avatar":"","theone":"b2f48ce9-ded3-135f-a25f-faa61fa0a1ba"},{"id":101,"nickname":"舒文强","email":"","phone":"","job":"","avatar":"","theone":"5543291f-9896-d4a0-cbb8-1bd588670955"}]
     */

    private String group_name;
    private List<UserListBean> user_list;

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public List<UserListBean> getUser_list() {
        return user_list;
    }

    public void setUser_list(List<UserListBean> user_list) {
        this.user_list = user_list;
    }

    public static class UserListBean implements Serializable{
        /**
         * id : 23
         * nickname : 系统演示
         * email : skyinfor@skyinfor.com
         * phone : 13525698742
         * job : 演示
         * avatar : http://skyinfor.cc:58080/shangwuqu/api/public/uploads/5ba35e161e210.png
         * theone : a1faf36b-c096-f81b-9176-3aaaa2dc0bc2
         */

        private int id;
        private String nickname;
        private String email;
        private String phone;
        private String job;
        private String avatar;
        private String theone;
        private boolean check;

        public boolean isCheck() {
            return check;
        }

        public void setCheck(boolean check) {
            this.check = check;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getTheone() {
            return theone;
        }

        public void setTheone(String theone) {
            this.theone = theone;
        }
    }
}
