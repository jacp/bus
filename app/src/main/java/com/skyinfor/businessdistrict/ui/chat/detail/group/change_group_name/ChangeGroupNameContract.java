package com.skyinfor.businessdistrict.ui.chat.detail.group.change_group_name;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class ChangeGroupNameContract {

    interface Model extends MModel {
        void setResult(String name);

    }

    interface Presenter extends IPresenter<Model> {
        void ChangeNameAction(String group_uuid, String group_name);

    }

}
