package com.skyinfor.businessdistrict.fragment.main.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.custom.IndexView;
import com.skyinfor.businessdistrict.custom.LedTextView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisZhiModel;
import com.skyinfor.businessdistrict.ui.chat.detail.person.PersonInfoActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.CreateStuOrTaskActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.RelatedDeviceActivity;
import com.skyinfor.businessdistrict.ui.main.data_aly.BigDataActivity;
import com.skyinfor.businessdistrict.ui.main.notice.NoticeActivity;
import com.skyinfor.businessdistrict.ui.main.notice.notice_detail.NoticeDetailActivity;
import com.skyinfor.businessdistrict.ui.main.plan_check.PlanCheckActivity;
import com.skyinfor.businessdistrict.ui.map.map_view.MapCheckActivity;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.UIHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.bingoogolapple.bgabanner.BGABanner;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class HomeFragment extends BaseFragment<HomePresent> implements
        HomeContract.Model, BGABanner.Adapter<View, HomeNoticeModel.DataBean>, BGABanner.Delegate<View, HomeNoticeModel.DataBean> {


    @BindView(R.id.banner_main_depth)
    BGABanner banner;
    @BindView(R.id.rec_home_new_msg)
    RecyclerView recHomeNewMsg;

    @BindView(R.id.text_home_device)
    TextView textHomeDevice;
    @BindView(R.id.text_home_plan)
    TextView textHomePlan;
    @BindView(R.id.text_home_aly)
    TextView textHomeAly;
    @BindView(R.id.text_home_notice)
    TextView textHomeNotice;
    @BindView(R.id.index_one)
    IndexView indexOne;
    @BindView(R.id.index_two)
    IndexView indexTwo;
    @BindView(R.id.index_three)
    IndexView indexThree;
    @BindView(R.id.chart)
    LineChart chart;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    @BindView(R.id.nest_scrollview_home)
    NestedScrollView nestScrollviewHome;
    @BindView(R.id.led_text_one)
    LedTextView ledTextOne;
    @BindView(R.id.led_text_two)
    LedTextView ledTextTwo;
    @BindView(R.id.text_home_request_focus)
    TextView textHomeRequestFocus;

    private List<View> bannerList = new ArrayList<>();
    private List<HomeNoticeModel.DataBean> list;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .appComponent(appComponent)
                .fragmentModule(fragmentModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initData() {
        super.initData();
        initRefresh(ptrStu, nestScrollviewHome);
        mPresenter.initAdapter(recHomeNewMsg);
        mPresenter.waitTasksCount(Constants.USER_ID + "");
        mPresenter.noticeList(1 + "", 10 + "");
        mPresenter.newTaskList();
        mPresenter.dataAnalysis();
        initPer();
        close();
        noLoadMode();
        textHomeRequestFocus.requestFocus();
        textHomeRequestFocus.setFocusableInTouchMode(true);
    }

    @OnClick({R.id.img_home_person, R.id.img_home_task, R.id.text_home_map, R.id.text_home_notice
            , R.id.text_home_plan, R.id.text_home_device, R.id.text_home_aly})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_home_person:
                Bundle data = new Bundle();
                data.putInt("type", 1);
                UIHelper.startActivity(mContext, PersonInfoActivity.class, data);

                break;
            case R.id.img_home_task:
                UIHelper.startActivity(mContext, CreateStuOrTaskActivity.class);

                break;
            case R.id.text_home_map:
                UIHelper.startActivity(mContext, MapCheckActivity.class);

                break;
            case R.id.text_home_notice:
                UIHelper.startActivity(mContext, NoticeActivity.class);

                break;
            case R.id.text_home_plan:
                UIHelper.startActivity(mContext, PlanCheckActivity.class);

                break;
            case R.id.text_home_device:
                Bundle bundle = new Bundle();
                bundle.putInt("type", 3);
                UIHelper.startActivity(mContext, RelatedDeviceActivity.class, bundle);

                break;
            case R.id.text_home_aly:
                UIHelper.startActivity(mContext, BigDataActivity.class);

                break;
        }
    }


    @Override
    public void fillBannerItem(BGABanner banner, View itemView, @Nullable HomeNoticeModel.DataBean mode, int position) {
        HomeNoticeModel.DataBean model = list.get(position);
        TextView name = itemView.findViewById(R.id.text_banner_name);
        TextView from = itemView.findViewById(R.id.text_banner_from);
        TextView date = itemView.findViewById(R.id.text_banner_date);
        ImageView img = itemView.findViewById(R.id.text_banner_img);
        img.setTag(R.id.text_banner_img, position);

        name.setText(model.getTitle());
        from.setText(model.getNickname());
        date.setText(model.getCreate_time().substring(0, model.getCreate_time().length() - 3));
        if (!TextUtils.isEmpty(model.getPicture())) {
            img.setVisibility(View.VISIBLE);
            GlideUtils.loadImg(mContext, model.getPicture(), img);
        } else {
            img.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBannerItemClick(BGABanner banner, View itemView, @Nullable HomeNoticeModel.DataBean model, int position) {
        Bundle data = new Bundle();
        data.putString("content", list.get(position).getContent());
        UIHelper.startActivity(mContext, NoticeDetailActivity.class, data);
    }

    @Override
    public void setBannerResult(List<HomeNoticeModel.DataBean> list) {
        bannerList.clear();
        this.list = list;
        if (list.size() > 0) {
            banner.setVisibility(View.VISIBLE);
            for (int i = 0; i < list.size(); i++) {
                View itemView = LayoutInflater.from(mContext).inflate(R.layout.view_home_banner, null);

                bannerList.add(itemView);
            }
            banner.setAutoPlayAble(bannerList.size() > 1);
            banner.setAdapter(HomeFragment.this);
            banner.setDelegate(this);
            banner.setData(bannerList);
        }
    }

    @Override
    public void setTaskCount(int stuSize, int taskSize) {
        String stuStr = String.valueOf(stuSize);
        String taskStr = String.valueOf(taskSize);
        ledTextOne.setTextStr(stuStr);
        ledTextTwo.setTextStr(taskStr);
    }

    @Override
    public void setIndexResult(AnalysisZhiModel model) {
        indexOne.setProgressStr(model.getAnju());
        indexTwo.setProgressStr(model.getShangye());
        indexThree.setProgressStr(model.getZhili());
    }

    @Override
    public void setLineResult(List<HashMap<String, Integer>> lineList) {
        mPresenter.generateDataLine(chart, lineList);
    }

    private void initPer() {
        textHomePlan.setVisibility(Constants.PRIVILEGE_PLAN == true ? View.VISIBLE : View.GONE);
        textHomeDevice.setVisibility(Constants.PRIVILEGE_DEVICE == true ? View.VISIBLE : View.GONE);

    }

    @Override
    protected void updateData() {
        super.updateData();
        mPresenter.waitTasksCount(Constants.USER_ID + "");
        mPresenter.noticeList(1 + "", 10 + "");
        mPresenter.newTaskList();
        mPresenter.dataAnalysis();

    }

    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrFrame != null)
            ptrFrame.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrFrame != null) {
            ptrFrame.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }

}
