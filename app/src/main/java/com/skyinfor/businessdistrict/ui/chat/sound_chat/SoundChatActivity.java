package com.skyinfor.businessdistrict.ui.chat.sound_chat;

import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.custom.roundedImage.RoundedImageView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.SoundStatusEvent;
import com.skyinfor.businessdistrict.eventbus.TimeCountEvent;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.model.SoundChatModel;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.TimeCountUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import io.agora.AgoraAPIOnlySignal;
import io.agora.rtc.Constants;
import io.agora.rtc.RtcEngine;

public class SoundChatActivity extends BaseActivity<SoundChatPresent> implements SoundChatContract.Model {

    @BindView(R.id.comm_topBarSteep)
    View commTopBarSteep;
    @BindView(R.id.img_sound_chat)
    RoundedImageView imgSoundChat;
    @BindView(R.id.text_sound_chat_name)
    TextView textSoundChatName;
    @BindView(R.id.text_sound_chat_cancel)
    TextView textSoundChatCancel;
    @BindView(R.id.text_sound_chat_accept)
    TextView textSoundChatAccept;
    @BindView(R.id.text_sound_chat_status)
    TextView textSoundChatStatus;
    @BindView(R.id.lin_sound_begin)
    LinearLayout linSoundBegin;
    @BindView(R.id.lin_sound_after)
    LinearLayout linSoundAfter;
    @BindView(R.id.text_sound_chat_sound)
    TextView textSoundChatSound;
    @BindView(R.id.text_sound_chat_close)
    TextView textSoundChatClose;
    @BindView(R.id.text_sound_chat_large)
    TextView textSoundChatLarge;
    @BindView(R.id.text_video_group_chat_cancel_self)
    TextView textVideoGroupChatCancelSelf;
    @BindView(R.id.lin_video_group_begin)
    LinearLayout linVideoGroupBegin;
    @BindView(R.id.rel_chat_group_self)
    RelativeLayout relChatGroupSelf;
    @BindView(R.id.text_sound_main)
    TextView textSoundMain;
    @BindView(R.id.rec_sound_list)
    RecyclerView recSoundList;
    @BindView(R.id.text_video_group_chat_cancel)
    TextView textVideoGroupChatCancel;
    @BindView(R.id.text_video_group_chat_accept)
    TextView textVideoGroupChatAccept;
    @BindView(R.id.rel_chat_group_other)
    LinearLayout relChatGroupOther;
    @BindView(R.id.rec_sound_chat_self)
    RecyclerView recSoundChatSelf;
    @BindView(R.id.text_sound_group_sound)
    TextView textSoundGroupSound;
    @BindView(R.id.text_sound_group_large)
    TextView textSoundGroupLarge;
    @BindView(R.id.img_sound_group_chat_other)
    RoundedImageView imgSoundGroupChatOther;
    @BindView(R.id.text_sound_group_chat_name_other)
    TextView textSoundGroupChatNameOther;
    @BindView(R.id.text_sound_group_time)
    TextView textSoundGroupTime;
    @BindView(R.id.text_sound_group_chat_name_status_other)
    TextView textSoundGroupChatNameStatusOther;
    private RtcEngine mRtcEngine;
    private boolean isCloseSound;
    private boolean isLargeSound;
    private String channelId;
    private String userId;
    private int channelType;
    private List<GroupMemberModel.UserListBean> userListBeanList;
    private List<GroupMemberModel.UserListBean> mySelfList = new ArrayList<>();
    private TimeCountUtil timeCountUtil;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sound_chat;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);

        String str = getIntent().getStringExtra("str");
        int type = getIntent().getIntExtra("type", 0);
        channelId = getIntent().getStringExtra("channel_id");
        userId = getIntent().getStringExtra("user_id");
        channelType = getIntent().getIntExtra("channelType", 0);
        userListBeanList = (List<GroupMemberModel.UserListBean>)
                getIntent().getSerializableExtra("model");

        if (userListBeanList == null || userListBeanList.size() == 0) {
            channelType = 3;
        } else {
            channelType = 33;
        }

        if (channelType < 5) {
            mPresenter.initSingleConfig();

        } else {
            textSoundChatStatus.setVisibility(View.GONE);
            linSoundAfter.setVisibility(View.GONE);
            linSoundBegin.setVisibility(View.GONE);
            mPresenter.initAdapter(recSoundChatSelf, recSoundList);
            mPresenter.setNotifyList(userListBeanList);
            relChatGroupSelf.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
            relChatGroupOther.setVisibility(type == 0 ? View.GONE : View.VISIBLE);

            GroupMemberModel.UserListBean bean = userListBeanList.get(0);
            GlideUtils.loadImg(mContext,bean.getAvatar(), imgSoundGroupChatOther);
            textSoundGroupChatNameOther.setText(bean.getNickname());

            mPresenter.initGroupConfig(Constants.CLIENT_ROLE_BROADCASTER);
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        }

        setBaseInfo(type, str);
    }

    @OnClick({R.id.text_sound_chat_cancel, R.id.text_sound_chat_accept,
            R.id.text_sound_chat_sound, R.id.text_sound_chat_close, R.id.text_sound_chat_large
            , R.id.text_video_group_chat_cancel_self, R.id.text_video_group_chat_cancel,
            R.id.text_video_group_chat_accept, R.id.text_sound_group_sound, R.id.text_sound_group_large})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_sound_chat_accept:
                mRtcEngine.leaveChannel();
                acceptSuc();
                setStatusCallBack(88);

                //加入频道
                mRtcEngine.joinChannel(null, channelId, "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you

                break;
            case R.id.text_sound_chat_sound://静音
            case R.id.text_sound_group_sound:
                isCloseSound = !isCloseSound;
                mRtcEngine.muteLocalAudioStream(isCloseSound);
//                isCloseSound == true ?R.mipmap.icon_sound_mute_close?R.mipmap.icon_sound_mute
                Drawable drawableTop = getResources().getDrawable(isCloseSound == false ?
                        R.mipmap.icon_sound_mute : R.mipmap.icon_sound_mute_close);
                textSoundChatSound.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);
                textSoundGroupSound.setCompoundDrawablesWithIntrinsicBounds(null, drawableTop, null, null);

                break;
            case R.id.text_sound_chat_large://扩音
            case R.id.text_sound_group_large:
                isLargeSound = !isLargeSound;
                mRtcEngine.setEnableSpeakerphone(isLargeSound);
                Drawable drawableTopMore = getResources().getDrawable(isLargeSound == false
                        ? R.mipmap.icon_sound_large_close : R.mipmap.icon_sound_large);

                textSoundChatLarge.setCompoundDrawablesWithIntrinsicBounds(null,
                        drawableTopMore, null, null);

                textSoundGroupLarge.setCompoundDrawablesWithIntrinsicBounds(null,
                        drawableTopMore, null, null);

                break;

            case R.id.text_sound_chat_cancel:
            case R.id.text_sound_chat_close://挂断
            case R.id.text_video_group_chat_cancel_self://群聊挂断
            case R.id.text_video_group_chat_cancel://
                setStatusCallBack(99);
                appManager.finishActivity();
                break;

            case R.id.text_video_group_chat_accept://群聊接受
                textSoundGroupSound.setVisibility(View.VISIBLE);
                textSoundGroupLarge.setVisibility(View.VISIBLE);
                textVideoGroupChatCancelSelf.setText("挂断");

                GroupMemberModel.UserListBean bean = new GroupMemberModel.UserListBean();
                bean.setAvatar(com.skyinfor.businessdistrict.app.Constants.ICON);
                bean.setId(com.skyinfor.businessdistrict.app.Constants.USER_ID);
                bean.setNickname(com.skyinfor.businessdistrict.app.Constants.NICK_NAME);
                mySelfList.add(bean);

                mRtcEngine.leaveChannel();
                mRtcEngine.joinChannel(null, channelId, "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID);

                break;
        }
    }


    @Override
    public void setRtcEngine(RtcEngine mRtcEngine) {
        this.mRtcEngine = mRtcEngine;
        playMusic();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onSingleDestroy();
        if (timeCountUtil != null) {
            timeCountUtil.releaseDestroyTimeSource();
        }
        EventBus.getDefault().unregister(this);

    }

    private void setBaseInfo(int type, String str) {
        textSoundChatAccept.setVisibility(type == 0 ? View.GONE : View.VISIBLE);
        textSoundChatStatus.setText(type == 0 ? "正在呼叫" : "邀请你语音通话");
        //自己先加入
        if (type == 0) {
            GroupMemberModel.UserListBean bean = new GroupMemberModel.UserListBean();
            bean.setAvatar(com.skyinfor.businessdistrict.app.Constants.ICON);
            bean.setId(com.skyinfor.businessdistrict.app.Constants.USER_ID);
            bean.setNickname(com.skyinfor.businessdistrict.app.Constants.NICK_NAME);
            mySelfList.add(bean);

            MyApplication.getInstance().getmAgoraAPI().channelInviteUser(channelId, userId, 0);
            mRtcEngine.joinChannel(null, channelId, "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you

        } else {
            mRtcEngine.joinChannel(null, String.format("%s%s", com.skyinfor.businessdistrict.app.Constants.USER_ID, UUID.randomUUID()), "Extra Optional Data", com.skyinfor.businessdistrict.app.Constants.USER_ID); // if you do not specify the uid, we will generate the uid for you
        }

        Gson gson = new Gson();
        SoundChatModel model = gson.fromJson(str, SoundChatModel.class);

        if (model != null) {
            GlideUtils.loadImg(mContext,model.getSend_img(), imgSoundChat);
            textSoundChatName.setText(model.getSend_name());
        }
    }

    private void acceptSuc() {
        MyApplication.getInstance().getmAgoraAPI().channelInviteAccept(channelId, userId, 0, "");
        linSoundBegin.setVisibility(View.GONE);
        linSoundAfter.setVisibility(View.VISIBLE);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getEvent(SoundStatusEvent event) {

        switch (event.getStatus()) {
            case 1:
                if (channelType < 5) {
                    acceptSuc();
                } else {
                    changeGroupStatus(event.getUuid(), event.getStatus());
                }
                mRtcEngine.stopAudioMixing();
                if (timeCountUtil==null){
                    timeCountUtil = TimeCountUtil.getInstance().startTimeCount();
                }

                break;
            case 2:
                if (channelType > 5) {
                    changeGroupStatus(event.getUuid(), event.getStatus());

                } else {
                    textSoundChatStatus.setText("通话结束");
                    appManager.finishActivity(SoundChatActivity.class);
                }
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCurContactTime(TimeCountEvent event) {
        int type = channelType > 5 ? 1 : 0;
        if (type == 0) {
            textSoundChatStatus.setText(event.getStr());
        } else {
            textSoundGroupTime.setText(event.getStr());
        }
    }

    private void changeGroupStatus(int uuid, int type) {
        relChatGroupSelf.setVisibility(View.VISIBLE);
        relChatGroupOther.setVisibility(View.GONE);

        textSoundGroupSound.setVisibility(View.VISIBLE);
        textSoundGroupLarge.setVisibility(View.VISIBLE);
        textVideoGroupChatCancelSelf.setText("挂断");

        for (int i = 0; i < userListBeanList.size(); i++) {
            GroupMemberModel.UserListBean data = userListBeanList.get(i);
            if (data.getId() == uuid) {
                if (type == 1) {
                    GroupMemberModel.UserListBean bean = new GroupMemberModel.UserListBean();
                    bean.setAvatar(data.getAvatar());
                    bean.setNickname(data.getNickname());
                    bean.setId(uuid);
                    mySelfList.add(bean);
                    break;
                } else if (type == 2) {
                    for (int j = 0; j < mySelfList.size(); j++) {
                        if (uuid == mySelfList.get(j).getId()) {
                            mySelfList.remove(mySelfList.get(j));
                            break;
                        }
                    }
                }
            }
        }

        mPresenter.setNotifyList(mySelfList);

    }

    private void setStatusCallBack(int num) {
        AgoraAPIOnlySignal agoraAPI = MyApplication.getInstance().getmAgoraAPI();
        agoraAPI.messageInstantSend(userId, 0, setMsgInfo(num), "");

    }

    private String setMsgInfo(int num) {

        Gson gson = new Gson();
        SoundChatModel model = new SoundChatModel(num + "",
                com.skyinfor.businessdistrict.app.Constants.USER_ID + "",
                userId, channelId);

        return gson.toJson(model);
    }

    private void playMusic() {
        String p = "/assets/voip_calling_ring.mp3";
        mRtcEngine.startAudioMixing(p, true, true, -1);
    }


}
