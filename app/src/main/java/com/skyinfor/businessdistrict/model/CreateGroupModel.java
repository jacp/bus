package com.skyinfor.businessdistrict.model;

public class CreateGroupModel {
    private String group_uuid;

    public String getGroup_uuid() {
        return group_uuid;
    }

    public void setGroup_uuid(String group_uuid) {
        this.group_uuid = group_uuid;
    }
}
