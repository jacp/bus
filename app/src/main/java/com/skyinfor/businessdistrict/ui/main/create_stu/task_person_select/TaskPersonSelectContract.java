package com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class TaskPersonSelectContract {

    interface Model extends MModel {
        void setResult();

    }

    interface Presenter extends IPresenter<Model> {
        void createGroup(String group_name, String user_id, String group_owner_id);

    }
}
