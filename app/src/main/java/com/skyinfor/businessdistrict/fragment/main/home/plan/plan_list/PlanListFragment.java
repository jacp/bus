package com.skyinfor.businessdistrict.fragment.main.home.plan.plan_list;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class PlanListFragment extends BaseFragment<PlanListPresent> implements PlanListContract.Model {


    @BindView(R.id.rv_plan_list)
    RecyclerView rvPlanList;
    @BindView(R.id.ptr_stu)
    PtrClassicFrameLayout ptrStu;
    private int page=1;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .appComponent(appComponent)
                .fragmentModule(fragmentModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_plan_list;
    }

    @Override
    protected void initData() {
        super.initData();
        initRefresh(ptrStu, rvPlanList);
        mPresenter.dialog.show();
        String type = getmFragmentTitle();
        mPresenter.initAdapter(rvPlanList,Integer.valueOf(type));
        initNetData();
    }

    /**
     * 更新
     */
    @Override
    protected void updateData() {
        page = 1;
        initNetData();
    }

    /**
     * 加载更多
     */
    @Override
    protected void loadMoreData() {
        page++;
        initNetData();
    }

    private void initNetData() {
        mPresenter.gePlanList(page, Integer.parseInt(getmFragmentTitle()));
    }


    /**
     * 完成刷新
     */
    @Override
    public void compelete() {
        if (ptrStu != null)
            ptrStu.refreshComplete();
    }

    /**
     * 关闭刷新
     */
    @Override
    public void close() {
        if (ptrStu != null) {
            ptrStu.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void noLoadMode() {
        if (ptrStu != null) {
            ptrStu.setMode(PtrFrameLayout.Mode.REFRESH);
        }
    }

    @Override
    public void defalutMode() {
        if (ptrStu != null) {
            ptrStu.setMode(PtrFrameLayout.Mode.BOTH);
        }
    }


}
