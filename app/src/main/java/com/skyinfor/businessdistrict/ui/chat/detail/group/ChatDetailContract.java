package com.skyinfor.businessdistrict.ui.chat.detail.group;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class ChatDetailContract {

    interface Model extends MModel {
        void setResult(GroupMemberModel model);
        void exitResult();
    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);
        void getGroupInfo(String uuId);
        void exitGroup(String group_uuid ,String user_id );

    }

}
