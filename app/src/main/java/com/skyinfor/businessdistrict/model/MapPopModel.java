package com.skyinfor.businessdistrict.model;

public class MapPopModel {
    private String str;
    private boolean isCheck;

    public MapPopModel(String str, boolean isCheck) {
        this.str = str;
        this.isCheck = isCheck;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

}
