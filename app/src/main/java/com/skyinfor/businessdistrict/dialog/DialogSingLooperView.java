package com.skyinfor.businessdistrict.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.listener.OnDialogEvent;
import com.skyinfor.businessdistrict.util.UiUtils;
import com.weigan.loopview.LoopView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 单列滚轮dialog
 * Created by Min on 2017/4/26.
 */

public class DialogSingLooperView<T> extends Dialog implements View.OnClickListener {
    private View _view;
    public TextView tv_dialog_yes;
    public TextView tv_dialog_cencel;
    public LoopView loopView;

    public OnDialogEvent<T> _mEvent;
    public List<T> list;

    public List<String> list_str;

    /**
     * 使用arraysResId初始化
     *
     * @param context
     * @param listRes
     * @param o       无用
     */
    public DialogSingLooperView(Context context, int listRes, Object o) {
        this(context, context.getResources().getStringArray(listRes));
    }

    public DialogSingLooperView(Context context, String... lists) {
        this(context, (List<T>) Arrays.asList(lists));
    }

    public DialogSingLooperView(Context context, List<T> lists) {
        this(context);
        list_str = new ArrayList<>();
        list = lists;
        if (list == null) {
            list = new ArrayList<>();
        }
        /**
         * 迭代
         */
        for (T data : list) {
            list_str.add(tConverString(data));
        }
        dialogInit(context);
    }

    protected String tConverString(T data) {
        if (data instanceof String) {
            return (String) data;
        }
        return "";
    }

    private DialogSingLooperView(Context context) {
        super(context, R.style.ActionSheetDialogStyle);
    }

    private DialogSingLooperView(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    private DialogSingLooperView(Context context, int themeResId) {
        super(context, themeResId);
    }

    /**
     * 对dialog进行赋值和操作
     *
     * @param context
     */
    private void dialogInit(Context context) {
        _view = getLayoutInflater().inflate(R.layout.dialog_leavetype, null);
        tv_dialog_yes = (TextView) _view.findViewById(R.id.tv_submit);
        tv_dialog_cencel = (TextView) _view.findViewById(R.id.tv_close);
        // 内容区域
        loopView = (LoopView) _view.findViewById(R.id.loopView);
        //设置是否循环播放
        loopView.setNotLoop();

        //获取当前Activity所在的窗体
        Window dialogWindow = this.getWindow();
        //设置原始数据
        loopView.setItems(list_str);

        //设置Dialog从窗体底部弹出
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.getDecorView().setPadding(0, 0, 0, 0);

        WindowManager windowManager = dialogWindow.getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        //获得window窗口的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        //设置窗口宽度为充满全屏
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; //设置宽度
        //设置窗口高度为包裹内容
        //TODO 修改了空白处消失操作 FrameJack 2017-10-30 15:33 操作
        lp.height = UiUtils.dp2px(240, context);
        //将设置好的属性set回去
        dialogWindow.setAttributes(lp);


        setContentView(_view);
        setCanceledOnTouchOutside(true);

        tv_dialog_yes.setOnClickListener(this);
        tv_dialog_cencel.setOnClickListener(this);
    }

    public DialogSingLooperView setDialogEvent(OnDialogEvent<T> event) {
        this._mEvent = event;
        return this;
    }

    public T getItem(int position) {
        return list.get(position);
    }

    public String getItemStr(int position) {
        return list_str.get(position);
    }

    public int getCurrIndex() {
        return loopView.getSelectedItem();
    }

    @Override
    public void onClick(View v) {
        if (_mEvent != null) {
            if (v.equals(tv_dialog_yes)) {
                _mEvent.ok(this, getItem(getCurrIndex()), getCurrIndex(), 0);
            } else if (v.equals(tv_dialog_cencel)) {
                _mEvent.cancel(this);
            }
        }
        if (autoDismiss()) {
            this.dismiss();
        }
    }

    protected boolean autoDismiss() {
        return true;
    }
}
