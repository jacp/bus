package com.skyinfor.businessdistrict.ui.main.stu_detail;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.model.StatusSonModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;
import com.skyinfor.businessdistrict.ui.main.MainContract;

import java.util.List;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class StatusDetailContract {

    interface Model extends MModel {
        void setTaskData(TaskSonModel model);
        void setStatusData(StatusSonModel model);
        void setNotifyView();
        void finishAction();
        void setNotifyStu(int status,String name);

    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);

        void setImgNotify(String str);

        void initAcceptAdapter(RecyclerView recyclerView);

        void setAcceptNotify(List<UserIconBean> list);

        void statusChange(String id, String status_code, int type);

        void deleteTask(String id);

        void getNoticeAction(String rele_id,String notice_type, final boolean isNotify);

    }

}
