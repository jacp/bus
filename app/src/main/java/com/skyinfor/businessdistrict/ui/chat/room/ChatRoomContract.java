package com.skyinfor.businessdistrict.ui.chat.room;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.model.ChatContentModel;
import com.skyinfor.businessdistrict.model.GroupMemberModel;
import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

import java.util.List;

import okhttp3.MultipartBody;

public class ChatRoomContract {

    interface Model extends MModel {
        void compelete();

        void close();

        void noLoadMode();

        void defalutMode();

        void setSucImg(String path);

        void setGroupList(List<GroupMemberModel.UserListBean> groupList);
    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);

        void getConversationList(List<MultipartBody.Part> part, int type, int page,String nickName,String ava);

        void talkRecordSave(List<MultipartBody.Part> part,int type,String nickName);

        void setNotifySend(ChatContentModel model);

        void upFile(List<MultipartBody.Part> part, final int position);

        void getGroupInfo(String uuId);
    }

}
