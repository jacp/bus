package com.skyinfor.businessdistrict.fragment.main.task.task_sec;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.eventbus.StatusEvent;
import com.skyinfor.businessdistrict.eventbus.TaskEvent;
import com.skyinfor.businessdistrict.eventbus.UpdateTaskEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

public class TaskSecFragment extends BaseFragment<TaskSecPresent> implements TaskSecContract.Model {
    @BindView(R.id.viewpager_status_sec)
    ViewPager viewpagerStatusSec;
    @BindView(R.id.tab_stu)
    TabLayout tabStu;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .fragmentModule(fragmentModule)
                .appComponent(appComponent)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_status_sec;
    }

    @Override
    protected void initEventAndData() {
        super.initEventAndData();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void initData() {
        super.initData();
        mPresenter.initAdapter(getChildFragmentManager(), viewpagerStatusSec, tabStu);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(getActivity());

    }

    @Subscribe
    public void getEvent(TaskEvent event) {
        int type = event.getSelectType();
        mPresenter.setNotify(type);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusEvent(UpdateTaskEvent event) {
        mPresenter.setNotify(0);
        if (event.getType()>0&&event.getType()<4){
            mPresenter.setNotify(event.getType()-1);
            mPresenter.setNotify(event.getType());
        }
    }

    public void setNotify(){
        for (int i = 0; i < 5; i++) {
            mPresenter.setNotify(i);
        }
    }

}
