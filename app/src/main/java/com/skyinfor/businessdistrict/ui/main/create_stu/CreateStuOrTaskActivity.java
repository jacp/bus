package com.skyinfor.businessdistrict.ui.main.create_stu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.ImagePickerAdapter;
import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.AudioPlayerView;
import com.skyinfor.businessdistrict.custom.AudioRecorderButton;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.BitmapEvent;
import com.skyinfor.businessdistrict.eventbus.StatusEvent;
import com.skyinfor.businessdistrict.eventbus.TaskEvent;
import com.skyinfor.businessdistrict.eventbus.VideoEvent;
import com.skyinfor.businessdistrict.model.PersonListBean;
import com.skyinfor.businessdistrict.model.RelatedDeviceModel;
import com.skyinfor.businessdistrict.model.StatusTypeModel;
import com.skyinfor.businessdistrict.model.TaskModel;
import com.skyinfor.businessdistrict.model.TaskSonModel;
import com.skyinfor.businessdistrict.ui.main.create_stu.related_device.RelatedDeviceActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.stu_type.SelectStatusTypeActivity;
import com.skyinfor.businessdistrict.ui.main.create_stu.task_person_select.TaskPersonSelectActivity;
import com.skyinfor.businessdistrict.ui.main.video_play.VideoPlayActivity;
import com.skyinfor.businessdistrict.ui.main.video_rec.VideoRecActivity;
import com.skyinfor.businessdistrict.ui.map.MapChooseActivity;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.ImagePickerUtil;
import com.skyinfor.businessdistrict.util.LuBanCompress;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.audio.AudioManage;
import com.skyinfor.businessdistrict.util.http.HttpParams;
import com.skyinfor.businessdistrict.util.recyclerview.manager.ScrollGridLayoutManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;

import static com.skyinfor.businessdistrict.util.Utils.listToString;

/**
 * Created by SKYINFOR on 2018/10/24.
 */

public class CreateStuOrTaskActivity extends BaseActivity<CreateStuOrTaskPresent> implements CreateStuOrTaskContract.Model, ImagePickerAdapter.OnRecyclerViewItemClickListener, AudioRecorderButton.AudioFinishRecorderListener {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.edit_create_stu_name)
    EditText editCreateStuName;
    @BindView(R.id.text_create_stu_type)
    TextView textCreateStuType;
    @BindView(R.id.rel_create_stu_type)
    RelativeLayout relCreateStuType;
    @BindView(R.id.text_create_stu_location)
    TextView textCreateStuLocation;
    @BindView(R.id.rel_create_stu_location)
    RelativeLayout relCreateStuLocation;

    @BindView(R.id.rec_create_stu_execute)
    RecyclerView recCreateStuExecute;
    @BindView(R.id.edit_create_task_name)
    EditText editCreateTaskName;
    @BindView(R.id.text_create_task_type)
    TextView textCreateTaskType;
    @BindView(R.id.rel_create_task_type)
    RelativeLayout relCreateTaskType;
    @BindView(R.id.text_create_task_location)
    TextView textCreateTaskLocation;
    @BindView(R.id.rel_create_task_location)
    RelativeLayout relCreateTaskLocation;
    @BindView(R.id.img_create_task_manager)
    TextView imgCreateTaskManager;
    @BindView(R.id.rel_create_task_manager)
    RelativeLayout relCreateTaskManager;
    @BindView(R.id.text_create_task_device)
    TextView textCreateTaskDevice;
    @BindView(R.id.rel_create_task_device)
    RelativeLayout relCreateTaskDevice;
    @BindView(R.id.edit_create_mark)
    EditText editCreateMark;
    @BindView(R.id.text_create_sound)
    AudioRecorderButton textCreateSound;
    @BindView(R.id.rec_create_img)
    RecyclerView recCreateImg;
    @BindView(R.id.img_create_video)
    ImageView imgCreateVideo;
    @BindView(R.id.rec_create_stu_manager)
    RecyclerView recCreateStuManager;
    @BindView(R.id.img_video_end)
    ImageView imgVideoEnd;
    @BindView(R.id.view_stu)
    View stuView;
    @BindView(R.id.view_task)
    View taskView;
    @BindView(R.id.rec_accept_list)
    RecyclerView recAcceptList;

    @BindView(R.id.text_create_sound_end)
    AudioPlayerView textCreateSoundEnd;
    @BindView(R.id.rel_video)
    RelativeLayout relVideo;

    private ArrayList<ImageItem> selImageList = new ArrayList<>(); //当前选择的所有图片
    private ArrayList<ImageItem> list; //当前选择的所有图片
    private ImagePickerAdapter adapter;
    private int isDelete = 1;
    private int maxImgCount = 3;               //允许选择图片最大数
    public static final int IMAGE_ITEM_ADD = -1;
    public static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_PREVIEW = 101;
    public static final int REQUEST_CODE_TAKE_SELECT = 107;
    private ArrayList<ImageItem> images;
    private StatusTypeModel model;
    private TaskModel taskModel;
    private int typeId = -1;
    private String position;
    private String locationPosiyion;
    //路径
    private String videoPath;
    private String soundPath;
    private String soundPathTmp;
    //地址
    private String picUrl;
    private String videoUrl;
    private String soundUrl;
    private int type;
    private int IMG_TYPE = 1;
    private int VOICE_TYPE = 2;
    private int VIDEO_TYPE = 3;
    private List<String> picList = new ArrayList<>();
    private int typePosition;
    private List<PersonListBean> personListBeanList = new ArrayList<>();
    private List<UserIconBean> personList = new ArrayList<>();
    private List<Integer> personTmpList = new ArrayList<>();
    private List<RelatedDeviceModel.ListBean> deviceList = new ArrayList<>();
    private List<String> deviceIdList = new ArrayList<>();
    private List<String> deviceNameList = new ArrayList<>();
    private String deviceStr;
    private String personStr;
    private boolean isPlay;
    private AudioManage manage;
    private List<UserIconBean> userIconBeans = new ArrayList<>();
    private Thread thread;
    private String path;
    private String videoName;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_create_stu_or_task;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void initView() {
        super.initView();
        initImagePacker();

        type = getIntent().getIntExtra("type", 0);

        switch (type) {
            case 0:
                mPresenter.initManagerAdapter(recCreateStuManager);
                mPresenter.initExecuteAdapter(recCreateStuExecute);
                break;
            case 1:
                mPresenter.initManagerAdapter(recAcceptList);

                break;
        }
        stuView.setVisibility(type == 0 ? View.VISIBLE : View.GONE);
        taskView.setVisibility(type == 0 ? View.GONE : View.VISIBLE);

        tvTitle.setText(String.format("上报%s", type == 0 ? "情况" : "任务"));
        if (type == 3) {
            tvTitle.setText("重新分配");
            TaskSonModel sonModel = (TaskSonModel) getIntent().getSerializableExtra("model");
            if (sonModel != null) {
                mPresenter.initManagerAdapter(recAcceptList);
                setInitTaskData(sonModel);
            }
        }

        textCreateSound.setAudioFinishRecorderListener(this);
    }

    /**
     * 初始化单据选择控件
     */
    private void initImagePacker() {
        ImagePickerUtil.initImageMorePicker();
        // 添加图片
        selImageList = new ArrayList<>();
        adapter = new ImagePickerAdapter(this, selImageList, maxImgCount);
        adapter.setOnItemClickListener(this);
        recCreateImg.setLayoutManager(new ScrollGridLayoutManager(this, 3));
        recCreateImg.setHasFixedSize(true);
        recCreateImg.setAdapter(adapter);
    }


    @OnClick({R.id.rel_create_stu_type, R.id.rel_create_stu_location, R.id.rel_create_task_type,
            R.id.rel_create_task_location, R.id.rel_create_task_manager, R.id.rel_create_task_device
            , R.id.rel_video, R.id.text_upload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rel_create_stu_type:
                Bundle data = new Bundle();
                data.putInt("type", 0);
                data.putInt("position", typePosition);
                UIHelper.startActivityForResult(mContext, SelectStatusTypeActivity.class, 200, data);
                break;
            case R.id.rel_create_stu_location:
            case R.id.rel_create_task_location:
                Bundle bundle = new Bundle();
                bundle.putInt("type", 0);
                UIHelper.startActivityForResult(mContext, MapChooseActivity.class, 300, bundle);
                break;
            case R.id.rel_create_task_type:
                Bundle setData = new Bundle();
                setData.putInt("type", 1);
                setData.putInt("position", typePosition);
                UIHelper.startActivityForResult(mContext, SelectStatusTypeActivity.class, 200, setData);

                break;

            case R.id.rel_create_task_manager:
                Bundle perData = new Bundle();
                perData.putInt("type", 0);
                perData.putSerializable("model", (Serializable) personListBeanList);

                UIHelper.startActivityForResult(mContext, TaskPersonSelectActivity.class, 400, perData);

                break;
            case R.id.rel_create_task_device:
                Bundle deviceData = new Bundle();
                deviceData.putInt("type", 1);
                deviceData.putSerializable("model", (Serializable) deviceIdList);
                UIHelper.startActivityForResult(mContext, RelatedDeviceActivity.class, 500, deviceData);

                break;
            case R.id.rel_video:
                relVideo.requestFocus();
                relVideo.setFocusable(true);
                relVideo.setFocusableInTouchMode(true);

                Bundle bundle1 = new Bundle();
                if (TextUtils.isEmpty(videoPath)) {
                    bundle1.putInt("type", 0);
                    UIHelper.startActivity(mContext, VideoRecActivity.class, bundle1);

                } else {
                    bundle1.putString("uri", videoPath);
                    UIHelper.startActivity(mContext, VideoPlayActivity.class, bundle1);
                }
                break;
            case R.id.text_upload:
                mPresenter.dialog.show();
                initStuNetData();
                break;

        }
    }

    @Override
    public void onItemClick(View view, int position) {
        switch (position) {
            case IMAGE_ITEM_ADD:
                //打开选择,本次允许选择的数量
                ImagePicker.getInstance().setSelectLimit(maxImgCount - selImageList.size());
                Intent intent1 = new Intent(CreateStuOrTaskActivity.this, ImageGridActivity.class);
                /* 如果需要进入选择的时候显示已经选中的图片，
                 * 详情请查看ImagePickerActivity
                 * */
//                                intent1.putExtra(ImageGridActivity.EXTRAS_IMAGES,images);
                startActivityForResult(intent1, REQUEST_CODE_SELECT);

                break;
            default:
                //打开预览
                Intent intentPreview = new Intent(this, ImagePreviewDelActivity.class);
                intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, (ArrayList<ImageItem>) adapter.getImages());
                intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, position);
                intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
                intentPreview.putExtra(ImagePicker.DELETE_IS_SHOW, false);
                startActivityForResult(intentPreview, REQUEST_CODE_PREVIEW);
                break;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            //添加图片返回
            if (data != null && requestCode == REQUEST_CODE_SELECT) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                if (images != null) {
                    selImageList.addAll(images);
                    adapter.setImages(selImageList);
                }
            }
        } else if (resultCode == ImagePicker.RESULT_CODE_BACK) {
            //预览图片返回
            if (data != null && requestCode == REQUEST_CODE_PREVIEW) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_ITEMS);
                if (images != null) {
                    selImageList.clear();
                    selImageList.addAll(images);
                    adapter.setImages(selImageList);
                }
            }
        } else if (requestCode == 200) {
            if (data != null) {
                typePosition = data.getIntExtra("position", 0);

                if (type == 0) {
                    model = (StatusTypeModel) data.getSerializableExtra("model");
                    typeId = model.getId();
                    mPresenter.setNotifyManager(model.getManage_user());
                    mPresenter.setNotifyHandler(model.getHandle_user());
                    textCreateStuType.setText(model.getName());
                } else {
                    taskModel = (TaskModel) data.getSerializableExtra("model");
                    typeId = taskModel.getId();
                    textCreateTaskType.setText(taskModel.getName());
                }
            }
        } else if (requestCode == 300 && data != null) {
            //地图选择
            position = data.getStringExtra("position");
            locationPosiyion = data.getStringExtra("address");
            if (type == 0) {
                textCreateStuLocation.setText(locationPosiyion);
            } else {
                textCreateTaskLocation.setText(locationPosiyion);
            }
        } else if (requestCode == 400 && data != null) {
            //任务人员选择
            personListBeanList = (List<PersonListBean>) data.getSerializableExtra("model");
            personList.clear();
            personTmpList.clear();

            for (PersonListBean mode :
                    personListBeanList) {
                UserIconBean userIconBean = new UserIconBean();
                userIconBean.setAvatar(mode.getAvatar());
                userIconBean.setNickname(mode.getNickname());
                personList.add(userIconBean);
                personTmpList.add(mode.getId());
            }
            personStr = listToString(personTmpList, ',');

            imgCreateTaskManager.setHint(personList.size() > 0 ? "" : "请选择");
            mPresenter.setNotifyManager(personList);
        } else if (requestCode == 500 && data != null) {
            deviceList = (List<RelatedDeviceModel.ListBean>) data.getSerializableExtra("model");
            deviceIdList.clear();
            deviceNameList.clear();

            if (deviceList != null) {
                for (RelatedDeviceModel.ListBean bean :
                        deviceList) {
                    deviceIdList.add(bean.getTheone());
                    deviceNameList.add(bean.getName());
                }
                deviceStr = listToString(deviceIdList, ',');
                String deviceNameStr = listToString(deviceNameList, ',');
                textCreateTaskDevice.setText(deviceNameStr);
            } else {
                textCreateTaskDevice.setText("");
            }
        }
    }

    @Override
    protected void onDestroy() {
        textCreateSoundEnd.release();

        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    @Subscribe
    public void getEvent(VideoEvent event) {
        if (event != null) {
            imgCreateVideo.setImageBitmap(event.getBitmap());
            videoPath = event.getUrl();
            imgVideoEnd.setVisibility(View.VISIBLE);
        }

    }

    private void upLoadFileBase(String path, int position, int type) {
        HttpParams params = new HttpParams();
        if (type == 1) {
            params.put("file", LuBanCompress.compressWithLs(new File(path), mContext));
        } else {
            params.put("file", new File(path));
        }
        List<MultipartBody.Part> parts = params.setMoreImgType();
        mPresenter.upFile(parts, position, type);

    }

    @Override
    public void setSucImg(int position, String path) {
        position = position + 1;

        picList.add(path);
        if (position < selImageList.size()) {
            upLoadFileBase(selImageList.get(position).path, position, IMG_TYPE);
        } else {
            picUrl = listToString(picList, ',');
            selImageList.clear();
            initStuNetData();
        }
    }

    @Override
    public void setSucVoice(int position, String path) {
        soundPath = "";
        soundUrl = path;
        initStuNetData();
    }

    @Override
    public void setSucVideo(int position, String path) {
        videoPath = "";
        videoUrl = path;
        initStuNetData();
    }

    @Override
    public void finishAc() {
        if (type == 1) {
            EventBus.getDefault().post(new TaskEvent(0));
        } else {
            EventBus.getDefault().post(new StatusEvent(0));
        }
        appManager.finishActivity(CreateStuOrTaskActivity.class);
    }

    //情况
    private void setStuNetData() {
        HttpParams map = new HttpParams();
        String name = editCreateStuName.getText().toString().trim();
        String address = textCreateStuLocation.getText().toString().trim();
        String mark = editCreateMark.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            ToashUtils.show(mContext, "请输入名称");
            mPresenter.dialog.dismiss();

        } else if (typeId == -1) {
            ToashUtils.show(mContext, "请选择类型");
            mPresenter.dialog.dismiss();

        } else if (TextUtils.isEmpty(address)) {
            ToashUtils.show(mContext, "请选择地址");
            mPresenter.dialog.dismiss();

        } else {
            map.put("name", name);
            map.put("type_id", typeId + "");
            map.put("origin_type", "user");
            map.put("origin_id", Constants.USER_ID + "");
            map.put("address", address);
            map.put("position", position);
            if (!TextUtils.isEmpty(picUrl)) {
                map.put("picture", picUrl);
            }
            if (!TextUtils.isEmpty(soundUrl)) {
                map.put("audio", soundUrl);
            }
            if (!TextUtils.isEmpty(videoUrl)) {
                map.put("video", videoUrl);
            }

            if (!TextUtils.isEmpty(mark)) {
                map.put("details", mark);
            }
            mPresenter.uploadStuData(map.setType());
        }
    }

    private void setTaskNetData() {
        HttpParams map = new HttpParams();
        String name = editCreateTaskName.getText().toString().trim();
        String address = textCreateTaskLocation.getText().toString().trim();
        String mark = editCreateMark.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            ToashUtils.show(mContext, "请输入名称");
            mPresenter.dialog.dismiss();
        } else if (typeId == -1) {
            ToashUtils.show(mContext, "请选择类型");
            mPresenter.dialog.dismiss();

        } else if (TextUtils.isEmpty(address)) {
            ToashUtils.show(mContext, "请选择地址");
            mPresenter.dialog.dismiss();

        } else if (TextUtils.isEmpty(personStr)) {
            ToashUtils.show(mContext, "请选择接收人");
            mPresenter.dialog.dismiss();

        } else {
            map.put("name", name);
            map.put("type_id", typeId + "");
            map.put("address", address);
            map.put("position", position);
            map.put("send_user_id", Constants.USER_ID + "");
            map.put("accept_user_id", personStr);

            if (!TextUtils.isEmpty(deviceStr)) {
                map.put("relation_device", deviceStr);
            }

            if (!TextUtils.isEmpty(picUrl)) {
                map.put("picture", picUrl);
            }
            if (!TextUtils.isEmpty(soundUrl)) {
                map.put("audio", soundUrl);
            }
            if (!TextUtils.isEmpty(videoUrl)) {
                map.put("video", videoUrl);
            }

            if (!TextUtils.isEmpty(mark)) {
                map.put("details", mark);
            }

            mPresenter.uploadTaskData(map.setType(),type);
        }

    }

    private void setInitTaskData(TaskSonModel model) {
        editCreateTaskName.setText(model.getName());
        position = model.getPosition();
        textCreateTaskLocation.setText(model.getAddress());
        editCreateMark.setText(model.getDetails());
        typeId = Integer.parseInt(model.getType_id());
        //接收人
        if (model.getAccept_user_list() != null && model.getAccept_user_list().size() > 0) {
            for (TaskSonModel.AcceptUserListBean userListBean :
                    model.getAccept_user_list()) {
                UserIconBean item = new UserIconBean();
                item.setAvatar(userListBean.getAvatar());
                item.setNickname(userListBean.getNickname());
                item.setUser_id(userListBean.getUser_id());
                userIconBeans.add(item);
            }
        }

        for (UserIconBean mode :
                userIconBeans) {
            UserIconBean userIconBean = new UserIconBean();
            userIconBean.setAvatar(mode.getAvatar());
            userIconBean.setNickname(mode.getNickname());
            personList.add(userIconBean);
            personTmpList.add(mode.getUser_id());
            personListBeanList.add(new PersonListBean(mode.getUser_id()));
        }
        personStr = listToString(personTmpList, ',');
        mPresenter.setNotifyManager(userIconBeans);
        imgCreateTaskManager.setHint(!TextUtils.isEmpty(personStr) ? "" : "请选择");

        //关联设备
        StringBuffer buffer = new StringBuffer();
        if (model.getRelation_device() != null && model.getRelation_device().size() > 0) {

            for (int i = 0; i < model.getRelation_device().size(); i++) {
                TaskSonModel.RelationDeviceBean data = model.getRelation_device().get(i);
                List<TaskSonModel.RelationDeviceBean.deviceBean> beanList = data.getList();

                RelatedDeviceModel relatedDeviceModel = new RelatedDeviceModel();
                relatedDeviceModel.setName(data.getType());
                List<RelatedDeviceModel.ListBean> dataList = new ArrayList<>();
                dataList.clear();

                for (int j = 0; j < beanList.size(); j++) {
                    buffer.append(String.format("%s,", beanList.get(j).getDevice_name()));

                    RelatedDeviceModel.ListBean listBean = new RelatedDeviceModel.ListBean();
                    listBean.setShow(true);
                    listBean.setTheone(beanList.get(j).getTheone());
                    listBean.setName(beanList.get(j).getDevice_name());//设置二级数据list
                    dataList.add(listBean);
                }
                relatedDeviceModel.setExpand(true);
                relatedDeviceModel.setList(dataList);
                deviceList.addAll(relatedDeviceModel.getList());
            }

            for (RelatedDeviceModel.ListBean bean :
                    deviceList) {
                deviceIdList.add(bean.getTheone());
                deviceNameList.add(bean.getName());
            }
            deviceStr = listToString(deviceIdList, ',');
        }

        if (!TextUtils.isEmpty(buffer.toString())) {
            textCreateTaskDevice.setText(buffer.toString());
        }

        //类型名称
        textCreateTaskType.setText(model.getType_name());

        //语音
        if (!TextUtils.isEmpty(model.getAudio())) {
            textCreateSoundEnd.setUrl(model.getAudio());
            soundUrl = model.getAudio();
        }
        textCreateSound.setVisibility(TextUtils.isEmpty(model.getAudio()) ? View.VISIBLE : View.GONE);
        textCreateSoundEnd.setVisibility(!TextUtils.isEmpty(model.getAudio()) ? View.VISIBLE : View.GONE);

        //视频
        if (!TextUtils.isEmpty(model.getVideo())) {
            videoPath = "";
            videoUrl = model.getVideo();
            createVideoThumbnail(model.getVideo());
            imgVideoEnd.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(model.getPicture())) {
            String[] picUrl = model.getPicture().split(",");
            for (int i = 0; i < picUrl.length; i++) {
                ImageItem item = new ImageItem();
                item.path = picUrl[i];
                selImageList.add(item);
                picList.add(picUrl[i]);
            }
            maxImgCount = picUrl.length;
            adapter.setImages(selImageList, 0, maxImgCount);
            selImageList.clear();

        }
        picUrl = listToString(picList, ',');


    }


    /**
     * 上传类型和顺序判断
     */
    private void initStuNetData() {
        if (selImageList.size() != 0) {
            upLoadFileBase(selImageList.get(0).path, 0, IMG_TYPE);
        } else if (!TextUtils.isEmpty(soundPath)) {
            upLoadFileBase(soundPath, 0, VOICE_TYPE);
        } else if (!TextUtils.isEmpty(videoPath)) {
            upLoadFileBase(videoPath, 0, VIDEO_TYPE);
        } else {
            switch (type) {
                case 0:
                    setStuNetData();
                    break;
                case 1:
                case 3:
                    setTaskNetData();
                    break;
            }
        }
    }

    @Override
    public void onFinish(int seconds, String FilePath) {
        Log.e("sount", FilePath);
        Log.e("长度", seconds + "");

        soundPath = FilePath;
        soundPathTmp = FilePath;

        textCreateSound.setVisibility(View.GONE);
        textCreateSoundEnd.setVisibility(View.VISIBLE);
        textCreateSoundEnd.setUrl(FilePath);
    }

    private void createVideoThumbnail(final String filePath) {
        String data[] = filePath.split("/");
        path = "/sdcard/" + data[data.length - 1] + ".jpg";
        File file = new File(path);

        if (file.exists()) {
            GlideUtils.loadImg(path, imgCreateVideo);
        } else {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    setFFemg(filePath);

                }
            });
            thread.start();
        }
    }


    private void setFFemg(String filePath) {
        String data[] = filePath.split("/");
        path = "/sdcard/" + data[data.length - 1] + ".jpg";
        videoName = data[data.length - 1];
        String cmd = String.format("-i %s -y -f image2 -t 0.001 -s %s %s "
                , filePath, "704X576", path);

        String[] command = cmd.split(" ");
        try {
            FFmpeg.getInstance(mContext).execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.e("失败", s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.e("成功", s);
                }

                @Override
                public void onStart() {
                    Log.e("开始", "1");
                }

                @Override
                public void onFinish() {
                    Log.e("完成", "3");
                    EventBus.getDefault().post(new BitmapEvent(path));
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            e.printStackTrace();
        }
    }
}
