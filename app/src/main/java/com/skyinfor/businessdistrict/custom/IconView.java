package com.skyinfor.businessdistrict.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.util.GlideUtils;

import static com.skyinfor.businessdistrict.util.UiUtils.dp2px;

/**
 * Glide 有个bug 把 img 一些属性都改了比如 setImageResource,会出现复用状态
 * GradientDrawable 也会出现复用的bug
 */
public class IconView extends RelativeLayout {

    private TextView text;
    private ImageView img;
    private int width;
    private int height;
    private int textColor;
    private int textSize;
    private Context context;

    public IconView(Context context) {
        super(context);
    }

    public IconView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initData(context, attrs);
    }

    private void initData(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.IconViewStyle);
        if (typedArray != null) {
            width = typedArray.getDimensionPixelSize(R.styleable.IconViewStyle_icon_width,
                    dp2px(40, context));

            height = typedArray.getDimensionPixelSize(R.styleable.IconViewStyle_icon_height,
                    dp2px(40, context));

            textColor = typedArray.getColor(R.styleable.IconViewStyle_icon_text_color,
                    getResources().getColor(R.color.colorWhite));

            textSize = typedArray.getDimensionPixelSize(R.styleable.IconViewStyle_icon_text_size,
                    12);

            typedArray.recycle();
        }

        View view = LayoutInflater.from(context).inflate(R.layout.view_round_cus, null);
        img = view.findViewById(R.id.img_icon);
        text = view.findViewById(R.id.text_icon);
        text.setTextSize(textSize);
        text.setTextColor(textColor);

        ViewGroup.LayoutParams params = img.getLayoutParams();
        params.width = width;
        params.height = height;

        img.setLayoutParams(params);
        text.setLayoutParams(params);

        addView(view);
    }

    public void setBaseInfo(Context context, String str, String iconUrl) {
        text.setText("");

        setStatus(iconUrl, str);
    }

    public void setBaseInfo(Context context, String str, String iconUrl, int bacColor) {
        text.setText("");

        setStatus(iconUrl, str, bacColor);
    }

    public void setTextStr(String str) {
        setStatus("", str);
    }

    public void setTextColor(int color) {
        text.setTextColor(color);
    }

    public void setTextSize(int size) {
        text.setTextColor(size);
    }

    public void setImgResource(String iconUrl) {
        setStatus(iconUrl, "");
    }

    public void setImgResource(int iconUrl) {
        setStatus(iconUrl, "");
    }

    public void setImgUri(Uri uri) {
        img.setImageURI(uri);
    }

    private void setStatus(String iconUrl, String str) {
        GlideUtils.loadCircleImg(width, height, context, String.valueOf(iconUrl), img);

        if (TextUtils.isEmpty(iconUrl)) {
            text.setText(str.substring(str.length() > 2 ? str.length() - 2 : 0, str.length()));
        } else {
            text.setText("");
        }
        //设置显示和隐藏
        img.setVisibility(!TextUtils.isEmpty(iconUrl) ? VISIBLE : GONE);
        text.setBackgroundResource(TextUtils.isEmpty(iconUrl) ? R.drawable.shape_icon_view_bac : R.color.transparent);

    }

    /**
     * setColor bug 一直保留状态
     * GradientDrawable myGrad = (GradientDrawable)text.getBackground();
     * myGrad.setColor(getResources().getColor(color));
     *
     * @param iconUrl
     * @param str
     * @param color
     */
    private void setStatus(String iconUrl, String str, int color) {
        GlideUtils.loadCircleImg(width, height, context, String.valueOf(iconUrl), img);

        if (TextUtils.isEmpty(iconUrl)) {
            text.setText(str.substring(str.length() > 2 ? str.length() - 2 : 0, str.length()));
        } else {
            text.setText("");
        }
        //设置显示和隐藏
        img.setVisibility(!TextUtils.isEmpty(iconUrl) ? VISIBLE : GONE);

        text.setBackgroundResource(R.drawable.shape_icon_view_bac);
        text.setBackgroundResource(color);
    }

    private void setStatus(int iconUrl, String str) {
        text.setBackgroundResource(R.color.transparent);
        GlideUtils.loadImg(iconUrl, img);

    }

    public void setClearView() {
        if (img != null) {
            Glide.clear(img);
        }
    }

}
