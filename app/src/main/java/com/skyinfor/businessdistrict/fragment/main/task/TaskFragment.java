package com.skyinfor.businessdistrict.fragment.main.task;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.flyco.tablayout.SegmentTabLayout;
import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.TaskPopAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseFragment;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerFragmentComponent;
import com.skyinfor.businessdistrict.di.module.FragmentModule;
import com.skyinfor.businessdistrict.ui.main.create_stu.CreateStuOrTaskActivity;
import com.skyinfor.businessdistrict.ui.main.search_task_or_stu.SearchTaskAndStuActivity;
import com.skyinfor.businessdistrict.util.PopupWindowUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class TaskFragment extends BaseFragment<TaskPresent> implements TaskContract.Model {

    @BindView(R.id.text_stu_left)
    TextView textStuLeft;
    @BindView(R.id.ap_bar_stl)
    SegmentTabLayout apBarStl;
    @BindView(R.id.img_stu_search)
    ImageView imgStuSearch;
    @BindView(R.id.img_stu_add)
    ImageView imgStuAdd;
    @BindView(R.id.viewpager_stu)
    ViewPager viewpagerStu;
    @BindView(R.id.t_title)
    View title;
    @BindView(R.id.text_task_title)
    TextView textTaskTitle;

    private String[] msgArr = {"我派发的", "派发我的"};
    private View popView;
    private PopupWindow popup;
    private int mPosition;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, FragmentModule fragmentModule) {
        DaggerFragmentComponent.builder()
                .appComponent(appComponent)
                .fragmentModule(fragmentModule)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_stu;
    }

    @Override
    protected void initData() {
        super.initData();
        apBarStl.setTabData(msgArr);
        apBarStl.setVisibility(Constants.PRIVILEGE_TASK ? View.VISIBLE : View.GONE);//tab
        textStuLeft.setVisibility(Constants.PRIVILEGE_TASK ? View.VISIBLE : View.GONE);//我的
        imgStuAdd.setVisibility(Constants.PRIVILEGE_TASK ? View.VISIBLE : View.GONE);//新增
        textTaskTitle.setVisibility(Constants.PRIVILEGE_TASK ? View.GONE : View.VISIBLE);//title文字
        mPresenter.initAdapter(getChildFragmentManager(), viewpagerStu, apBarStl);
    }

    @OnClick({R.id.img_stu_search, R.id.img_stu_add, R.id.text_stu_left})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.img_stu_search:
                Bundle bundle = new Bundle();
                bundle.putInt("currentType", Constants.TASK_TYPE);
                bundle.putInt("type", 0);
                UIHelper.startActivity(mContext, SearchTaskAndStuActivity.class, bundle);

                break;
            case R.id.img_stu_add:
                Bundle data = new Bundle();
                data.putInt("type", 1);
                UIHelper.startActivityForResult(mContext, CreateStuOrTaskActivity.class, 101, data);
                break;
            case R.id.text_stu_left:
                View popView = getLayoutInflater().inflate(R.layout.view_task_pop, null);
                RecyclerView recyclerView = popView.findViewById(R.id.rec_task_pop);
                final List<String> strList = new ArrayList<>();
                strList.add("我的");
                strList.add("所有人");

                final TaskPopAdapter adapter = new TaskPopAdapter(mContext, strList);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
                recyclerView.setAdapter(adapter);
                adapter.mPosition = mPosition;
                adapter.notifyDataSetChanged();

                popup = PopupWindowUtils.getInstance(mContext).showPopup(true, popView, title, 0, 4);
                adapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        mPosition = position;
                        textStuLeft.setText(strList.get(position));

                        popup.dismiss();
                        if (mPosition == 1) {
                            apBarStl.setVisibility(View.GONE);
                            textTaskTitle.setVisibility(View.VISIBLE);
                        } else {
                            textTaskTitle.setVisibility(View.GONE);
                            apBarStl.setVisibility(View.VISIBLE);
                        }
                        mPresenter.setNotify(position);
                    }
                });

                break;
        }
    }

}
