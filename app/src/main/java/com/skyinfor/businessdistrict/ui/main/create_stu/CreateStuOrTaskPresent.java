package com.skyinfor.businessdistrict.ui.main.create_stu;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.skyinfor.businessdistrict.adapter.StatusIconAdapter;
import com.skyinfor.businessdistrict.adapter.UserIconBean;
import com.skyinfor.businessdistrict.eventbus.TaskUpdateModel;
import com.skyinfor.businessdistrict.model.FileModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.ToashUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by SKYINFOR on 2018/10/24.
 */

public class CreateStuOrTaskPresent extends BasePresenter<CreateStuOrTaskContract.Model>
        implements CreateStuOrTaskContract.Presenter {

    private List<UserIconBean> managerList = new ArrayList<>();
    private List<UserIconBean> handlerList = new ArrayList<>();
    private StatusIconAdapter handlerAdapter;
    private StatusIconAdapter managerAdapter;
    public HttpDialog dialog;
    private CreateStuOrTaskApi api;


    @Inject
    public CreateStuOrTaskPresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(CreateStuOrTaskApi.class);

    }


    @Override
    public void initManagerAdapter(RecyclerView recyclerView) {
        managerAdapter = new StatusIconAdapter(mActivity, managerList);
        LinearLayoutManager manager = new LinearLayoutManager(mActivity);
        manager.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(managerAdapter);
    }

    @Override
    public void initExecuteAdapter(RecyclerView recyclerView) {
        handlerAdapter = new StatusIconAdapter(mActivity, handlerList);
        LinearLayoutManager manager = new LinearLayoutManager(mActivity);
        manager.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(handlerAdapter);

    }

    @Override
    public void setNotifyManager(List<UserIconBean> list) {
        managerList.clear();

        for (int i = 0; i < list.size(); i++) {
            if (i < 6) {
                managerList.add(list.get(i));
            }

        }
        managerAdapter.notifyDataSetChanged();
    }

    @Override
    public void setNotifyHandler(List<UserIconBean> list) {
        handlerList.clear();
        for (int i = 0; i < list.size(); i++) {
            if (i < 6) {
                handlerList.add(list.get(i));
            }

        }
        handlerAdapter.notifyDataSetChanged();
    }

    @Override
    public void uploadStuData(List<MultipartBody.Part> part) {
        addSubscription(api.acceptList(part).compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        mModel.finishAc();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();
                        ToashUtils.show(mActivity, "上传成功！");
                        mModel.finishAc();
                    }
                });

    }

    @Override
    public void uploadTaskData(List<MultipartBody.Part> part, int type) {
        dialog.show();

        addSubscription(api.conditionSendList(part).compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber() {
                    @Override
                    public void onCompleted() {
                        mModel.finishAc();

                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNext(Object o) {
                        dialog.dismiss();
                        ToashUtils.show(mActivity, "上传成功！");
                        if (type == 3) {
                            EventBus.getDefault().post(new TaskUpdateModel(4));
                        }
                    }
                });

    }

    @Override
    public void upFile(List<MultipartBody.Part> part, final int position, final int type) {
        addSubscription(api.upFile(part)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<FileModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(FileModel bean) {
                        switch (type) {
                            case 1:
                                mModel.setSucImg(position, bean.getSave_path());
                                break;
                            case 2:
                                mModel.setSucVoice(position, bean.getSave_path());
                                break;
                            case 3:
                                mModel.setSucVideo(position, bean.getSave_path());
                                break;
                        }
                    }
                });

    }
}
