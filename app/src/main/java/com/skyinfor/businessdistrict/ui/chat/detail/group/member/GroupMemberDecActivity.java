package com.skyinfor.businessdistrict.ui.chat.detail.group.member;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.model.GroupMemberModel;

import java.util.List;

import butterknife.BindView;

public class GroupMemberDecActivity extends BaseActivity<GroupMemberDecPresent>
        implements GroupMemberDecContract.Model {


    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.rec_member_dec)
    RecyclerView recMemberDec;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_group_member_dec;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        List<GroupMemberModel.UserListBean> model= (List<GroupMemberModel.UserListBean>) getIntent()
                .getSerializableExtra("model");

        tvTitle.setText("群聊成员");
        if (model!=null){
            mPresenter.initAdapter(recMemberDec);
            mPresenter.setNotify(model);
        }

    }
}
