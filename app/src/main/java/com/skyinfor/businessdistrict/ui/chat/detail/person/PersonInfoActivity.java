package com.skyinfor.businessdistrict.ui.chat.detail.person;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.custom.IconView;
import com.skyinfor.businessdistrict.custom.roundedImage.RoundedImageView;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.EditPersonEvent;
import com.skyinfor.businessdistrict.model.UserInfoModel;
import com.skyinfor.businessdistrict.ui.chat.detail.person.edit.EditPersonInfoActivity;
import com.skyinfor.businessdistrict.ui.chat.room.ChatRoomActivity;
import com.skyinfor.businessdistrict.ui.setting.SettingActivity;
import com.skyinfor.businessdistrict.util.GlideUtils;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonInfoActivity extends BaseActivity<PersonInfoPresent>
        implements PersonInfoContract.Model {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.img_person_head)
    IconView imgPersonHead;
    @BindView(R.id.text_person_name)
    TextView textPersonName;
    @BindView(R.id.text_person_job)
    TextView textPersonJob;
    @BindView(R.id.text_person_sex)
    TextView textPersonSex;
    @BindView(R.id.text_person_phone)
    TextView textPersonPhone;
    @BindView(R.id.text_person_email)
    TextView textPersonEmail;
    @BindView(R.id.text_person_time)
    TextView textPersonTime;
    @BindView(R.id.text_person_qq)
    TextView textPersonQq;
    @BindView(R.id.text_person_weixin)
    TextView textPersonWeixin;
    @BindView(R.id.text_person_address)
    TextView textPersonAddress;
    @BindView(R.id.text_person_call_phone)
    TextView textPersonCallPhone;
    @BindView(R.id.text_person_send_msg)
    TextView textPersonSendMsg;
    @BindView(R.id.img_right)
    ImageView imgRight;
    @BindView(R.id.lin_person_boom)
    LinearLayout linPersonBoom;
    @BindView(R.id.img_person_more)
    ImageView imgPersonMore;
    private String phoneNum;
    private String userId;
    private String userName;
    private UserInfoModel model;
    private String avatar;
    private int type;

    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(activityModule)
                .build()
                .inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_person_info;
    }


    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        EventBus.getDefault().register(this);

        type = getIntent().getIntExtra("type", 0);
        userId = getIntent().getStringExtra("user_id");
        userName = getIntent().getStringExtra("user_name");

        mPresenter.getUserInfo(TextUtils.isEmpty(userId) ? Constants.USER_ID + "" : userId);
        tvTitle.setText(userName);

        if (type == 1) {
            imgRight.setVisibility(View.VISIBLE);
            imgPersonMore.setVisibility(View.VISIBLE);
            linPersonBoom.setVisibility(View.GONE);
            tvTitle.setText("我的");
        }

    }

    @OnClick({R.id.text_person_qq, R.id.text_person_weixin, R.id.text_person_call_phone
            , R.id.text_person_send_msg, R.id.lin_base_info, R.id.img_right})
    public void onViewClicked(View view) {
        Bundle data = new Bundle();

        switch (view.getId()) {
            case R.id.text_person_qq:

                break;
            case R.id.text_person_weixin:

                break;
            case R.id.text_person_call_phone:
                if (!TextUtils.isEmpty(phoneNum)) {
                    Utils.callPhone(phoneNum, mContext);
                } else {
                    ToashUtils.show(mContext, "手机号暂未提供!");
                }

                break;
            case R.id.text_person_send_msg:
                data.putString("name", userName);
                data.putString("id", userId);
                data.putString("avatar", avatar);
                UIHelper.startActivity(mContext, ChatRoomActivity.class, data);
                break;
            case R.id.lin_base_info:
                if (type == 1) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("model", model);
                    UIHelper.startActivity(mContext, EditPersonInfoActivity.class, bundle);
                }

                break;
            case R.id.img_right:
                data.putSerializable("model", model);
                UIHelper.startActivity(mContext, SettingActivity.class, data);

                break;

        }
    }


    @Override
    public void setResult(UserInfoModel model) {
        if (model != null) {
            this.model = model;
            imgPersonHead.setTag(R.id.img_person_head);
            imgPersonHead.setBaseInfo(mContext,model.getNickname(),model.getAvatar());
            textPersonName.setText(model.getNickname());
            textPersonJob.setText(model.getJob());
            textPersonSex.setText(model.getSex());
            textPersonPhone.setText(model.getPhone());
            textPersonEmail.setText(model.getEmail());
            textPersonTime.setText(model.getBirthday());
            textPersonQq.setText(model.getQq());
            textPersonWeixin.setText(model.getWechat());
            textPersonAddress.setText(model.getAddress());
            avatar = model.getAvatar();
            phoneNum = model.getPhone();
        }

    }


    @Subscribe
    public void getEvent(EditPersonEvent event) {
        mPresenter.getUserInfo(TextUtils.isEmpty(userId) ? Constants.USER_ID + "" : userId);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

}
