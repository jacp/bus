package com.skyinfor.businessdistrict.model;

import java.io.Serializable;

public class ConversationModel implements Serializable {


    /**
     * record_id : 274
     * content : bh
     * type : 1
     * send_id : 101
     * action :
     * time : 2018-11-14 14:24:15
     * group_uuid : b67ad1df-b701-89f4-899c-c8c1147a18cc
     * group_id : 61
     * group_name : 刘骁,舒文强
     * nickname : 舒文强
     * avatar :
     * list_type : group
     * id : 8048
     * user_id : 101
     * notice_name : 摄像头
     * notice_type : condition
     * rele_id : 246
     * related_user : 阿牛哥
     * notice_status : 待处理
     * create_time : 2018-11-14 14:11:22
     * is_read : 0
     * unread_num : 6
     * receive_id : 25
     * detail :
     * new_msg_num : 0
     * talk_list_id : 121
     * target_id : 25
     */

    private int record_id;
    private String content;
    private int send_id;
    private String action;
    private String time;
    private String group_uuid;
    private int group_id;
    private String group_name;
    private String nickname;
    private String avatar;
    private String list_type;
    private int id;
    private int user_id;
    private String notice_name;
    private String notice_type;
    private int rele_id;
    private String related_user;
    private String notice_status;
    private String create_time;
    private int unread_num;
    private int receive_id;
    private String detail;
    private int new_msg_num;

    public int getRecord_id() {
        return record_id;
    }

    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public int getSend_id() {
        return send_id;
    }

    public void setSend_id(int send_id) {
        this.send_id = send_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGroup_uuid() {
        return group_uuid;
    }

    public void setGroup_uuid(String group_uuid) {
        this.group_uuid = group_uuid;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getList_type() {
        return list_type;
    }

    public void setList_type(String list_type) {
        this.list_type = list_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getNotice_name() {
        return notice_name;
    }

    public void setNotice_name(String notice_name) {
        this.notice_name = notice_name;
    }

    public String getNotice_type() {
        return notice_type;
    }

    public void setNotice_type(String notice_type) {
        this.notice_type = notice_type;
    }

    public int getRele_id() {
        return rele_id;
    }

    public void setRele_id(int rele_id) {
        this.rele_id = rele_id;
    }

    public String getRelated_user() {
        return related_user;
    }

    public void setRelated_user(String related_user) {
        this.related_user = related_user;
    }

    public String getNotice_status() {
        return notice_status;
    }

    public void setNotice_status(String notice_status) {
        this.notice_status = notice_status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public int getUnread_num() {
        return unread_num;
    }

    public void setUnread_num(int unread_num) {
        this.unread_num = unread_num;
    }

    public int getReceive_id() {
        return receive_id;
    }

    public void setReceive_id(int receive_id) {
        this.receive_id = receive_id;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getNew_msg_num() {
        return new_msg_num;
    }

    public void setNew_msg_num(int new_msg_num) {
        this.new_msg_num = new_msg_num;
    }

}
