package com.skyinfor.businessdistrict.ui.main.create_stu.stu_type;

import com.skyinfor.businessdistrict.app.ApiUrl;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.StatusTypeModel;
import com.skyinfor.businessdistrict.model.TaskModel;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface SelectStatusTypeApi {

    @GET(ApiUrl.conditionTypeList)
    Observable<Response<List<StatusTypeModel>>> conditionTypeList();

    @GET(ApiUrl.taskTypeList)
    Observable<Response<List<TaskModel>>> taskTypeList();

}
