package com.skyinfor.businessdistrict.fragment.main.home;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.skyinfor.businessdistrict.adapter.HomeThingAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.model.HomeNoticeModel;
import com.skyinfor.businessdistrict.model.HomeTaskCountModel;
import com.skyinfor.businessdistrict.model.HomeThingModel;
import com.skyinfor.businessdistrict.model.Response;
import com.skyinfor.businessdistrict.model.analysis.AnalysisCountModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisOnlineModel;
import com.skyinfor.businessdistrict.model.analysis.AnalysisZhiModel;
import com.skyinfor.businessdistrict.presenter.BasePresenter;
import com.skyinfor.businessdistrict.rx.data.RxResultHelper;
import com.skyinfor.businessdistrict.rx.data.SchedulersCompat;
import com.skyinfor.businessdistrict.ui.main.stu_detail.StatusDetailActivity;
import com.skyinfor.businessdistrict.util.HttpDialog;
import com.skyinfor.businessdistrict.util.UIHelper;
import com.skyinfor.businessdistrict.util.recyclerview.BaseMyRecyclerVIewAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import rx.Subscriber;

/**
 * Created by SKYINFOR on 2018/10/23.
 */

public class HomePresent extends BasePresenter<HomeContract.Model> implements HomeContract.Presenter {

    private HttpDialog dialog;
    private HomeApi api;
    private List<HomeNoticeModel.DataBean> mList = new ArrayList<>();
    private List<HomeThingModel> thingList = new ArrayList<>();
    private HomeThingAdapter thingAdapter;
    private AnalysisZhiModel analysisZhiModel;
    private List<HashMap<String, Integer>> mapList;


    @Inject
    public HomePresent(Activity activity, OkHttpClient okHttpClient, Retrofit retrofit) {
        super(activity, okHttpClient, retrofit);
        dialog = new HttpDialog(mActivity);
        api = retrofit.create(HomeApi.class);

    }

    @Override
    public void waitTasksCount(String userId) {
        addSubscription(api.waitTasksCount(userId)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<HomeTaskCountModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(HomeTaskCountModel model) {
                        mModel.setTaskCount(model.getCondition(), model.getTask());

                    }
                });

    }

    @Override
    public void noticeList(String page, final String limit) {
        addSubscription(api.noticeList(Constants.USER_ID + "", page, limit)
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<HomeNoticeModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(HomeNoticeModel o) {
                        mList.clear();
                        List<HomeNoticeModel.DataBean> list = o.getData();
                        if (list.size() > 4) {
                            mList.addAll(list.subList(0, 4));
                        } else {
                            mList.addAll(list);
                        }
                        mModel.setBannerResult(mList);
                    }
                });
    }

    @Override
    public void newTaskList() {
        addSubscription(api.newTaskList()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(RxResultHelper.handleResult())
                , new Subscriber<List<HomeThingModel>>() {
                    @Override
                    public void onCompleted() {
                        mModel.compelete();

                    }

                    @Override
                    public void onError(Throwable throwable) {
                        mModel.compelete();

                    }

                    @Override
                    public void onNext(List<HomeThingModel> list) {
                        thingList.clear();
                        thingList.addAll(list);
                        thingAdapter.notifyDataSetChanged();

                    }
                });

    }

    @Override
    public void initAdapter(RecyclerView recyclerView) {
        thingAdapter = new HomeThingAdapter(mActivity, thingList);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(thingAdapter);
        thingAdapter.setOnItemClickListener(new BaseMyRecyclerVIewAdapter.setOnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Bundle data = new Bundle();
                data.putInt("rele_id", thingList.get(position).getId());
                data.putInt("type", thingList.get(position).getList_type().equals("task") ? 0 : 1);
                UIHelper.startActivity(mActivity, StatusDetailActivity.class, data);
            }
        });

    }

    @Override
    public void dataAnalysis() {
        addSubscription(api.dataAnalysis()
                        .compose(SchedulersCompat.applyIoSchedulers())
                        .compose(SchedulersCompat.applyIoSchedulers())
                , new Subscriber<Response<Object>>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<Object> data) {
                        //分段解析集合，其中类型差异太大,不然可以用List<Object>解析的
                        Gson gson = new Gson();
                        JsonArray jsonArray = new JsonParser().parse(gson.toJson(data.data)).getAsJsonArray();

                        for (int i = 0; i < jsonArray.size(); i++) {
                            JsonElement element = jsonArray.get(i);
                            switch (i) {
                                case 1:
                                    analysisZhiModel = gson.fromJson(element, AnalysisZhiModel.class);
                                    break;
                                case 2:
                                    mapList = gson.fromJson(element, new TypeToken<List<HashMap<String, Integer>>>() {
                                    }.getType());
                                    break;

                            }
                        }
                        mModel.setIndexResult(analysisZhiModel);
                        mModel.setLineResult(mapList);
                    }
                });

    }

    /**
     * generates a random ChartData object with just one DataSet
     *
     * @return Line data
     */
    public void generateDataLine(LineChart chart, List<HashMap<String, Integer>> lineList) {
        ArrayList<Entry> values1 = new ArrayList<>();//第一条线真实数据
        ArrayList<Entry> values2 = new ArrayList<>();//第二条线真实数据
        List<String> dateList = new ArrayList<>();//日期
        List<Integer> valueList = new ArrayList<>();//暂时存放第一条线
        List<Integer> valueList1 = new ArrayList<>();//暂时存放第二条线

        Map<String, Integer> map1 = getTrueHasMap(lineList.get(0));
        Map<String, Integer> map2 = getTrueHasMap(lineList.get(1));

        //第一条
        for (Map.Entry<String, Integer> entry : map1.entrySet()) {
            valueList.add(entry.getValue());
            dateList.add(entry.getKey().replace("-","/"));
        }

        //第二条
        for (Map.Entry<String, Integer> entry : map2.entrySet()) {
            valueList1.add(entry.getValue());
        }

        for (int i = 0; i < valueList.size(); i++) {
            values1.add(new Entry(i, valueList.get(i)));
        }

        for (int i = 0; i < valueList1.size(); i++) {
            values2.add(new Entry(i, valueList1.get(i)));
        }

        LineDataSet d1 = new LineDataSet(values1, "");
        d1.setLineWidth(2.5f);
        d1.setDrawHighlightIndicators(false);
        d1.setCircleRadius(4.5f);
        d1.setColor(Color.rgb(39, 142, 237));
        d1.setCircleColor(Color.rgb(39, 142, 237));
        d1.setDrawValues(false);

        LineDataSet d2 = new LineDataSet(values2, "");
        d2.setDrawHighlightIndicators(false);

        d2.setLineWidth(2.5f);
        d2.setCircleRadius(4.5f);
        d2.setColor(Color.rgb(246, 153, 44));
        d2.setCircleColor(Color.rgb(246, 153, 44));
        d2.setDrawValues(false);

        ArrayList<ILineDataSet> sets = new ArrayList<>();
        sets.add(d1);
        sets.add(d2);
        LineData lineData = new LineData(sets);

        setChartData(chart, lineData, dateList);
    }


    private void setChartData(LineChart chart, LineData sets, List<String> dateList) {
        Typeface mTf = Typeface.createFromAsset(mActivity.getAssets(), "fonts/OpenSans-Regular.ttf");
        // chart.setValueTypeface(mTf);
        chart.getDescription().setEnabled(false);
        chart.setDrawGridBackground(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTf);
        xAxis.setLabelCount(5, false);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);
        xAxis.setSpaceMin(0.5f);

        //准备好每个点对应的x轴数值
        List<String> list = new ArrayList<>();
        list.clear();
        for (int i = 0; i < dateList.size(); i++) {
            String xStr = dateList.get(i).substring(5, dateList.get(i).length());
            list.add(xStr);
        }

        xAxis.setValueFormatter(new IndexAxisValueFormatter(list));

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTypeface(mTf);
        leftAxis.setLabelCount(5, false);
        leftAxis.setDrawAxisLine(true);

        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//        leftAxis.setAxisMaximum(100);
        List<String> yList = new ArrayList<>();
//        for (int i = 0; i < 6; i++) {
//            yList.add((i*20)+"%");
//        }
//        leftAxis.setValueFormatter(new IndexAxisValueFormatter(yList));

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);

        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        // set data
        chart.setData(sets);
        // do not forget to refresh the chart
        // chart.invalidate();
        chart.setDoubleTapToZoomEnabled(false);
        chart.animateX(750);
    }

    private Map<String, Integer> getTrueHasMap(HashMap<String, Integer> aMap) {
        Set<Map.Entry<String, Integer>> mapEntries = aMap.entrySet();
        List<Map.Entry<String, Integer>> aList = new LinkedList<Map.Entry<String, Integer>>(mapEntries);
        // sorting the List
        Collections.sort(aList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> ele1, Map.Entry<String, Integer> ele2) {
                return ele1.getKey().compareTo(ele2.getKey());
            }
        });
        Map<String, Integer> aMap2 = new LinkedHashMap<String, Integer>();
        aMap2.clear();
        for (Map.Entry<String, Integer> entry : aList) {
            aMap2.put(entry.getKey(), entry.getValue());
        }
        return aMap2;
    }

}
