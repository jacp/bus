package com.skyinfor.businessdistrict.ui.contact;

import android.support.v7.widget.RecyclerView;

import com.skyinfor.businessdistrict.presenter.IPresenter;
import com.skyinfor.businessdistrict.presenter.MModel;

public class MsgSearchContract {

    interface Model extends MModel {


    }

    interface Presenter extends IPresenter<Model> {
        void initAdapter(RecyclerView recyclerView);

        void getConversationList(String userId, String keyword);

        void talkRecordStatus(String user_id, String target_id);

        void setNotify();

    }

}
