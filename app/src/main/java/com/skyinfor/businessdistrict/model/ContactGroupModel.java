package com.skyinfor.businessdistrict.model;

import java.util.List;

public class ContactGroupModel {

    private String name;
    private List<GroupListBean> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupListBean> getList() {
        return list;
    }

    public void setList(List<GroupListBean> list) {
        this.list = list;
    }

    public class GroupListBean {

        /**
         * id : 59
         * user_id : 48,51,101
         * group_name : 陈磊,朱中响,舒文强
         * group_uuid : ffd95032-0ea4-cc8a-410b-61d294a2706a
         */

        private int id;
        private String user_id;
        private String group_name;
        private String group_uuid;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public String getGroup_uuid() {
            return group_uuid;
        }

        public void setGroup_uuid(String group_uuid) {
            this.group_uuid = group_uuid;
        }
    }

}
