package com.skyinfor.businessdistrict.ui.main;

import android.Manifest;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.skyinfor.businessdistrict.R;
import com.skyinfor.businessdistrict.adapter.BaseFragmentAdapter;
import com.skyinfor.businessdistrict.app.Constants;
import com.skyinfor.businessdistrict.base.BaseActivity;
import com.skyinfor.businessdistrict.base.MyApplication;
import com.skyinfor.businessdistrict.di.component.AppComponent;
import com.skyinfor.businessdistrict.di.component.DaggerActivityComponent;
import com.skyinfor.businessdistrict.di.module.ActivityModule;
import com.skyinfor.businessdistrict.eventbus.ChatNameEvent;
import com.skyinfor.businessdistrict.eventbus.ChatNotifyEvent;
import com.skyinfor.businessdistrict.eventbus.MeaasgeEvent;
import com.skyinfor.businessdistrict.eventbus.MsgNewCountEvent;
import com.skyinfor.businessdistrict.eventbus.PositionEvent;
import com.skyinfor.businessdistrict.eventbus.socket.SocketCallEvent;
import com.skyinfor.businessdistrict.fragment.main.contact.ContactFragment;
import com.skyinfor.businessdistrict.fragment.main.home.HomeFragment;
import com.skyinfor.businessdistrict.fragment.main.msg.MsgFragment;
import com.skyinfor.businessdistrict.fragment.main.stu.StatusFragment;
import com.skyinfor.businessdistrict.fragment.main.task.TaskFragment;
import com.skyinfor.businessdistrict.service.EverUpdatePositionService;
import com.skyinfor.businessdistrict.service.SocketService;
import com.skyinfor.businessdistrict.util.AppManager;
import com.skyinfor.businessdistrict.util.ImagePickerUtil;
import com.skyinfor.businessdistrict.util.ScreenListener;
import com.skyinfor.businessdistrict.util.ToashUtils;
import com.tencent.android.tpush.XGCustomPushNotificationBuilder;
import com.tencent.android.tpush.XGPushClickedResult;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;
//import com.tencent.android.tpush.XGPushConfig;
//import com.tencent.android.tpush.XGPushManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import cn.bingoogolapple.badgeview.BGABadgeRadioButton;

public class MainActivity extends BaseActivity<MainPresent> implements MainContract.Model {

    @BindView(R.id.rb_main_msg)
    BGABadgeRadioButton rbMainMsg;
    @BindView(R.id.rb_main_contact)
    RadioButton rbMainContact;
    @BindView(R.id.rb_main_home)
    RadioButton rbMainHome;
    @BindView(R.id.rb_main_task)
    RadioButton rbMainTask;
    @BindView(R.id.rb_main_stu)
    RadioButton rbMainStu;
    @BindView(R.id.main_radiodgroup)
    RadioGroup mainRadiodgroup;
    @BindView(R.id.viewpager_main)
    ViewPager viewPager;

    private static Boolean isExit = false;
    private List<String> strList = new ArrayList<>();
    private List<Fragment> fragments = new ArrayList<>();
    private String[] msgArr = {"上报我的", "我上报的"};
    private Intent intent;
    private Intent posIntent;


    @Override
    protected void setupActivityComponent(AppComponent appComponent, ActivityModule activityModule) {
        DaggerActivityComponent.builder()
                .activityModule(activityModule)
                .appComponent(appComponent)
                .build()
                .inject(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        ImagePickerUtil.initImageMorePicker();

        EventBus.getDefault().register(this);

        initAdapter();
        rbMainHome.setChecked(true);
        getPer();

        MyApplication.getInstance().getmAgoraAPI().logout();
        //声网登录
        MyApplication.getInstance().getmAgoraAPI().login2(getString(R.string.agora_app_id), Constants.USER_ID + "", "_no_need_token", 0, "", 5, 1);

        mPresenter.addLoginBack();
        intent = new Intent(mContext, SocketService.class);
        startService(intent);

        posIntent = new Intent(mContext, EverUpdatePositionService.class);
        startService(posIntent);

        XGPushConfig.setAccessId(mContext, 2100314782);
        XGPushConfig.setAccessKey(mContext, "AZA32P2ZY25T");

        XGPushConfig.enableDebug(this, false);
        XGPushManager.registerPush(this, Constants.LOGIN_NAME);

    }


    private void initAdapter() {
        strList.add("消息");
        strList.add("通讯录");
        strList.add("首页");
        strList.add("任务");
        strList.add("情况");
        fragments.add(new MsgFragment());
        fragments.add(ContactFragment.getInstance(false));
        fragments.add(new HomeFragment());
        fragments.add(new TaskFragment());
        fragments.add(new StatusFragment());

        BaseFragmentAdapter adapter = new BaseFragmentAdapter(getSupportFragmentManager(), strList, fragments);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(2);
        viewPager.setOffscreenPageLimit(5);

        mainRadiodgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int index = group.indexOfChild(group.findViewById(checkedId));
                viewPager.setCurrentItem(index);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                ((RadioButton) mainRadiodgroup.getChildAt(position)).setChecked(true);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void getPer() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "没有权限,请手动开启定位权限", Toast.LENGTH_SHORT).show();
            // 申请一个（或多个）权限，并提供用于回调返回的获取码（用户定义）
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE
                    , Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA}, 101);
        } else {

        }
    }

    //Android6.0申请权限的回调方法
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            // requestCode即所声明的权限获取码，在checkSelfPermission时传入
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 获取到权限，作相应处理（调用定位SDK应当确保相关权限均被授权，否则可能引起定位失败）


//                    init();
                } else {
                    // 没有获取到权限，做特殊处理
                    Toast.makeText(getApplicationContext(), "获取位置权限失败，请手动开启", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void getSocketEvent(SocketCallEvent event) {
        if (event.getType() == 1) {
            String clientId = event.getStr();
            mPresenter.bindUserId(String.valueOf(Constants.USER_ID), clientId);
        }
    }

    @Subscribe
    public void getNewMsgCount(MsgNewCountEvent event) {
        if (event.getCount() > 0) {
            rbMainMsg.showTextBadge(String.valueOf(event.getCount()));
        } else {
            rbMainMsg.hiddenBadge();
        }
    }

    @Subscribe
    public void getRegisterSucEvent(MeaasgeEvent event) {
        mPresenter.dialog.dismiss();
    }

    @Subscribe
    public void getEvent(ChatNameEvent event) {
        ((MsgFragment) fragments.get(0)).setNameChange();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getEvent(ChatNotifyEvent event) {
        ((MsgFragment) fragments.get(0)).setEvent(event);

    }

    @Subscribe
    public void getPositionEvent(PositionEvent event) {
        mPresenter.userLocationUpload(Constants.USER_ID + "", event.getPosition());

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        stopService(intent);
        stopService(posIntent);
    }

    /**
     * 设置通知自定义View，这样在下发通知时可以指定build_id。编号由开发者自己维护,build_id=0为默认设置
     *
     * @param context
     */
    private void initCustomPushNotificationBuilder(Context context) {
        XGCustomPushNotificationBuilder build = new XGCustomPushNotificationBuilder();
        build.setSound(
                RingtoneManager.getActualDefaultRingtoneUri(
                        getApplicationContext(), RingtoneManager.TYPE_ALARM)) // 设置声音
                // setSound(
                // Uri.parse("android.resource://" + getPackageName()
                // + "/" + R.raw.wind)) 设定Raw下指定声音文件
                .setDefaults(Notification.DEFAULT_VIBRATE) // 振动
                .setFlags(Notification.FLAG_NO_CLEAR); // 是否可清除
        // 设置自定义通知layout,通知背景等可以在layout里设置

        // 若不设定以上自定义layout，又想简单指定通知栏图片资源（注：自定义layout和setNotificationLargeIcon两种方式指定图片资源只能选其一，不能同时使用）
        build.setNotificationLargeIcon(R.mipmap.ic_launcher);
        // 客户端保存build_id
        XGPushManager.setPushNotificationBuilder(this, 1, build);

    }

    public AppManager getAppMager() {
        return appManager;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //调用双击退出函数
            exitBy2Click();
        }
        return false;
    }


    /**
     * 退出应用
     */
    public void exitBy2Click() {
        if (isExit == false) {
            isExit = true; // 准备退出
//            Toast.makeText(mContext, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            ToashUtils.show(mContext, "再按一次退出程序");
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出
                }
            }, 2000); // 如果2秒钟内没有按下返回键，则启动定时器取消掉刚才执行的任务

        } else {
            appManager.appExit(mContext);
        }
    }
}
